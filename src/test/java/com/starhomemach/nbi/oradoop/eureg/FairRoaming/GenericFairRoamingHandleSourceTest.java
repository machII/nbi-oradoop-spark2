package com.starhomemach.nbi.oradoop.eureg.FairRoaming;

import com.google.common.collect.Lists;
import org.junit.Test;

import java.util.Collection;

import static org.junit.Assert.*;

public class GenericFairRoamingHandleSourceTest {

    @Test
    public void run() throws Exception {

        String user = "o2uk";
        String maxWfToRun = "10";
        String prestoUrl = "jdbc:presto://10.135.11.24:8080/hive";
        String userName = "02uk";
        String wfId = "11111";

        Collection<Object> availableSources = Lists.newArrayList("TAP");
        GenericFairRoamingHandleSource handle = new GenericFairRoamingHandleSource(prestoUrl, userName);
        handle.run(availableSources, user, Integer.valueOf(maxWfToRun),  wfId);
    }
}