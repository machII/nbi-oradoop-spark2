package com.starhomemach.nbi.oradoop.util.queryRunners;

import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.io.File;
import java.util.Properties;

/**
 * Created by tpin on 19/12/2016.
 */

public class QueryAppAnySourceTest {

    @Test
    public void testMe() throws Exception {
        System.out.println("################################################ START ####################################################");

        String[] args_presto = {
                "--jdbcUrl", "jdbc:trino://10.135.11.24:8080/hive",
                "--userName", "hdfs",
               // "--password", "telefonica",
                "--queryDelimiter", ";",
                "--runLastResult","false",
                "--queryString", "set session writer_min_size=1500MB;set session scale_writers=true;set session task_writer_count=4;select 1"
                };

//                String[] args_hive = {
//                "--jdbcUrl", "jdbc:hive2://isr-poc-1-hdf-1.starhome.com:10000",
//                 "--jdbcDriver","org.apache.hive.jdbc.HiveDriver",
//                "--userName", "bce",
//                "--password", "",
//                "--queryDelimiter", ";",
//                "--runLastResult","false",
//                "--queryString", "select 1"
//                };

//        String[] args_oracle = {
//                "--jdbcUrl", "jdbc:oracle:thin:@10.30.40.113:1521/BIDXE",
//                 "--jdbcDriver","oracle.jdbc.driver.OracleDriver",
//                "--userName", "bce_ph2",
//                 "--password", "bce_ph2",
//                "--queryDelimiter", ";",
//                "--runLastResult","false",
//                "--queryString", "select 1 from dual"
//        };


        QueryRunnerArgumentsHolder queryRunnerArgumentsHolder = QueryRunnerArgumentsHolder.getInstance();
        queryRunnerArgumentsHolder.initQureyRunnerArgumentsHolder(args_presto);

        String propertiesFilePath = System.setProperty("oozie.action.output.properties","aaa.txt");
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(QueryAppSpringConfig.class);

        context.getBean(QueryAppAnySourceHandler.class).run();

        (new File("aaa.txt")).delete();

    }


    @Test
    public void replaceRegex() {
        Properties properties = new Properties();
        properties.setProperty(0 + "-" + 0 + "-" + 1, "aaaa");
        properties.setProperty(0 + "-" + 0 + "-" + 2, "bbb");
        properties.setProperty(1 + "-" + 0 + "-" + 1, "ccc");

        String str = "r[1-0-1";
        System.out.println(properties);
        System.out.println(str);

        if (str.matches(".*p\\[\\d-\\d-\\d\\].*")) {
            System.out.println("Match");
            for (Object key : properties.keySet()) {
                str = str.replace("r[" + key.toString() + "]", properties.get(key).toString());
            }
        }
        System.out.println(str);
    }

}

