package com.starhomemach.nbi.oradoop.export.service;

import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.Path;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ProcessHdfsDirectoryServiceTest {

    public static final String SUCCESS = "_SUCCESS";
    @Mock
    public FileStatus fileStatus;
    @Mock
    public FileStatus fileStatus1;
    @Mock
    public FileStatus fileStatus2;

    @Mock
    public Path path;

    @InjectMocks
    public ProcessHdfsDirectoryService processHdfsDirectoryService;

    @Test
    public void when_array_contains_file_name__sucess_return_true()
    {
        Assertions.assertThat(true).isTrue();
    }


//    @Test
//    public void when_array_contains_file_name__sucess_return_true()
//    {
//        FileStatus[] fileStatusArray = {fileStatus1};
//
//        when(fileStatus1.getPath()).thenReturn(path);
//        when(path.getName()).thenReturn(SUCCESS);
//
//        boolean shouldBeTrue = processHdfsDirectoryService.shouldProcessDirectory(fileStatusArray);
//        assertThat(shouldBeTrue).isTrue();
//
//    }
//
//    @Test
//    public void when_array_dont_contains_file_name__sucess_return_true()
//    {
//        FileStatus[] fileStatusArray = {fileStatus1};
//
//        when(fileStatus1.getPath()).thenReturn(path);
//        when(path.getName()).thenReturn("OTHER");
//
//        boolean shouldBeFalse = processHdfsDirectoryService.shouldProcessDirectory(fileStatusArray);
//        assertThat(shouldBeFalse).isFalse();
//
//    }




}

