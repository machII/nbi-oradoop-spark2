package com.starhomemach.nbi.oradoop.export.file;

import com.starhomemach.nbi.oradoop.export.file.dto.AvroFileRecord;
import com.starhomemach.nbi.oradoop.export.file.dto.T_fluc_hr;
import com.starhomemach.nbi.oradoop.export.file.filehandler.TFlucHrFileHandler;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

@RunWith(MockitoJUnitRunner.class)
public class TFsLuHrFileHandlerTest {

    public static final long CLID1 = 2L;

    public static final long CLID = 1L;
    public static final long CLGRPID = 2l;
    public static final long DKADT = 3l;
    public static final long DKDAT = 4l;
    public static final long DKDIR = 5l;
    public static final long DKORGCNP = 6l;
    public static final long DKORGHNW = 7l;
    public static final long DKORGSNW = 9l;
    public static final long DKPET = 10l;
    public static final int DKSBP = 11;
    public static final long DKSGS = 12l;
    public static final long DKSGT = 12l;
    public static final long DKSPE = 13l;
    public static final long DKTIMHR = 14l;
    public static final long SLUATPT = 15l;
    public static final Double SLUDLY = new Double(16);
    public static final Double SLUDLY_NULL = null;
    public static final long SLUFAIL = 17l;
    public static final long SLUSUCC = 18l;
    public static final int INDEX_1 = 1;
    public static final int INDEX_2 = 2;
    public static final int INDEX_3 = 3;
    public static final int INDEX_4 = 4;
    public static final int INDEX_5 = 5;
    public static final int INDEX_6 = 6;
    public static final int INDEX_7 = 7;
    public static final int INDEX_8 = 8;
    public static final int INDEX_9 = 9;
    public static final int INDEX_10 = 10;
    public static final int INDEX_11 = 11;
    public static final int INDEX_12 = 12;
    public static final int INDEX_13 = 13;
    public static final int INDEX_14 = 14;
    public static final int INDEX_15 = 15;
    public static final int INDEX_16 = 16;
    public static final int INDEX_17 = 17;
    public static final int INDEX_18 = 18;
    public static final int WANTED_ZERO_NUMBER_OF_INVOCATIONS = 0;
    public static final int WANTED_ONE_NUMBER_OF_INVOCATIONS = 1;
    public static final long SEQUENCE_ID = 111l;
    public static final int MINUS_ONE = -1;

    @InjectMocks
    public TFlucHrFileHandler tFsLuHrFileHandler;

    @Mock
    public PreparedStatement preparedStatement;
//    public static final List<AvroFileRecord> AVRO_FILE_RECORDS = newArrayList();
//    public final T_fluc_hr t_FSLU_HROLD = new T_fluc_hr().newBuilder().setClId(CLID).setDkDat(DKDAT).setDkDir(DKDIR).setDkOrgCnp(DKORGCNP)
//            .setDkOrgHnw(DKORGHNW).setDkOrgSnw(DKORGSNW).setDkPet(DKPET).setDkSgs(DKSGS)
//            .setDkSpe(DKSPE).setDkTimHr(DKTIMHR).setLucAtpt(SLUATPT).setLucDly(SLUDLY).setLucFail(SLUFAIL).setLucSucc(SLUSUCC).build();
//
//    public final T_fluc_hr t_fslu_hr_1 = new T_fluc_hr().newBuilder().setClId(CLID1).setDkDat(DKDAT).setDkDir(DKDIR).setDkOrgCnp(DKORGCNP)
//            .setDkOrgHnw(DKORGHNW).setDkOrgSnw(DKORGSNW).setDkPet(DKPET).setDkSgs(DKSGS)
//            .setDkSpe(DKSPE).setDkTimHr(DKTIMHR).setLucAtpt(SLUATPT).setLucDly(SLUDLY).setLucFail(SLUFAIL).setLucSucc(SLUSUCC).build();
//
//
//    public final T_fluc_hr t_fslu_hr_SLUDLY_NULL_ = new T_fluc_hr().newBuilder().setClId(CLID).setDkDat(DKDAT).setDkDir(DKDIR).setDkOrgCnp(DKORGCNP)
//            .setDkOrgHnw(DKORGHNW).setDkOrgSnw(DKORGSNW).setDkPet(DKPET).setDkSgs(DKSGS)
//            .setDkSpe(DKSPE).setDkTimHr(DKTIMHR).setLucAtpt(SLUATPT).setLucDly(SLUDLY_NULL).setLucFail(SLUFAIL).setLucSucc(SLUSUCC).build();


//    @Before
//    public void beforeTest() {
//        AVRO_FILE_RECORDS.clear();
//    }



    @Test
    public void when_list_of_avro_file_is_empty_dont_update_preparedStatement() throws SQLException {

        Assertions.assertThat(true).isTrue();

    }

//    @Test
//    public void when_list_of_avro_file_is_empty_dont_update_preparedStatement() throws SQLException {
//        tFsLuHrFileHandler.setPreparedStatement(AVRO_FILE_RECORDS, SEQUENCE_ID, preparedStatement);
//
//        verify(preparedStatement, times(WANTED_ZERO_NUMBER_OF_INVOCATIONS)).setLong(anyInt(), anyLong());
//
//    }
//
//
//    @Test
//    public void when_list_of_avro_file_contains_two_files_loop_each_and_update_preparedStatement_twice() throws SQLException {
//
//
//        AVRO_FILE_RECORDS.add(t_FSLU_HROLD);
//        AVRO_FILE_RECORDS.add(t_fslu_hr_1);
//
//        tFsLuHrFileHandler.setPreparedStatement(AVRO_FILE_RECORDS, SEQUENCE_ID, preparedStatement);
//
//        verify(preparedStatement, times(WANTED_ONE_NUMBER_OF_INVOCATIONS)).setLong(INDEX_1, CLID1);
//        verify(preparedStatement, times(WANTED_ONE_NUMBER_OF_INVOCATIONS)).setLong(INDEX_1, CLID);
//
//    }

//    @Test
//    public void set_preparedStatement_by_index_correctly() throws SQLException {
//
//
//        AVRO_FILE_RECORDS.add(t_FSLU_HROLD);
//
//        tFsLuHrFileHandler.setPreparedStatement(AVRO_FILE_RECORDS, SEQUENCE_ID, preparedStatement);
//
//        verify(preparedStatement, times(WANTED_ONE_NUMBER_OF_INVOCATIONS)).setLong(INDEX_1, CLID);
//        verify(preparedStatement, times(WANTED_ONE_NUMBER_OF_INVOCATIONS)).setLong(INDEX_2, CLGRPID);
//        verify(preparedStatement, times(WANTED_ONE_NUMBER_OF_INVOCATIONS)).setLong(INDEX_3, DKORGSNW);
//        verify(preparedStatement, times(WANTED_ONE_NUMBER_OF_INVOCATIONS)).setLong(INDEX_4, DKORGHNW);
//        verify(preparedStatement, times(WANTED_ONE_NUMBER_OF_INVOCATIONS)).setLong(INDEX_5, DKORGCNP);
//        verify(preparedStatement, times(WANTED_ONE_NUMBER_OF_INVOCATIONS)).setLong(INDEX_6, DKDIR);
//        verify(preparedStatement, times(WANTED_ONE_NUMBER_OF_INVOCATIONS)).setLong(INDEX_7, SEQUENCE_ID);
//        verify(preparedStatement, times(WANTED_ONE_NUMBER_OF_INVOCATIONS)).setLong(INDEX_8, DKDAT);
//        verify(preparedStatement, times(WANTED_ONE_NUMBER_OF_INVOCATIONS)).setLong(INDEX_9, DKTIMHR);
//        verify(preparedStatement, times(WANTED_ONE_NUMBER_OF_INVOCATIONS)).setLong(INDEX_10, DKSPE);
//        verify(preparedStatement, times(WANTED_ONE_NUMBER_OF_INVOCATIONS)).setLong(INDEX_11, DKSGT);
//        verify(preparedStatement, times(WANTED_ONE_NUMBER_OF_INVOCATIONS)).setLong(INDEX_12, DKPET);
//        verify(preparedStatement, times(WANTED_ONE_NUMBER_OF_INVOCATIONS)).setLong(INDEX_13, DKSGS);
//        verify(preparedStatement, times(WANTED_ONE_NUMBER_OF_INVOCATIONS)).setLong(INDEX_14, SLUATPT);
//        verify(preparedStatement, times(WANTED_ONE_NUMBER_OF_INVOCATIONS)).setLong(INDEX_15, SLUSUCC);
//        verify(preparedStatement, times(WANTED_ONE_NUMBER_OF_INVOCATIONS)).setLong(INDEX_16, SLUFAIL);
//        verify(preparedStatement, times(WANTED_ONE_NUMBER_OF_INVOCATIONS)).setDouble(INDEX_17, SLUDLY);
//        verify(preparedStatement, times(WANTED_ONE_NUMBER_OF_INVOCATIONS)).setLong(INDEX_18, DKSBP);
//    }

//    @Test
//    public void when_SLU_DLY_is_null_set_value_to_minus_1() throws SQLException {
//        AVRO_FILE_RECORDS.add(t_fslu_hr_SLUDLY_NULL_);
//
//        tFsLuHrFileHandler.setPreparedStatement(AVRO_FILE_RECORDS, SEQUENCE_ID, preparedStatement);
//
//        verify(preparedStatement, times(WANTED_ONE_NUMBER_OF_INVOCATIONS)).setDouble(INDEX_17, MINUS_ONE);
//
//    }


}
