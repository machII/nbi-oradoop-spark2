package com.starhomemach.nbi.oradoop.export.util;

import com.starhomemach.nbi.oradoop.util.spark.QueryResultsToTableSaverArgumentsHolder;
import com.starhomemach.nbi.oradoop.util.spark.QueryResultsToTableSaverHandler;
import com.starhomemach.nbi.oradoop.util.spark.SparkSpringConfig;
import com.starhomemach.nbi.oradoop.util.spark.TableStructureUtil;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import scala.collection.JavaConverters;
import scala.collection.Seq;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;

import static org.apache.spark.sql.functions.col;

public class QueryResultsToTableSaverTest {

    @Test
    public void testMe() throws Exception {
        System.out.println("################################################ START ####################################################");

        String[] args = {
                "--destinationTable","tap_db.t_ftcd_agg_dy_a1",
               // "--partitionMidDataframeBy","cl_id,dk_dat_mo,ld_group_id",
                "--numPartitions","10",
                "--sourceQuery","select * from tap_db.t_ftcd_agg_dy_a1 where dk_dat in (11236,11238)"
                ,"--overWrite","true"
        };

        System.setProperty("HADOOP_USER_NAME","hdfs");

        QueryResultsToTableSaverArgumentsHolder queryResultsToTableSaverArgumentsHolder = QueryResultsToTableSaverArgumentsHolder.getInstance(QueryResultsToTableSaverArgumentsHolder.class);
        queryResultsToTableSaverArgumentsHolder.initArgumentsHolder(args);

        SparkSession.builder().appName("save to file").config("spark.testing.memory","471859200").
                config("spark.shuffle.service.enabled","true").
                config("hive.support.concurrency","true").
                config("spark.master", "local").
                enableHiveSupport().getOrCreate();

        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SparkSpringConfig.class);

        context.getBean(QueryResultsToTableSaverHandler.class).run();

        System.out.println("################################################ End ####################################################");


    }

    @Test
    public void stam()
    {
        SparkSession sparkSession = SparkSession.builder().appName("cdr").config("spark.testing.memory", "471859200").config("spark.master", "local").enableHiveSupport().getOrCreate();
        Dataset<Row> df = sparkSession.sql("select 1 a, 11 b, 111 c union all \n" +
                "select 1 a, 11 b, 111 c union all \n" +
                "select 2 a, 11 b, 111 c union all \n" +
                "select 2 a, 44 b, 111 c union all \n" +
                "select 3 a, 11 b, 111 c union all \n" +
                "select 3 a, 44 b, 111 c union all \n" +
                "select 1 a, 11 b, 222 c union all \n" +
                "select 1 a, 11 b, 222 c union all \n" +
                "select 2 a, 11 b, 222 c union all \n" +
                "select 2 a, 44 b, 222 c union all \n" +
                "select 3 a, 11 b, 222 c union all \n" +
                "select 3 a, 44 b, 222 c ");
        df.show();

        List<Column> partitionColums = new ArrayList<>();
        //partitionColums.add(col("a"));
        partitionColums.add(col("b"));

        Seq columnSeq = JavaConverters.asScalaIteratorConverter(partitionColums.iterator()).asScala().toSeq();
    df = df.repartition(columnSeq);
//        df = df.repartition(col("a"), col("c"));
        df.foreachPartition(p -> {
            BufferedWriter writer = null;
            while (p.hasNext()) {
                Row currentP = p.next();
                String currA = currentP.getAs("a").toString();
                String currB = currentP.getAs("b").toString();
                String currC = currentP.getAs("c").toString();
                if (writer == null)
                    writer = new BufferedWriter(new FileWriter("aaa\\" + currB));
                writer.append(currA + "-" + currB + "-" + currC+"\n");
            }
            if(writer!=null)
                writer.close();
        });
    }
    @Test
    public void tableStructTest() throws Exception {

        System.setProperty("HADOOP_USER_NAME","tap");

        SparkSession.builder().appName("save to file").config("spark.testing.memory","471859200").
                config("spark.shuffle.service.enabled","true").
                config("hive.support.concurrency","true").
                config("spark.master", "local").
                enableHiveSupport().getOrCreate();

        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SparkSpringConfig.class);

        TableStructureUtil tableStructUtil = context.getBean(TableStructureUtil.class);
        TableStructureUtil.Table t1 = tableStructUtil.getTableStructure("tap_db", "a");
        System.out.println(t1);

        TableStructureUtil.Table t2 = tableStructUtil.getTableStructure("tap_db", "t_ftcd");
        System.out.println(t2);

        System.out.println("################################################ End ####################################################");
    }



    @Test
    public void bitwisecheck() throws Exception {

        SparkSession.builder().appName("save to file").config("spark.testing.memory","471859200").
                config("spark.shuffle.service.enabled","true").
                config("hive.support.concurrency","true").
                config("spark.master", "local").
                enableHiveSupport().getOrCreate();

        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SparkSpringConfig.class);
        SparkSession sparkSession = context.getBean(SparkSession.class);

        String query = "select bitWiseOrAgg(cast( power(2,cast(dy as int)-1) as bigint)) from \n" +
                "(select '01' dy \n" +
                "union all \n" +
                "select '02' \n " +
                "union all \n" +
                "select '03' \n" +
                ") T" ;

        sparkSession.sql(query).show();

    }


}

