

package com.starhomemach.nbi.oradoop.export.DataRetentionTest;



/**
 * Created by IntelliJ IDEA.
 * User: armb
 * Date: 25/7/16
 * Time: 6:35 PM
 * To change this template use File | Settings | File Templates.
 */



import com.starhomemach.nbi.oradoop.dataretention.GenericDataRetention;
import com.starhomemach.nbi.oradoop.dataretention.RetentionHandler;
import com.starhomemach.nbi.oradoop.dataretention.RetentionHandlerFactory;
import com.starhomemach.nbi.oradoop.dataretention.retentionhandler.ModificationTimeHandler;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.CALLS_REAL_METHODS;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class DataRetentionTest  {

    @Mock
    FileSystem fs;


    @Mock
    FileStatus fs_oradoop_status;

    @Mock
    FileStatus fs_etl_status;

    @Mock
    List<String> getAllFilesListFromPath;


    RetentionHandler genericDataRetain;

    @Mock
    public ModificationTimeHandler modificationTimeHandler;

    public static final String userCode = "telefonica";
    public static String user_code[];
    public String confUserPath =  "/starhome/qos/user/telefonica/conf/";
    public String confSharePath =  "/starhome/qos/share/telefonica/conf/";
    public static final int retention_period = 30;


    @Test
    public void when_dataretention_check() throws ParseException {


        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date date = formatter.parse("2016-06-26");
        long mills = date.getTime();

        modificationTimeHandler.isTimeIsLessThanDays(mills, retention_period);
        Assertions.assertThat(modificationTimeHandler.isTimeIsLessThanDays(mills, retention_period)).isFalse();

    }
}







