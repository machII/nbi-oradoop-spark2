package com.starhomemach.nbi.oradoop.export.ooziemonitoring;

import org.apache.oozie.client.OozieClient;
import org.apache.oozie.client.WorkflowJob;

import java.sql.Connection;
import java.util.List;

/**
 * Created by tpin on 15/08/2016.
 */
public class OzzieClientTest {

    public static void main(String[] arg)  throws Exception{
        OozieClient oozieClient = new OozieClient("http://10.135.11.24:11000/oozie");
        List<WorkflowJob> jobsInfo = oozieClient.getJobsInfo(null, 1, 3);
        System.out.print(" number of workflows should process is:" + jobsInfo.size());
    }
}
