package com.starhomemach.nbi.oradoop.export.util;

import com.starhomemach.nbi.oradoop.commons.CommonSpringConfig;
import com.starhomemach.nbi.oradoop.util.genericexport.GenericExportToFileSaverHandler;
import com.starhomemach.nbi.oradoop.util.genericexport.GenericExportSpringConfig;
import com.starhomemach.nbi.oradoop.util.genericexport.GenericExportToFileSaverArgumentsHolder;
import com.starhomemach.nbi.oradoop.util.oracle.OracleSpringConfig;
import com.starhomemach.nbi.oradoop.util.spark.SparkSpringConfig;
import org.apache.spark.sql.SparkSession;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class GenericExportToFileSaverTest
{
    @Test
    public void testQueryFromFile() throws Exception
    {
        System.out.println("################################################ START ####################################################");

        String[] args = {
                "--oracleUrl","jdbc:oracle:thin:@10.135.11.230:1521/BCED1",
                "--oracleUser","BI_UTILS",
                "--oraclePassword","BI_UTILS",
                "--expJobQueId", "21116", // 21061
                "--jobId","xxx_"+System.currentTimeMillis(),
                "--numPartitions","30",
                "--numberOfDigitsAfterDecimalDot","6"
        };

        System.setProperty("HADOOP_USER_NAME","tap");

        GenericExportToFileSaverArgumentsHolder genericExportToFileSaverArgumentsHolder = GenericExportToFileSaverArgumentsHolder.getInstance(GenericExportToFileSaverArgumentsHolder.class);
        genericExportToFileSaverArgumentsHolder.initArgumentsHolder(args);

        SparkSession.builder().appName("generic save to file").config("spark.testing.memory","471859200").
                config("spark.shuffle.service.enabled","true").
                config("hive.support.concurrency","true").
                config("spark.master", "local").
                enableHiveSupport().getOrCreate();

        OradoopProcessArgs.getInstance().oracleDbUrl(genericExportToFileSaverArgumentsHolder.getOracleUrl())
                                        .user(genericExportToFileSaverArgumentsHolder.getOracleUser())
                                        .password(genericExportToFileSaverArgumentsHolder.getOraclePassword());

        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(GenericExportSpringConfig.class, SparkSpringConfig.class, OracleSpringConfig.class, CommonSpringConfig.class);

        context.getBean(GenericExportToFileSaverHandler.class).run();

        System.out.println("################################################ End ####################################################");
    }
}

