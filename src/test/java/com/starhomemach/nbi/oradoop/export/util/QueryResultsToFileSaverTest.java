package com.starhomemach.nbi.oradoop.export.util;

import com.starhomemach.nbi.oradoop.util.spark.QueryResultsToFileSaverArgumentsHolder;
import com.starhomemach.nbi.oradoop.util.spark.QueryResultsToFileSaverHandler;
import com.starhomemach.nbi.oradoop.util.spark.SparkSpringConfig;
import org.apache.spark.sql.SparkSession;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class QueryResultsToFileSaverTest {

    @Test
    public void testMe() throws Exception {
        System.out.println("################################################ START ####################################################");

        String[] args = {
                "--query", "select 4 y, 'fdgdgd' s,70.80980989090909 x,10/cast(30 as double) a, cast(9.56 as double) b, cast(0 as double) c,cast(60 as double) d,60.4 e,60 f",
                "--filePath","hdfs://10.135.11.24:8020/starhome/utils/sparkdataframetofile/files/test.csv",
                "--csvDelimiter","|",
                "--headers","true",
                "--numberOfDigitsAfterDecimalDot","6",
                "--numPartitions","30",
                "--user","tap",
                "--jobId","xxx_"+System.currentTimeMillis()
        };

        System.setProperty("HADOOP_USER_NAME","tap");

        QueryResultsToFileSaverArgumentsHolder queryResultsToFileSaverArgumentsHolder = QueryResultsToFileSaverArgumentsHolder.getInstance(QueryResultsToFileSaverArgumentsHolder.class);
        queryResultsToFileSaverArgumentsHolder.initArgumentsHolder(args);

        SparkSession.builder().appName("save to file").config("spark.testing.memory","471859200").
                config("spark.shuffle.service.enabled","true").
                config("hive.support.concurrency","true").
                config("spark.master", "local").
                enableHiveSupport().getOrCreate();

        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SparkSpringConfig.class);

        context.getBean(QueryResultsToFileSaverHandler.class).run();

        System.out.println("################################################ End ####################################################");


    }


    @Test
    public void replaceRegex() {
        String fullFilePath = "gabi/testing/20203003/fff.csv";

        String fileDirPath = fullFilePath.substring(0, fullFilePath.lastIndexOf("/"));
        String fileName = fullFilePath.substring(fullFilePath.lastIndexOf("/")+1);

        System.out.println(fileDirPath);
        System.out.println(fileName);
    }

}

