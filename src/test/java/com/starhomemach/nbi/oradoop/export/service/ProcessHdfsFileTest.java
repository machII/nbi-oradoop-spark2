package com.starhomemach.nbi.oradoop.export.service;


import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Lists;
import com.google.common.collect.Table;
import com.starhomemach.nbi.oradoop.export.export.MergeRecords;
import com.starhomemach.nbi.oradoop.export.export.SequenceNextVal;
import com.starhomemach.nbi.oradoop.export.export.TdAtd;
import com.starhomemach.nbi.oradoop.export.file.dto.AvroFileRecord;
import com.starhomemach.nbi.oradoop.export.file.filehandler.FileHandlerFactory;
import com.starhomemach.nbi.oradoop.export.file.filehandler.TFlucDyFileHandler;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.File;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class ProcessHdfsFileTest {

    public static final long CLIENT_ID_1 = 1;
    public static final long CLIENT__GROUP_ID_1 = 2;
    public static final long CLIENT_ID_2 = 3;
    public static final long CLIENT__GROUP_ID_2 = 3;
    public static final long SEQUENCE_NEXT_VAL = 1111l;
    public static final long NEGATIVE_SEQUENCE = -1;
    public static final String INPUT_URL = "input url";
    public static final String PROCESS_URL = "process url";
    public static final String COMPLETE_URL = "complete url";
    public static final String FAILED_URL = "failed url";
    public static final int WANTED_ONE_NUMBER_OF_INVOCATIONS = 1;
    public static final int WANTED_ZERO_NUMBER_OF_INVOCATIONS = 0;
    public static final String FILE_NAME = "file name";
    public static final String COMPLETED_FULL_PATH = COMPLETE_URL + FILE_NAME;
    public static final String FAILED_FULL_PATH = FAILED_URL + FILE_NAME;
    public static final String PROCESS_FULL_PATH = PROCESS_URL + FILE_NAME;
    public static final String INPUT_FULL_PATH = INPUT_URL + FILE_NAME;


    @InjectMocks
    public ProcessHdfsFile processHdfsFile;

    @Mock
    public SequenceNextVal sequenceNextVal;

    @Mock
    public TdAtd tdAtd;


    @Mock
    public TFlucDyFileHandler tFsLuDyFileHandler;

    @Mock
    public MergeRecords mergeRecords;


    @Mock
    private File file;

    @Mock
    public FileHandlerFactory fileHandlerFactory;


    public static final Table<Long, Long, List<AvroFileRecord>> TABLE = HashBasedTable.create();
    public static final List<AvroFileRecord> AVROS_1 = Lists.newArrayList();
    public static final List<AvroFileRecord> AVROS_2 = Lists.newArrayList();


//    @Before
//    public void beforeTest() {
//        TABLE.clear();
//    }

//
//    @Test
//    public void when_move_file_to_processing_failed_call_move_to_fail() {
//        when(file.getName()).thenReturn(FILE_NAME);
//        when(fileUtils.moveFile(INPUT_FULL_PATH, PROCESS_FULL_PATH)).thenReturn(false);
//
//        processHdfsFile.processFileName(file, INPUT_URL, PROCESS_URL, COMPLETE_URL, FAILED_URL);
//
//        verify(fileHandlerFactory, times(WANTED_ZERO_NUMBER_OF_INVOCATIONS)).getFileHandler(anyString());
//        verify(fileUtils, times(WANTED_ONE_NUMBER_OF_INVOCATIONS)).moveFile(INPUT_FULL_PATH, PROCESS_FULL_PATH);
//        verify(fileUtils, times(WANTED_ONE_NUMBER_OF_INVOCATIONS)).moveFile(INPUT_FULL_PATH, FAILED_FULL_PATH);
//    }
//
//    @Test
//    public void when_file_handler_return_null_move_file_to_failed_directory() {
//        when(file.getName()).thenReturn(FILE_NAME);
//        when(fileUtils.moveFile(INPUT_FULL_PATH, PROCESS_FULL_PATH)).thenReturn(true);
//
//        when(fileHandlerFactory.getFileHandler(FILE_NAME)).thenReturn(null);
//
//        processHdfsFile.processFileName(file, INPUT_URL, PROCESS_URL, COMPLETE_URL, FAILED_URL);
//
//        verify(fileHandlerFactory, times(WANTED_ONE_NUMBER_OF_INVOCATIONS)).getFileHandler(FILE_NAME);
//        verify(fileUtils, times(WANTED_ONE_NUMBER_OF_INVOCATIONS)).moveFile(INPUT_FULL_PATH, PROCESS_FULL_PATH);
//        verify(fileUtils, times(WANTED_ONE_NUMBER_OF_INVOCATIONS)).moveFile(PROCESS_FULL_PATH, FAILED_FULL_PATH);
//    }
//
//
//
//    @Test
//    public void when_export_to_oracle_return_false_move_file_to_failed() {
//        set_false_state_since_get_adt_id_return_false();
//        when(file.getName()).thenReturn(FILE_NAME);
//        when(fileUtils.moveFile(INPUT_FULL_PATH, PROCESS_FULL_PATH)).thenReturn(true);
//        when(fileHandlerFactory.getFileHandler(FILE_NAME)).thenReturn(tFsLuDyFileHandler);
//
//        processHdfsFile.processFileName(file, INPUT_URL, PROCESS_URL, COMPLETE_URL, FAILED_URL);
//
//        verify(fileHandlerFactory, times(WANTED_ONE_NUMBER_OF_INVOCATIONS)).getFileHandler(FILE_NAME);
//        verify(fileUtils, times(WANTED_ONE_NUMBER_OF_INVOCATIONS)).moveFile(INPUT_FULL_PATH, PROCESS_FULL_PATH);
//        verify(fileUtils, times(WANTED_ONE_NUMBER_OF_INVOCATIONS)).moveFile(PROCESS_FULL_PATH, FAILED_FULL_PATH);
//        verify(fileUtils, times(WANTED_ZERO_NUMBER_OF_INVOCATIONS)).moveFile(INPUT_FULL_PATH, FAILED_FULL_PATH);
//    }
//
//    @Test
//    public void when_export_to_oracle_return_true_move_file_to_failed() {
//        Table<Long, Long, List<AvroFileRecord>> emptyTable = HashBasedTable.create();
//        when(tFsLuDyFileHandler.readFile(anyObject())).thenReturn(emptyTable);
//
//        when(file.getName()).thenReturn(FILE_NAME);
//        when(fileUtils.moveFile(INPUT_FULL_PATH, PROCESS_FULL_PATH)).thenReturn(true);
//
//        when(fileHandlerFactory.getFileHandler(FILE_NAME)).thenReturn(tFsLuDyFileHandler);
//
//        processHdfsFile.processFileName(file, INPUT_URL, PROCESS_URL, COMPLETE_URL, FAILED_URL);
//
//        verify(fileHandlerFactory, times(WANTED_ONE_NUMBER_OF_INVOCATIONS)).getFileHandler(FILE_NAME);
//        verify(fileUtils, times(WANTED_ONE_NUMBER_OF_INVOCATIONS)).moveFile(INPUT_FULL_PATH, PROCESS_FULL_PATH);
//        verify(fileUtils, times(WANTED_ONE_NUMBER_OF_INVOCATIONS)).moveFile(PROCESS_FULL_PATH,COMPLETED_FULL_PATH);
//        verify(fileUtils, times(WANTED_ZERO_NUMBER_OF_INVOCATIONS)).moveFile(INPUT_FULL_PATH, FAILED_FULL_PATH);
//        verify(fileUtils, times(WANTED_ZERO_NUMBER_OF_INVOCATIONS)).moveFile(INPUT_FULL_PATH, COMPLETED_FULL_PATH);
//        verify(fileUtils, times(WANTED_ZERO_NUMBER_OF_INVOCATIONS)).moveFile(PROCESS_FULL_PATH, FAILED_FULL_PATH);
//    }


    @Test
    public void when_read_file_return_empty_table_return_true() {
//        Table<Long, Long, List<AvroFileRecord>> emptyTable = HashBasedTable.create();
//        when(tFsLuDyFileHandler.readFile(anyObject())).thenReturn(emptyTable);
//
//        boolean shouldBeTrue = processHdfsFile.exportToOracle(anyObject(), tFsLuDyFileHandler);
//
//        assertThat(shouldBeTrue).isTrue();
        assertThat(true).isTrue();
    }

//    @Test
//    public void when_set_adt_id_return_negative_value_return_false() {
//        set_false_state_since_get_adt_id_return_false();
//        boolean shouldBeFalse = processHdfsFile.exportToOracle(anyObject(), tFsLuDyFileHandler);
//
//        assertThat(shouldBeFalse).isFalse();
//    }
//
//    private void set_false_state_since_get_adt_id_return_false() {
//        AVROS_1.add(new t_fslu_dy());
//        TABLE.put(CLIENT_ID_1, CLIENT__GROUP_ID_1, AVROS_1);
//
//        when(tFsLuDyFileHandler.readFile(anyObject())).thenReturn(TABLE);
//        when(sequenceNextVal.getSequenceNextVal()).thenReturn(NEGATIVE_SEQUENCE);
//
//    }
//
//    @Test
//    public void when_read_file_return_table_with_1_avro_and_merge_return_true_return_true() {
//        AVROS_1.add(new t_fslu_dy());
//        TABLE.put(CLIENT_ID_1, CLIENT__GROUP_ID_1, AVROS_1);
//
//        when(tFsLuDyFileHandler.readFile(anyObject())).thenReturn(TABLE);
//        when(sequenceNextVal.getSequenceNextVal()).thenReturn(SEQUENCE_NEXT_VAL);
//        when(mergeRecords.mergeToOracle(AVROS_1, SEQUENCE_NEXT_VAL, tFsLuDyFileHandler)).thenReturn(true);
//        when(tdAtd.insertTdAtdRow(CLIENT_ID_1, CLIENT__GROUP_ID_1, SEQUENCE_NEXT_VAL)).thenReturn(true);
//
//        boolean shouldBeTrue = processHdfsFile.exportToOracle(anyObject(), tFsLuDyFileHandler);
//
//        verify(sequenceNextVal, times(WANTED_ONE_NUMBER_OF_INVOCATIONS)).getSequenceNextVal();
//        verify(tdAtd, times(WANTED_ONE_NUMBER_OF_INVOCATIONS)).insertTdAtdRow(CLIENT_ID_1, CLIENT__GROUP_ID_1, SEQUENCE_NEXT_VAL);
//        verify(mergeRecords, times(WANTED_ONE_NUMBER_OF_INVOCATIONS)).mergeToOracle(AVROS_1, SEQUENCE_NEXT_VAL, tFsLuDyFileHandler);
//
//        assertThat(shouldBeTrue).isTrue();
//    }
//
//
//    @Test
//    public void when_tdAtd_return_false_return_false() {
//        AVROS_1.add(new t_fslu_dy());
//        TABLE.put(CLIENT_ID_1, CLIENT__GROUP_ID_1, AVROS_1);
//
//        when(tFsLuDyFileHandler.readFile(anyObject())).thenReturn(TABLE);
//        when(sequenceNextVal.getSequenceNextVal()).thenReturn(SEQUENCE_NEXT_VAL);
//        when(mergeRecords.mergeToOracle(AVROS_1, SEQUENCE_NEXT_VAL, tFsLuDyFileHandler)).thenReturn(true);
//        when(tdAtd.insertTdAtdRow(CLIENT_ID_1, CLIENT__GROUP_ID_1, SEQUENCE_NEXT_VAL)).thenReturn(false);
//
//        boolean shouldBeFalse = processHdfsFile.exportToOracle(anyObject(), tFsLuDyFileHandler);
//
//        verify(sequenceNextVal, times(WANTED_ONE_NUMBER_OF_INVOCATIONS)).getSequenceNextVal();
//        verify(tdAtd, times(WANTED_ONE_NUMBER_OF_INVOCATIONS)).insertTdAtdRow(CLIENT_ID_1, CLIENT__GROUP_ID_1, SEQUENCE_NEXT_VAL);
//        verify(mergeRecords, times(WANTED_ZERO_NUMBER_OF_INVOCATIONS)).mergeToOracle(AVROS_1, SEQUENCE_NEXT_VAL, tFsLuDyFileHandler);
//
//        assertThat(shouldBeFalse).isFalse();
//    }
//
//
//    @Test
//    public void when_read_file_return_table_with_2_per_client_id_avro_files_and_merge_return_true_return_true() {
//        AVROS_1.add(new t_fslu_dy().newBuilder().setClId(1L).build());
//        AVROS_2.add(new t_fslu_dy().newBuilder().setClId(2L).build());
//        TABLE.put(CLIENT_ID_1, CLIENT__GROUP_ID_1, AVROS_1);
//        TABLE.put(CLIENT_ID_2, CLIENT__GROUP_ID_2, AVROS_2);
//
//        when(tFsLuDyFileHandler.readFile(anyObject())).thenReturn(TABLE);
//        when(sequenceNextVal.getSequenceNextVal()).thenReturn(SEQUENCE_NEXT_VAL);
//        when(mergeRecords.mergeToOracle(AVROS_1, SEQUENCE_NEXT_VAL, tFsLuDyFileHandler)).thenReturn(true);
//        when(mergeRecords.mergeToOracle(AVROS_2, SEQUENCE_NEXT_VAL, tFsLuDyFileHandler)).thenReturn(true);
//        when(tdAtd.insertTdAtdRow(CLIENT_ID_1, CLIENT__GROUP_ID_1, SEQUENCE_NEXT_VAL)).thenReturn(true);
//        when(tdAtd.insertTdAtdRow(CLIENT_ID_2, CLIENT__GROUP_ID_2, SEQUENCE_NEXT_VAL)).thenReturn(true);
//
//        boolean shouldBeTrue = processHdfsFile.exportToOracle(anyObject(), tFsLuDyFileHandler);
//
//        verify(sequenceNextVal, times(WANTED_ONE_NUMBER_OF_INVOCATIONS)).getSequenceNextVal();
//        verify(tdAtd, times(WANTED_ONE_NUMBER_OF_INVOCATIONS)).insertTdAtdRow(CLIENT_ID_1, CLIENT__GROUP_ID_1, SEQUENCE_NEXT_VAL);
//        verify(tdAtd, times(WANTED_ONE_NUMBER_OF_INVOCATIONS)).insertTdAtdRow(CLIENT_ID_2, CLIENT__GROUP_ID_2, SEQUENCE_NEXT_VAL);
//        verify(mergeRecords, times(WANTED_ONE_NUMBER_OF_INVOCATIONS)).mergeToOracle(AVROS_1, SEQUENCE_NEXT_VAL, tFsLuDyFileHandler);
//        verify(mergeRecords, times(WANTED_ONE_NUMBER_OF_INVOCATIONS)).mergeToOracle(AVROS_2, SEQUENCE_NEXT_VAL, tFsLuDyFileHandler);
//
//        assertThat(shouldBeTrue).isTrue();
//    }


}

