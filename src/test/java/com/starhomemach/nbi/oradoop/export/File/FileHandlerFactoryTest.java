package com.starhomemach.nbi.oradoop.export.file;


import static org.assertj.core.api.Assertions.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.starhomemach.nbi.oradoop.export.file.filehandler.FileHandler;
import com.starhomemach.nbi.oradoop.export.file.filehandler.FileHandlerFactory;
import com.starhomemach.nbi.oradoop.export.file.filehandler.TFlucDyFileHandler;
import com.starhomemach.nbi.oradoop.export.file.filehandler.TFlucHrFileHandler;
import com.starhomemach.nbi.oradoop.export.file.filehandler.TFsacDyFileHandler;
import com.starhomemach.nbi.oradoop.export.file.filehandler.TFsacHrFileHandler;

//@RunWith(MockitoJUnitRunner.class)
public class FileHandlerFactoryTest {

    @InjectMocks
    public FileHandlerFactory fileHandlerFactory;



    @Mock
    public TFlucHrFileHandler tFsLuHrFileHandler;

    @Mock
    public TFlucDyFileHandler tFsLuDyFileHandler;

    @Mock
    public TFsacDyFileHandler tFsSaDyFileHandler;

    @Mock
    public TFsacHrFileHandler tFsacHrFileHandler;


   /* @Test
    public void when_file_name_is_null_return_null() {
        FileHandler fileHandler = fileHandlerFactory.getFileHandler(null);

        assertThat(fileHandler).isNull();
    }

    @Test
    public void when_file_name_is_empty_return_null() {
        FileHandler fileHandler = fileHandlerFactory.getFileHandler("");

        assertThat(fileHandler).isNull();
    }

    @Test
    public void when_file_name_is_TFsLuDy_return_TFsLuDyFileHandler() {
        FileHandler fileHandler = fileHandlerFactory.getFileHandler("t_fluc_dy");

        assertThat(fileHandler).isInstanceOf(TFlucDyFileHandler.class);
    }
    @Test
    public void when_file_name_is_TFsLuHr_return_TFsLuDyFileHandler() {
        FileHandler fileHandler = fileHandlerFactory.getFileHandler("t_fluc_hr");

        assertThat(fileHandler).isInstanceOf(TFlucHrFileHandler.class);
    }

    @Test
    public void when_file_name_is_TFsSaDy_return_TFsLuDyFileHandler() {
        FileHandler fileHandler = fileHandlerFactory.getFileHandler("t_fsac_dy");

        assertThat(fileHandler).isInstanceOf(TFsacDyFileHandler.class);
    }

    @Test
    public void when_file_name_is_TFsSaHr_return_TFsLuDyFileHandler() {
        FileHandler fileHandler = fileHandlerFactory.getFileHandler("t_fsac_hr");

        assertThat(fileHandler).isInstanceOf(TFsacHrFileHandler.class);
    }
*/

}

