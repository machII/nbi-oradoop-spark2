package com.starhomemach.nbi.oradoop.export.util;

import com.starhomemach.nbi.oradoop.export.SpringConfig;
import com.starhomemach.nbi.oradoop.oozie.WfLoopSubmitArgumentsHolder;
import com.starhomemach.nbi.oradoop.oozie.WfLoopSubmitHandler;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class WfLoopSubmitTest {

    @Test
    public void testMe() throws Exception
    {
        System.out.println("################################################ START ####################################################");
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SpringConfig.class);

        String[] args = {
                "--repeatingParam1Name","table_name",
                "--repeatingParam1Values","ref_intc_account;ref_intc_account_type;ref_intc_call_char;ref_intc_call_type;ref_intc_company",

                "--oozieUrl","http://10.135.11.24:11000/oozie",
                "--parentWfId","test",
                "--subWorkflowPath","hdfs://isr-poc-1-hdf-1.starhome.com:8020/tomia/fmsng/nrt/ref-data/workflow/run_ref_load",
                "--user","fmsng"
        };


        WfLoopSubmitArgumentsHolder arguments = WfLoopSubmitArgumentsHolder.getInstance(WfLoopSubmitArgumentsHolder.class);
        arguments.initArgumentsHolder(args);

        context.getBean(WfLoopSubmitHandler.class).run();
        System.out.println("################################################ END ####################################################");
    }
}
