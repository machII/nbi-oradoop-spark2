package com.starhomemach.nbi.oradoop.export.export.movefiles;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;
import com.starhomemach.nbi.oradoop.export.movefiles.Mover;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by tpin on 07/08/2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class MoverTest {

    @InjectMocks
    public Mover mover;
     private static final int LIST_SIZE = 30;
    private static final int UNLIMITED = 0;
    private static final int LOWER = 10;
    private static final int HIGHER = 40;

    @Test
    public void move_files_unlimited() throws IOException{
        assertThat(mover.getNumFilesToMove(LIST_SIZE,UNLIMITED)).isEqualTo(LIST_SIZE);
    }
    @Test
    public void move_files_lower_then_max() throws IOException{
        assertThat(mover.getNumFilesToMove(LIST_SIZE,LOWER)).isEqualTo(LOWER);
    }
    @Test
    public void move_files_higher_then_max() throws IOException{
        assertThat(mover.getNumFilesToMove(LIST_SIZE,HIGHER)).isEqualTo(LIST_SIZE);
    }
}
