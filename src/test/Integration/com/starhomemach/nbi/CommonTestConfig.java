package com.starhomemach.nbi;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

import javax.persistence.EntityManagerFactory;

import static org.mockito.Mockito.mock;

@Configuration
@ComponentScan("com.starhomemach" )
//@ImportResource({"classpath:idc-persistence.xml", "classpath:bi-datasource.xml"})

@ImportResource({"classpath:nbi-oradoop-spring.xml"})
public class CommonTestConfig {
    @Bean
    public EntityManagerFactory entityManagerFactory() {
        return mock(EntityManagerFactory.class);
    }
}
