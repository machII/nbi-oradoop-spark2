package com.starhomemach.nbi.oradoop.export.Ida;

import com.starhomemach.nbi.oradoop.export.SpringConfig;
import com.starhomemach.nbi.oradoop.export.conf.ExportArgumentsHolder;
import com.starhomemach.nbi.oradoop.export.util.HandleProcessConfiguration;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class IdaTrafficExportMain
{

    public static void main(String[] args) throws Exception {

        System.out.println("################################### START ###############################################################");
        IdaExportArgumentsHolder idaExportArgumentsHolder = IdaExportArgumentsHolder.getInstance();
        idaExportArgumentsHolder.initExportArgumentsHolder(args);

        // load oradoop properties file
        HandleProcessConfiguration processConfiguration = new HandleProcessConfiguration();
        processConfiguration.setPath(HandleProcessConfiguration.STARHOME_IDA_SHARE_CONF);
        ExportArgumentsHolder.getInstance().setPropertiesFileName(processConfiguration.handleOradoopConf(new String[]{}, 0));

        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SpringConfig.class);

        context.getBean(IdaTrafficExportHandler.class).run();

        System.out.println("########################################################## END EXPORT TO ORACLE #############################");

    }

}

