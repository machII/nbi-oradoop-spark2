package com.starhomemach.nbi.oradoop.export.service;

import com.google.common.collect.Maps;
import com.starhomemach.nbi.oradoop.OradoopConf;
import com.starhomemach.nbi.oradoop.export.conf.ExportArgumentsHolder;
import com.starhomemach.nbi.oradoop.export.export.MergeRecords;
import com.starhomemach.nbi.oradoop.export.file.EtlFileStatus;
import com.starhomemach.nbi.oradoop.export.file.dto.AvroFileRecord;
import com.starhomemach.nbi.oradoop.export.file.filehandler.FileHandler;
import com.starhomemach.nbi.oradoop.export.file.filehandler.FileHandlerFactory;
import com.starhomemach.nbi.oradoop.export.util.HandleProcessConfiguration;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Service
public class ProcessHdfsDirectoryService {


    @Autowired
    private ProcessHdfsFile processHdfsFile;

    @Autowired
    public MergeRecords mergeRecords;

    @Autowired
    public OradoopConf oradoopConf;

    private final Log logger = LogFactory.getLog(getClass());

    private static final String PROCESSING = "/etl/oradoop/processing";

    @Autowired
    private FileHandlerFactory fileHandlerFactory;

    private String getInputUrlToProcess(String userName, String jobId, String path) {
        return path + userName + PROCESSING + "/" + jobId + "/";
    }

    private void processHdfsDirectory(String inputUrl, boolean throwNotFoundException) throws IOException, SQLException, ParseException {
        logger.info("going to process directory:" + inputUrl);
        Path inputPath = new Path(inputUrl);
        FileSystem fs = inputPath.getFileSystem(oradoopConf.getConf());
        FileStatus[] jobIdDirectories = fs.globStatus(inputPath);

        if (jobIdDirectories != null) {
            Map<FileHandler, List<AvroFileRecord>> fileToAvroRecords = Maps.newHashMap();
            try {
                for (FileStatus jobIdDirectory : jobIdDirectories) {
                    if (jobIdDirectory.isDirectory()) {
                        FileStatus[] jobDirectoryFiles = fs.listStatus(jobIdDirectory.getPath());
                        for (FileStatus tableFile : jobDirectoryFiles) {
                            {
                                processFileDirectory(fs, tableFile, fileToAvroRecords);
                            }
                        }
                    }
                }
                logger.info("Finished to process files. Number of processed files is:" + fileToAvroRecords.size());
                mergeRecords.mergeToOracle(fileToAvroRecords);
            } catch (Exception e) {
                logger.error("Failed to process url: " + inputUrl + ". The exception is: " + e.getMessage() + ".going to throw exception");
                throw e;
            }
        }
        else
        {
            if (throwNotFoundException) {
                throw new NullPointerException();
            }
        }
    }

    private void processHdfsDirectoryOrdered(String path) throws IOException, SQLException, ParseException
    {
        String jobProcessingDirectory = getInputUrlToProcess(ExportArgumentsHolder.getInstance().getUser(), ExportArgumentsHolder.getInstance().getJobId(), path);
        ArrayList<String> orderlist = ExportArgumentsHolder.getInstance().getExportDirOrderSplit();

        Path inputPath = new Path(jobProcessingDirectory);
        FileSystem fs = inputPath.getFileSystem(oradoopConf.getConf());
        FileStatus[] directories = fs.listStatus(inputPath);

        for ( String element: orderlist ) {
            if (ExportArgumentsHolder.getInstance().isElementAPattern(element))
                for ( FileStatus directory : directories ) {
                    if (directory.getPath().getName().matches(ExportArgumentsHolder.getInstance().getPatternFromElement(element))) {
                        String[] secondLevelList = ExportArgumentsHolder.getInstance().getPatternSecondLevelListFromElement(element);
                        if (secondLevelList.length == 0)
                          processHdfsDirectory(directory.getPath() + "/", false);
                        else
                            for ( String secondLevelDir: secondLevelList ) {
                                processHdfsDirectory(directory.getPath() + "/" + secondLevelDir + "/", false);
                            }
                    }
                }
            else
                processHdfsDirectory(jobProcessingDirectory + element + "/", false);
        }
    }


    private void processFileDirectory(FileSystem fs, FileStatus tableFile, Map<FileHandler, List<AvroFileRecord>> fileToAvroFiles) throws IOException {
        if (tableFile.isDirectory()) {
            Path tableDirectoryPath = tableFile.getPath();
            String tableDirectoryName = tableDirectoryPath.getName();
            String[] tokens = tableDirectoryName.split(Path.SEPARATOR);

            // take the name of the folder were the avro files are to see what handler is needed
            FileHandler fileHandler = fileHandlerFactory.getFileHandler(tokens[tokens.length-1]);
            FileStatus[] perTableFiles = fs.listStatus(tableDirectoryPath);
            if (shouldProcessDirectory(perTableFiles).equalValue(EtlFileStatus._SUCCESS)) {
                logger.info("number of avro files to process in directory:" + tableDirectoryName + " is:" + (perTableFiles.length - 1));
                for (FileStatus perTableFile : perTableFiles) {
                    Path perTableFilePath = perTableFile.getPath();
                    String fileToProcessName = perTableFilePath.getName();
                    if (!fileToProcessName.equals(EtlFileStatus._SUCCESS.toString())) {
                        processHdfsFile.processFileName(perTableFilePath.toString(), fileHandler, fileToAvroFiles);
                    }
                }
            } else {
                //TODO throw exception
                logger.error("JOB status is not _SUCCESS going to end run and throw exception");
            }
        }
    }


    private EtlFileStatus shouldProcessDirectory(FileStatus[] files) {
        EtlFileStatus returnEtlFileStatus = EtlFileStatus.OTHER;
        if (Arrays.stream(files).filter(file -> EtlFileStatus._SUCCESS.toString().equals(file.getPath().getName().toUpperCase())).count() > 0) {
            returnEtlFileStatus = EtlFileStatus._SUCCESS;
        } else if (Arrays.stream(files).filter(file -> EtlFileStatus._FAILED.toString().equals(file.getPath().getName().toUpperCase())).count() > 0) {
            returnEtlFileStatus = EtlFileStatus._FAILED;
        }
        logger.info("Directory status is:" + returnEtlFileStatus.toString());
        return returnEtlFileStatus;
    }


    public void run(String... args) throws Exception {
        logger.info("################################### START ###############################################################");
        String userName = args[0];
        String jobId = args[1];

        if (userName.isEmpty() || jobId.isEmpty()) {
            logger.error("User name or jobId is incorrect going to return");
            return;
        }
        logger.info("User name to process files is: " + userName);
        logger.info("Job id to process files is: " + jobId);

        // load HDFS path from properties file or defaultVal
        String defVal = HandleProcessConfiguration.HADOOP_FILE_SYSTEM_USER_PATH;
        if (args.length > 3) {
            defVal = args[3];
        }
        String hdfsUserPath =  new HandleProcessConfiguration().handleCustomizedHdfsUserPath(args[2], defVal);

        // process files
        processHdfsDirectory(getInputUrlToProcess(userName, jobId, hdfsUserPath), true);
        logger.info("################################### END ###############################################################");
    }

    public void runWithArgumentsHolder() throws Exception {
        logger.info("################################### START ###############################################################");

        // load HDFS path from properties file or defaultVal
        String defVal = HandleProcessConfiguration.HADOOP_FILE_SYSTEM_USER_PATH;
        if (ExportArgumentsHolder.getInstance().isUserPathSet()) {
            defVal = ExportArgumentsHolder.getInstance().getUserPath();
        }
        String hdfsUserPath = new HandleProcessConfiguration().handleCustomizedHdfsUserPath(
                ExportArgumentsHolder.getInstance().getPropertiesFileName(),
                defVal);

        // process files
        processHdfsDirectoryOrdered(hdfsUserPath);
        logger.info("################################### END ###############################################################");
    }


}





