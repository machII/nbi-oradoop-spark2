package com.starhomemach.nbi.oradoop.export.file.filehandler;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;
import com.starhomemach.nbi.oradoop.export.export.Tdsvk;
import com.starhomemach.nbi.oradoop.export.file.AvroFsInput;
import com.starhomemach.nbi.oradoop.export.file.dto.AvroFileRecord;
import com.starhomemach.nbi.oradoop.export.file.dto.T_dsvk;

@Component
public class TDsvkFileHandler extends QosFileHandler {


    private final Log logger = LogFactory.getLog(getClass());


    private final String INSERT_INTO_T_DSVK = "INSERT INTO bi.t_dsvk (dk_svk, svk_cd, cre_dat, mod_dat) VALUES (BI.Q_SVK_ID.NEXTVAL, ?, SYSDATE, SYSDATE)";

    @Autowired
    public Tdsvk tdsvk;


    @Override
    public String[] getInsertQuery() {
        return new String[]{ INSERT_INTO_T_DSVK};
    }

    @Override
    public String[] getMergeQuery() {
        return new String[]{""};
    }

    @Override
    public List<AvroFileRecord> readFileToList(String avroFilePath) throws IOException {
        List<AvroFileRecord> avroRecords = Lists.newArrayList();
        Path path = new Path(avroFilePath);
        Configuration config = new Configuration();
        org.apache.avro.file.SeekableInput input = new AvroFsInput(path, config);
        org.apache.avro.io.DatumReader<T_dsvk> tDSvkDatumReader = new org.apache.avro.specific.SpecificDatumReader<T_dsvk>(T_dsvk.class);
        org.apache.avro.file.FileReader<T_dsvk> t_dsvk_dyRecords = org.apache.avro.file.DataFileReader.openReader(input, tDSvkDatumReader);
        while (t_dsvk_dyRecords.hasNext()) {
            T_dsvk record = t_dsvk_dyRecords.next();
            avroRecords.add(record);
        }

        t_dsvk_dyRecords.close();
        logger.info("succeeded to process file:" + avroFilePath + ". number of records:" + avroRecords.size());

        return avroRecords;
    }

    @Override
    public boolean isMergeOperation() {
        return false;
    }


    public void setPreparedStatement(AvroFileRecord avroFileRecords, Long sequenceId, PreparedStatement preparedStatement) throws SQLException, ParseException {


        T_dsvk tDsvkRecord = (T_dsvk) avroFileRecords;

        String SVK_CD = tDsvkRecord.getSvkCd().toString();
        Long DK_SVK = tdsvk.getTdsvk(SVK_CD);

        if (DK_SVK == -1) {
            logger.info("going to set PreparedStatement for record:" + avroFileRecords);
            preparedStatement.setString(1, SVK_CD);

            preparedStatement.addBatch();
        }
        else
        {
            logger.info("going to ignore record :" + avroFileRecords + " since it already exists and the record DK_SVK :" + DK_SVK);
        }
    }

    @Override
    public boolean isSequenceOperation() {
        return false;
    }

    @Override
    public String getHandlerKey() {
        return "t_dsvk";
    }


}
