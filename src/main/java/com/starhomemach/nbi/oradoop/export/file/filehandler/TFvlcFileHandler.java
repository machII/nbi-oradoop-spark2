package com.starhomemach.nbi.oradoop.export.file.filehandler;

import com.google.common.collect.Lists;
import com.starhomemach.nbi.oradoop.export.file.AvroFsInput;
import com.starhomemach.nbi.oradoop.export.file.dto.AvroFileRecord;
import com.starhomemach.nbi.oradoop.export.file.dto.T_fvlc;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

/**
 * Created by kcorsia on 20/02/2017.
 */
@Component
public class TFvlcFileHandler extends QosFileHandler {

    private final Log logger = LogFactory.getLog(getClass());

    private final String INSERT_INTO_T_FVLC = "insert into BI.HDP_T_FVLC_TMP values(?,?,?,?,?,?,?,?,?,?)";

    private final String MERGE_T_FVLC_DY_WITH_T_FVLC_TMP = "MERGE INTO BI.T_FVLC DY\n" +
            "USING (SELECT DISTINCT * FROM BI.HDP_T_FVLC_TMP) temp \n" +
            "ON (\n" +
            "DY.CL_ID = temp.CL_ID AND\n" +
            "DY.DK_CMM = temp.DK_CMM AND\n" +
            "DY.DK_ORG_CNP  = temp.DK_ORG_CNP AND\n" +
            "DY.DK_DAT = temp.DK_DAT AND\n" +
            "DY.DK_DIR = temp.DK_DIR AND\n" +
            "DY.DK_PET = temp.DK_PET)\n" +
            "WHEN MATCHED THEN UPDATE SET \n" +
            "DY.DK_ADT = temp.DK_ADT, \n" +
            "DY.HOPS = temp.HOPS, \n" +
            "DY.ATPT = temp.ATPT, \n" +
            "DY.VLRS = temp.VLRS \n" +
            "WHEN NOT MATCHED THEN INSERT (DY.CL_ID, DY.DK_CMM, DY.DK_ORG_CNP, DY.DK_DAT, DY.DK_DIR, DY.DK_PET, DY.DK_ADT, DY.HOPS, DY.ATPT, DY.VLRS )\n" +
            "VALUES (temp.CL_ID, temp.DK_CMM, temp.DK_ORG_CNP, temp.DK_DAT, temp.DK_DIR, temp.DK_PET, temp.DK_ADT, temp.HOPS, temp.ATPT, temp.VLRS )";
    @Override
    public String[] getInsertQuery() { return new String[]{INSERT_INTO_T_FVLC};
    }

    @Override
    public String[] getMergeQuery() { return new String[]{MERGE_T_FVLC_DY_WITH_T_FVLC_TMP};
    }

    @Override
    public List<AvroFileRecord> readFileToList(String avroFilePath) throws IOException {
        List<AvroFileRecord> avroRecords = Lists.newArrayList();
        Path path = new Path(avroFilePath);
        Configuration config = new Configuration();

        org.apache.avro.file.SeekableInput input = new AvroFsInput(path, config);

        org.apache.avro.io.DatumReader<T_fvlc> tFvlcDatumReader = new org.apache.avro.specific.SpecificDatumReader<T_fvlc>(T_fvlc.class);
        org.apache.avro.file.FileReader<T_fvlc> t_fvlcRecords = org.apache.avro.file.DataFileReader.openReader(input, tFvlcDatumReader);

        while (t_fvlcRecords.hasNext()) {
            T_fvlc record = t_fvlcRecords.next();
            avroRecords.add(record);
        }

        t_fvlcRecords.close();
        logger.info("succeeded to process file:" + avroFilePath + ". number of records:" + avroRecords.size());

        return avroRecords;
    }

    @Override
    public boolean isMergeOperation() {
        return true;
    }

    @Override
    public boolean isSequenceOperation() {
        return true;
    }

    @Override
    public String getHandlerKey() {
        return "T_FVLC";
    }


    public void setPreparedStatement(AvroFileRecord avroFileRecords, Long sequenceId, PreparedStatement preparedStatement) throws SQLException, ParseException {

        logger.info("going to set PreparedStatement for record:" + avroFileRecords);

        T_fvlc tFVlcRecord = (T_fvlc) avroFileRecords;
        preparedStatement.setLong(1, tFVlcRecord.getDkCmm());
        preparedStatement.setLong(2, tFVlcRecord.getClId());
        preparedStatement.setLong(3, tFVlcRecord.getDkDat());
        preparedStatement.setLong(4, tFVlcRecord.getDkDir());
        preparedStatement.setLong(5, tFVlcRecord.getDkOrgCnp());
        preparedStatement.setLong(6, tFVlcRecord.getDkPet());
        preparedStatement.setLong(7, sequenceId);
        preparedStatement.setLong(8, tFVlcRecord.getHops());
        preparedStatement.setLong(9, tFVlcRecord.getAtpt());
        preparedStatement.setLong(10, tFVlcRecord.getVlrs());

        preparedStatement.addBatch();
    }
}
