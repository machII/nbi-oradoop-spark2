package com.starhomemach.nbi.oradoop.export.file.sequence;

import com.starhomemach.nbi.oradoop.export.file.dto.AvroFileRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 * Created by tpin on 09/10/2016.
 */
public abstract class SequenceHandler {
    @Autowired
    protected JdbcTemplate jdbcTemplate;

    public abstract Long getSequenceID(AvroFileRecord record);
}
