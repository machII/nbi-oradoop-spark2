package com.starhomemach.nbi.oradoop.export.ReprocessData;

import com.starhomemach.nbi.oradoop.export.DailyReprocessData;
import com.starhomemach.nbi.oradoop.export.JobHistory.JobHistoryHandler;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.perf4j.aop.Profiled;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.sql.*;
import java.text.ParseException;
/**
 * Created by armb on 16-11-2016.
 */
@Service
public class ReprocessMonthlyDataHandler {

    private final Log logger = LogFactory.getLog(getClass());

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Profiled
    public int fetchDkDatMo(String dkDatMo) throws SQLException {

        Connection connection = jdbcTemplate.getDataSource().getConnection();
        CallableStatement callStmt = connection.prepareCall("{?= call bi.k_bi_utl.dat2DkDatMO(TO_DATE(?, 'YYYYMMDD'))}");
        callStmt.setString(2,dkDatMo);
        callStmt.registerOutParameter(1,Types.INTEGER);
        callStmt.execute();
        return callStmt.getInt(1);
    }

    @Profiled
    public void calculateDkDatMo(String dkMoBegin, String dkMoFinish) throws SQLException, DataAccessException, ParseException
    {
        try {
            JobHistoryHandler jobHistoryHandler = new JobHistoryHandler();
            DailyReprocessData dailyReprocessData = new DailyReprocessData();
            long dkDatMo = jobHistoryHandler.getDkDatMo();
            int rsBeginDkDatMo = 0, rsEndDkDatMo = 0;
            logger.info("going to fetch last DK_DAT_MO");
             if (((rsBeginDkDatMo = fetchDkDatMo(dkMoBegin))!= 0) && ((rsEndDkDatMo = fetchDkDatMo(dkMoFinish)) != 0)) {
                rsEndDkDatMo++;
                logger.info("Value of initial dkDatMO is " + rsBeginDkDatMo + " and value of final dkDatMo is " + rsEndDkDatMo);
                dailyReprocessData.ReprocessDataPeriod(dkDatMo, rsBeginDkDatMo, rsEndDkDatMo);
            }
            logger.info("Value of initial dkDatMO and final dkDatMo are null");
        }catch (Exception e)
        {
            logger.info("Exception raised while fetching the column value "+e.getMessage());
        }
    }

}
