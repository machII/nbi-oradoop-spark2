package com.starhomemach.nbi.oradoop.export.conf;

import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;

import java.util.ArrayList;

public class ExportArgumentsHolder {

    public static final String ORDER_LIST_SEPARATOR = ",";
    public static final String SECOND_LEVEL_START = "[";
    public static final String SECOND_LEVEL_END = "]";
    public static final String PATTERN_DETECTION_CHARACTER = "*";

    private static ExportArgumentsHolder ourInstance = new ExportArgumentsHolder();

    public static ExportArgumentsHolder getInstance() { return ourInstance; }

    private static final String DEFAULT_PROPERTIES_FILE_NAME = "oradoop.properties";

    public ArgumentParser parser;
    public Namespace parser_namespace;

    private String user;
    private String jobId;
    private String propertiesFileName;
    private String userPath = "";
    private String outSchema = "";
    private String exportDirOrder = "";
    private String extraParams = "";

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public String getPropertiesFileName() {
        return propertiesFileName;
    }

    public void setPropertiesFileName(String propertiesFileName) {
        this.propertiesFileName = propertiesFileName;
    }

    public String getUserPath() {
        return userPath;
    }

    public void setUserPath(String userPath) {
        this.userPath = userPath;
    }

    public String getOutSchema() {
        return outSchema;
    }

    public void setOutSchema(String outSchema) {
        this.outSchema = outSchema;
    }

    public boolean isUserPathSet() {
        return !userPath.isEmpty();
    }

    public String getExportDirOrder() {
        return exportDirOrder;
    }

    public void setExportDirOrder(String exportDirOrder) {
        this.exportDirOrder = exportDirOrder;
    }

    public String getExtraParams() {
        return extraParams;
    }

    public void setExtraParams(String extraParams) {
        this.extraParams = extraParams;
    }

    public boolean isExportDirOrderSet() {
        return !exportDirOrder.isEmpty();
    }

    public ArrayList<String> getExportDirOrderSplit() {
        String orderList = getExportDirOrder();

        ArrayList<String> dirArray = new ArrayList<>();
        int substringStart = 0;

        while (true) {
            String leftToSplit = orderList.substring(substringStart <= orderList.length() ? substringStart : orderList.length());

            if (leftToSplit.length() == 0)
                break;

            int limit = leftToSplit.length();
            int commaIndex = leftToSplit.indexOf(ORDER_LIST_SEPARATOR);
            int bracketIndex = leftToSplit.indexOf(SECOND_LEVEL_START);

            if (commaIndex != -1 || bracketIndex != -1)
            {
                if (bracketIndex != -1 && bracketIndex < commaIndex) {
                    limit = leftToSplit.indexOf(SECOND_LEVEL_END) + 1;
                }
                else {
                    limit = commaIndex;
                }
            }

            String element = leftToSplit.substring(0, limit);
            substringStart += element.length() + 1;
            dirArray.add(element);
        }

        return dirArray;
    }

    public boolean isElementAPattern(String element) {
        if (element.contains(PATTERN_DETECTION_CHARACTER)) {
            return true;
        }
        return false;
    }

    public String getPatternFromElement(String element) {
        return element.substring(0, element.indexOf(SECOND_LEVEL_START) > 0 ? element.indexOf(SECOND_LEVEL_START) : element.length());
    }

    public String[] getPatternSecondLevelListFromElement(String element) {
        if (element.indexOf(SECOND_LEVEL_START) > 0) {
            return element.substring(element.indexOf(SECOND_LEVEL_START) + 1, element.indexOf(SECOND_LEVEL_END)).split(ORDER_LIST_SEPARATOR);
        }
        else
            return new String[]{};
    }

    public void initExportArgumentsHolder(String[] args) {
        parser = ArgumentParsers.newArgumentParser("Oradoop Export");

        buildArgumentsParser(parser);

        parseArguments(parser, args);
    }

    private void buildArgumentsParser(ArgumentParser parser) {
        parser.addArgument("--user")
                .type(String.class)
                .required(true);

        parser.addArgument("--job_id")
                .type(String.class)
                .required(true);

        parser.addArgument("--user_path")
                .type(String.class)
                .required(true);

        parser.addArgument("--out_schema")
                .type(String.class)
                .required(true);

        parser.addArgument("--properties_file_name")
                .type(String.class);

        parser.addArgument("--export_dir_order")
                .type(String.class);

        parser.addArgument("--extra_params")
                .type(String.class);
    }

    private void parseArguments(ArgumentParser parser, String[] args) {

        try {
            parser_namespace = parser.parseArgs(args);

            if (parser_namespace.get("user") != null)
                this.setUser(parser_namespace.get("user"));

            if (parser_namespace.get("job_id") != null)
                this.setJobId(parser_namespace.get("job_id"));

            if (parser_namespace.get("properties_file_name") != null)
                this.setPropertiesFileName(parser_namespace.get("properties_file_name"));
            else
                this.setPropertiesFileName(DEFAULT_PROPERTIES_FILE_NAME);

            if (parser_namespace.get("user_path") != null)
                this.setUserPath(parser_namespace.get("user_path"));

            if (parser_namespace.get("out_schema") != null)
                this.setOutSchema(parser_namespace.get("out_schema"));

            if (parser_namespace.get("export_dir_order") != null)
                this.setExportDirOrder(parser_namespace.get("export_dir_order"));

            if (parser_namespace.get("extra_params") != null)
                this.setExtraParams(parser_namespace.get("extra_params"));
        }
        catch (ArgumentParserException e) {
            System.out.println("parse Arguments failed: syntax is " + parser.toString());
            parser.handleError(e);
        }
    }
}
