package com.starhomemach.nbi.oradoop.export.file.dto;

public interface AvroFileRecord {

    public Long getClId();

    public default Long getDkCmm() {
        return new Long(0);
    }
}
