package com.starhomemach.nbi.oradoop.export.file.sequence;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import com.starhomemach.nbi.oradoop.export.file.dto.AvroFileRecord;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Component;

/**
 * Created by tpin on 09/10/2016.
 */
@Component
public class QOSSequenceHandler extends SequenceHandler {

    public static final String SELECT_BI_NEW_Q_HADOOP_ADT_NEXTVAL_FROM_DUAL = "select  BI.Q_HADOOP_ADT.NEXTVAL from dual";

    private static Table<Long, Long, Long> clientIdDkCommToSequence=HashBasedTable.create();

    private final Log logger = LogFactory.getLog(getClass());

    @Override
    public Long getSequenceID(AvroFileRecord record) {
        Long clId = record.getClId();
        Long dkCmm = record.getDkCmm();

        if (clId != null && dkCmm != null && !clientIdDkCommToSequence.contains(clId, dkCmm)) {
            logger.info("sequence number is not cached for client id:" + clId + " dk cmm:" + dkCmm);
            handleNewSequenceId(clId, dkCmm, clientIdDkCommToSequence);
        }
        return clientIdDkCommToSequence.get(clId, dkCmm);
    }

    public Long getSequenceID(Long clId,Long dkCmm) {

        if (clId != null && dkCmm != null && !clientIdDkCommToSequence.contains(clId, dkCmm)) {
            logger.info("sequence number is not cached for client id:" + clId + " dk cmm:" + dkCmm);
            handleNewSequenceId(clId, dkCmm, clientIdDkCommToSequence);
        }
        return clientIdDkCommToSequence.get(clId, dkCmm);
    }

    public void handleNewSequenceId(Long clId, Long dkCmm, Table<Long, Long, Long> clientIdClientGroupToSequence) throws DataAccessException {
        Long sequenceNextVal = -1l;

        sequenceNextVal = jdbcTemplate.queryForObject(SELECT_BI_NEW_Q_HADOOP_ADT_NEXTVAL_FROM_DUAL, Long.class);
        logger.info("Sequence next val is:" + sequenceNextVal);
        logger.info("Cache sequence number for client id:" + clId + " dk cmm:" + dkCmm);
        clientIdClientGroupToSequence.put(clId, dkCmm, sequenceNextVal);
    }
}
