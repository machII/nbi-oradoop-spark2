package com.starhomemach.nbi.oradoop.export.file.filehandler;

import com.starhomemach.nbi.oradoop.export.file.sequence.M2MSequenceHandler;
import com.starhomemach.nbi.oradoop.export.file.sequence.QOSSequenceHandler;
import com.starhomemach.nbi.oradoop.export.file.sequence.SequenceHandler;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by tpin on 09/10/2016.
 */
public abstract class M2MFileHandler extends FileHandler {
    @Autowired
    private M2MSequenceHandler sequence;

    public SequenceHandler getSequenceHandler() {
        return sequence;
    }

    @Override
    public boolean isAppendOperation() {
        return false;
    }

    @Override
    public String[] getAppendQuery() {
        return new String[]{""};
    }

    @Override
    public boolean useFinalizeProcedure() { return false; }

    @Override
    public String[] getFinalizeProcedure() { return new String[]{""}; }
}
