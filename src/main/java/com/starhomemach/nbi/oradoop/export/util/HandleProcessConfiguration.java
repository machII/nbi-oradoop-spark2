package com.starhomemach.nbi.oradoop.export.util;

import java.io.IOException;
import java.util.Properties;

import com.starhomemach.nbi.oradoop.export.conf.ExportArgumentsHolder;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.springframework.stereotype.Service;

import com.starhomemach.nbi.oradoop.OradoopConf;

@Service
public class HandleProcessConfiguration {


    private static final String BI_URL = "bi.url";
    private static  String OOZIE_URL = "oozie.url";
    private static  String ZOOKEEPER_HOST = "zookeeper.host";
    private static final String BI_USERNAME = "bi.username";
    private static final String BI_PASSWORD = "bi.password";
    private static final String RUN_ONLY_HOURLY = "runOnlyHourlyData";
    private static final String HDFS_USER_PATH = "hdfs_user_path";
    private static final String ORADOOP_PROPERTIES = "oradoop.properties";
    private static final String MONITORING_PROPERTIES = "monitoring.properties";
    private static final String ZOOKEEPER_PROPERTIES = "zookeeper.properties";
    private static final String DB_CREDENTIALS_PROPERTIES = "db_credentials.properties";
    private static final String STARHOME_QOS_SHARE_CONF = "/starhome/qos/share/conf/";
    public static final String STARHOME_M2M_SHARE_CONF = "/starhome/m2m/share/conf/";
    public static final String STARHOME_EUREG_SHARE_CONF = "/starhome/eureg/share/conf/";
    public static final String STARHOME_SILENTROAMERS_SHARE_CONF = "/starhome/silentroamers/share/conf/";
    public static final String STARHOME_IDA_SHARE_CONF = "/starhome/ida/share/conf/";
    public static final String HADOOP_FILE_SYSTEM_USER_PATH = "/starhome/qos/user/";
    private static final String HADOOP_COMMON_DB_CONF = "/starhome/common/conf/";
    public static final String EUREG_USER_PROPERTIES_PATH = "/starhome/eureg/user/%s/conf/user.properties";
    private static final Log logger = LogFactory.getLog(HandleProcessConfiguration.class);
    private static Configuration conf;

    static
    {
        try{
            conf = new OradoopConf().getConf();
        }
        catch(Exception e)
        {
            throw new RuntimeException(e);
        }
    }

    private String path = STARHOME_QOS_SHARE_CONF;

    public void setPath(String path){
        this.path = path;
    }
    public void setConf(Configuration conf){
        HandleProcessConfiguration.conf = conf;
    }

    public String getPath(){
        return path;
    }


    public String handleOradoopConf(String[] args, int index) throws IOException {
        String propPath = path + getFileName(args, index, ORADOOP_PROPERTIES);
        Properties oradoopProperties = getPropertiesFile(propPath);
        String biUrl = oradoopProperties.getProperty(BI_URL);
        String biUserName = oradoopProperties.getProperty(BI_USERNAME);
        String biPassword = oradoopProperties.getProperty(BI_PASSWORD);
        logger.info("DB URL:" + biUrl);
        logger.info("DB User:" + biUserName);
        OradoopProcessArgs.getInstance().oracleDbUrl(biUrl).user(biUserName).password(biPassword);
        return propPath;
    }

    public String handleOradoopConf() throws IOException {
        String propPath = path + ExportArgumentsHolder.getInstance().getPropertiesFileName();
        Properties oradoopProperties = getPropertiesFile(propPath);
        String biUrl = oradoopProperties.getProperty(BI_URL);
        String biUserName = oradoopProperties.getProperty(BI_USERNAME);
        String biPassword = oradoopProperties.getProperty(BI_PASSWORD);
        logger.info("DB URL:" + biUrl);
        logger.info("DB User:" + biUserName);
        OradoopProcessArgs.getInstance().oracleDbUrl(biUrl).user(biUserName).password(biPassword);
        return propPath;
    }

    public String handleCustomizedHdfsUserPath(String propPath, String defVal) throws IOException {
        Properties oradoopProperties = getPropertiesFile(propPath);
        String hdfsUsrPath = oradoopProperties.getProperty(HDFS_USER_PATH, defVal);
        logger.info("HDFS USER PATH: " + hdfsUsrPath);
        return hdfsUsrPath;
    }

    public boolean isToRunOnlyHourlyDataProcessing(String propPath) throws IOException {
        Properties oradoopProperties = getPropertiesFile(propPath);
        String runOnlyHourly = oradoopProperties.getProperty(RUN_ONLY_HOURLY);
        if (runOnlyHourly != null && (runOnlyHourly.equalsIgnoreCase("true") || runOnlyHourly.equalsIgnoreCase("false"))) {
            return Boolean.parseBoolean(runOnlyHourly);
        } else {
            return false;
        }
    }

    public void handleWorkflowMonitoringConf(String[] args,int index) throws IOException {
        handleOradoopConf(args,index);
        Properties monitoringProperties = getPropertiesFile(path + MONITORING_PROPERTIES);
        String oozieUrl = monitoringProperties.getProperty(OOZIE_URL);
        logger.info("Oozie URL :"+oozieUrl);
        OradoopProcessArgs.getInstance().monitoringUrl(oozieUrl);
    }

    public void handleWorkflowSubmitConf(String[] args,int index) throws IOException {
        Properties monitoringProperties = getPropertiesFile(path + MONITORING_PROPERTIES);
        String oozieUrl = monitoringProperties.getProperty(OOZIE_URL);
        logger.info("Oozie URL :"+oozieUrl);
        OradoopProcessArgs.getInstance().monitoringUrl(oozieUrl);
        Properties zookeeperProperties = getPropertiesFile(path + ZOOKEEPER_PROPERTIES);
        String zookeeperHost = zookeeperProperties.getProperty(ZOOKEEPER_HOST);
        logger.info("ZOOKEEPER HOST :"+zookeeperHost);
        OradoopProcessArgs.getInstance().zooKeeperHost(zookeeperHost);
    }

    public static Properties handleDBConf() throws IOException {
        return getPropertiesFile(HADOOP_COMMON_DB_CONF + DB_CREDENTIALS_PROPERTIES);
    }

    public static Properties getPropertiesFile(String pathStr) throws IOException {
        logger.info("Going to load properties file from: " + pathStr);
        Properties properties = new Properties();
        Path path = new Path(pathStr);
        FileSystem fileSystem = path.getFileSystem(conf);
        FSDataInputStream schema = fileSystem.open(path);
        properties.load(schema);
        return properties;
    }

    private String getFileName(String[] args, int index, String fileName) {
        if(args.length > index && args[index] != null && !args[index].isEmpty() )
        {
            fileName = args[index];
            logger.info("Properties file name to use was send as argument and the file name is: " + fileName);
        }
        return fileName;
    }

}