package com.starhomemach.nbi.oradoop.export.export.QosAggregations;


import com.starhomemach.nbi.oradoop.OradoopConf;
import com.starhomemach.nbi.oradoop.export.util.HandleProcessConfiguration;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;


public class QosAggregationsExportMain
{

    public static void main(String args[]) throws Exception {

        System.out.println("################################### START ###############################################################");
        QosAggregationsExportArgumentsHolder qosAggregationsExportArgumentsHolder = QosAggregationsExportArgumentsHolder.getInstance(QosAggregationsExportArgumentsHolder.class);
        qosAggregationsExportArgumentsHolder.initArgumentsHolder(args);


        HandleProcessConfiguration procConf = new HandleProcessConfiguration();
        String propPath = procConf.handleOradoopConf(new String[]{}, 0);
        OradoopConf oradoopConf = new OradoopConf();
        oradoopConf.setRunOnlyHourly(procConf.isToRunOnlyHourlyDataProcessing(propPath));

        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(QosAggregationsExportSpringConfig.class);

        context.getBean(QosAggregationsExportHandler.class).run();

        System.out.println("########################################################## END EXPORT TO ORACLE #############################");

    }

}

