package com.starhomemach.nbi.oradoop.export.export.QosAggregations;

import com.starhomemach.nbi.oradoop.conf.arguments.ArgumentsHolder;
import com.starhomemach.nbi.oradoop.conf.arguments.IsRequired;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class QosAggregationsExportArgumentsHolder extends ArgumentsHolder
{

    @IsRequired
    private String hiveJdbcUrl;

    private String hiveJdbcDriver;
    @IsRequired
    private String hiveUserName;

    private String hivePassword;

    private Integer batchSize=10000;


}