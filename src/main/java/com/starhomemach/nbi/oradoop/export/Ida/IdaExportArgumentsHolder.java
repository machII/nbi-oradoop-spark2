package com.starhomemach.nbi.oradoop.export.Ida;

import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;

class IdaExportArgumentsHolder
{
    private static IdaExportArgumentsHolder ourInstance = new IdaExportArgumentsHolder();

    static IdaExportArgumentsHolder getInstance() { return ourInstance; }

    ArgumentParser parser;

    private String hiveJdbcUrl;
    private String hiveJdbcDriver;
    private String user;
    private String jobId;
    private String outSchema;
    private boolean debugMode = true;
    private int batchSize = 100;

    String getUser() {
        return user;
    }

    private void setUser(String user) {
        this.user = user;
    }

    String getJobId() {
        return jobId;
    }

    private void setJobId(String jobId) {
        this.jobId = jobId;
    }

    String getOutSchema() {
        return outSchema;
    }

    private void setOutSchema(String outSchema) {
        this.outSchema = outSchema;
    }

    String getHiveJdbcUrl() {
        return hiveJdbcUrl;
    }

    private void setHiveJdbcUrl(String hiveJdbcUrl) {
        this.hiveJdbcUrl = hiveJdbcUrl;
    }

    String getHiveJdbcDriver() {
        return hiveJdbcDriver;
    }

    private void setHiveJdbcDriver(String hiveJdbcDriver) {
        this.hiveJdbcDriver = hiveJdbcDriver;
    }

    boolean getDebugMode() {
        return debugMode;
    }

    private void setDebugMode(boolean debugMode) {
        this.debugMode = debugMode;
    }

    int getBatchSize() {
        return batchSize;
    }

    private void setBatchSize(int batchSize) {
        this.batchSize = batchSize;
    }


    void initExportArgumentsHolder(String[] args) {
        parser = ArgumentParsers.newArgumentParser("Ida Oradoop Export");

        buildArgumentsParser(parser);

        parseArguments(parser, args);
    }

    private void buildArgumentsParser(ArgumentParser parser) {
        parser.addArgument("--hive_jdbc_url")
                .type(String.class)
                .required(true);

        parser.addArgument("--hive_jdbc_driver")
                .type(String.class)
                .required(true);

        parser.addArgument("--user")
                .type(String.class)
                .required(true);

        parser.addArgument("--job_id")
                .type(String.class)
                .required(true);

        parser.addArgument("--out_schema")
                .type(String.class)
                .required(true);

        parser.addArgument("--debug_mode")
                .type(String.class)
                .required(false);

        parser.addArgument("--batch_size")
                .type(Integer.class)
                .required(false);
    }

    private void parseArguments(ArgumentParser parser, String[] args) {

        try {
            Namespace parser_namespace = parser.parseArgs(args);

            if (parser_namespace.get("hive_jdbc_url") != null)
                this.setHiveJdbcUrl(parser_namespace.get("hive_jdbc_url"));

            if (parser_namespace.get("hive_jdbc_driver") != null)
                this.setHiveJdbcDriver(parser_namespace.get("hive_jdbc_driver"));

            if (parser_namespace.get("user") != null)
                this.setUser(parser_namespace.get("user"));

            if (parser_namespace.get("job_id") != null)
                this.setJobId(parser_namespace.get("job_id"));

            if (parser_namespace.get("out_schema") != null)
                this.setOutSchema(parser_namespace.get("out_schema"));

            if (parser_namespace.get("batch_size") != null)
                this.setBatchSize(parser_namespace.get("batch_size"));

            if (parser_namespace.get("debug_mode") != null)
                this.setDebugMode(Boolean.parseBoolean(parser_namespace.get("debug_mode")));
        }
        catch (ArgumentParserException e) {
            System.out.println("parse Arguments failed: syntax is " + parser.toString());
            parser.handleError(e);
        }
    }

}