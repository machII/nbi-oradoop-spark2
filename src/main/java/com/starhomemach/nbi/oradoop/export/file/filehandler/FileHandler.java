package com.starhomemach.nbi.oradoop.export.file.filehandler;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

import com.starhomemach.nbi.oradoop.export.file.dto.AvroFileRecord;
import com.starhomemach.nbi.oradoop.export.file.sequence.SequenceHandler;

public abstract class FileHandler {

    public final int NO_BATCH = -1;
    private int batchRowCnt = 0;
    private int totalRowCnt = 0;


    public boolean isBatchMode()
    {
        return getBatchSize()!= NO_BATCH;
    }

    public int getBatchSize()
    {
        return NO_BATCH;
    }

    public int getCurrentBatchSize()
    {
        return batchRowCnt;
    }

    public int getTotalSize()
    {
        return totalRowCnt;
    }

    public void incrementCount()
    {
        batchRowCnt++;
        totalRowCnt++;
    }

    public void resetBatchCnt()
    {
        batchRowCnt = 0;
    }

    private SequenceHandler defaultHandler = new SequenceHandler() {
        @Override
        public Long getSequenceID(AvroFileRecord record) {
            return 1L;
        }
    };

    abstract public String[] getInsertQuery();

    abstract public String[] getMergeQuery();

    abstract public String[] getAppendQuery();

    abstract public String[] getFinalizeProcedure();

    abstract public void setPreparedStatement(AvroFileRecord avroFileRecords, Long sequenceId, PreparedStatement preparedStatement) throws SQLException, ParseException;

    abstract public List<AvroFileRecord> readFileToList(String avroFilePath) throws IOException;

    abstract public boolean isMergeOperation();

    abstract public boolean isAppendOperation();

    abstract public boolean isSequenceOperation();

    abstract public boolean useFinalizeProcedure();

    public SequenceHandler getSequenceHandler(){
        return defaultHandler;
    };

    abstract public String getHandlerKey();

}
