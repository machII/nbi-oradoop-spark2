package com.starhomemach.nbi.oradoop.export;

import com.starhomemach.nbi.oradoop.export.JobHistory.JobHistoryHandler;
import com.starhomemach.nbi.oradoop.export.ReprocessData.ReprocessMonthlyDataHandler;
import com.starhomemach.nbi.oradoop.export.util.HandleProcessConfiguration;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * Created by armb on 16-11-2016.
 */
@Service
public class ReprocessMonthlyDataMain {

    public static void main(String[] args) throws Exception {

        System.out.println("########################################################## START JOB HISTROY #############################");
        if (args.length < 5) {
            System.out.println("ERROR args should contain the following parameters 1)USER 2)JOB_NAME 3) Oradoop properties 4) From time and 5) To time");
            return;
        }

        HandleProcessConfiguration handleProcessConfiguration = new HandleProcessConfiguration();
        handleProcessConfiguration.handleOradoopConf(args, Application.INDEX_DB_PROPERTIES_FILE);
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SpringConfig.class);
        context.getBean(JobHistoryHandler.class).fetchLastRun(args[0], args[1]);

        DateFormat inputFormatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS");
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");

        String beginDate = (dateFormat.format(inputFormatter.parse(args[3]))).toString().replace("/","");
        String endDate = (dateFormat.format(inputFormatter.parse(args[4]))).toString().replace("/","");

        AnnotationConfigApplicationContext reprocessMonthlyContext = new AnnotationConfigApplicationContext(SpringConfig.class);
        reprocessMonthlyContext.getBean(ReprocessMonthlyDataHandler.class).calculateDkDatMo(beginDate,endDate);

        System.out.println("########################################################## END JOB HISTROY #############################");
    }
}
