package com.starhomemach.nbi.oradoop.export.file.filehandler;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;
import com.starhomemach.nbi.oradoop.export.file.AvroFsInput;
import com.starhomemach.nbi.oradoop.export.file.dto.AvroFileRecord;
import com.starhomemach.nbi.oradoop.export.file.dto.T_fluc_dy;

@Component
public class TFlucDyFileHandler extends QosFileHandler {

    private final Log logger = LogFactory.getLog(getClass());

    private final String INSERT_INTO_T_FLUC_DY_TMP = "insert into BI.HDP_T_FLUC_DY_TMP values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    private final String MERGE_T_FLUC_DY_WITH_T_FLUC_DY_TMP = "MERGE INTO BI.T_FLUC_DY DY\n" +
            "   USING (SELECT * FROM BI.HDP_T_FLUC_DY_TMP) temp\n" +
            "   ON (DY.DK_DAT = temp.DK_DAT AND DY.CL_ID = temp.CL_ID AND DY.DK_CMM = temp.DK_CMM AND DY.DK_ORG_SNW = temp.DK_ORG_SNW " +
            "AND DY.DK_ORG_HNW = temp.DK_ORG_HNW AND DY.DK_ORG_CNP = temp.DK_ORG_CNP AND " +
            "       DY.DK_DIR = temp.DK_DIR AND DY.DK_SPE = temp.DK_SPE AND DY.DK_PET = temp.DK_PET AND DY.DK_SGS = temp.DK_SGS AND DY.DK_SVK = temp.DK_SVK)\n" +
            "   WHEN MATCHED THEN UPDATE SET DY.LUC_SUCC = DY.LUC_SUCC + temp.LUC_SUCC , DY.LUC_ATPT = DY.LUC_ATPT + temp.LUC_ATPT ,DY.LUC_FAIL = DY.LUC_FAIL + temp.LUC_FAIL ,DY.LUC_DLY = DY.LUC_DLY + temp.LUC_DLY, DY.DK_ADT = temp.DK_ADT \n" +
            "   WHEN NOT MATCHED THEN INSERT (DY.CL_ID,DY.DK_CMM, DY.DK_ORG_SNW,DY.DK_ORG_HNW,DY.DK_ORG_CNP,DY.DK_DIR,DY.DK_ADT,DY.DK_DAT,DY.DK_SPE,DY.DK_PET,DY.DK_SGS,DY.DK_SVK,DY.LUC_ATPT,DY.LUC_SUCC,DY.LUC_FAIL,DY.LUC_DLY)\n" +
            "    VALUES  (temp.CL_ID,temp.DK_CMM, temp.DK_ORG_SNW,temp.DK_ORG_HNW,temp.DK_ORG_CNP,temp.DK_DIR,temp.DK_ADT,temp.DK_DAT,temp.DK_SPE,temp.DK_PET,temp.DK_SGS,temp.DK_SVK,temp.LUC_ATPT,temp.LUC_SUCC,temp.LUC_FAIL,temp.LUC_DLY)";


    @Override
    public String[] getInsertQuery() {
        return new String[]{INSERT_INTO_T_FLUC_DY_TMP};
    }

    @Override
    public String[] getMergeQuery() {
        return new String[]{MERGE_T_FLUC_DY_WITH_T_FLUC_DY_TMP};
    }

    @Override
    public List<AvroFileRecord> readFileToList(String avroFilePath) throws IOException {
        List<AvroFileRecord> avroRecords = Lists.newArrayList();
        Path path = new Path(avroFilePath);
        Configuration config = new Configuration();
            org.apache.avro.file.SeekableInput input = new AvroFsInput(path, config);
            org.apache.avro.io.DatumReader<T_fluc_dy> tFlucDyDatumReader = new org.apache.avro.specific.SpecificDatumReader<T_fluc_dy>(T_fluc_dy.class);
            org.apache.avro.file.FileReader<T_fluc_dy> t_fluc_dyRecords = org.apache.avro.file.DataFileReader.openReader(input, tFlucDyDatumReader);
            while (t_fluc_dyRecords.hasNext()) {
                T_fluc_dy record = t_fluc_dyRecords.next();
                avroRecords.add(record);
            }

            t_fluc_dyRecords.close();
            logger.info("succeeded to process file:" + avroFilePath + ". number of records:" + avroRecords.size());
        return avroRecords;
    }


    public void setPreparedStatement(AvroFileRecord avroFileRecords, Long sequenceId, PreparedStatement preparedStatement) throws SQLException {
        logger.info("going to set PreparedStatement for record:" + avroFileRecords);
        T_fluc_dy tFlucDyRecord = (T_fluc_dy) avroFileRecords;

        preparedStatement.setLong(1, tFlucDyRecord.getDkCmm());
        preparedStatement.setLong(2, tFlucDyRecord.getClId());
        preparedStatement.setLong(3, tFlucDyRecord.getDkDat());
        preparedStatement.setLong(4, tFlucDyRecord.getDkDir());
        preparedStatement.setLong(5, tFlucDyRecord.getDkOrgSnw());
        preparedStatement.setLong(6, tFlucDyRecord.getDkOrgHnw());
        preparedStatement.setLong(7, tFlucDyRecord.getDkOrgCnp());
        preparedStatement.setLong(8, tFlucDyRecord.getDkPet());
        preparedStatement.setLong(9, tFlucDyRecord.getDkSpe());
        preparedStatement.setLong(10, tFlucDyRecord.getDkSgs());
        preparedStatement.setLong(11, sequenceId);
        preparedStatement.setLong(12, tFlucDyRecord.getLucAtpt());
        preparedStatement.setLong(13, tFlucDyRecord.getLucSucc());
        preparedStatement.setLong(14, tFlucDyRecord.getLucFail());
        Double lucDly = tFlucDyRecord.getLucDly();
        if (lucDly == null)
            lucDly = new Double(-1);
        preparedStatement.setDouble(15, lucDly);
        preparedStatement.setLong(16, tFlucDyRecord.getDkSvk());


        preparedStatement.addBatch();

    }

    @Override
    public boolean isSequenceOperation() {
        return true;
    }

    @Override
    public String getHandlerKey() {
        return "T_FLUC_DY";
    }

    @Override
    public boolean isMergeOperation() {
        return false;
    }

}
