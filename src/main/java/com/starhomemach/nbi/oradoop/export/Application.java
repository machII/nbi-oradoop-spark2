package com.starhomemach.nbi.oradoop.export;


import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.starhomemach.nbi.oradoop.OradoopConf;
import com.starhomemach.nbi.oradoop.export.service.ProcessHdfsDirectoryService;
import com.starhomemach.nbi.oradoop.export.util.HandleProcessConfiguration;


//@ImportResource({"classpath:nbi-spring.xml"})
//@EnableAutoConfiguration


public class Application
{
    public static int INDEX_DB_PROPERTIES_FILE = 2;

    public static void main(String[] args) throws Exception {

        System.out.println("########################################################## START EXPORT TO ORACLE #############################");
        if (args.length < 2) {
            System.out.println("ERROR args should contain the following parameters 1)USER 2)SPARK-JOB-ID");
            return;
        }
        HandleProcessConfiguration procConf = new HandleProcessConfiguration();
        String propPath = procConf.handleOradoopConf(args, INDEX_DB_PROPERTIES_FILE);
        OradoopConf oradoopConf = new OradoopConf();
        oradoopConf.setRunOnlyHourly(procConf.isToRunOnlyHourlyDataProcessing(propPath));

        // prepare new list of args
        if (args.length > 2)
        {
            args[2] = propPath;
        }
        else
        {
            args = new String[]{args[0], args[1], propPath};
        }

        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SpringConfig.class);
        context.getBean(ProcessHdfsDirectoryService.class).run(args);

        System.out.println("########################################################## END EXPORT TO ORACLE #############################");

    }


}

