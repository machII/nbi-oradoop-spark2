package com.starhomemach.nbi.oradoop.export.file.filehandler;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;
import com.starhomemach.nbi.oradoop.export.file.AvroFsInput;
import com.starhomemach.nbi.oradoop.export.file.dto.AvroFileRecord;
import com.starhomemach.nbi.oradoop.export.file.dto.T_fisc_mo;

@Component
public class TfiscMoFileHandler extends QosFileHandler {

    private final Log logger = LogFactory.getLog(getClass());

    private final String INSERT_INTO_T_FISC_MO = "INSERT INTO bi.hdp_t_fisc_mo_tmp VALUES(?,?,?,?,?,?,?,?,?,?,?)";

    private final String MERGE_T_FISC_MO_WITH_T_FISC_TMP =
        "MERGE INTO bi.t_fisc_mo mo \n" +
        "  USING ( SELECT DISTINCT * FROM bi.hdp_t_fisc_mo_tmp ) temp\n" +
        "     ON ( mo.cl_id = temp.cl_id AND\n" +
        "          mo.dk_cmm = temp.dk_cmm AND\n" +
        "          mo.cfg_isc_agg_lvl_id = temp.cfg_isc_agg_lvl_id AND\n" +
        "          mo.dk_org_cy_cnp = temp.dk_org_cy_cnp AND\n" +
        "          mo.dk_org_cnp = temp.dk_org_cnp AND\n" +
        "          mo.dk_dat_mo = temp.dk_dat_mo AND\n" +
        "          mo.dk_dir = temp.dk_dir AND\n" +
        "          mo.dk_sgs = temp.dk_sgs AND\n" +
        "          mo.dk_pet_cat = temp.dk_pet_cat AND\n" +
        "          mo.dk_pet = temp.dk_pet )\n" +
        "  WHEN MATCHED THEN\n" +
        "    UPDATE SET MO.NO_IMSI = temp.NO_IMSI\n" +
        "  WHEN NOT MATCHED THEN\n" +
        "    INSERT ( cl_id, dk_cmm, cfg_isc_agg_lvl_id, dk_org_cy_cnp, dk_org_cnp,\n" +
        "             dk_dat_mo, dk_dir, dk_sgs, dk_pet_cat, no_imsi, dk_pet )\n" +
        "      VALUES ( temp.cl_id, temp.dk_cmm, temp.cfg_isc_agg_lvl_id, temp.dk_org_cy_cnp, temp.dk_org_cnp,\n" +
        "               temp.dk_dat_mo, temp.dk_dir, temp.dk_sgs, temp.dk_pet_cat, temp.no_imsi, temp.dk_pet )";


    @Override
    public String[] getInsertQuery() {
        return new String[]{INSERT_INTO_T_FISC_MO};
    }

    @Override
    public String[] getMergeQuery() {
        return new String[]{MERGE_T_FISC_MO_WITH_T_FISC_TMP};
    }

    @Override
    public List<AvroFileRecord> readFileToList(String avroFilePath) throws IOException {
        List<AvroFileRecord> avroRecords = Lists.newArrayList();
        Path path = new Path(avroFilePath);
        Configuration config = new Configuration();
        org.apache.avro.file.SeekableInput input = new AvroFsInput(path, config);
        org.apache.avro.io.DatumReader<T_fisc_mo> tFsisDatumReader = new org.apache.avro.specific.SpecificDatumReader<T_fisc_mo>(T_fisc_mo.class);
        org.apache.avro.file.FileReader<T_fisc_mo> t_fsisRecords = org.apache.avro.file.DataFileReader.openReader(input, tFsisDatumReader);
        while (t_fsisRecords.hasNext()) {
            T_fisc_mo record = t_fsisRecords.next();
            avroRecords.add(record);
        }

        t_fsisRecords.close();
        logger.info("succeeded to process file:" + avroFilePath + ". number of records:" + avroRecords.size());

        return avroRecords;
    }

    @Override
    public boolean isMergeOperation() {
        return true;
    }

    @Override
    public boolean isSequenceOperation() {
        return false;
    }

    @Override
    public String getHandlerKey() {
        return "T_FISC_MO";
    }


    public void setPreparedStatement(AvroFileRecord avroFileRecords, Long sequenceId, PreparedStatement preparedStatement) throws SQLException, ParseException {
        logger.info("going to set PreparedStatement for record:" + avroFileRecords);
        T_fisc_mo tFsisRecord = (T_fisc_mo) avroFileRecords;

        preparedStatement.setLong(1, tFsisRecord.getDkCmm());
        preparedStatement.setLong(2, tFsisRecord.getClId());
        preparedStatement.setDouble(3, tFsisRecord.getCfgIscAggLvlId());
        preparedStatement.setLong(4, tFsisRecord.getDkDatMo());
        preparedStatement.setLong(5, tFsisRecord.getNoImsi());
        preparedStatement.setLong(6, tFsisRecord.getDkDir());
        preparedStatement.setLong(7, tFsisRecord.getDkOrgCyCnp());
        preparedStatement.setLong(8, tFsisRecord.getDkOrgCnp());
        preparedStatement.setLong(9, tFsisRecord.getDkSgs());
        preparedStatement.setLong(10, tFsisRecord.getDkPetCat());
        preparedStatement.setLong(11, tFsisRecord.getDkPet());

        preparedStatement.addBatch();

    }



}
