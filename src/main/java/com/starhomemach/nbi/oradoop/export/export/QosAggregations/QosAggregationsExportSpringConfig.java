package com.starhomemach.nbi.oradoop.export.export.QosAggregations;

import com.starhomemach.nbi.oradoop.export.util.OradoopProcessArgs;
import oracle.jdbc.pool.OracleConnectionPoolDataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


@Configuration
@ComponentScan(basePackages = {
        "com.starhomemach.nbi.oradoop.export.export",
        "com.starhomemach.nbi.oradoop.export.file.filehandler",
                "com.starhomemach.nbi.oradoop.export.file.sequence"}
        )
public class QosAggregationsExportSpringConfig
{
        @Bean(name="hiveConnection")
        public Connection getHiveConnection() throws ClassNotFoundException, SQLException {
                QosAggregationsExportArgumentsHolder qosAggregationsExportArgumentsHolder = QosAggregationsExportArgumentsHolder.getInstance();
                String hiveJdbcUrl = qosAggregationsExportArgumentsHolder.getHiveJdbcUrl();
                String hiveJdbcDriver = qosAggregationsExportArgumentsHolder.getHiveJdbcDriver();
                String hiveUserName = qosAggregationsExportArgumentsHolder.getHiveUserName();
                String hivePassword = qosAggregationsExportArgumentsHolder.getHivePassword();

                Connection connection;
                System.out.println("Getting connecting: " + hiveJdbcUrl);
                if (hiveJdbcDriver != null && !hiveJdbcDriver.trim().equals("")) {
                        Class.forName(hiveJdbcDriver);
                }
                connection = DriverManager.getConnection(hiveJdbcUrl, hiveUserName, hivePassword);
                System.out.println("Success getting connecting: " + hiveJdbcUrl);

                return connection;
        }

        @Bean
        public JdbcTemplate jdbcTemplate() {
                OracleConnectionPoolDataSource oracleConnectionPoolDataSource = null;
                try {
                        oracleConnectionPoolDataSource = new OracleConnectionPoolDataSource();
                } catch (SQLException e) {
                        e.printStackTrace();
                }
                OradoopProcessArgs oradoopProcessArgs = OradoopProcessArgs.getInstance();
                oracleConnectionPoolDataSource.setURL(oradoopProcessArgs.getOracleDburl());
                oracleConnectionPoolDataSource.setUser(oradoopProcessArgs.getUser());
                oracleConnectionPoolDataSource.setPassword(oradoopProcessArgs.getPassword());
                return new JdbcTemplate(oracleConnectionPoolDataSource);
        }
}
