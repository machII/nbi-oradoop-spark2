package com.starhomemach.nbi.oradoop.export.JobHistory;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.perf4j.aop.Profiled;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.*;
import java.text.ParseException;
import java.util.Properties;

@Service
public class JobHistoryHandler {

    private final Log logger = LogFactory.getLog(getClass());

    @Autowired
    private JdbcTemplate jdbcTemplate;


    private final static String fetchDatesQuery = "select last_run , d.dk_dat as last_dk_dat, d.dk_dat_mo as last_dk_dat_mo, c.dk_dat as current_dk_dat, c.dk_dat_mo as current_dk_dat_mo " +
            "from bi.job_last_run j , bi.t_ddat d,  bi.t_ddat c "+
            "where to_date(j.last_run) = d.dat " +
            "and c.dat = to_date(SYSDATE) " +
            "and job_name=?" +
            "and user_name=?";

    private final static String fetchOldDateQuery = "select d.dk_dat as last_dk_dat, d.dk_dat_mo as last_dk_dat_mo, c.dk_dat as current_dk_dat,c.dk_dat_mo as current_dk_dat_mo " +
            " from bi.t_ddat d,  bi.t_ddat c " +
            " where d.dat =  to_date(SYSDATE-1)" +
            " and c.dat = to_date(SYSDATE)";

    private final static String updateJobQuery = "update bi.job_last_run set last_run =  ? , mod_tmst = SYSDATE where job_name = ? and user_name = ?";

    private final static String insertOldRow = "INSERT INTO bi.job_last_run (job_name,user_name,last_run, mod_tmst) VALUES (?,?,to_date(SYSDATE-100) ,SYSDATE)";

    public static long dkDat = 0;

    public static long dkDatMo = 0;

    public void setDkDat(long dkDat)
    {
        this.dkDat = dkDat;
    }

    public long getDkDat()
    {
        return dkDat;
    }

    public void setDkDatMo(long dkDatMo)
    {
        this.dkDatMo = dkDatMo;
    }

    public long getDkDatMo()
    {
        return dkDatMo;
    }

    @Profiled
    public void updateLastRun(String userName, String jobName, String lastRun) throws SQLException, DataAccessException, ParseException {
        lastRun = lastRun.substring(0,10);
        System.out.println("going to update last run for: user:" + userName + " job: " + jobName + " date " + lastRun);
        Connection connection = jdbcTemplate.getDataSource().getConnection();
        connection.setAutoCommit(false);
        PreparedStatement ps = connection.prepareStatement(updateJobQuery);
        ps.setDate(1, Date.valueOf(lastRun));
        ps.setString(2, jobName);
        ps.setString(3, userName);

        ps.executeUpdate();
        connection.commit();
    }

    @Profiled
    public void fetchLastRun(String userName, String jobName) throws SQLException, DataAccessException,
            ParseException {
        logger.info("going to fetch last run");
        Connection connection = jdbcTemplate.getDataSource().getConnection();
        PreparedStatement ps = connection.prepareStatement(fetchDatesQuery);
        ps.setString(1, jobName);
        ps.setString(2, userName);
        ResultSet rs = ps.executeQuery();
        if (rs.next()) {
            System.out.println("fetch Last Run: last time == " + rs.getString("last_run") + ", last DK time ==" + rs.getString("last_dk_dat") + ", " +
                    "current DK time ==" + rs.getString("current_dk_dat"));
            saveResultToOozieProperty(rs);
        }
        else {
            logger.info("there is not last run for user:" + userName + " and Job: " + jobName + " going to fetch current dates");
            ps = connection.prepareStatement(fetchOldDateQuery);
            rs = ps.executeQuery();
            if (rs.next()) {
                System.out.println("fetch old Dates 1 days before: - " +
                        "last DK time ==" + rs.getString("last_dk_dat") + ", " +
                        "current DK time ==" + rs.getString("current_dk_dat"));
                saveResultToOozieProperty(rs);
            }
            logger.info("going to insert job last run for 1 days older");
            ps = connection.prepareStatement(insertOldRow);
            ps.setString(1, jobName);
            ps.setString(2, userName);
            ps.executeUpdate();
            connection.commit();
        }
    }

    private void saveResultToOozieProperty(ResultSet rs) throws SQLException {

        String filePath = System.getProperty("oozie.action.output.properties");
        try {
            if (filePath != null) {
                OutputStream outputStream = new FileOutputStream(filePath);
                Properties p = new Properties();
                p.setProperty("last_dk_dat", "" + rs.getString("last_dk_dat"));
                setDkDat(Long.valueOf(rs.getString("last_dk_dat")));
                p.setProperty("current_dk_dat", "" + rs.getString("current_dk_dat"));
                p.setProperty("last_dk_dat_mo", "" + rs.getString("last_dk_dat_mo"));
                setDkDatMo(Long.valueOf(rs.getString("last_dk_dat_mo")));
                p.setProperty("current_dk_dat_mo", "" + rs.getString("current_dk_dat_mo"));
                p.store(outputStream, "");
                outputStream.close();
            } else {
                throw new RuntimeException("oozie.action.output.properties System property not defined");
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
