package com.starhomemach.nbi.oradoop.export.util;

public class OradoopProcessArgs {

    private String oracleDbUrl;
    private String monitoringUrl;
    private String zookeperHost;
    private String user;
    private String password;
    private String hadoopNameNode;


    private OradoopProcessArgs() {}

    private static class ArgsHolder {
        private static final OradoopProcessArgs INSTANCE = new OradoopProcessArgs();
    }

    public static OradoopProcessArgs getInstance() {
        return ArgsHolder.INSTANCE;
    }

    public String getOracleDburl() {
        return oracleDbUrl;
    }


    public String getMonitoringUrl() {
        return monitoringUrl;
    }

    public String getZooKeeperHost() {
        return zookeperHost;
    }

    public OradoopProcessArgs oracleDbUrl(String url) {
        this.oracleDbUrl = url;
        return this;
    }

    public OradoopProcessArgs monitoringUrl(String url) {
        this.monitoringUrl = url;
        return this;
    }

    public OradoopProcessArgs zooKeeperHost(String url) {
        this.zookeperHost = url;
        return this;
    }

    public String getUser() {
        return user;
    }

    public OradoopProcessArgs user(String user) {
        this.user = user;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public OradoopProcessArgs password(String password) {
        this.password = password;
        return this;
    }


    public String getHadoopNameNode() {
        return hadoopNameNode;
    }

    public OradoopProcessArgs hadoopNameNode(String hadoopNameNode) {
        this.hadoopNameNode = hadoopNameNode;
        return this;
    }



}
