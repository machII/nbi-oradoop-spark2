package com.starhomemach.nbi.oradoop.export;


import com.starhomemach.nbi.oradoop.export.JobHistory.JobHistoryHandler;
import com.starhomemach.nbi.oradoop.export.util.HandleProcessConfiguration;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class FetchJobHistoryMain {

    public static void main(String[] args) throws Exception {

        System.out.println("########################################################## START JOB HISTROY #############################");
        if (args.length < 2) {
            System.out.println("ERROR args should contain the following parameters 1)USER 2)JOB_NAME");
            return;
        }
        new HandleProcessConfiguration().handleOradoopConf(args,Application.INDEX_DB_PROPERTIES_FILE);
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SpringConfig.class);
        context.getBean(JobHistoryHandler.class).fetchLastRun(args[0],args[1]);

        System.out.println("########################################################## END JOB HISTROY #############################");

    }

}

