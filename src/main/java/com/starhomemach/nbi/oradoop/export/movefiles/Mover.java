package com.starhomemach.nbi.oradoop.export.movefiles;


import com.starhomemach.nbi.oradoop.OradoopConf;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.math3.util.Pair;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

public class Mover {


    private final Log logger = LogFactory.getLog(getClass());
    public String incomingPath="incoming";
    public String processingPath="processing";

    public Mover() {

    }

    public void setIncomingPath(String incoming)
    {
        incomingPath=incoming;
    }

    public void setProcessingPath(String processing)
    {
        processingPath=processing;
    }

    public static void printUsage() {
        System.out.println("Usage: Mover" + "<user_code> <workflow_id> <number of files[0=unlimited]>");
    }

    public String getIncomingPath(String userCode) {
        String IncomingPath = "/starhome/qos/user/" + userCode + "/etl/spark/" + incomingPath;
        return IncomingPath;
    }

    public String getProcessingPath(String userCode, String workflowId) {
        String ProcessingPath = "/starhome/qos/user/" + userCode + "/etl/spark/" + processingPath + "/" + workflowId + "/";
        return ProcessingPath;
    }


    public int getNumFilesToMove(int fileListSize, int maxNumFiles)
    {
        int  fileToMove=0;
        if ((fileListSize < maxNumFiles & fileListSize >= 1) || (maxNumFiles==0)){
            fileToMove = fileListSize;
            logger.info("number of files is smaller then " + maxNumFiles + ", move " + fileToMove + "files");

        } else if (fileListSize >= maxNumFiles) {
            logger.info("number of files is bigger then " + maxNumFiles + ", move " + maxNumFiles + " files");
            fileToMove = maxNumFiles;
        } else if (fileListSize < 1) {
            logger.info("no files");
            fileToMove = 0;
        }

        return fileToMove;
    }

    public void checkAndMove(String userCode, String workflowId, int maxNumFiles) throws IOException {

        String source = getIncomingPath(userCode);
        String target = getProcessingPath(userCode, workflowId);
        Integer fileToMove = 0;

        Configuration conf = new OradoopConf().getConf();
        FileSystem fileSystem = FileSystem.newInstance(conf);
        Path srcPath = new Path(source);

        List<String> fileList;
        fileList = getAllFilePath(srcPath, fileSystem);


        //remove files in transition (*.tmp) from the list
        CharSequence cs1 = ".tmp";
        for (Iterator<String> iter = fileList.listIterator(); iter.hasNext(); ) {
            String a = iter.next();
            if (a.contains(cs1)) {
                iter.remove();
            }
        }


        Integer fileListSize = fileList.size();

        List<Pair> oozieParameters = new ArrayList<>();

        //TODO handle zero files
        fileToMove = getNumFilesToMove(fileListSize,maxNumFiles);


        for (int i = 0; i < fileToMove; i++) {
            logger.info("moving file: " + fileList.get(i));

            Path fromPath = new Path(source + "/" + fileList.get(i));
            Path toPath = new Path(target + "/" + fileList.get(i));

            try {
                boolean isRenamed = fileSystem.rename(fromPath, toPath);
                if (isRenamed) {
                    logger.info("Moved from " + fromPath + " to " + toPath);
                }
            } catch (Exception e) {
                logger.info("Exception :" + e);
                fileSystem.close();
                System.exit(1);
            }
        }

        //adding oozie parameter to list
        Pair<String, String> numOfFiles = new Pair<>("num_of_files",String.valueOf(fileToMove));
        Pair<String, String> backlog = new Pair<>("backlog_of_files",String.valueOf(fileListSize-fileToMove));
        oozieParameters.add(numOfFiles);
        oozieParameters.add(backlog);

        saveResultToOozieProperty(oozieParameters);

        fileSystem.close();

    }

    public static void main(String[] args) throws IOException {

        System.out.println("########################################################## START MOVE WORKFLOW FILE #############################");
        if (args.length > 5 || args.length < 3) {
            printUsage();
            System.exit(1);
        }

        Mover client = new Mover();

        if (args.length >= 4) {
            client.setIncomingPath(args[3]);
        }

        if (args.length == 5) {
            client.setProcessingPath(args[4]);
        }

        client.checkAndMove(args[0], args[1], Integer.parseInt(args[2]));

        System.out.println("########################################################## END MOVE WORKFLOW FILE #############################");

    }


    //TODO need to merge to one method with the same method in class JobHistoryHandler
    private void saveResultToOozieProperty(List<Pair> params) {

        String filePath = System.getProperty("oozie.action.output.properties");
        try {
            if (filePath != null) {
                OutputStream outputStream = new FileOutputStream(filePath);
                Properties p = new Properties();

                for (int i = 0; i < params.size(); i++) {
                    p.setProperty(params.get(i).getKey().toString(),params.get(i).getValue().toString());
                }

                p.store(outputStream, "");
                outputStream.close();
            } else {
                throw new RuntimeException("oozie.action.output.properties System property not defined");
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static List<String> getAllFilePath(Path filePath, FileSystem fs) throws FileNotFoundException, IOException {
        String fileName;
        String fullPath;
        List<String> fileList = new ArrayList<String>();
        FileStatus[] fileStatus = fs.listStatus(filePath);
        for (FileStatus fileStat : fileStatus) {
            fullPath = fileStat.getPath().toString();
            fileName = fullPath.substring(fullPath.lastIndexOf('/') + 1, fullPath.length());
            fileList.add(fileName);
        }
        return fileList;
    }
}