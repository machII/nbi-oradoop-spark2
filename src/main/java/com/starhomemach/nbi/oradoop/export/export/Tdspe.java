package com.starhomemach.nbi.oradoop.export.export;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

@Component
public class Tdspe {

    public static final String SELECT_DK_SPE = "select DK_SPE from BI.T_DSPE where SPE_CAT = ? AND SPE_CD = ?";


    @Autowired
    private JdbcTemplate jdbcTemplate;


    private final Log logger = LogFactory.getLog(getClass());

    public Long getTdspe(String SPE_CAT, String SPE_CD) throws DataAccessException {
        List<Long> longs = jdbcTemplate.queryForList(SELECT_DK_SPE, new Object[]{SPE_CAT, SPE_CD}, Long.class);
        if(longs.size() == 1)
            return longs.get(0);
        return -1l;
    }

}
