package com.starhomemach.nbi.oradoop.export.export;

import com.google.common.collect.Maps;
import com.starhomemach.nbi.oradoop.export.export.QosAggregations.QosAggregationsExportArgumentsHolder;
import com.starhomemach.nbi.oradoop.export.export.QosAggregations.QosAggregationsFileHandler;
import com.starhomemach.nbi.oradoop.export.file.dto.AvroFileRecord;
import com.starhomemach.nbi.oradoop.export.file.filehandler.FileHandler;
import com.starhomemach.nbi.oradoop.export.file.sequence.QOSSequenceHandler;
import org.perf4j.aop.Profiled;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.*;

@Component
public class MergeRecords {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public void mergeToOracleFromRs(Map<FileHandler, ResultSet> resultSets) throws SQLException, DataAccessException, ParseException {
        System.out.println("Going merge data to oracle...");

        Connection oracleConnection =jdbcTemplate.getDataSource().getConnection();
        try {

            oracleConnection.setAutoCommit(false);
            Map<FileHandler, List<PreparedStatement>> fileToPreparedStatements = Maps.newHashMap();

            Set<FileHandler> fileHandlers = resultSets.keySet();
            for (FileHandler fileHandler : fileHandlers) {

                ResultSet records = resultSets.get(fileHandler);

                while (records.next())
                {
                    long sequenceId = 1L;

                    if (fileHandler.isSequenceOperation()) {
                        //System.out.println("Using Sequencer " + fileHandler.getSequenceHandler().toString());
                        sequenceId = ((QOSSequenceHandler)fileHandler.getSequenceHandler()).getSequenceID(records.getLong("cl_id"),records.getLong("dk_cmm"));
                    }
                    List<PreparedStatement> stmtLst = getPreparedStatement(oracleConnection, fileHandler, fileToPreparedStatements);

                    for(PreparedStatement preparedStatement : stmtLst) {
                        show_record(fileHandler.getHandlerKey(),records);

                        ((QosAggregationsFileHandler) fileHandler).setPreparedStatement(records, sequenceId, preparedStatement);
                    }

                    // Added the batch functionality for big chunk of loading
                    fileHandler.incrementCount();

                    if (fileHandler.isBatchMode() && fileHandler.getCurrentBatchSize() >= fileHandler.getBatchSize())
                    {
                        for(PreparedStatement preparedStatement : stmtLst)
                        {
                            preparedStatement.executeBatch();
                            preparedStatement.clearBatch();
                        }

                        System.out.println("finished to insert into table for handler type:" + fileHandler + "  In Batch mode since reached limit of " + fileHandler.getBatchSize());
                        fileHandler.resetBatchCnt();
                    }
                }
            }
            executePreparedStatements(fileToPreparedStatements);

            System.out.println("going to run sql commit for all changes");
            oracleConnection.commit();

            ClosePreparedStatements(fileToPreparedStatements);
            System.out.println("Finished to merge to oracle ");
        }
        catch (Exception e)
        {
            if (oracleConnection != null){
                oracleConnection.rollback();
            }
            throw e;
        }
        finally {
            if (oracleConnection != null)
                oracleConnection.close();
        }
    }

    @Profiled
    public void mergeToOracle(Map<FileHandler, List<AvroFileRecord>> fileToAvroFiles) throws SQLException, DataAccessException, ParseException {
        System.out.println("going merge data to oracle");
        Connection connection = null;
        try {
            connection = jdbcTemplate.getDataSource().getConnection();
            connection.setAutoCommit(false);
            Map<FileHandler, List<PreparedStatement>> fileToPreparedStatements = Maps.newHashMap();

            Set<FileHandler> fileHandlers = fileToAvroFiles.keySet();
            for (FileHandler fileHandler : fileHandlers) {
                System.out.println("handling avro files with " + fileHandler.toString());
                List<AvroFileRecord> avroFileRecords = fileToAvroFiles.get(fileHandler);
                System.out.println("Amount of records " + avroFileRecords.size());
                for (AvroFileRecord avroFileRecord : avroFileRecords) {
                    handleAvroFile(connection, fileHandler, fileToPreparedStatements, avroFileRecord);
                }
            }
            executePreparedStatements(fileToPreparedStatements);
            System.out.println("going to run sql commit for all changes");
            connection.commit();

            ClosePreparedStatements(fileToPreparedStatements);
            System.out.println("Finished to merge to oracle ");
        }
        catch (Exception e)
        {
            if (connection != null){
                connection.rollback();
            }
            throw e;
        }
        finally {
            if (connection != null)
                connection.close();
        }
    }

    private void ClosePreparedStatements(Map<FileHandler, List<PreparedStatement>> fileToPreparedStatements) throws SQLException {
        Collection<List<PreparedStatement>> preparedStatements = fileToPreparedStatements.values();
        for (List<PreparedStatement> psList : preparedStatements) {
            for(PreparedStatement ps :psList)
                ps.close();
        }
    }


    public void executePreparedStatements(Map<FileHandler, List<PreparedStatement>> fileToPreparedStatements) throws SQLException {

        Set<FileHandler> fileHandlers = fileToPreparedStatements.keySet();
        for (FileHandler fileHandler : fileHandlers) {
            System.out.println("going to execute batch for preparedStatement for handler:" + fileHandler.toString());
            executePreparedStatements(fileHandler, fileToPreparedStatements.get(fileHandler));
        }
    }

    public void executePreparedStatements(FileHandler fileHandler, List<PreparedStatement> fileToPreparedStatement) throws SQLException {

        if (fileHandler.getCurrentBatchSize() > 0) {
            for (PreparedStatement stmt : fileToPreparedStatement) {
                stmt.executeBatch();
                stmt.clearBatch();
            }
            System.out.println("finished to insert into table for handler type:" + fileHandler );
        }
        else
            System.out.println("insert into table was skipped for handler type:" + fileHandler + "  Since no rows were loaded");

        System.out.println("inserted total of " + fileHandler.getTotalSize() + "  for handler type:" + fileHandler);

        if (fileHandler.isMergeOperation()) {
            for  (String mergeSql : fileHandler.getMergeQuery()) {
                System.out.println("about to run merge query: " + mergeSql + " for handler " + fileHandler);
                int mergeExecute = fileToPreparedStatement.get(0).executeUpdate(mergeSql);
                System.out.println("finished to merge into table merge execute result:" + mergeExecute);
            }
        }
        else if (fileHandler.isAppendOperation()) {
            for  (String appendSql : fileHandler.getAppendQuery()) {
                System.out.println("about to run append query: " + appendSql + " for handler " + fileHandler);
                int appendExecute = fileToPreparedStatement.get(0).executeUpdate(appendSql);
                System.out.println("finished to insert into table insert execute result:" + appendExecute);
            }
        }

        if (fileHandler.useFinalizeProcedure()) {
            for  (String finalizeProcedureSql : fileHandler.getFinalizeProcedure()) {
                System.out.println("about to run finalize procedure: " + finalizeProcedureSql + " for handler " + fileHandler);
                PreparedStatement prep = fileToPreparedStatement.get(0);
                int finalizeExecute = prep.executeUpdate(finalizeProcedureSql);
                System.out.println("executed finalize procedure" );
            }
        }
    }


    public void handleAvroFile(Connection connection, FileHandler fileHandler, Map<FileHandler, List<PreparedStatement>> fileToPreparedStatements, AvroFileRecord avroFileRecord) throws SQLException, DataAccessException, ParseException {
        long sequenceId = 1L;

        if (fileHandler.isSequenceOperation()) {
            System.out.println("Using Sequencer " + fileHandler.getSequenceHandler().toString());
            sequenceId = fileHandler.getSequenceHandler().getSequenceID(avroFileRecord);
        }
        List<PreparedStatement> stmtLst = getPreparedStatement(connection, fileHandler, fileToPreparedStatements);

        for(PreparedStatement preparedStatement : stmtLst)
            fileHandler.setPreparedStatement(avroFileRecord, sequenceId, preparedStatement);

        // Added the batch functionality for big chunk of loading
        fileHandler.incrementCount();

        if (fileHandler.isBatchMode() && fileHandler.getCurrentBatchSize() >= fileHandler.getBatchSize())
        {
            for(PreparedStatement preparedStatement : stmtLst)
            {
                preparedStatement.executeBatch();
                preparedStatement.clearBatch();
            }

            System.out.println("finished to insert into table for handler type:" + fileHandler + "  In Batch mode since reached limit of " + fileHandler.getBatchSize());
            fileHandler.resetBatchCnt();
        }
    }

    public List<PreparedStatement> getPreparedStatement(Connection connection, FileHandler fileHandler, Map<FileHandler, List<PreparedStatement>> fileToPreparedStatements) throws SQLException {
        if (!fileToPreparedStatements.containsKey(fileHandler)) {
            List<PreparedStatement> stmtLst = new ArrayList<PreparedStatement>(fileHandler.getInsertQuery().length);
            for(String sql : fileHandler.getInsertQuery()) {

                System.out.println("Prepare stmt : " + sql + " for handler type:" + fileHandler);
                stmtLst.add(connection.prepareStatement(sql));
            }
            fileToPreparedStatements.put(fileHandler, stmtLst);
        }
        return fileToPreparedStatements.get(fileHandler);
    }

    public static void show_record(String handlerName, ResultSet record) {
        if(QosAggregationsExportArgumentsHolder.getInstance().getDebugMode()) {
            System.out.println("Preparing record for handler:" + handlerName);
            try {
                for (int i = 1; i <= record.getMetaData().getColumnCount(); i++) {
                    System.out.print(record.getMetaData().getColumnName(i) + ":" + record.getString(record.getMetaData().getColumnName(i)) + "| ");
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            System.out.println();
        }
    }
}
