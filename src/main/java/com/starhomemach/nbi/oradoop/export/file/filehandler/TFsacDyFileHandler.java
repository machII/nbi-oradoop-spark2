package com.starhomemach.nbi.oradoop.export.file.filehandler;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.apache.avro.file.DataFileReader;
import org.apache.avro.file.SeekableInput;
import org.apache.avro.io.DatumReader;
import org.apache.avro.specific.SpecificDatumReader;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;
import com.starhomemach.nbi.oradoop.export.file.AvroFsInput;
import com.starhomemach.nbi.oradoop.export.file.dto.AvroFileRecord;
import com.starhomemach.nbi.oradoop.export.file.dto.T_fsac_dy;

@Component
public class TFsacDyFileHandler extends QosFileHandler {

    private final Log logger = LogFactory.getLog(getClass());

    private final String INSERT_INTO_T_FSAC_DY_TMP = "insert into BI.HDP_T_FSAC_DY_TMP values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

    private final String MERGE_T_FSAC_DY_WITH_T_FSAC_DY_TMP = "MERGE INTO BI.T_FSAC_DY DY\n" +
            "USING (SELECT * FROM BI.HDP_T_FSAC_DY_TMP) temp \n" +
            " ON (DY.CL_ID = temp.CL_ID AND DY.DK_CMM = temp.DK_CMM AND DY.DK_ORG_SNW = temp.DK_ORG_SNW AND DY.DK_ORG_HNW = temp" +
            ".DK_ORG_HNW AND DY.DK_ORG_CNP = temp.DK_ORG_CNP AND DY.DK_DIR = temp.DK_DIR AND DY.DK_DAT = temp.DK_DAT AND DY.DK_SPE = temp.DK_SPE AND DY.DK_PET = temp.DK_PET AND DY.DK_SGS = temp.DK_SGS AND DY.DK_SSC = temp.DK_SSC)\n" +
            " WHEN MATCHED THEN UPDATE SET DY.SAC_SUCC = DY.SAC_SUCC + temp.SAC_SUCC , DY.SAC_ATPT = DY.SAC_ATPT + temp.SAC_ATPT ,DY.SAC_FAIL = DY.SAC_FAIL + temp.SAC_FAIL ,DY.SAC_DLY = DY.SAC_DLY + temp.SAC_DLY, DY.DK_ADT = temp.DK_ADT\n" +
            " WHEN NOT MATCHED THEN INSERT (DY.DK_CMM,DY.CL_ID, DY.DK_ORG_SNW, DY.DK_ORG_HNW, DY.DK_ORG_CNP, DY.DK_DIR,DY.DK_ADT," +
            " DY.DK_DAT, DY.DK_SPE, DY.DK_PET, DY.DK_SGS, DY.SAC_ATPT, DY.SAC_SUCC, DY.SAC_FAIL, DY.SAC_DLY, DY.DK_SSC)\n" +
            " VALUES  (\n" +
            " temp.DK_CMM,\n" +
            " temp.CL_ID, \n" +
            " temp.DK_ORG_SNW,\n" +
            " temp.DK_ORG_HNW,\n" +
            " temp.DK_ORG_CNP,\n" +
            " temp.DK_DIR,\n" +
            " temp.DK_ADT,\n" +
            " temp.DK_DAT,\n" +
            " temp.DK_SPE,\n" +      
            " temp.DK_PET,\n" +
            " temp.DK_SGS,\n" +
            " temp.SAC_ATPT,\n" +
            " temp.SAC_SUCC,\n" +
            " temp.SAC_FAIL,\n" +
            " temp.SAC_DLY,\n" +         
            " temp.DK_SSC)";

    @Override
    public String[] getInsertQuery() {
        return new String[]{INSERT_INTO_T_FSAC_DY_TMP};
    }

    @Override
    public String[] getMergeQuery() {
        return new String[]{MERGE_T_FSAC_DY_WITH_T_FSAC_DY_TMP};
    }

    @Override
    public void setPreparedStatement(AvroFileRecord avroFileRecords, Long sequenceId, PreparedStatement preparedStatement) throws SQLException {

        logger.info("going to set PreparedStatement for record:" + avroFileRecords);
        T_fsac_dy tFsacDyRecord = (T_fsac_dy) avroFileRecords;


        preparedStatement.setLong(1, tFsacDyRecord.getDkCmm());
        preparedStatement.setLong(2, tFsacDyRecord.getClId());
        preparedStatement.setLong(3, tFsacDyRecord.getDkDat());
        preparedStatement.setLong(4, tFsacDyRecord.getDkDir());
        preparedStatement.setLong(5, tFsacDyRecord.getDkOrgSnw());
        preparedStatement.setLong(6, tFsacDyRecord.getDkOrgHnw());
        preparedStatement.setLong(7, tFsacDyRecord.getDkOrgCnp());
        preparedStatement.setLong(8, tFsacDyRecord.getDkPet());
        preparedStatement.setLong(9, tFsacDyRecord.getDkSpe());
        preparedStatement.setLong(10, tFsacDyRecord.getDkSgs());
        preparedStatement.setLong(11, tFsacDyRecord.getDkSsc());
        preparedStatement.setLong(12, sequenceId);
        preparedStatement.setLong(13, tFsacDyRecord.getSacAtpt());
        preparedStatement.setLong(14, tFsacDyRecord.getSacSucc());
        preparedStatement.setLong(15, tFsacDyRecord.getSacFail());
        Double sacDly = tFsacDyRecord.getSacDly();
        if (sacDly == null)
            sacDly = new Double(-1);
        preparedStatement.setDouble(16, sacDly);
        preparedStatement.addBatch();
    }

    @Override
    public List<AvroFileRecord> readFileToList(String avroFilePath) throws IOException {
        List<AvroFileRecord> avroRecords = Lists.newArrayList();
        Path path = new Path(avroFilePath);
        Configuration config = new Configuration();
        SeekableInput input = new AvroFsInput(path, config);
        DatumReader<T_fsac_dy> tFsacDyDatumReader = new SpecificDatumReader<T_fsac_dy>(T_fsac_dy.class);
        org.apache.avro.file.FileReader<T_fsac_dy> t_fsac_dyRecords = DataFileReader.openReader(input, tFsacDyDatumReader);
        while (t_fsac_dyRecords.hasNext()) {
            T_fsac_dy record = t_fsac_dyRecords.next();
            avroRecords.add(record);
        }
        t_fsac_dyRecords.close();
        logger.info("succeeded to process file:" + avroFilePath + ". number of records:" + avroRecords.size());
        return avroRecords;
    }

    @Override
    public boolean isSequenceOperation() {
        return true;
    }

    @Override
    public String getHandlerKey() {
        return "T_FSAC_DY";
    }

    @Override
    public boolean isMergeOperation() {
        return false;
    }
}
