package com.starhomemach.nbi.oradoop.export.export;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.stereotype.Component;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;


@Component
public class Tdsgl {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public Long insertTdsglRow(Long clientId,Long groupId,Long sequenceId,Timestamp minEventTime,Timestamp maxEventTime,SignalingProtocolCode signalingProtocolCode,SignallingData signallingData)
    {
        Long sequenceNextVal = jdbcTemplate.queryForObject("  select  SGL_LNDG.Q_SGL_ID.NEXTVAL from dual", Long.class);


        jdbcTemplate.update("insert into  BI.T_DSGL values(?,?,?,?,?,?,?,?,?,?,?,?)",new PreparedStatementSetter() {
            public void setValues(PreparedStatement ps) throws SQLException {
                Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                ps.setLong(1, groupId);
                ps.setLong(2, clientId);
                ps.setLong(3, sequenceNextVal);
                ps.setString(4, signalingProtocolCode.toString());
                ps.setString(5,signallingData.toString());
                ps.setTimestamp(6, minEventTime);
                ps.setTimestamp(7,maxEventTime);
                ps.setString(7, "N");
                ps.setTimestamp(8,timestamp );
                ps.setTimestamp(9,timestamp);
                ps.setLong(10, sequenceId);
                ps.setTimestamp(11,timestamp);
            }
        });


        return sequenceNextVal;

    }

}
