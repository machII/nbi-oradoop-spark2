package com.starhomemach.nbi.oradoop.export;

import com.starhomemach.nbi.oradoop.export.util.OradoopProcessArgs;
import oracle.jdbc.pool.OracleConnectionPoolDataSource;
import org.perf4j.commonslog.aop.TimingAspect;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.jdbc.core.JdbcTemplate;

import java.sql.SQLException;
import java.time.LocalDateTime;


@Configuration
@ComponentScan(basePackages = {"com.starhomemach.nbi.oradoop"}, excludeFilters = {
        @ComponentScan.Filter(type=FilterType.REGEX,pattern ="com.starhomemach.nbi.oradoop.export.export.QosAggregations.*"),
        @ComponentScan.Filter(type=FilterType.REGEX,pattern = "com.starhomemach.nbi.oradoop.util.spark.*"),
        @ComponentScan.Filter(type=FilterType.REGEX,pattern = "com.starhomemach.nbi.oradoop.util.genericexport.*"),
        @ComponentScan.Filter(type=FilterType.REGEX,pattern = "com.starhomemach.nbi.oradoop.lockmanager.*")
})
public class SpringConfig {

    @Bean
    public TimingAspect timingAspect() {
        return new TimingAspect();
    }


    @Bean
    @Qualifier("oracleConnectionPoolDataSource")
    public OracleConnectionPoolDataSource oracleConnectionPoolDataSource() {

        OracleConnectionPoolDataSource oracleConnectionPoolDataSource = null;
        try {
            oracleConnectionPoolDataSource = new OracleConnectionPoolDataSource();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        OradoopProcessArgs oradoopProcessArgs = OradoopProcessArgs.getInstance();
        oracleConnectionPoolDataSource.setURL(oradoopProcessArgs.getOracleDburl());
        oracleConnectionPoolDataSource.setUser(oradoopProcessArgs.getUser());
        oracleConnectionPoolDataSource.setPassword(oradoopProcessArgs.getPassword());
        return oracleConnectionPoolDataSource;
    }

    @Bean
    public JdbcTemplate jdbcTemplate() {
        return new JdbcTemplate(oracleConnectionPoolDataSource());
    }

    @Bean
    public LocalDateTime etlStartTime() {
        return LocalDateTime.now();
    }
}
