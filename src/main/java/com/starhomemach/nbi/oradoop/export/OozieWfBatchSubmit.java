package com.starhomemach.nbi.oradoop.export;

import com.starhomemach.nbi.oradoop.lockmanager.DistributedLock;
import com.starhomemach.nbi.oradoop.export.util.HandleProcessConfiguration;
import com.starhomemach.nbi.oradoop.export.util.OradoopProcessArgs;
import org.apache.oozie.client.OozieClient;
import org.apache.oozie.client.OozieClientException;
import org.apache.oozie.client.WorkflowJob;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.ZooKeeper;

import java.util.List;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;


/**
 * Created by tpin on 18/10/2016.
 */
public class OozieWfBatchSubmit {
    public static void main(String[] args) throws Exception {

        System.out.println("########################################################## START OOZIE WORKFLOW BATCH SUBMITTER #############################");
        HandleProcessConfiguration oradoopConf = new HandleProcessConfiguration();
        /*Configuration conf1 = new Configuration();
        conf1.set("fs.defaultFS", "hdfs://10.135.11.24:8020");
        conf1.set("fs.hdfs.impl", "org.apache.hadoop.hdfs.DistributedFileSystem");
        System.setProperty("HADOOP_USER_NAME", "telefonica");
        oradoopConf.setConf(conf1);*/
        oradoopConf.handleWorkflowSubmitConf(args,4);
        String jobId = null;

        OozieClient wc = new OozieClient(OradoopProcessArgs.getInstance().getMonitoringUrl());
        // create a workflow job configuration and set the workflow application path
        Properties conf = oradoopConf.getPropertiesFile(args[2]+"/job.properties");

        // setting workflow parameters
        conf.setProperty("parentWfID", args[0]);
        conf.setProperty("user.name",args[1]);
        conf.setProperty(OozieClient.APP_PATH, args[2]);
        Boolean allowConcurrent = Boolean.valueOf(args[3]);

        String[] paths = args[2].split("/");

        if (!allowConcurrent) {
            final CountDownLatch connectedSignal = new CountDownLatch(1);
            ZooKeeper zoo = new ZooKeeper(OradoopProcessArgs.getInstance().getZooKeeperHost() + "/lock_node/"+args[1], 5000, new Watcher() {

                public void process(WatchedEvent we) {

                    if (we.getState() == Event.KeeperState.SyncConnected) {
                        connectedSignal.countDown();
                    }
                }
            });

            connectedSignal.await();
            System.out.println("Before creating lock");
            DistributedLock distributedLock = new DistributedLock(zoo,"/" + paths[paths.length-1], "lock-" );
            System.out.println("Trying to acquire lock on /" + paths[paths.length-1]);
            distributedLock.lock();
            System.out.println("Locked");

            while(isAppPathRunningAlready(wc, args[2], args[1]))
                Thread.sleep(10*1000);

            jobId = wc.run(conf);
            System.out.println("Submitted");
            distributedLock.unlock();
            System.out.println("UnLocked");
            zoo.close();
        }
        else
            jobId = wc.run(conf);

        System.out.println("Workflow job submitted, ID: " + jobId);
        System.out.println("########################################################## END OOZIE WORKFLOW BATCH SUBMITTER #############################");
    }

    private static boolean isAppPathRunningAlready(OozieClient oc, String appPath, String user) throws OozieClientException {
        List<WorkflowJob> wfList = oc.getJobsInfo(OozieClient.FILTER_STATUS + "=" + WorkflowJob.Status.RUNNING + ";" +
                                                  OozieClient.FILTER_STATUS + "=" + WorkflowJob.Status.PREP  + ";" +
                                                  OozieClient.FILTER_STATUS + "=" + WorkflowJob.Status.SUSPENDED + ";" +
                                                  OozieClient.FILTER_USER + "=" + user);

        for (WorkflowJob job : wfList)
        {
            System.out.println("Checking job ID:" + job.getId());
            String jobPath = oc.getJobInfo(job.getId()).getAppPath();
            System.out.println("Path to check is:" + jobPath);
            System.out.println("current App Path to run is:" + appPath);
            if (jobPath.equals(appPath)) {
                System.out.println("Currently Running already");
                return true;
            }
        }

        System.out.println("Not Running already");
        return false;
    }
}
