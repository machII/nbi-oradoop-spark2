package com.starhomemach.nbi.oradoop.export.file.filehandler;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;
import com.starhomemach.nbi.oradoop.export.export.Tdspe;
import com.starhomemach.nbi.oradoop.export.file.AvroFsInput;
import com.starhomemach.nbi.oradoop.export.file.dto.AvroFileRecord;
import com.starhomemach.nbi.oradoop.export.file.dto.T_dspe;

@Component
public class TDspeFileHandler extends QosFileHandler {


    private final Log logger = LogFactory.getLog(getClass());


    private final String INSERT_INTO_T_DSPE =
        "INSERT INTO bi.t_dspe (dk_spe, spe_cat, spe_cd, spe_nm, outdat, cre_dat, mod_dat)\n" +
        "  VALUES (BI.Q_SPE_ID.NEXTVAL, ?, ?, ?, ?, SYSDATE, SYSDATE)";

    @Autowired
    public Tdspe tdspe;


    @Override
    public String[] getInsertQuery() {
        return new String[]{INSERT_INTO_T_DSPE};
    }

    @Override
    public String[] getMergeQuery() {
        return new String[]{""};
    }

    @Override
    public List<AvroFileRecord> readFileToList(String avroFilePath) throws IOException {
        List<AvroFileRecord> avroRecords = Lists.newArrayList();
        Path path = new Path(avroFilePath);
        Configuration config = new Configuration();
        org.apache.avro.file.SeekableInput input = new AvroFsInput(path, config);
        org.apache.avro.io.DatumReader<T_dspe> tDSpeDatumReader = new org.apache.avro.specific.SpecificDatumReader<T_dspe>(T_dspe.class);
        org.apache.avro.file.FileReader<T_dspe> t_dspe_dyRecords = org.apache.avro.file.DataFileReader.openReader(input, tDSpeDatumReader);
        while (t_dspe_dyRecords.hasNext()) {
            T_dspe record = t_dspe_dyRecords.next();
            avroRecords.add(record);
        }

        t_dspe_dyRecords.close();
        logger.info("succeeded to process file:" + avroFilePath + ". number of records:" + avroRecords.size());

        return avroRecords;
    }

    @Override
    public boolean isMergeOperation() {
        return false;
    }


    public void setPreparedStatement(AvroFileRecord avroFileRecords, Long sequenceId, PreparedStatement preparedStatement) throws SQLException, ParseException {


        T_dspe tDspeRecord = (T_dspe) avroFileRecords;

        String SPE_CAT = tDspeRecord.getSpeCat().toString();
        String SPE_CD = tDspeRecord.getSpeCd().toString();
        String SPE_NM = tDspeRecord.getSpeNm().toString();
        Long DK_SPE = tdspe.getTdspe(SPE_CAT, SPE_CD);

        if (DK_SPE == -1) {
            logger.info("going to set PreparedStatement for record:" + avroFileRecords);
            preparedStatement.setString(1, SPE_CAT);
            preparedStatement.setString(2, SPE_CD);
            preparedStatement.setString(3, SPE_NM);
            preparedStatement.setString(4, "N");

            preparedStatement.addBatch();
        }
        else
        {
            logger.info("going to ignore record :" + avroFileRecords + " since it already exists and the record DK_SPE :" + DK_SPE);
        }
    }

    @Override
    public boolean isSequenceOperation() {
        return false;
    }

    @Override
    public String getHandlerKey() {
        return "T_DSPE";
    }


}
