package com.starhomemach.nbi.oradoop.export.export;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

@Component
public class SequenceNextVal {

    public static final String SELECT_BI_NEW_Q_HADOOP_ADT_NEXTVAL_FROM_DUAL = "select  BI.Q_HADOOP_ADT.NEXTVAL from dual";

    public static final String SELECT_BI_NEW_Q_SQL_ID_NEXTVAL_FROM_DUAL = "select sgl_lndg.q_sgl_id.NEXTVAL from dual";

    @Autowired
    private JdbcTemplate jdbcTemplate;


    private final Log logger = LogFactory.getLog(getClass());

    public Long getSequenceNextVal(String query) throws DataAccessException {
        Long sequenceNextVal = -1l;

            sequenceNextVal = jdbcTemplate.queryForObject(query, Long.class);
            logger.info("Sequence next val is:" + sequenceNextVal);
        return sequenceNextVal;
    }


}
