package com.starhomemach.nbi.oradoop.export.file.filehandler;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.apache.avro.file.DataFileReader;
import org.apache.avro.file.SeekableInput;
import org.apache.avro.io.DatumReader;
import org.apache.avro.specific.SpecificDatumReader;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;
import com.starhomemach.nbi.oradoop.export.file.AvroFsInput;
import com.starhomemach.nbi.oradoop.export.file.dto.AvroFileRecord;
import com.starhomemach.nbi.oradoop.export.file.dto.T_fprc_dy;

@Component
public class TFprcDyFileHandler extends QosFileHandler {

    @Autowired
    private FileHandlerUtil fileHandlerUtil;


    private final Log logger = LogFactory.getLog(getClass());

    private final String INSERT_INTO_T_FPRC_DY_TMP = "insert into BI.HDP_T_FPRC_DY_TMP values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    private final String MERGE_T_FPRC_DY_WITH_T_FPRC_HR_TMP = "MERGE INTO BI.T_FPRC_DY DY \n" +
            "               USING (SELECT * FROM BI.HDP_T_FPRC_DY_TMP) temp\n" +
            "               ON (DY.DK_DAT = temp.DK_DAT AND DY.CL_ID = temp.CL_ID AND DY.DK_CMM = temp.DK_CMM AND DY.DK_ORG_SNW = temp.DK_ORG_SNW AND DY.DK_ORG_HNW = temp.DK_ORG_HNW AND DY.DK_ORG_CNP = temp.DK_ORG_CNP AND \n" +
            "                   DY.DK_DIR = temp.DK_DIR AND DY.DK_SPE = temp.DK_SPE AND DY.DK_PET = temp.DK_PET AND DY.DK_SGS = temp.DK_SGS )\n" +
            "               WHEN MATCHED THEN UPDATE SET DY.PRC_SUCC = DY.PRC_SUCC + temp.PRC_SUCC , DY.PRC_ATPT = DY.PRC_ATPT + temp.PRC_ATPT ,DY.PRC_FAIL = DY.PRC_FAIL + temp.PRC_FAIL ,DY.PRC_DLY = DY.PRC_DLY + temp.PRC_DLY, DY.DK_ADT = temp.DK_ADT\n" +
            "               WHEN NOT MATCHED THEN INSERT (DY.CL_ID,DY.DK_CMM, DY.DK_ORG_SNW,DY.DK_ORG_HNW,DY.DK_ORG_CNP,DY.DK_DIR," +
            "               DY.DK_ADT, DY.DK_DAT, DY.DK_SPE, DY.DK_PET, DY.DK_SGS, DY.PRC_ATPT, DY.PRC_SUCC, DY.PRC_FAIL, DY.PRC_DLY)\n" +
            "                VALUES  (temp.CL_ID,temp.DK_CMM, temp.DK_ORG_SNW,temp.DK_ORG_HNW,temp.DK_ORG_CNP,temp.DK_DIR,temp.DK_ADT,temp.DK_DAT,temp.DK_SPE,temp.DK_PET,temp.DK_SGS,temp.PRC_ATPT,temp.PRC_SUCC,temp.PRC_FAIL,temp.PRC_DLY)";

    @Override
    public String[] getInsertQuery() {
        return new String[]{INSERT_INTO_T_FPRC_DY_TMP};
    }

    @Override
    public String[] getMergeQuery() {
        return new String[]{MERGE_T_FPRC_DY_WITH_T_FPRC_HR_TMP};
    }

    @Override
    public List<AvroFileRecord> readFileToList(String avroFilePath) throws IOException {
        List<AvroFileRecord> avroRecords = Lists.newArrayList();
        Path path = new Path(avroFilePath);
        Configuration config = new Configuration();
        SeekableInput input = new AvroFsInput(path, config);
        DatumReader<T_fprc_dy> tFprcDyDatumReader = new SpecificDatumReader<T_fprc_dy>(T_fprc_dy.class);
        org.apache.avro.file.FileReader<T_fprc_dy> t_fprc_dyRecords = DataFileReader.openReader(input, tFprcDyDatumReader);
        while (t_fprc_dyRecords.hasNext()) {
            T_fprc_dy record = t_fprc_dyRecords.next();
            avroRecords.add(record);
        }
        t_fprc_dyRecords.close();
        logger.info("succeeded to process file:" + avroFilePath + ". number of records:" + avroRecords.size());
        return avroRecords;
    }


    public void setPreparedStatement(AvroFileRecord avroFileRecords, Long sequenceId, PreparedStatement preparedStatement) throws SQLException {

        logger.info("going to set PreparedStatement for record:" + avroFileRecords);
        T_fprc_dy tFprcDyRecord = (T_fprc_dy) avroFileRecords;

        preparedStatement.setLong(1, tFprcDyRecord.getDkCmm());
        preparedStatement.setLong(2, tFprcDyRecord.getClId());
        preparedStatement.setLong(3, tFprcDyRecord.getDkDat());
        preparedStatement.setLong(4, tFprcDyRecord.getDkDir());
        preparedStatement.setLong(5, tFprcDyRecord.getDkOrgSnw());
        preparedStatement.setLong(6, tFprcDyRecord.getDkOrgHnw());
        preparedStatement.setLong(7, tFprcDyRecord.getDkOrgCnp());
        preparedStatement.setLong(8, tFprcDyRecord.getDkPet());
        preparedStatement.setLong(9, tFprcDyRecord.getDkSpe());
        preparedStatement.setLong(10, tFprcDyRecord.getDkSgs());
        preparedStatement.setLong(11, sequenceId);
        fileHandlerUtil.setPreparedStatementIfNullOrLong(12, tFprcDyRecord.getPrcAtpt(), preparedStatement);
        fileHandlerUtil.setPreparedStatementIfNullOrLong(13, tFprcDyRecord.getPrcSucc(), preparedStatement);
        fileHandlerUtil.setPreparedStatementIfNullOrLong(14, tFprcDyRecord.getPrcFail(), preparedStatement);
        fileHandlerUtil.setPreparedStatementIfNullOrDouble(15, tFprcDyRecord.getPrcDly(), preparedStatement);

        preparedStatement.addBatch();
    }

    @Override
    public boolean isSequenceOperation() {
        return true;
    }

    @Override
    public String getHandlerKey() {
        return "T_FPRC_DY";
    }

    @Override
    public boolean isMergeOperation() {
        return false;
    }

}
