package com.starhomemach.nbi.oradoop.export.Ida;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.sql.*;

@Service
class IdaTrafficExportHandler {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private String jobId;
    private String hiveJdbcUrl;
    private String hiveJdbcDriver;
    private String hiveUsername;
    private String hivePassword;

    private Connection hiveConnection;
    private Connection oracleConnection;

    private final String CLEANUP_STATEMENT = "DELETE FROM " + IdaExportArgumentsHolder.getInstance().getOutSchema() + ".t_ftrf_dla_dy\n" +
            "  WHERE cl_id = ?\n" +
            "    AND dk_dat >= ?";

    private final String INSERT_INTO_T_FTRF_DLA_DY_CHE = "INSERT INTO " + IdaExportArgumentsHolder.getInstance().getOutSchema() + ".hdp_t_ftrf_dla_dy_che\n" +
            "  ( cl_grp_id, cl_id, dk_org_cnp, dk_dat, dk_nws_ct, dk_ced, dk_dir, dk_cdt_dc, cgs_tp, dk_cur_smc,\n" +
            "    chgd_unt, chgb_unt, tx, net_chg, dk_dst_cg, dk_sgm_lst, dk_nws_ec, tadig_cdt_lvl2_cd )\n" +
            "  VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? / 1000000, ? / 1000000, ?, NVL(?, '0'), ?, ?)";

    private final String INSERT_SEGMENT_COMBINATIONS = "INSERT INTO " + IdaExportArgumentsHolder.getInstance().getOutSchema() + ".v_sgm_cmb_map\n" +
            "  ( dk_sgm_lst )\n" +
            "  VALUES (?)";


    private final String MERGE_T_FTRF_DLA_DY_WITH_T_FTRF_DLA_DY_CHE =
            "INSERT /*+ APPEND */ INTO " + IdaExportArgumentsHolder.getInstance().getOutSchema() + ".t_ftrf_dla_dy ( cl_grp_id, cl_id, dk_org_cnp,\n" +
                    "        rdk_org_cy_cnp, dk_dat, dk_nws_ct, dk_ced, dk_dir, dk_cdt_dc,\n" +
                    "        chgd_unt, chgb_unt, tx, net_chg, dk_dst_cg, dk_org_cm,\n" +
                    "        dk_org_cm_cnp, cgs_tp, sgm_cmb_id, dk_nws_ec, tadig_cdt_lvl2_cd )\n" +
                    "  SELECT agg.cl_grp_id, agg.cl_id, agg.dk_org_cnp,\n" +
                    "         cnp.dk_org_cy, agg.dk_dat, agg.dk_nws_ct, agg.dk_ced, agg.dk_dir, agg.dk_cdt_dc,\n" +
                    "         SUM(agg.chgd_unt), SUM(agg.chgb_unt), SUM(agg.tx), SUM(agg.net_chg), agg.dk_dst_cg, cl.dk_org_cm,\n" +
                    "         cnp.dk_org_cm, agg.cgs_tp, agg.sgm_cmb_id, agg.dk_nws_ec, agg.tadig_cdt_lvl2_cd\n" +
                    "    FROM (\n" +
                    "            SELECT f.cl_grp_id, f.cl_id, f.dk_org_cnp,\n" +
                    "                   f.dk_dat, f.dk_nws_ct, f.dk_ced, f.dk_dir, f.dk_cdt_dc,\n" +
                    "                   f.chgd_unt, f.chgb_unt, f.tx / ex.exr_val tx, f.net_chg / ex.exr_val net_chg,\n" +
                    "                   f.dk_dst_cg, f.cgs_tp, NVL(scm.sgm_cmb_id, 0) sgm_cmb_id,\n" +
                    "                   f.dk_nws_ec, f.tadig_cdt_lvl2_cd\n" +
                    "              FROM " + IdaExportArgumentsHolder.getInstance().getOutSchema() + ".hdp_t_ftrf_dla_dy_che f\n" +
                    "              JOIN " + IdaExportArgumentsHolder.getInstance().getOutSchema() + ".t_fexd_sdr_last ex\n" +
                    "                ON f.dk_cur_smc = ex.dk_cur_buy\n" +
                    "                   AND f.dk_dat = ex.dk_dat\n" +
                    "              LEFT JOIN " + IdaExportArgumentsHolder.getInstance().getOutSchema() + ".v_sgm_cmb_map scm\n" +
                    "                ON f.dk_sgm_lst = scm.dk_sgm_lst\n" +
                    "         ) agg\n" +
                    "    JOIN bi.t_dorg cl\n" +
                    "      ON agg.cl_id = cl.dk_org\n" +
                    "    JOIN bi.t_dorg cnp\n" +
                    "      ON agg.dk_org_cnp = cnp.dk_org\n" +
                    "   GROUP BY agg.cl_grp_id, agg.cl_id, agg.dk_org_cnp,\n" +
                    "            cnp.dk_org_cy, agg.dk_dat, agg.dk_nws_ct, agg.dk_ced, agg.dk_dir, agg.dk_cdt_dc,\n" +
                    "            agg.dk_dst_cg, cl.dk_org_cm, cnp.dk_org_cm, agg.cgs_tp, agg.sgm_cmb_id,\n" +
                    "            agg.dk_nws_ec, agg.tadig_cdt_lvl2_cd";

    private final String REBUILD_MONTHLY_TRAFFIC = "{ call " + IdaExportArgumentsHolder.getInstance().getOutSchema() + "_api.k_dl_oradoop_api.rebuildMonthlyFromDaily(?, ?) }";

    void run() throws Exception {

        getArguments();

        Class.forName(hiveJdbcDriver);
        hiveConnection = DriverManager.getConnection(hiveJdbcUrl, hiveUsername, hivePassword);

        oracleConnection = jdbcTemplate.getDataSource().getConnection();
        oracleConnection.setAutoCommit(false);

        Statement stmtSegments = hiveConnection.createStatement();
        stmtSegments.execute("SELECT * FROM ida_db.t_sgm_cmb WHERE wf_id = '" + jobId + "'");
        ResultSet rs = stmtSegments.getResultSet();
        PreparedStatement segmentsStmt = oracleConnection.prepareStatement(INSERT_SEGMENT_COMBINATIONS);
        while (rs.next()) {
            segmentsStmt.setString(1, rs.getString("dk_sgm_lst").replaceAll("[\\[\\]]", ""));
            segmentsStmt.addBatch();
        }

        segmentsStmt.executeBatch();
        doCommit();

        Statement stmtCleanup = hiveConnection.createStatement();
        stmtCleanup.execute("SELECT * FROM ida_db.t_del_ftrf_dla_dy WHERE wf_id = '" + jobId + "'");
        rs = stmtCleanup.getResultSet();
        StringBuilder clientList = new StringBuilder();
        int minDkDatMo = 9999999;

        while (rs.next()) {
            PreparedStatement cleanupStmt = oracleConnection.prepareStatement(CLEANUP_STATEMENT);
            PreparedStatement insertStmt = oracleConnection.prepareStatement(INSERT_INTO_T_FTRF_DLA_DY_CHE);
            PreparedStatement finalizeStmt = oracleConnection.prepareStatement(MERGE_T_FTRF_DLA_DY_WITH_T_FTRF_DLA_DY_CHE);
            clientList.append(",").append(rs.getLong("cl_id"));
            minDkDatMo = Math.min(rs.getInt("min_dk_dat_mo"), minDkDatMo);
            cleanupStmt.setLong(1, rs.getLong("cl_id"));
            cleanupStmt.setInt(2, rs.getInt("min_dk_dat"));
            System.out.println( "########################################################################################" );
            System.out.println( "Cleanup parameters");
            System.out.println( "cl_id [" + rs.getLong("cl_id") + "]" );
            System.out.println( "min_dk_dat [" + rs.getInt("min_dk_dat") + "]");
            System.out.println( "########################################################################################" );

            Statement stmtTraffic = hiveConnection.createStatement();
            stmtTraffic.execute("SELECT * FROM ida_db.t_ftrf_dla_dy WHERE wf_id = '" + jobId + "' AND cl_id = " + rs.getInt("cl_id"));
            ResultSet rsTraffic = stmtTraffic.getResultSet();

            cleanupStmt.execute();

            int currentBatchSize = 0;
            while (rsTraffic.next()) {
                String dkSgmLst;
                try {
                    dkSgmLst = rsTraffic.getString("dk_sgm_lst").replaceAll("[\\[\\]]", "");
                }
                catch(NullPointerException e) {
                    dkSgmLst = "0";
                }

                insertStmt.setInt(1, rsTraffic.getInt("cl_grp_id"));
                insertStmt.setLong(2, rsTraffic.getLong("cl_id"));
                insertStmt.setInt(3, rsTraffic.getInt("dk_org_cnp"));
                insertStmt.setInt(4, rsTraffic.getInt("dk_dat"));
                insertStmt.setInt(5, rsTraffic.getInt("dk_nws_ct"));
                insertStmt.setInt(6, rsTraffic.getInt("dk_ced"));
                insertStmt.setInt(7, rsTraffic.getInt("dk_dir"));
                insertStmt.setInt(8, rsTraffic.getInt("dk_cdt_dc"));
                insertStmt.setString(9, rsTraffic.getString("cgs_tp"));
                insertStmt.setInt(10, rsTraffic.getInt("dk_cur_smc"));
                insertStmt.setDouble(11, rsTraffic.getDouble("chgd_unt"));
                insertStmt.setDouble(12, rsTraffic.getDouble("chgb_unt"));
                insertStmt.setDouble(13, rsTraffic.getDouble("tx"));
                insertStmt.setDouble(14, rsTraffic.getDouble("net_chg"));
                insertStmt.setInt(15, rsTraffic.getInt("dk_dst_cg"));
                insertStmt.setString(16, dkSgmLst);
                insertStmt.setInt(17, rsTraffic.getInt("dk_nws_ec"));
                insertStmt.setString(18, rsTraffic.getString("tadig_cdt_lvl2_cd"));

                currentBatchSize++;
                insertStmt.addBatch();
                if (currentBatchSize >= IdaExportArgumentsHolder.getInstance().getBatchSize()) {
                    insertStmt.executeBatch();
                    currentBatchSize = 0;
                }
            }

            insertStmt.executeBatch();
            finalizeStmt.execute();
            doCommit();
        }

        if (clientList.length() > 0) {
            PreparedStatement rebuildMonthlyTrafficStmt = oracleConnection.prepareStatement(REBUILD_MONTHLY_TRAFFIC);
            rebuildMonthlyTrafficStmt.setInt(1, minDkDatMo);
            rebuildMonthlyTrafficStmt.setString(2, clientList.toString().replaceFirst(",", ""));
            System.out.println( "########################################################################################" );
            System.out.println( "Rebuild parameters");
            System.out.println( "min_dk_dat_mo [" + minDkDatMo + "]");
            System.out.println( "cl_id_lst [" + clientList.toString().replaceFirst(",", "") + "]" );
            System.out.println( "########################################################################################" );
            rebuildMonthlyTrafficStmt.execute();
            doCommit();
        }
        doCloseConnection();
    }

    private void getArguments() {
        IdaExportArgumentsHolder idaExportArgumentsHolder = IdaExportArgumentsHolder.getInstance();
        jobId = idaExportArgumentsHolder.getJobId();
        hiveJdbcUrl = idaExportArgumentsHolder.getHiveJdbcUrl();
        hiveJdbcDriver = idaExportArgumentsHolder.getHiveJdbcDriver();
        hiveUsername = idaExportArgumentsHolder.getUser();
        hivePassword = idaExportArgumentsHolder.getUser();

        System.out.println("-----------------------------Arguments: ----------------------------");
        System.out.println(idaExportArgumentsHolder.toString());
        System.out.println("---------------------------------------------------------------------");
    }

    private void doCommit() throws SQLException {
        oracleConnection.commit();
    }

    private void doCloseConnection() throws SQLException {
        oracleConnection.close();
        hiveConnection.close();
    }
}