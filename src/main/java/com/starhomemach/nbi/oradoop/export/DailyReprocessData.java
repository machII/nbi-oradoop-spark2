package com.starhomemach.nbi.oradoop.export;


import java.io.*;
import java.util.Date;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Properties;

import com.starhomemach.nbi.oradoop.export.JobHistory.JobHistoryHandler;
import com.starhomemach.nbi.oradoop.export.util.HandleProcessConfiguration;
import com.starhomemach.nbi.oradoop.util.GeneralUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Created by armb on 02-11-2016.
 */
public class DailyReprocessData {

    private final Log logger = LogFactory.getLog(getClass());

    public void ReprocessDataPeriod(long dk_dat,long fromDate,long toDate) throws SQLException {

        long initialDate = 0,finalDate = 0;

        logger.info("Value of DK_DAT is "+dk_dat);
        logger.info("Value of fromDate is "+fromDate);
        logger.info("Value of toDate is "+toDate);

        if((dk_dat > fromDate) && (dk_dat <= toDate))
        {
            initialDate = fromDate;
            finalDate = dk_dat;
        }
        else if((dk_dat > fromDate) && (dk_dat > toDate))
        {
            initialDate = fromDate;
            finalDate = toDate;
        }
        logger.info("Initial and final date values are "+initialDate+" and "+finalDate);
        saveResultToOozieProperty(initialDate,finalDate);
    }

    public void saveResultToOozieProperty(long initialDate, long finalDate) throws SQLException {

        String filePath = System.getProperty("oozie.action.output.properties");
        try {
            if (filePath != null) {
                OutputStream outputStream = new FileOutputStream(filePath);
                Properties p = new Properties();
                p.setProperty("initialDate", "" + initialDate);
                p.setProperty("finalDate", "" + finalDate);
                p.store(outputStream, "");
                outputStream.close();
            } else {
                throw new RuntimeException("oozie.action.output.properties System property not defined");
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws Exception {

        long fromDate,toDate;
        System.out.println("########################################################## START JOB HISTROY #############################");
        if (args.length < 2) {
            System.out.println("ERROR args should contain the following parameters 1)USER 2)JOB_NAME");
            return;
        }
        JobHistoryHandler jobHistoryHandler = new JobHistoryHandler();
        DailyReprocessData dailyReprocessData = new DailyReprocessData();
        GeneralUtils generalUtils = new GeneralUtils();
        DateFormat inputFormatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS");
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        Date startDate = inputFormatter.parse(args[3]);
        Date finishDate = inputFormatter.parse(args[4]);

        HandleProcessConfiguration handleProcessConfiguration = new HandleProcessConfiguration();
        handleProcessConfiguration.handleOradoopConf(args,Application.INDEX_DB_PROPERTIES_FILE);
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SpringConfig.class);
        context.getBean(JobHistoryHandler.class).fetchLastRun(args[0],args[1]);

        DateFormat outputFormatter = new SimpleDateFormat("yyyy/MM/dd");
        long dkDat =  jobHistoryHandler.getDkDat();
        fromDate = Long.valueOf(generalUtils.calculateDK_DAT(dateFormat.parse(outputFormatter.format(startDate))));
        toDate = Long.valueOf(generalUtils.calculateDK_DAT(dateFormat.parse(outputFormatter.format(finishDate))));
        dailyReprocessData.ReprocessDataPeriod(dkDat,fromDate,toDate);

        System.out.println("########################################################## END JOB HISTROY #############################");
    }
}
