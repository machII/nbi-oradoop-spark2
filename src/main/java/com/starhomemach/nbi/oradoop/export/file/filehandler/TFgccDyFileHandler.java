package com.starhomemach.nbi.oradoop.export.file.filehandler;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;
import com.starhomemach.nbi.oradoop.export.file.AvroFsInput;
import com.starhomemach.nbi.oradoop.export.file.dto.AvroFileRecord;
import com.starhomemach.nbi.oradoop.export.file.dto.T_fgcc_dy;

@Component
public class TFgccDyFileHandler extends QosFileHandler {

    @Autowired
    private FileHandlerUtil fileHandlerUtil;

    private final Log logger = LogFactory.getLog(getClass());

    private final String INSERT_INTO_T_FGCC_DY_TMP = "INSERT INTO bi.hdp_t_fgcc_dy_tmp VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

    private final String MERGE_T_FGCC_HR_WITH_T_FGCC_DY_TMP =
        "MERGE INTO bi.t_fgcc_dy dy\n" +
        "  USING (SELECT * FROM bi.hdp_t_fgcc_dy_tmp) temp\n" +
        "     ON ( dy.dk_cmm = temp.dk_cmm AND \n" +
        " DY.cl_id = temp.cl_id AND\n" +
        " DY.dk_dat = temp.dk_dat AND\n" +
        " DY.dk_dir = temp.dk_dir AND \n" +
        " DY.dk_org_cnp = temp.dk_org_cnp AND \n" +
        " DY.dk_org_hnw = temp.dk_org_hnw AND \n" +
        " DY.dk_org_snw = temp.dk_org_snw AND \n" +
        " DY.dk_sgs = temp.dk_sgs AND \n" +
        " DY.dk_sbp = temp.dk_sbp AND \n" +
        " DY.dk_spe = temp.dk_spe AND \n" +
        " DY.dk_pet = temp.dk_pet AND \n" +
        " DY.dk_rat = temp.dk_rat )\n" +
        " WHEN MATCHED THEN UPDATE SET \n" +
        " DY.sgc_succ=  DY.sgc_succ + temp.sgc_succ ,\n" +
        " DY.sgc_fail = DY.sgc_fail + temp.sgc_fail,\n" +
        " DY.sgc_atpt = DY.sgc_atpt + temp.sgc_atpt ,\n" +
        " DY.sgc_dly = DY.sgc_dly + temp.sgc_dly,\n" +
        " DY.sgc_dwnlnk_byt = DY.sgc_dwnlnk_byt + temp.sgc_dwnlnk_byt,\n" +
        " DY.sgc_dwnlnk_dur=DY.sgc_dwnlnk_dur+ temp.sgc_dwnlnk_dur,\n" +
        " DY.sgc_dwnlnk_good_byt = DY.sgc_dwnlnk_good_byt+ temp.sgc_dwnlnk_good_byt,\n" +
        " DY.sgc_sess_dur = DY.sgc_sess_dur+ temp.sgc_sess_dur,\n" +
        " DY.sgc_tcp_pcks = DY.sgc_tcp_pcks+ temp.sgc_tcp_pcks,\n" +
        " DY.sgc_tcp_rdtrp_no = DY.sgc_tcp_rdtrp_no+ temp.sgc_tcp_rdtrp_no,\n" +
        " DY.sgc_tcp_rdtrp_tim = DY.sgc_tcp_rdtrp_tim+ temp.sgc_tcp_rdtrp_tim,\n" +
        " DY.sgc_tcp_rtrns_pcks = DY.sgc_tcp_rtrns_pcks+ temp.sgc_tcp_rtrns_pcks,\n" +
        " DY.sgc_uplnk_byt = DY.sgc_uplnk_byt+ temp.sgc_uplnk_byt,\n" +
        " DY.sgc_uplnk_dur = DY.sgc_uplnk_dur+ temp.sgc_uplnk_dur,\n" +
        " DY.sgc_uplnk_good_byt = DY.sgc_uplnk_good_byt+ temp.sgc_uplnk_good_byt\n" +
        "  WHEN NOT MATCHED THEN INSERT (\tDY.cl_grp_id,DY.cl_id,DY.dk_dat,DY.dk_dir,DY.dk_org_cnp,DY.dk_org_hnw,DY.dk_org_snw,DY.dk_sgs,DY.dk_sbp,DY.dk_spe,DY.dk_pet,DY.dk_rat,DY.dk_adt,DY.sgc_succ,DY.sgc_fail,DY.sgc_atpt,DY.sgc_dly,DY.sgc_dwnlnk_byt,DY.sgc_dwnlnk_dur,DY.sgc_dwnlnk_good_byt,DY.sgc_sess_dur,DY.sgc_tcp_pcks,DY.sgc_tcp_rdtrp_no,DY.sgc_tcp_rdtrp_tim,DY.sgc_tcp_rtrns_pcks,DY.sgc_uplnk_byt,DY.sgc_uplnk_dur,DY.sgc_uplnk_good_byt)\n" +
        " values (temp.cl_grp_id,temp.cl_id,temp.dk_dat,temp.dk_dir,temp.dk_org_cnp,temp.dk_org_hnw,temp.dk_org_snw,temp.dk_sgs,temp.dk_sbp,temp.dk_spe,temp.dk_pet,temp.dk_rat,temp.dk_adt,temp.sgc_succ,temp.sgc_fail,temp.sgc_atpt,temp.sgc_dly,temp.sgc_dwnlnk_byt,temp.sgc_dwnlnk_dur,temp.sgc_dwnlnk_good_byt,temp.sgc_sess_dur,temp.sgc_tcp_pcks,temp.sgc_tcp_rdtrp_no,temp.sgc_tcp_rdtrp_tim,temp.sgc_tcp_rtrns_pcks,temp.sgc_uplnk_byt,temp.sgc_uplnk_dur,temp.sgc_uplnk_good_byt)";

    @Override
    public String[] getInsertQuery() {
        return new String[]{INSERT_INTO_T_FGCC_DY_TMP};
    }

    @Override
    public String[] getMergeQuery() {
        return new String[]{MERGE_T_FGCC_HR_WITH_T_FGCC_DY_TMP};
    }

    @Override
    public List<AvroFileRecord> readFileToList(String avroFilePath) throws IOException {
        List<AvroFileRecord> avroRecords = Lists.newArrayList();
        Path path = new Path(avroFilePath);
        Configuration config = new Configuration();
            org.apache.avro.file.SeekableInput input = new AvroFsInput(path, config);
            org.apache.avro.io.DatumReader<T_fgcc_dy> tFsgcDyDatumReader = new org.apache.avro.specific.SpecificDatumReader<T_fgcc_dy>(T_fgcc_dy.class);
            org.apache.avro.file.FileReader<T_fgcc_dy> t_fsgc_dyRecords = org.apache.avro.file.DataFileReader.openReader(input, tFsgcDyDatumReader);
            while (t_fsgc_dyRecords.hasNext()) {
                T_fgcc_dy record = t_fsgc_dyRecords.next();
                avroRecords.add(record);
            }

            t_fsgc_dyRecords.close();
            logger.info("succeeded to process file:" + avroFilePath + ". number of records:" + avroRecords.size());
        return avroRecords;
    }


    public void setPreparedStatement(AvroFileRecord avroFileRecords, Long sequenceId, PreparedStatement preparedStatement) throws SQLException {
        logger.info("going to set PreparedStatement for record:" + avroFileRecords);
        T_fgcc_dy tFsGcDyRecord = (T_fgcc_dy) avroFileRecords;

        preparedStatement.setLong(1, tFsGcDyRecord.getDkCmm());
        preparedStatement.setLong(2, tFsGcDyRecord.getClId());
        preparedStatement.setLong(3, tFsGcDyRecord.getDkDat());
        preparedStatement.setLong(4, tFsGcDyRecord.getDkDir());
        preparedStatement.setLong(5, tFsGcDyRecord.getDkOrgCnp());
        preparedStatement.setLong(6, tFsGcDyRecord.getDkOrgHnw());
        preparedStatement.setLong(7, tFsGcDyRecord.getDkOrgSnw());
        preparedStatement.setLong(8, tFsGcDyRecord.getDkPet());
        preparedStatement.setLong(9, tFsGcDyRecord.getDkSpe());
        preparedStatement.setLong(10, tFsGcDyRecord.getDkSgs());
        preparedStatement.setLong(11, tFsGcDyRecord.getDkRat());
        preparedStatement.setLong(12, sequenceId);
        fileHandlerUtil.setPreparedStatementIfNullOrDouble(13, tFsGcDyRecord.getGccSucc(), preparedStatement);
        fileHandlerUtil.setPreparedStatementIfNullOrDouble(14, tFsGcDyRecord.getGccFail(), preparedStatement);
        fileHandlerUtil.setPreparedStatementIfNullOrDouble(15, tFsGcDyRecord.getGccAtpt(), preparedStatement);
        fileHandlerUtil.setPreparedStatementIfNullOrDouble(16, tFsGcDyRecord.getGccDly(), preparedStatement);
        fileHandlerUtil.setPreparedStatementIfNullOrDouble(17, tFsGcDyRecord.getGccDwnlnkByt(), preparedStatement);
        fileHandlerUtil.setPreparedStatementIfNullOrDouble(18, tFsGcDyRecord.getGccDwnlnkDur(), preparedStatement);
        fileHandlerUtil.setPreparedStatementIfNullOrDouble(19, tFsGcDyRecord.getGccDwnlnkGoodByt(), preparedStatement);
        fileHandlerUtil.setPreparedStatementIfNullOrDouble(20, tFsGcDyRecord.getGccSessDur(), preparedStatement);
        fileHandlerUtil.setPreparedStatementIfNullOrDouble(21, tFsGcDyRecord.getGccTcpPcks(), preparedStatement);
        fileHandlerUtil.setPreparedStatementIfNullOrDouble(22, tFsGcDyRecord.getGccTcpRdtrpNo(), preparedStatement);
        fileHandlerUtil.setPreparedStatementIfNullOrDouble(23, tFsGcDyRecord.getGccTcpRdtrpTim(), preparedStatement);
        fileHandlerUtil.setPreparedStatementIfNullOrDouble(24, tFsGcDyRecord.getGccTcpRtrnsPcks(), preparedStatement);
        fileHandlerUtil.setPreparedStatementIfNullOrDouble(25, tFsGcDyRecord.getGccUplnkByt(), preparedStatement);
        fileHandlerUtil.setPreparedStatementIfNullOrDouble(26, tFsGcDyRecord.getGccUplnkDur(), preparedStatement);
        fileHandlerUtil.setPreparedStatementIfNullOrDouble(27, tFsGcDyRecord.getGccUplnkGoodByt(), preparedStatement);

        preparedStatement.addBatch();

    }

    @Override
    public boolean isSequenceOperation() {
        return true;
    }

    @Override
    public String getHandlerKey() {
        return "T_FGCC_DY";
    }


    @Override
    public boolean isMergeOperation() {
        return false;
    }

}
