package com.starhomemach.nbi.oradoop.export.export.QosAggregations;

import com.starhomemach.nbi.oradoop.export.file.filehandler.QosFileHandler;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;

/**
 * Created by tpin on 09/10/2016.
 */
public abstract class QosAggregationsFileHandler extends QosFileHandler {

    abstract public void setPreparedStatement(ResultSet record, Long sequenceId, PreparedStatement preparedStatement) throws SQLException, ParseException;
}
