package com.starhomemach.nbi.oradoop.export.movefiles;

import java.io.IOException;

/**
 * Created by kcorsia on 19/09/2017.
 */
public class MoverGeneric extends Mover{

    @Override
    public String getIncomingPath(String userCode) {
        String IncomingPath = incomingPath;
        return IncomingPath;
    }

    @Override
    public String getProcessingPath(String userCode, String workflowId) {
        String ProcessingPath = processingPath;
        return ProcessingPath;
    }


    public static void main(String[] args) throws IOException {

        System.out.println("########################################################## START MOVE GENRIC WORKFLOW FILE #############################");
        if (args.length > 5 || args.length < 3) {
            printUsage();
            System.exit(1);
        }

        Mover client = new MoverGeneric();

        if (args.length >= 4) {
            client.setIncomingPath(args[3]);
        }

        if (args.length == 5) {
            client.setProcessingPath(args[4]);
        }

        client.checkAndMove(args[0], args[1], Integer.parseInt(args[2]));

        System.out.println("########################################################## END MOVE WORKFLOW FILE #############################");

    }
}
