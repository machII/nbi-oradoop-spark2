package com.starhomemach.nbi.oradoop.export.export.QosAggregations;

import com.google.common.collect.Lists;
import com.starhomemach.nbi.oradoop.export.export.Tdsvk;
import com.starhomemach.nbi.oradoop.export.file.AvroFsInput;
import com.starhomemach.nbi.oradoop.export.file.dto.AvroFileRecord;
import com.starhomemach.nbi.oradoop.export.file.dto.T_dsvk;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

import static com.starhomemach.nbi.oradoop.export.export.MergeRecords.show_record;

@Component
public class TDsvkHandler extends QosAggregationsFileHandler {


    private final String INSERT_INTO_T_DSVK = "INSERT INTO bi.t_dsvk (dk_svk, svk_cd, cre_dat, mod_dat) VALUES (BI.Q_SVK_ID.NEXTVAL, ?, SYSDATE, SYSDATE)";

    @Autowired
    public Tdsvk tdsvk;

    @Override
    public String[] getInsertQuery() {
        return new String[]{ INSERT_INTO_T_DSVK};
    }

    @Override
    public String[] getMergeQuery() {
        return new String[]{""};
    }

    @Override
    public List<AvroFileRecord> readFileToList(String avroFilePath) throws IOException {
        List<AvroFileRecord> avroRecords = Lists.newArrayList();
        Path path = new Path(avroFilePath);
        Configuration config = new Configuration();
        org.apache.avro.file.SeekableInput input = new AvroFsInput(path, config);
        org.apache.avro.io.DatumReader<T_dsvk> tDSvkDatumReader = new org.apache.avro.specific.SpecificDatumReader<T_dsvk>(T_dsvk.class);
        org.apache.avro.file.FileReader<T_dsvk> t_dsvk_dyRecords = org.apache.avro.file.DataFileReader.openReader(input, tDSvkDatumReader);
        while (t_dsvk_dyRecords.hasNext()) {
            T_dsvk record = t_dsvk_dyRecords.next();
            avroRecords.add(record);
        }

        t_dsvk_dyRecords.close();
        System.out.println("succeeded to process file:" + avroFilePath + ". number of records:" + avroRecords.size());

        return avroRecords;
    }

    @Override
    public boolean isMergeOperation() {
        return false;
    }

    @Override
    public void setPreparedStatement(ResultSet record, Long sequenceId, PreparedStatement preparedStatement) throws SQLException, ParseException {

        String SVK_CD = String.valueOf(record.getLong("svk_cd"));
        Long DK_SVK = tdsvk.getTdsvk(SVK_CD);

        if (DK_SVK == -1) {
            System.out.println("going to set PreparedStatement for record:");
            show_record(this.getHandlerKey(), record);
            preparedStatement.setString(1, SVK_CD);

            preparedStatement.addBatch();
        } else {
            System.out.println("going to ignore record since it already exists and the record DK_SVK :" + DK_SVK);
            show_record(this.getHandlerKey(), record);
        }
    }

    @Override
    public void setPreparedStatement(AvroFileRecord avroFileRecords, Long sequenceId, PreparedStatement preparedStatement) throws SQLException, ParseException {
    
    }

    @Override
    public boolean isSequenceOperation() {
        return false;
    }

    @Override
    public String getHandlerKey() {
        return "t_dsvk";
    }


}
