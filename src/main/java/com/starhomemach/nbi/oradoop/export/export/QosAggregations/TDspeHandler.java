package com.starhomemach.nbi.oradoop.export.export.QosAggregations;

import com.google.common.collect.Lists;
import com.starhomemach.nbi.oradoop.export.export.Tdspe;
import com.starhomemach.nbi.oradoop.export.file.AvroFsInput;
import com.starhomemach.nbi.oradoop.export.file.dto.AvroFileRecord;
import com.starhomemach.nbi.oradoop.export.file.dto.T_dspe;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

import static com.starhomemach.nbi.oradoop.export.export.MergeRecords.show_record;

@Component
public class TDspeHandler extends QosAggregationsFileHandler {

    private final String INSERT_INTO_T_DSPE =
            "INSERT INTO bi.t_dspe (dk_spe, spe_cat, spe_cd, spe_nm, outdat, cre_dat, mod_dat)\n" +
                    "  VALUES (BI.Q_SPE_ID.NEXTVAL, ?, ?, ?, ?, SYSDATE, SYSDATE)";

    @Autowired
    public Tdspe tdspe;


    @Override
    public String[] getInsertQuery() {
        return new String[]{INSERT_INTO_T_DSPE};
    }

    @Override
    public String[] getMergeQuery() {
        return new String[]{""};
    }

    @Override
    public List<AvroFileRecord> readFileToList(String avroFilePath) throws IOException {
        List<AvroFileRecord> avroRecords = Lists.newArrayList();
        Path path = new Path(avroFilePath);
        Configuration config = new Configuration();
        org.apache.avro.file.SeekableInput input = new AvroFsInput(path, config);
        org.apache.avro.io.DatumReader<T_dspe> tDSpeDatumReader = new org.apache.avro.specific.SpecificDatumReader<T_dspe>(T_dspe.class);
        org.apache.avro.file.FileReader<T_dspe> t_dspe_dyRecords = org.apache.avro.file.DataFileReader.openReader(input, tDSpeDatumReader);
        while (t_dspe_dyRecords.hasNext()) {
            T_dspe record = t_dspe_dyRecords.next();
            avroRecords.add(record);
        }

        t_dspe_dyRecords.close();
        System.out.println("succeeded to process file:" + avroFilePath + ". number of records:" + avroRecords.size());

        return avroRecords;
    }

    @Override
    public boolean isMergeOperation() {
        return false;
    }

    @Override
    public void setPreparedStatement(ResultSet record, Long sequenceId, PreparedStatement preparedStatement) throws SQLException, ParseException {

        String SPE_CAT = record.getString("spe_cat");
        String SPE_CD = String.valueOf(record.getLong("spe_cd"));
        String SPE_NM = String.valueOf(record.getLong("spe_nm"));
        Long DK_SPE = tdspe.getTdspe(SPE_CAT, SPE_CD);

        if (DK_SPE == -1) {
            System.out.println("going to set PreparedStatement for record:");
            show_record(this.getHandlerKey(),record);
            preparedStatement.setString(1, SPE_CAT);
            preparedStatement.setString(2, SPE_CD);
            preparedStatement.setString(3, SPE_NM);
            preparedStatement.setString(4, "N");

            preparedStatement.addBatch();
        }
        else
        {
            System.out.println("going to ignore record since it already exists and the record DK_SPE :" + DK_SPE);
            show_record(this.getHandlerKey(),record);
        }

    }

    @Override
    public void setPreparedStatement(AvroFileRecord avroFileRecords, Long sequenceId, PreparedStatement preparedStatement) throws SQLException, ParseException {
    }

    @Override
    public boolean isSequenceOperation() {
        return false;
    }

    @Override
    public String getHandlerKey() {
        return "T_DSPE";
    }


}
