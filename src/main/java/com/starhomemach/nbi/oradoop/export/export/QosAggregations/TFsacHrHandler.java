package com.starhomemach.nbi.oradoop.export.export.QosAggregations;

import com.google.common.collect.Lists;
import com.starhomemach.nbi.oradoop.export.file.AvroFsInput;
import com.starhomemach.nbi.oradoop.export.file.dto.AvroFileRecord;
import com.starhomemach.nbi.oradoop.export.file.dto.T_fsac_hr;
import com.starhomemach.nbi.oradoop.export.file.filehandler.FileHandlerUtil;
import org.apache.avro.file.DataFileReader;
import org.apache.avro.file.SeekableInput;
import org.apache.avro.io.DatumReader;
import org.apache.avro.specific.SpecificDatumReader;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

@Component
public class TFsacHrHandler extends QosAggregationsFileHandler {

    private final String MERGE_T_FSAC_DY_WITH_T_FSAC_HR_TMP =
        "MERGE INTO bi.t_fsac_dy d\n" +
        "  USING (SELECT dk_cmm, cl_id, dk_dat, dk_dir, dk_org_snw, dk_org_hnw, dk_org_cnp, dk_pet, dk_spe, dk_sgs, dk_ssc,\n" +
        "                MAX(dk_adt) dk_adt, SUM(sac_atpt) sac_atpt, SUM(sac_succ) sac_succ, SUM(sac_fail) sac_fail, SUM(sac_dly) sac_dly\n" +
        "           FROM bi.hdp_t_fsac_hr_tmp\n" +
        "          GROUP BY dk_cmm, cl_id, dk_org_snw, dk_org_hnw, dk_org_cnp, dk_dir, dk_dat, dk_spe, dk_pet, dk_sgs, dk_ssc ) temp\n" +
        "     ON (d.cl_id = temp.cl_id AND d.dk_cmm = temp.dk_cmm AND d.dk_org_snw = temp.dk_org_snw AND d.dk_org_hnw = temp.dk_org_hnw\n" +
        "         AND d.dk_org_cnp = temp.dk_org_cnp AND d.dk_dir = temp.dk_dir AND d.dk_dat = temp.dk_dat AND d.dk_spe = temp.dk_spe\n" +
        "         AND d.dk_pet = temp.dk_pet AND d.dk_sgs = temp.dk_sgs AND d.dk_ssc = temp.dk_ssc )\n" +
        "  WHEN MATCHED THEN" +
        "    UPDATE SET d.sac_succ = d.sac_succ + temp.sac_succ,\n" +
        "               d.sac_atpt = d.sac_atpt + temp.sac_atpt,\n" +
        "               d.sac_fail = d.sac_fail + temp.sac_fail,\n" +
        "               d.sac_dly = d.sac_dly + temp.sac_dly,\n" +
        "               d.dk_adt = temp.dk_adt\n" +
        "  WHEN NOT MATCHED THEN" +
        "    INSERT ( dk_cmm, cl_id, dk_org_snw, dk_org_hnw, dk_org_cnp, dk_dir, dk_adt,\n" +
        "             dk_dat, dk_spe, dk_pet, dk_sgs, sac_atpt, sac_succ, sac_fail, sac_dly, dk_ssc )\n" +
        "      VALUES ( temp.dk_cmm, temp.cl_id,  temp.dk_org_snw, temp.dk_org_hnw, temp.dk_org_cnp, temp.dk_dir, temp.dk_adt,\n" +
        "               temp.dk_dat, temp.dk_spe, temp.dk_pet, temp.dk_sgs, temp.sac_atpt, temp.sac_succ, temp.sac_fail, temp.sac_dly, temp.dk_ssc )";

    @Autowired
    private FileHandlerUtil fileHandlerUtil;

    @Override
    public String[] getInsertQuery() {
        String INSERT_INTO_T_FSAC_HR_TMP = "INSERT INTO bi.hdp_t_fsac_hr_tmp VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        return new String[]{INSERT_INTO_T_FSAC_HR_TMP};
    }

    @Override
    public String[] getMergeQuery() {
        String MERGE_T_FSAC_HR_WITH_T_FSAC_HR_TMP =
            "MERGE INTO bi.t_fsac_hr d\n" +
            "  USING ( SELECT * FROM bi.hdp_t_fsac_hr_tmp ) temp\n" +
            "     ON ( d.cl_id = temp.cl_id\n" +
            "          AND d.dk_cmm = temp.dk_cmm\n" +
            "          AND d.dk_org_snw = temp.dk_org_snw\n" +
            "          AND d.dk_org_hnw = temp.dk_org_hnw\n" +
            "          AND d.dk_org_cnp = temp.dk_org_cnp\n" +
            "          AND d.dk_dir = temp.dk_dir\n" +
            "          AND d.dk_dat = temp.dk_dat\n" +
            "          AND d.dk_spe = temp.dk_spe\n" +
            "          AND d.dk_pet = temp.dk_pet\n" +
            "          AND d.dk_sgs = temp.dk_sgs\n" +
            "          AND d.dk_tim_hr = temp.dk_tim_hr\n" +
            "          AND d.dk_ssc = temp.dk_ssc )\n" +
            "  WHEN MATCHED THEN\n" +
            "    UPDATE SET d.sac_succ = d.sac_succ + temp.sac_succ,\n" +
            "               d.sac_atpt = d.sac_atpt + temp.sac_atpt,\n" +
            "               d.sac_fail = d.sac_fail + temp.sac_fail,\n" +
            "               d.sac_dly = d.sac_dly + temp.sac_dly,\n" +
            "               d.dk_adt = temp.dk_adt\n" +
            "  WHEN NOT MATCHED THEN\n" +
            "    INSERT ( dk_cmm, cl_id, dk_org_snw, dk_org_hnw, dk_org_cnp, dk_dir, dk_adt,\n" +
            "             dk_dat, dk_tim_hr, dk_spe, dk_pet, dk_sgs, sac_atpt,\n" +
            "             sac_succ, sac_fail, sac_dly, dk_ssc )\n" +
            "      VALUES ( temp.dk_cmm, temp.cl_id, temp.dk_org_snw, temp.dk_org_hnw, temp.dk_org_cnp, temp.dk_dir, temp.dk_adt,\n" +
            "               temp.dk_dat, temp.dk_tim_hr, temp.dk_spe, temp.dk_pet, temp.dk_sgs,\n" +
            "               temp.sac_atpt, temp.sac_succ, temp.sac_fail, temp.sac_dly, temp.dk_ssc )";


        return fileHandlerUtil.getMergeStatmentsArray(MERGE_T_FSAC_HR_WITH_T_FSAC_HR_TMP, MERGE_T_FSAC_DY_WITH_T_FSAC_HR_TMP);
    }

    @Override
    public String[] getAppendQuery() {
        String APPEND_INTO_T_FSAC_HR_FROM_T_FSAC_HR_TMP =
            "INSERT /*+ APPEND */ INTO bi.t_fsac_hr\n" +
            "       ( dk_cmm, cl_id, dk_dat, dk_tim_hr, dk_dir, dk_org_snw, dk_org_hnw, dk_org_cnp, dk_pet, dk_spe,\n" +
            "         dk_sgs, dk_ssc, dk_adt, sac_atpt, sac_succ, sac_fail, sac_dly)\n" +
            "  SELECT dk_cmm, cl_id, dk_dat, dk_tim_hr, dk_dir, dk_org_snw, dk_org_hnw, dk_org_cnp, dk_pet, dk_spe,\n" +
            "         dk_sgs, dk_ssc, dk_adt, sac_atpt, sac_succ, sac_fail, sac_dly\n" +
            "    FROM bi.hdp_t_fsac_hr_tmp";

        return fileHandlerUtil.getAppendStatmentsArray(APPEND_INTO_T_FSAC_HR_FROM_T_FSAC_HR_TMP, MERGE_T_FSAC_DY_WITH_T_FSAC_HR_TMP);
    }

    @Override
    public void setPreparedStatement(AvroFileRecord avroFileRecords, Long sequenceId, PreparedStatement preparedStatement) throws SQLException, ParseException {

    }

    @Override
    public void setPreparedStatement(ResultSet record, Long sequenceId, PreparedStatement preparedStatement) throws SQLException {

        preparedStatement.setLong(1, record.getLong("dk_cmm"));
        preparedStatement.setLong(2, record.getLong("cl_id"));
        preparedStatement.setLong(3, record.getLong("dk_dat"));
        preparedStatement.setLong(4, record.getLong("dk_tim_hr"));
        preparedStatement.setLong(5, record.getLong("dk_dir"));
        preparedStatement.setLong(6, record.getLong("dk_org_snw"));
        preparedStatement.setLong(7, record.getLong("dk_org_hnw"));
        preparedStatement.setLong(8, record.getLong("dk_org_cnp"));
        preparedStatement.setLong(9, record.getLong("dk_pet"));
        preparedStatement.setLong(10,  record.getLong("dk_spe"));
        preparedStatement.setLong(11,record.getLong("dk_sgs"));
        preparedStatement.setLong(12,record.getLong("dk_ssc"));
        preparedStatement.setLong(13, sequenceId);
        preparedStatement.setLong(14,record.getLong("sac_atpt"));
        preparedStatement.setLong(15, record.getLong("sac_succ"));
        preparedStatement.setLong(16, record.getLong("sac_fail"));
        Double sacDly = record.getDouble("sac_dly");
        if (sacDly == null)
            sacDly = new Double(-1);
        preparedStatement.setDouble(17, sacDly);
        preparedStatement.addBatch();
    }

    @Override
    public List<AvroFileRecord> readFileToList(String avroFilePath) throws IOException {
        List<AvroFileRecord> avroRecords = Lists.newArrayList();
        Path path = new Path(avroFilePath);
        Configuration config = new Configuration();
        SeekableInput input = new AvroFsInput(path, config);
        DatumReader<T_fsac_hr> tFsacDyDatumReader = new SpecificDatumReader<T_fsac_hr>(T_fsac_hr.class);
        org.apache.avro.file.FileReader<T_fsac_hr> t_fsac_hrRecords = DataFileReader.openReader(input, tFsacDyDatumReader);
        while (t_fsac_hrRecords.hasNext()) {
            T_fsac_hr record = t_fsac_hrRecords.next();
            avroRecords.add(record);
        }
        t_fsac_hrRecords.close();
        System.out.println("succeeded to process file:" + avroFilePath + ". number of records:" + avroRecords.size());
        return avroRecords;
    }

    @Override
    public boolean isSequenceOperation() {
        return true;
    }

    @Override
    public String getHandlerKey() {
        return "T_FSAC_HR";
    }

    @Override
    public boolean isMergeOperation() {
        return true;
    }

    @Override
    public boolean isAppendOperation() {
        return false;
    }

}
