package com.starhomemach.nbi.oradoop.export.export.QosAggregations;

import com.google.common.collect.Lists;
import com.starhomemach.nbi.oradoop.export.export.SequenceNextVal;
import com.starhomemach.nbi.oradoop.export.file.AvroFsInput;
import com.starhomemach.nbi.oradoop.export.file.dto.AvroFileRecord;
import com.starhomemach.nbi.oradoop.export.file.dto.T_dsgl;
import com.starhomemach.nbi.oradoop.export.file.filehandler.FileHandlerUtil;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

@Component
public class TDsglHandler extends QosAggregationsFileHandler {

    private final String INSERT_INTO_T_DSGL =
        "INSERT INTO bi.t_dsgl\n" +
        "  ( cl_grp_id, cl_id, dk_sgl, sgl_prtcl_cd, sgl_fct_tbl_cd, tmst_sgl_first_evt,\n" +
        "    tmst_sgl_last_evt, outdat, cre_dat, mod_dat, adt_id, mod_tmst, dk_cmm )\n" +
        " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

    @Autowired
    private SequenceNextVal sequenceNextVal;

    @Autowired
    private FileHandlerUtil fileHandlerUtil;

    @Override
    public String[] getInsertQuery() {
        return new String[]{INSERT_INTO_T_DSGL};
    }

    @Override
    public String[] getMergeQuery() {
        return new String[]{""};
    }

    @Override
    public List<AvroFileRecord> readFileToList(String avroFilePath) throws IOException {
        List<AvroFileRecord> avroRecords = Lists.newArrayList();
        Path path = new Path(avroFilePath);
        Configuration config = new Configuration();
        org.apache.avro.file.SeekableInput input = new AvroFsInput(path, config);
        org.apache.avro.io.DatumReader<T_dsgl> tDSglDatumReader = new org.apache.avro.specific.SpecificDatumReader<T_dsgl>(T_dsgl.class);
        org.apache.avro.file.FileReader<T_dsgl> t_dsgl_dyRecords = org.apache.avro.file.DataFileReader.openReader(input, tDSglDatumReader);
        while (t_dsgl_dyRecords.hasNext()) {
            T_dsgl record = t_dsgl_dyRecords.next();
            avroRecords.add(record);
        }

        t_dsgl_dyRecords.close();
        System.out.println("succeeded to process file:" + avroFilePath + ". number of records:" + avroRecords.size());

        return avroRecords;
    }

    @Override
    public boolean isMergeOperation() {
        return false;
    }

    @Override
    public boolean isSequenceOperation() {
        return true;
    }

    @Override
    public String getHandlerKey() {
        return "T_DSGL";
    }

    @Override
    public void setPreparedStatement(ResultSet record, Long sequenceId, PreparedStatement preparedStatement) throws SQLException, ParseException {
        System.out.println("Need to fetch new sequence id");

        Long sqlSquenceId = sequenceNextVal.getSequenceNextVal(sequenceNextVal.SELECT_BI_NEW_Q_SQL_ID_NEXTVAL_FROM_DUAL);

        preparedStatement.setLong(1, record.getLong("cl_grp_id"));
        preparedStatement.setLong(2, record.getLong("cl_id"));
        preparedStatement.setLong(3, sqlSquenceId);
        preparedStatement.setString(4, record.getString("sgl_prtcl_cd"));
        preparedStatement.setString(5,record.getString("sgl_fct_tbl_cd"));
        preparedStatement.setTimestamp(6, fileHandlerUtil.getSqlTimeStemp(record.getString("tmst_sgl_first_evt")));
        preparedStatement.setTimestamp(7, fileHandlerUtil.getSqlTimeStemp(record.getString("tmst_sgl_last_evt")));
        preparedStatement.setString(8, record.getString("outdat"));

        String creDate =record.getString("cre_dat");
        String mod =record.getString("mod_dat");
        preparedStatement.setDate(9, fileHandlerUtil.getSqlDate(creDate));
        preparedStatement.setDate(10, fileHandlerUtil.getSqlDate(mod));

        preparedStatement.setLong(11, sequenceId);
        preparedStatement.setTimestamp(12, fileHandlerUtil.getSqlTimeStemp(record.getString("mod_tmst")));
        preparedStatement.setLong(13, record.getLong("dk_cmm"));
        preparedStatement.addBatch();

    }

    @Override
    public void setPreparedStatement(AvroFileRecord avroFileRecords, Long sequenceId, PreparedStatement preparedStatement) throws SQLException, ParseException {
 
    }

}
