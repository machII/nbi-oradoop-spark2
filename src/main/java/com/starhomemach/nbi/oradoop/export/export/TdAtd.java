package com.starhomemach.nbi.oradoop.export.export;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.stereotype.Component;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;

@Component
public class TdAtd {

    public static final String INSERT_INTO_BI_NEW_T_DADT_VALUES = "insert into  BI.T_DADT values (?,?,?,?,?,?,?,?,?,?,?)";
    private final Log logger = LogFactory.getLog(getClass());

    @Autowired
    private JdbcTemplate jdbcTemplate;


    public void insertTdAtdRow(Long clientId, Long groupId, Long sequenceId) throws DataAccessException  {
        logger.info("going to insert new row to table T_DADT for values: client id=" + clientId + " group id=" + groupId + " sequence id=" + sequenceId);
        jdbcTemplate.update(INSERT_INTO_BI_NEW_T_DADT_VALUES, new PreparedStatementSetter() {
            public void setValues(PreparedStatement ps) throws SQLException {
                Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                ps.setLong(1, clientId);
                ps.setLong(2, sequenceId);
                ps.setTimestamp(3, timestamp);
                ps.setString(4, "N");
                ps.setTimestamp(5, timestamp);
                ps.setTimestamp(6, timestamp);
                ps.setLong(7, sequenceId);
                ps.setLong(8, groupId);
                ps.setTimestamp(9, timestamp);
                ps.setInt(10, -1);
                ps.setInt(11, -1);
            }
        });

    }
}
