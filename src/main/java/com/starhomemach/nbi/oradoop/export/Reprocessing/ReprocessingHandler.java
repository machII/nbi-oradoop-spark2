package com.starhomemach.nbi.oradoop.export.Reprocessing;

import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.jdbc.core.JdbcTemplate;

import java.sql.*;
import java.time.LocalDateTime;
import java.time.ZoneId;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Created by xdda on 2016-08-31.
 */
@Service
public class ReprocessingHandler {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public static final String CLEANUP_PROCEDURE = "{ call BI.K_CMM_RPRC.clearAggregates(?, ?, ?) }";
    public static final String DAILY_DATA_REAGGREGATION_PROCEDURE = "{ call BI.K_CMM_RPRC.rebuildDailyAggregates(?, ?, ?) }";

    private final Log logger = LogFactory.getLog(getClass());

    public void callReprocessingProcedure( String statement, Integer[] clientList, LocalDateTime fromDateTime, LocalDateTime toDateTime ) throws Exception
    {
        logger.info("################################### START ###############################################################");

        Timestamp fromDate = new Timestamp(fromDateTime.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli());
        Timestamp toDate = new Timestamp(toDateTime.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli());

        Connection connection = null;
        CallableStatement cstmt = null;

        try {
            connection = jdbcTemplate.getDataSource().getConnection();
            connection.setAutoCommit(false);
            cstmt = connection.prepareCall(statement);

            ArrayDescriptor des = ArrayDescriptor.createDescriptor("MD.NUMBERLIST", connection);
            ARRAY clientIdentifierList = new ARRAY(des, connection, clientList);

            cstmt.setArray (1, clientIdentifierList);
            cstmt.setTimestamp(2, fromDate);
            cstmt.setTimestamp(3, toDate);
            cstmt.execute();
        }
        finally {
            cstmt.close();
            connection.close();
        }

        logger.info("###################################  END  ###############################################################");
    }

    public void deleteBiDataRangeForClients( Integer[] clientList, LocalDateTime fromDateTime, LocalDateTime toDateTime ) throws Exception
    {
        callReprocessingProcedure(CLEANUP_PROCEDURE, clientList, fromDateTime, toDateTime);
    }

    public void rebuildDailyDataFromHourlyData(Integer[] clientList, LocalDateTime fromDateTime, LocalDateTime toDateTime) throws Exception
    {
        callReprocessingProcedure(DAILY_DATA_REAGGREGATION_PROCEDURE, clientList, fromDateTime, toDateTime);
    }
}
