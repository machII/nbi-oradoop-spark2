package com.starhomemach.nbi.oradoop.export.file.filehandler;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.starhomemach.nbi.oradoop.export.file.FileName;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;

@Component
public class FileHandlerFactory {

    private final Log logger = LogFactory.getLog(getClass());

    @Autowired
    public List<FileHandler> fileHandlerList;

    HashMap<String,FileHandler> fileHandlerMap=new HashMap<String,FileHandler>();

    @PostConstruct
    public void init()
    {
        for(FileHandler handler : fileHandlerList)
        {
            fileHandlerMap.put(handler.getHandlerKey().toLowerCase(),handler);
        }
    }

    public FileHandler getFileHandler(String fileName)
    {
        FileHandler fileHandler = fileHandlerMap.get(fileName.toLowerCase());
        logger.info("file handler class for file name:"+fileName+" is: "+fileHandler!=null ?fileHandler.getClass().toString() : " Missing");
        return fileHandler;
    }
}
