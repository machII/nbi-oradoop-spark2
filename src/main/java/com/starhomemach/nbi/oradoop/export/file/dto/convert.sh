#provide avsc file name without avsc suffix
#ensure to have namespace in the avsc already
base_name=$1
upper_name="$(tr '[:lower:]' '[:upper:]' <<< ${base_name:0:1})${base_name:1}"

sed -i '1,4s\"t_\"T_\g' $base_name.avsc
avro-tools compile schema $base_name.avsc .
grep namespace t_fsic_dy.avsc | cut -d'"' -f4 | tr '.' '/' | awk '{print "mv "$1"/* ."}' | bash
rm -fr com
#sed -i '1s/^/import com.starhomemach.nbi.oradoop.export.file.dto.AvroFileRecord;\n/' $upper_name.java
sed -i 's\SpecificRecord {\SpecificRecord,AvroFileRecord {\' $upper_name.java