package com.starhomemach.nbi.oradoop.export.service;

import com.google.common.collect.Lists;
import com.starhomemach.nbi.oradoop.export.file.filehandler.FileHandler;
import com.starhomemach.nbi.oradoop.export.file.dto.AvroFileRecord;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.perf4j.aop.Profiled;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.Map;


@Service
public class ProcessHdfsFile {


    private final Log logger = LogFactory.getLog(getClass());


    @Profiled
    public void processFileName(String fileToProcessPath, FileHandler fileHandler, Map<FileHandler, List<AvroFileRecord>> fileToAvroFiles) throws IOException {
        logger.info("Going to start process the file:" + fileToProcessPath);
        List<AvroFileRecord> avroFiles = fileHandler.readFileToList(fileToProcessPath);
        List<AvroFileRecord> existingFiles = getAvroFileRecords(fileHandler, fileToAvroFiles);
        existingFiles.addAll(avroFiles);
        fileToAvroFiles.put(fileHandler, existingFiles);
    }

    private List<AvroFileRecord> getAvroFileRecords(FileHandler fileHandler, Map<FileHandler, List<AvroFileRecord>> fileToAvroFiles) {
        List<AvroFileRecord> existingFiles = Lists.newArrayList();
        if (fileToAvroFiles.containsKey(fileHandler)) {
            existingFiles = fileToAvroFiles.get(fileHandler);
        }
        return existingFiles;
    }
}
