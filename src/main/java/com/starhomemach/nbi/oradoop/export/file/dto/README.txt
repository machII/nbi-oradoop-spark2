How to create T_dxxx.java using avro-tools:

1. create an avsc file nbi-deployment\deployment\database\schemas\dimension\t_dsvk.avsc in a format like:
{ 
  "type" : "record",
  "name": "t_dsvk",
  "doc": "service_key dimension",
  "fields" : [ {
    "name": "DK_SVK",
    "type": "long", 
    "doc": "service_key ID"
  }, {
    "name": "SVK_CD",
    "type": "long",
    "doc": "service key value"
  } ]
}
2. copy avsc file into edge machine temp dir  (*From here on, next steps can be done automatically using this script: hdfs@isr-poc-1-hdf-1:/tmp/esasson/avsc/convert.sh)
3. edit avsc file and replace the type name attribute to format like: T_dsvk (capital T)
4. change all field names into lower case
5. save and exit, example:
{ 
  "type" : "record",
  "name": "T_dsvk",
  "doc": "service_key dimension",
  "fields" : [ {
    "name": "dk_svk",
    "type": "long", 
    "doc": "service_key ID"
  }, {
    "name": "svk_cd",
    "type": "long",
    "doc": "service key value"
  } ]
}
6. run cmd: avro-tools compile schema t_dsvk.avsc .
7. copy the generated T_dxxx.java file to: nbi-oradoop\src\main\java\com\starhomemach\nbi\oradoop\export\file\dto\
8. edit the file: \nbi-oradoop\src\main\java\com\starhomemach\nbi\oradoop\export\file\dto\T_dsvk.java
9. add class implementation: AvroFileRecord (implements org.apache.avro.specific.SpecificRecord, AvroFileRecord)
10. add to schema parse: "namespace":"com.starhomemach.nbi.oradoop.export.file.dto" between "name" and "doc", e.g:
  public static final org.apache.avro.Schema SCHEMA$ = new org.apache.avro.Schema.Parser().parse("{\"type\":\"record\",\"name\":\"T_dsvk\",\"namespace\":\"com.starhomemach.nbi.oradoop.export.file.dto\",\"doc\":\"service_key dimension\",\"fields\":[{\"name\":\"dk_svk\",\"type\":\"long\",\"doc\":\"service_key ID\"},{\"name\":\"svk_cd\",\"type\":\"long\",\"doc\":\"service key value\"},{\"name\":\"cre_dat\",\"type\":\"string\",\"doc\":\"creation date\"},{\"name\":\"mod_dat\",\"type\":\"string\",\"doc\":\"modification date\"}]}");
11. in T_dsvk.java, implement method getClId as follows:
  @Override
  public Long getClId() {
    return null;
  }
12. make sure T_dxxx package com.starhomemach.nbi.oradoop.export.file.dto;
13. in TDsvkFileHandler.java, import import com.starhomemach.nbi.oradoop.export.file.dto.T_dxxx;