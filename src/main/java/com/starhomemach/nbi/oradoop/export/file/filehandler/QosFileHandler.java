package com.starhomemach.nbi.oradoop.export.file.filehandler;

import com.starhomemach.nbi.oradoop.export.export.QosAggregations.QosAggregationsExportArgumentsHolder;
import org.springframework.beans.factory.annotation.Autowired;

import com.starhomemach.nbi.oradoop.export.file.sequence.QOSSequenceHandler;
import com.starhomemach.nbi.oradoop.export.file.sequence.SequenceHandler;

/**
 * Created by tpin on 09/10/2016.
 */
public abstract class QosFileHandler extends FileHandler{
    @Autowired
    private QOSSequenceHandler sequence;

    public SequenceHandler getSequenceHandler() {
        return sequence;
    }

    @Override
    public boolean isAppendOperation() {
        return false;
    }

    @Override
    public String[] getAppendQuery() {
        return new String[]{""};
    }

    @Override
    public boolean useFinalizeProcedure() { return false; }

    @Override
    public String[] getFinalizeProcedure() { return new String[]{""}; }

    public boolean isBatchMode()
    {
        return ((QosAggregationsExportArgumentsHolder)QosAggregationsExportArgumentsHolder.getInstance()).getBatchSize()!= NO_BATCH;
    }

    public int getBatchSize()
    {
        return ((QosAggregationsExportArgumentsHolder)QosAggregationsExportArgumentsHolder.getInstance()).getBatchSize();
    }
}
