package com.starhomemach.nbi.oradoop.export.file.filehandler;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.apache.avro.file.DataFileReader;
import org.apache.avro.file.SeekableInput;
import org.apache.avro.io.DatumReader;
import org.apache.avro.specific.SpecificDatumReader;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;
import com.starhomemach.nbi.oradoop.export.file.AvroFsInput;
import com.starhomemach.nbi.oradoop.export.file.dto.AvroFileRecord;
import com.starhomemach.nbi.oradoop.export.file.dto.T_fprc_hr;

@Component
public class TFprcHrFileHandler extends QosFileHandler {


    @Autowired
    private FileHandlerUtil fileHandlerUtil;


    private final Log logger = LogFactory.getLog(getClass());

    String MERGE_T_FPRC_DY_WITH_T_FPRC_HR_TMP =
        "MERGE INTO bi.t_fprc_dy d\n" +
        "  USING ( SELECT dk_cmm, cl_id, dk_dat, dk_dir, dk_org_snw, dk_org_hnw, dk_org_cnp, dk_pet, dk_spe, dk_sgs,\n" +
        "                 MAX(dk_adt) dk_adt, SUM(prc_atpt) prc_atpt, SUM(prc_succ) prc_succ, SUM(prc_fail) prc_fail, SUM(prc_dly) prc_dly\n" +
        "            FROM bi.hdp_t_fprc_hr_tmp\n" +
        "           GROUP BY dk_cmm, cl_id, dk_org_snw, dk_org_hnw, dk_org_cnp, dk_dir, dk_dat, dk_spe, dk_pet, dk_sgs ) temp\n" +
        "     ON ( d.dk_dat = temp.dk_dat\n" +
        "          AND d.cl_id = temp.cl_id\n" +
        "          AND d.dk_cmm = temp.dk_cmm\n" +
        "          AND d.dk_org_snw = temp.dk_org_snw\n" +
        "          AND d.dk_org_hnw = temp.dk_org_hnw\n" +
        "          AND d.dk_org_cnp = temp.dk_org_cnp\n" +
        "          AND d.dk_dir = temp.dk_dir\n" +
        "          AND d.dk_spe = temp.dk_spe\n" +
        "          AND d.dk_pet = temp.dk_pet\n" +
        "          AND d.dk_sgs = temp.dk_sgs )\n" +
        "  WHEN MATCHED THEN\n" +
        "    UPDATE SET d.prc_succ = d.prc_succ + temp.prc_succ,\n" +
        "               d.prc_atpt = d.prc_atpt + temp.prc_atpt,\n" +
        "               d.prc_fail = d.prc_fail + temp.prc_fail,\n" +
        "               d.prc_dly = d.prc_dly + temp.prc_dly,\n" +
        "               d.dk_adt = temp.dk_adt\n" +
        "  WHEN NOT MATCHED THEN\n" +
        "    INSERT ( cl_id, dk_cmm, dk_org_snw, dk_org_hnw, dk_org_cnp, dk_dir,\n" +
        "             dk_adt, dk_dat, dk_spe, dk_pet, dk_sgs, prc_atpt, prc_succ, prc_fail, prc_dly )\n" +
        "      VALUES ( temp.cl_id, temp.dk_cmm, temp.dk_org_snw, temp.dk_org_hnw, temp.dk_org_cnp, temp.dk_dir,\n" +
        "               temp.dk_adt, temp.dk_dat, temp.dk_spe, temp.dk_pet, temp.dk_sgs, temp.prc_atpt, temp.prc_succ, temp.prc_fail, temp.prc_dly )";

    @Override
    public String[] getInsertQuery() {
        String INSERT_INTO_T_FPRC_HR_TMP = "insert into BI.HDP_T_FPRC_HR_TMP values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        return new String[]{INSERT_INTO_T_FPRC_HR_TMP};
    }

    @Override
    public String[] getMergeQuery() {
        String MERGE_T_FPRC_HR_WITH_T_FPRC_HR_TMP =
            "MERGE INTO bi.t_fprc_hr d\n" +
            "  USING ( SELECT * FROM bi.hdp_t_fprc_hr_tmp ) temp\n" +
            "     ON ( d.dk_dat = temp.dk_dat\n" +
            "          AND d.cl_id = temp.cl_id\n" +
            "          AND d.dk_cmm = temp.dk_cmm\n" +
            "          AND d.dk_org_snw = temp.dk_org_snw\n" +
            "          AND d.dk_org_hnw = temp.dk_org_hnw\n" +
            "          AND d.dk_org_cnp = temp.dk_org_cnp\n" +
            "          AND d.dk_dir = temp.dk_dir\n" +
            "          AND d.dk_tim_hr = temp.dk_tim_hr\n" +
            "          AND d.dk_spe = temp.dk_spe\n" +
            "          AND d.dk_pet = temp.dk_pet\n" +
            "          AND d.dk_sgs = temp.dk_sgs )\n" +
            "  WHEN MATCHED THEN\n" +
            "    UPDATE SET d.prc_succ = d.prc_succ + temp.prc_succ,\n" +
            "               d.prc_atpt = d.prc_atpt + temp.prc_atpt,\n" +
            "               d.prc_fail = d.prc_fail + temp.prc_fail,\n" +
            "               d.prc_dly = d.prc_dly + temp.prc_dly,\n" +
            "               d.dk_adt = temp.dk_adt\n" +
            "  WHEN NOT MATCHED THEN\n" +
            "    INSERT ( cl_id, dk_cmm, dk_org_snw, dk_org_hnw, dk_org_cnp, dk_dir,\n" +
            "             dk_adt, dk_dat, dk_tim_hr, dk_spe, dk_pet, dk_sgs, prc_atpt, prc_succ, prc_fail, prc_dly )\n" +
            "      VALUES ( temp.cl_id, temp.dk_cmm, temp.dk_org_snw, temp.dk_org_hnw, temp.dk_org_cnp, temp.dk_dir, temp.dk_adt," +
            "               temp.dk_dat, temp.dk_tim_hr, temp.dk_spe, temp.dk_pet, temp.dk_sgs, temp.prc_atpt, temp.prc_succ, temp.prc_fail, temp.prc_dly)";

        return fileHandlerUtil.getMergeStatmentsArray(MERGE_T_FPRC_HR_WITH_T_FPRC_HR_TMP, MERGE_T_FPRC_DY_WITH_T_FPRC_HR_TMP);
    }

    @Override
    public String[] getAppendQuery() {
        String APPEND_INTO_T_FPRC_HR_FROM_T_FPRC_HR_TMP =
            "INSERT /*+ APPEND */ INTO bi.t_fprc_hr\n" +
            "       ( dk_cmm, cl_id, dk_dat, dk_tim_hr, dk_dir, dk_org_cnp, dk_org_snw, dk_org_hnw, dk_pet, dk_spe,\n" +
            "         dk_sgs, dk_adt, prc_atpt, prc_succ, prc_fail, prc_dly )\n" +
            "  SELECT dk_cmm, cl_id, dk_dat, dk_tim_hr, dk_dir, dk_org_cnp, dk_org_snw, dk_org_hnw, dk_pet, dk_spe,\n" +
            "         dk_sgs, dk_adt, prc_atpt, prc_succ, prc_fail, prc_dly\n" +
            "    FROM bi.hdp_t_fprc_hr_tmp";

        return fileHandlerUtil.getAppendStatmentsArray(APPEND_INTO_T_FPRC_HR_FROM_T_FPRC_HR_TMP, MERGE_T_FPRC_DY_WITH_T_FPRC_HR_TMP);
    }

    @Override
    public List<AvroFileRecord> readFileToList(String avroFilePath) throws IOException {
        List<AvroFileRecord> avroRecords = Lists.newArrayList();
        Path path = new Path(avroFilePath);
        Configuration config = new Configuration();
        SeekableInput input = new AvroFsInput(path, config);
        DatumReader<T_fprc_hr> tFprcHrDatumReader = new SpecificDatumReader<T_fprc_hr>(T_fprc_hr.class);
        org.apache.avro.file.FileReader<T_fprc_hr> t_fprc_hrRecords = DataFileReader.openReader(input, tFprcHrDatumReader);
        while (t_fprc_hrRecords.hasNext()) {
            T_fprc_hr record = t_fprc_hrRecords.next();
            avroRecords.add(record);
        }
        t_fprc_hrRecords.close();
        logger.info("succeeded to process file:" + avroFilePath + ". number of records:" + avroRecords.size());
        return avroRecords;
    }


    public void setPreparedStatement(AvroFileRecord avroFileRecords, Long sequenceId, PreparedStatement preparedStatement) throws SQLException {

        logger.info("going to set PreparedStatement for record:" + avroFileRecords);
        T_fprc_hr tFsprHrRecord = (T_fprc_hr) avroFileRecords;

        preparedStatement.setLong(1, tFsprHrRecord.getDkCmm());
        preparedStatement.setLong(2, tFsprHrRecord.getClId());
        preparedStatement.setLong(3, tFsprHrRecord.getDkDat());
        preparedStatement.setLong(4, tFsprHrRecord.getDkTimHr());
        preparedStatement.setLong(5, tFsprHrRecord.getDkDir());
        preparedStatement.setLong(6, tFsprHrRecord.getDkOrgHnw());
        preparedStatement.setLong(7, tFsprHrRecord.getDkOrgSnw());
        preparedStatement.setLong(8, tFsprHrRecord.getDkOrgCnp());
        preparedStatement.setLong(9, tFsprHrRecord.getDkPet());
        preparedStatement.setLong(10, tFsprHrRecord.getDkSpe());
        preparedStatement.setLong(11, tFsprHrRecord.getDkSgs());
        preparedStatement.setLong(12, sequenceId);
        fileHandlerUtil.setPreparedStatementIfNullOrLong(13, tFsprHrRecord.getPrcAtpt(), preparedStatement);
        fileHandlerUtil.setPreparedStatementIfNullOrLong(14, tFsprHrRecord.getPrcSucc(), preparedStatement);
        fileHandlerUtil.setPreparedStatementIfNullOrLong(15, tFsprHrRecord.getPrcFail(), preparedStatement);
        fileHandlerUtil.setPreparedStatementIfNullOrDouble(16, tFsprHrRecord.getPrcDly(), preparedStatement);
        

        preparedStatement.addBatch();
    }

    @Override
    public boolean isSequenceOperation() {
        return true;
    }

    @Override
    public String getHandlerKey() {
        return "T_FPRC_HR";
    }

    @Override
    public boolean isMergeOperation() {
        return true;
    }

    @Override
    public boolean isAppendOperation() {
        return false;
    }

}
