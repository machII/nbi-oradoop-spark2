package com.starhomemach.nbi.oradoop.export.file.filehandler;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.apache.avro.file.DataFileReader;
import org.apache.avro.file.SeekableInput;
import org.apache.avro.io.DatumReader;
import org.apache.avro.specific.SpecificDatumReader;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;
import com.starhomemach.nbi.oradoop.export.file.AvroFsInput;
import com.starhomemach.nbi.oradoop.export.file.dto.AvroFileRecord;
import com.starhomemach.nbi.oradoop.export.file.dto.T_fluc_hr;

@Component
public class TFlucHrFileHandler extends QosFileHandler {

    @Autowired
    private FileHandlerUtil fileHandlerUtil;

    private final Log logger = LogFactory.getLog(getClass());

    @Override
    public String[] getInsertQuery() {
        String INSERT_INTO_T_FLUC_HR_TMP = "INSERT INTO bi.hdp_t_fluc_hr_tmp VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        return new String[]{INSERT_INTO_T_FLUC_HR_TMP};
    }

    private final String MERGE_T_FLUC_DY_WITH_T_FLUC_HR_TMP =
        "MERGE INTO bi.t_fluc_dy d\n" +
        "  USING (SELECT dk_cmm, cl_id, dk_dat, dk_dir, dk_org_snw, dk_org_hnw, dk_org_cnp, dk_pet, dk_spe, dk_svk, dk_sgs,\n" +
        "                MAX(dk_adt) dk_adt, SUM(luc_atpt) luc_atpt, SUM(luc_succ) luc_succ, SUM(luc_fail) luc_fail, SUM(luc_dly) luc_dly\n" +
        "           FROM bi.hdp_t_fluc_hr_tmp\n" +
        "          GROUP BY dk_cmm, cl_id, dk_dir, dk_dat, dk_org_snw, dk_org_hnw, dk_org_cnp, dk_pet, dk_spe, dk_svk, dk_sgs ) temp\n" +
        "     ON ( d.DK_DAT = temp.DK_DAT\n" +
        "          AND d.cl_id = temp.cl_id\n" +
        "          AND d.dk_cmm = temp.dk_cmm\n" +
        "          AND d.dk_org_snw = temp.dk_org_snw\n" +
        "          AND d.dk_org_hnw = temp.dk_org_hnw\n" +
        "          AND d.dk_org_cnp = temp.dk_org_cnp\n" +
        "          AND d.dk_dir = temp.dk_dir\n" +
        "          AND d.dk_spe = temp.dk_spe\n" +
        "          AND d.dk_pet = temp.dk_pet\n" +
        "          AND d.dk_sgs = temp.dk_sgs\n" +
        "          AND d.dk_svk = temp.dk_svk )\n" +
        "  WHEN MATCHED THEN\n" +
        "    UPDATE SET d.luc_succ = d.luc_succ + temp.luc_succ,\n" +
        "               d.luc_atpt = d.luc_atpt + temp.luc_atpt,\n" +
        "               d.luc_fail = d.luc_fail + temp.luc_fail,\n" +
        "               d.luc_dly = d.luc_dly + temp.luc_dly,\n" +
        "               d.dk_adt = temp.dk_adt\n" +
        "  WHEN NOT MATCHED THEN\n" +
        "    INSERT ( cl_id, dk_cmm, dk_org_snw, dk_org_hnw, dk_org_cnp, dk_dir,\n" +
        "             dk_adt, dk_dat, dk_spe, dk_pet, dk_sgs, dk_svk, luc_atpt,\n" +
        "             luc_succ, luc_fail, luc_dly )\n" +
        "      VALUES ( temp.cl_id, temp.dk_cmm, temp.dk_org_snw, temp.dk_org_hnw, temp.dk_org_cnp, temp.dk_dir,\n" +
        "               temp.dk_adt, temp.dk_dat, temp.dk_spe, temp.dk_pet, temp.dk_sgs, temp.dk_svk, temp.luc_atpt,\n" +
        "               temp.luc_succ, temp.luc_fail, temp.luc_dly )";

    @Override
    public String[] getMergeQuery() {
        String MERGE_T_FLUC_HR_TMP_WITH_T_FLUC_HR_TMP =
            "MERGE INTO bi.t_fluc_hr d\n" +
            "  USING ( SELECT * FROM bi.hdp_t_fluc_hr_tmp ) temp\n" +
            "     ON ( d.dk_dat = temp.dk_dat\n" +
            "          AND d.cl_id = temp.cl_id\n" +
            "          AND d.dk_cmm = temp.dk_cmm\n" +
            "          AND d.dk_org_snw = temp.dk_org_snw\n" +
            "          AND d.dk_org_hnw = temp.dk_org_hnw\n" +
            "          AND d.dk_org_cnp = temp.dk_org_cnp\n" +
            "          AND d.dk_dir = temp.dk_dir\n" +
            "          AND d.dk_spe = temp.dk_spe\n" +
            "          AND d.dk_pet = temp.dk_pet\n" +
            "          AND d.dk_sgs = temp.dk_sgs\n" +
            "          AND d.dk_svk = temp.dk_svk\n" +
            "          AND d.dk_tim_hr = temp.dk_tim_hr )\n" +
            "  WHEN MATCHED THEN\n" +
            "    UPDATE SET d.luc_succ = d.luc_succ + temp.luc_succ,\n" +
            "               d.luc_atpt = d.luc_atpt + temp.luc_atpt,\n" +
            "               d.luc_fail = d.luc_fail + temp.luc_fail,\n" +
            "               d.luc_dly = d.luc_dly + temp.luc_dly,\n" +
            "               d.dk_adt = temp.dk_adt\n" +
            "  WHEN NOT MATCHED THEN\n" +
            "    INSERT ( dk_cmm, cl_id, dk_org_snw, dk_org_hnw, dk_org_cnp, dk_dir,\n" +
            "             dk_adt, dk_dat, dk_tim_hr, dk_spe, dk_pet, dk_sgs, dk_svk,\n" +
            "             luc_atpt, luc_succ, luc_fail, luc_dly )\n" +
            "      VALUES ( temp.dk_cmm, temp.cl_id, temp.dk_org_snw, temp.dk_org_hnw, temp.dk_org_cnp, temp.dk_dir,\n" +
            "               temp.dk_adt, temp.dk_dat, temp.dk_tim_hr, temp.dk_spe, temp.dk_pet, temp.dk_sgs, temp.dk_svk,\n" +
            "               temp.luc_atpt, temp.luc_succ, temp.luc_fail, temp.luc_dly )";

        return fileHandlerUtil.getMergeStatmentsArray(MERGE_T_FLUC_HR_TMP_WITH_T_FLUC_HR_TMP, MERGE_T_FLUC_DY_WITH_T_FLUC_HR_TMP);
    }

    @Override
    public String[] getAppendQuery() {
        String APPEND_INTO_T_FLUC_HR_FROM_T_FLUC_HR_TMP =
            "INSERT /*+ APPEND */ INTO bi.t_fluc_hr\n" +
            "       ( dk_cmm, cl_id, dk_dat, dk_tim_hr, dk_dir, dk_org_snw, dk_org_hnw, dk_org_cnp, dk_pet, dk_spe,\n" +
            "         dk_svk, dk_sgs, dk_adt, luc_atpt, luc_succ, luc_fail, luc_dly )\n" +
            "  SELECT dk_cmm, cl_id, dk_dat, dk_tim_hr, dk_dir, dk_org_snw, dk_org_hnw, dk_org_cnp, dk_pet, dk_spe,\n" +
            "         dk_svk, dk_sgs, dk_adt, luc_atpt, luc_succ, luc_fail, luc_dly\n" +
            "    FROM bi.hdp_t_fluc_hr_tmp";

        return fileHandlerUtil.getAppendStatmentsArray(APPEND_INTO_T_FLUC_HR_FROM_T_FLUC_HR_TMP, MERGE_T_FLUC_DY_WITH_T_FLUC_HR_TMP);
    }

    @Override
    public List<AvroFileRecord> readFileToList(String avroFilePath) throws IOException {
        List<AvroFileRecord> avroRecords = Lists.newArrayList();
        Path path = new Path(avroFilePath);
        Configuration config = new Configuration();
        SeekableInput input = new AvroFsInput(path, config);
        DatumReader<T_fluc_hr> tFlucHrDatumReader = new SpecificDatumReader<T_fluc_hr>(T_fluc_hr.class);
        org.apache.avro.file.FileReader<T_fluc_hr> t_fluc_hrRecords = DataFileReader.openReader(input, tFlucHrDatumReader);
        while (t_fluc_hrRecords.hasNext()) {
            T_fluc_hr record = t_fluc_hrRecords.next();
            avroRecords.add(record);
        }
        t_fluc_hrRecords.close();
        logger.info("succeeded to process file:" + avroFilePath + ". number of records:" + avroRecords.size());
        return avroRecords;
    }


    public void setPreparedStatement(AvroFileRecord avroFileRecords, Long sequenceId, PreparedStatement preparedStatement) throws SQLException {

        logger.info("going to set PreparedStatement for record:" + avroFileRecords);
        T_fluc_hr tFlucHrRecord = (T_fluc_hr) avroFileRecords;

        preparedStatement.setLong(1, tFlucHrRecord.getDkCmm());
        preparedStatement.setLong(2, tFlucHrRecord.getClId());
        preparedStatement.setLong(3, tFlucHrRecord.getDkDat());
        preparedStatement.setLong(4, tFlucHrRecord.getDkTimHr());
        preparedStatement.setLong(5, tFlucHrRecord.getDkDir());
        preparedStatement.setLong(6, tFlucHrRecord.getDkOrgSnw());
        preparedStatement.setLong(7, tFlucHrRecord.getDkOrgHnw());
        preparedStatement.setLong(8, tFlucHrRecord.getDkOrgCnp());
        preparedStatement.setLong(9, tFlucHrRecord.getDkPet());
        preparedStatement.setLong(10, tFlucHrRecord.getDkSpe());
        preparedStatement.setLong(11, tFlucHrRecord.getDkSgs());
        preparedStatement.setLong(12, sequenceId);
        preparedStatement.setLong(13, tFlucHrRecord.getLucAtpt());
        preparedStatement.setLong(14, tFlucHrRecord.getLucSucc());
        preparedStatement.setLong(15, tFlucHrRecord.getLucFail());
        Double sluDly = tFlucHrRecord.getLucDly();
        if (sluDly == null)
            sluDly = new Double(-1);
        preparedStatement.setDouble(16, sluDly);
        preparedStatement.setLong(17, tFlucHrRecord.getDkSvk());

        preparedStatement.addBatch();

    }

    @Override
    public boolean isSequenceOperation() {
        return true;
    }

    @Override
    public String getHandlerKey() {
        return "T_FLUC_HR";
    }

    @Override
    public boolean isMergeOperation() {
        return true;
    }

    @Override
    public boolean isAppendOperation() {
        return false;
    }

}
