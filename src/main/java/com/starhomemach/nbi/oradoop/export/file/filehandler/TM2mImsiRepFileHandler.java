package com.starhomemach.nbi.oradoop.export.file.filehandler;

/**
 * Created by armb on 17-09-2016.
 */

import com.starhomemach.nbi.oradoop.export.file.dto.m2m.T_m2m_imsi_rep_sgl;
import org.springframework.stereotype.Component;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;

import com.google.common.collect.Lists;
import com.starhomemach.nbi.oradoop.export.file.AvroFsInput;
import com.starhomemach.nbi.oradoop.export.file.dto.AvroFileRecord;



@Component
public class TM2mImsiRepFileHandler extends M2MFileHandler {
    private final Log logger = LogFactory.getLog(getClass());

    @Override
    public String[] getInsertQuery() {
        String INSERT_INTO_T_M2M_IMSI_REP_SGL = "insert into M2M.HDP_T_M2M_IMSI_REP_SGL_TMP values(?,?,?,?,?,?)";
        return new String[]{INSERT_INTO_T_M2M_IMSI_REP_SGL};
    }

    @Override
    public String[] getMergeQuery() {
        String MERGE_T_M2M_IMSI_REP_SGL_WITH_T_FISC_TMP = "MERGE INTO M2M.T_M2M_IMSI_REP_SGL DY \n" +
                "USING (SELECT DISTINCT PE_ID, RANGE_ID,CRT_TYPE,IMSI,CL_ID,DK_DIR FROM M2M.HDP_T_M2M_IMSI_REP_SGL_TMP) temp \n" +
                "ON (\n" +
                "DY.RANGE_ID = temp.RANGE_ID AND\n" +
                "DY.CRT_TYPE = temp.CRT_TYPE AND\n" +
                "DY.IMSI = temp.IMSI AND\n" +
                "DY.CL_ID  = temp.CL_ID )\n" +
                "WHEN NOT MATCHED THEN INSERT (PE_ID, RANGE_ID, CRT_TYPE, IMSI, CL_ID, DK_DIR)\n" +
                "VALUES  (temp.PE_ID, temp.RANGE_ID, temp.CRT_TYPE,  temp.IMSI, temp.CL_ID, temp.DK_DIR )";
        return new String[]{MERGE_T_M2M_IMSI_REP_SGL_WITH_T_FISC_TMP};
    }

    @Override
    public List<AvroFileRecord> readFileToList(String avroFilePath) throws IOException {
        List<AvroFileRecord> avroRecords = Lists.newArrayList();
        Path path = new Path(avroFilePath);
        Configuration config = new Configuration();
        org.apache.avro.file.SeekableInput input = new AvroFsInput(path, config);
        org.apache.avro.io.DatumReader<T_m2m_imsi_rep_sgl> tImsiDatumReader = new org.apache.avro.specific.SpecificDatumReader<T_m2m_imsi_rep_sgl>(T_m2m_imsi_rep_sgl.class);
        org.apache.avro.file.FileReader<T_m2m_imsi_rep_sgl> t_imsiRecords = org.apache.avro.file.DataFileReader.openReader(input, tImsiDatumReader);
        while (t_imsiRecords.hasNext()) {
            T_m2m_imsi_rep_sgl record = t_imsiRecords.next();
            avroRecords.add(record);
        }

        t_imsiRecords.close();
        logger.info("succeeded to process file:" + avroFilePath + ". number of records:" + avroRecords.size());

        return avroRecords;
    }

    @Override
    public boolean isMergeOperation() {
        return true;
    }

    @Override
    public boolean isSequenceOperation() {
        return false;
    }

    @Override
    public String getHandlerKey() {
        return "T_M2M_IMSI_REP_SGL";
    }


    public void setPreparedStatement(AvroFileRecord avroFileRecords, Long sequenceId, PreparedStatement preparedStatement) throws SQLException, ParseException {
        logger.info("going to set PreparedStatement for record:" + avroFileRecords);
        T_m2m_imsi_rep_sgl tImsiRecord = (T_m2m_imsi_rep_sgl) avroFileRecords;

        String crt_type = tImsiRecord.getCrtType().toString();
        String pe_id =  tImsiRecord.getPeId().toString();

        preparedStatement.setLong(1, sequenceId);
        preparedStatement.setLong(2, tImsiRecord.getRangeId());
        preparedStatement.setString(3, crt_type);
        preparedStatement.setLong(4, tImsiRecord.getImsi());
        preparedStatement.setLong(5, tImsiRecord.getClId());
        preparedStatement.setLong(6, tImsiRecord.getDkDir());
        preparedStatement.addBatch();
    }
}
