package com.starhomemach.nbi.oradoop.export.file.filehandler;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;
import com.starhomemach.nbi.oradoop.export.file.AvroFsInput;
import com.starhomemach.nbi.oradoop.export.file.dto.AvroFileRecord;
import com.starhomemach.nbi.oradoop.export.file.dto.T_fsic_dy;

@Component
public class TfsicDyFileHandler extends QosFileHandler {

    private final Log logger = LogFactory.getLog(getClass());

    @Autowired
    private FileHandlerUtil fileHandlerUtil;

    private final String INSERT_INTO_T_FSIC_DY_TMP = "INSERT INTO bi.hdp_t_fsic_dy_tmp VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    private final String MERGE_T_FSIC_DY_WITH_T_FSIC_DY_TMP =
        "MERGE INTO bi.t_fsic_dy dy\n" +
        "  USING ( SELECT * FROM bi.hdp_t_fsic_dy_tmp ) temp\n" +
        "     ON ( dy.dk_dat = temp.dk_dat\n" +
        "          AND dy.cl_id = temp.cl_id\n" +
        "          AND dy.dk_cmm = temp.dk_cmm\n" +
        "          AND dy.dk_dir = temp.dk_dir\n" +
        "          AND dy.dk_pet = temp.dk_pet\n" +
        "          AND dy.dk_org_cy = temp.dk_org_cy )\n" +
        "  WHEN MATCHED THEN\n" +
        "    UPDATE SET dy.sic_cnt = dy.sic_cnt + temp.sic_cnt,\n" +
        "               dy.sic_dly = dy.sic_dly + temp.sic_dly,\n" +
        "               dy.sic_dly_bnd_1_cnt = dy.sic_dly_bnd_1_cnt + temp.sic_dly_bnd_1_cnt,\n" +
        "               dy.sic_dly_bnd_2_cnt = dy.sic_dly_bnd_2_cnt + temp.sic_dly_bnd_2_cnt,\n" +
        "               dy.sic_dly_bnd_3_cnt = dy.sic_dly_bnd_3_cnt + temp.sic_dly_bnd_3_cnt,\n" +
        "               dy.sic_dly_bnd_4_cnt = dy.sic_dly_bnd_4_cnt + temp.sic_dly_bnd_4_cnt,\n" +
        "               dy.sic_dly_bnd_5_cnt = dy.sic_dly_bnd_5_cnt + temp.sic_dly_bnd_5_cnt,\n" +
        "               dy.sic_dly_bnd_6_cnt = dy.sic_dly_bnd_6_cnt + temp.sic_dly_bnd_6_cnt,\n" +
        "               dy.sic_dly_bnd_7_cnt = dy.sic_dly_bnd_7_cnt + temp.sic_dly_bnd_7_cnt,\n" +
        "               dy.sic_dly_bnd_8_cnt = dy.sic_dly_bnd_8_cnt + temp.sic_dly_bnd_8_cnt,\n" +
        "               dy.sic_dly_bnd_9_cnt = dy.sic_dly_bnd_9_cnt + temp.sic_dly_bnd_9_cnt,\n" +
        "               dy.sic_dly_bnd_10_cnt = dy.sic_dly_bnd_10_cnt + temp.sic_dly_bnd_10_cnt,\n" +
        "               dy.sic_dly_bnd_oth_cnt = dy.sic_dly_bnd_oth_cnt + temp.sic_dly_bnd_oth_cnt,\n" +
        "               dy.sic_nw_redir_all  = dy.sic_nw_redir_all  + temp.sic_nw_redir_all, \n"+
        "               dy.sic_nw_redir_succ = dy.sic_nw_redir_succ + temp.sic_nw_redir_succ,\n" +
        "               dy.dk_adt = temp.dk_adt\n" +
        "  WHEN NOT MATCHED THEN\n" +
        "    INSERT ( dy.cl_id, dy.dk_cmm, dy.dk_dir, dy.dk_adt, dy.dk_dat, dy.dk_pet,\n" +
        "             dy.dk_org_cy, dy.sic_cnt, dy.sic_dly, dy.sic_dly_bnd_1_cnt, dy.sic_dly_bnd_2_cnt,\n" +
        "             dy.sic_dly_bnd_3_cnt, dy.sic_dly_bnd_4_cnt, dy.sic_dly_bnd_5_cnt,dy.sic_dly_bnd_6_cnt, dy.sic_dly_bnd_7_cnt,\n" +
        "             dy.sic_dly_bnd_8_cnt, dy.sic_dly_bnd_9_cnt, dy.sic_dly_bnd_10_cnt, dy.sic_dly_bnd_oth_cnt, dy.sic_nw_redir_all, dy.sic_nw_redir_succ)\n" +
        "      VALUES ( temp.cl_id,temp.dk_cmm, temp.dk_dir, temp.dk_adt, temp.dk_dat, temp.dk_pet, temp.dk_org_cy, temp.sic_cnt, temp.sic_dly,\n"+
        "               temp.sic_dly_bnd_1_cnt, temp.sic_dly_bnd_2_cnt, temp.sic_dly_bnd_3_cnt, temp.sic_dly_bnd_4_cnt,\n" +
        "               temp.sic_dly_bnd_5_cnt, temp.sic_dly_bnd_6_cnt, temp.sic_dly_bnd_7_cnt, temp.sic_dly_bnd_8_cnt, temp.sic_dly_bnd_9_cnt,\n" +
        "               temp.sic_dly_bnd_10_cnt, temp.sic_dly_bnd_oth_cnt, temp.sic_nw_redir_all, temp.sic_nw_redir_succ )";

    private final String APPEND_INTO_T_FSIC_DY_FROM_T_FSIC_DY_TMP =
        "INSERT /*+ APPEND */ INTO bi.t_fsic_dy\n" +
        "       ( dk_cmm, cl_id, dk_dir, dk_dat, dk_pet, dk_org_cy, dk_adt, sic_dly, sic_cnt,\n" +
        "         sic_dly_bnd_1_cnt, sic_dly_bnd_2_cnt, sic_dly_bnd_3_cnt, sic_dly_bnd_4_cnt,\n" +
        "         sic_dly_bnd_5_cnt, sic_dly_bnd_6_cnt, sic_dly_bnd_7_cnt, sic_dly_bnd_8_cnt,\n" +
        "         sic_dly_bnd_9_cnt, sic_dly_bnd_10_cnt, sic_dly_bnd_oth_cnt, sic_nw_redir_all, sic_nw_redir_succ)\n" +
        "  SELECT dk_cmm, cl_id, dk_dir, dk_dat, dk_pet, dk_org_cy, dk_adt, sic_dly, sic_cnt,\n" +
        "         sic_dly_bnd_1_cnt, sic_dly_bnd_2_cnt, sic_dly_bnd_3_cnt, sic_dly_bnd_4_cnt,\n" +
        "         sic_dly_bnd_5_cnt, sic_dly_bnd_6_cnt, sic_dly_bnd_7_cnt, sic_dly_bnd_8_cnt,\n" +
        "         sic_dly_bnd_9_cnt, sic_dly_bnd_10_cnt, sic_dly_bnd_oth_cnt, sic_nw_redir_all, sic_nw_redir_succ\n" +
        "   FROM bi.hdp_t_fsic_dy_tmp";

    @Override
    public String[] getInsertQuery() {
        return new String[]{INSERT_INTO_T_FSIC_DY_TMP};
    }

    @Override
    public String[] getMergeQuery() {
        return new String[]{MERGE_T_FSIC_DY_WITH_T_FSIC_DY_TMP};
    }

    @Override
    public String[] getAppendQuery() {
        return new String[]{APPEND_INTO_T_FSIC_DY_FROM_T_FSIC_DY_TMP};
    }

    @Override
    public List<AvroFileRecord> readFileToList(String avroFilePath) throws IOException {
        List<AvroFileRecord> avroRecords = Lists.newArrayList();
        Path path = new Path(avroFilePath);
        Configuration config = new Configuration();
        org.apache.avro.file.SeekableInput input = new AvroFsInput(path, config);
        org.apache.avro.io.DatumReader<T_fsic_dy> tFSicDyDatumReader = new org.apache.avro.specific.SpecificDatumReader<T_fsic_dy>(T_fsic_dy.class);
        org.apache.avro.file.FileReader<T_fsic_dy> T_fsic_dyRecords = org.apache.avro.file.DataFileReader.openReader(input, tFSicDyDatumReader);
        while (T_fsic_dyRecords.hasNext()) {
            T_fsic_dy record = T_fsic_dyRecords.next();
            avroRecords.add(record);
        }
        T_fsic_dyRecords.close();
        logger.info("succeeded to process file:" + avroFilePath + ". number of records:" + avroRecords.size());
        return avroRecords;
    }


    public void setPreparedStatement(AvroFileRecord avroFileRecords, Long sequenceId, PreparedStatement preparedStatement) throws SQLException {
        logger.info("going to set PreparedStatement for record:" + avroFileRecords);
        T_fsic_dy tFsicDyRecord = (T_fsic_dy) avroFileRecords;
        preparedStatement.setLong(1, tFsicDyRecord.getDkCmm());
        preparedStatement.setLong(2, tFsicDyRecord.getClId());
        preparedStatement.setLong(3, tFsicDyRecord.getDkDir());
        preparedStatement.setLong(4, tFsicDyRecord.getDkDat());
        preparedStatement.setLong(5, tFsicDyRecord.getDkPet());
        preparedStatement.setLong(6, tFsicDyRecord.getDkOrgCy()); 
        preparedStatement.setLong(7, sequenceId);
        fileHandlerUtil.setPreparedStatementIfNullOrDouble(8, tFsicDyRecord.getSicDly(), preparedStatement);
        fileHandlerUtil.setPreparedStatementIfNullOrLong(9, tFsicDyRecord.getSicCnt(), preparedStatement);
        fileHandlerUtil.setPreparedStatementIfNullOrLong(10, tFsicDyRecord.getSicDlyBnd1Cnt(), preparedStatement);
        fileHandlerUtil.setPreparedStatementIfNullOrLong(11, tFsicDyRecord.getSicDlyBnd2Cnt(), preparedStatement);
        fileHandlerUtil.setPreparedStatementIfNullOrLong(12, tFsicDyRecord.getSicDlyBnd3Cnt(), preparedStatement);
        fileHandlerUtil.setPreparedStatementIfNullOrLong(13, tFsicDyRecord.getSicDlyBnd4Cnt(), preparedStatement);
        fileHandlerUtil.setPreparedStatementIfNullOrLong(14, tFsicDyRecord.getSicDlyBnd5Cnt(), preparedStatement);
        fileHandlerUtil.setPreparedStatementIfNullOrLong(15, tFsicDyRecord.getSicDlyBnd6Cnt(), preparedStatement);
        fileHandlerUtil.setPreparedStatementIfNullOrLong(16, tFsicDyRecord.getSicDlyBnd7Cnt(), preparedStatement);
        fileHandlerUtil.setPreparedStatementIfNullOrLong(17, tFsicDyRecord.getSicDlyBnd8Cnt(), preparedStatement);
        fileHandlerUtil.setPreparedStatementIfNullOrLong(18, tFsicDyRecord.getSicDlyBnd9Cnt(), preparedStatement);
        fileHandlerUtil.setPreparedStatementIfNullOrLong(19, tFsicDyRecord.getSicDlyBnd10Cnt(), preparedStatement);
        fileHandlerUtil.setPreparedStatementIfNullOrLong(20, tFsicDyRecord.getSicDlyBndOthCnt(), preparedStatement);
        fileHandlerUtil.setPreparedStatementIfNullOrLong(21, tFsicDyRecord.getSicNwRedirAll(), preparedStatement);
        fileHandlerUtil.setPreparedStatementIfNullOrLong(22, tFsicDyRecord.getSicNwRedirSucc(), preparedStatement);

        preparedStatement.addBatch();

    }

    @Override
    public boolean isSequenceOperation() {
        return true;
    }

    @Override
    public String getHandlerKey() {
        return "T_FSIC_DY";
    }

    @Override
    public boolean isMergeOperation() {
        return true;
    }
}
