package com.starhomemach.nbi.oradoop.export.file;

public enum FileName {
    T_FLUC_DY, T_FLUC_HR, T_FSAC_HR, T_FSAC_DY, T_FGCC_HR, T_FGCC_DY, T_DSGL, T_DSPE, T_FPRC_DY, T_FPRC_HR, T_FSIC_DY, T_FISC, T_M2M_IMSI_REP_SGL
}
