package com.starhomemach.nbi.oradoop.export.file.filehandler;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;
import com.starhomemach.nbi.oradoop.export.export.SequenceNextVal;
import com.starhomemach.nbi.oradoop.export.file.AvroFsInput;
import com.starhomemach.nbi.oradoop.export.file.dto.AvroFileRecord;
import com.starhomemach.nbi.oradoop.export.file.dto.T_dsgl;

@Component
public class TDsglFileHandler extends QosFileHandler {

    private final Log logger = LogFactory.getLog(getClass());

    private final String INSERT_INTO_T_DSGL =
        "INSERT INTO bi.t_dsgl\n" +
        "  ( cl_grp_id, cl_id, dk_sgl, sgl_prtcl_cd, sgl_fct_tbl_cd, tmst_sgl_first_evt,\n" +
        "    tmst_sgl_last_evt, outdat, cre_dat, mod_dat, adt_id, mod_tmst, dk_cmm )\n" +
        " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

    @Autowired
    private SequenceNextVal sequenceNextVal;

    @Autowired
    private FileHandlerUtil fileHandlerUtil;

    @Override
    public String[] getInsertQuery() {
        return new String[]{INSERT_INTO_T_DSGL};
    }

    @Override
    public String[] getMergeQuery() {
        return new String[]{""};
    }

    @Override
    public List<AvroFileRecord> readFileToList(String avroFilePath) throws IOException {
        List<AvroFileRecord> avroRecords = Lists.newArrayList();
        Path path = new Path(avroFilePath);
        Configuration config = new Configuration();
        org.apache.avro.file.SeekableInput input = new AvroFsInput(path, config);
        org.apache.avro.io.DatumReader<T_dsgl> tDSglDatumReader = new org.apache.avro.specific.SpecificDatumReader<T_dsgl>(T_dsgl.class);
        org.apache.avro.file.FileReader<T_dsgl> t_dsgl_dyRecords = org.apache.avro.file.DataFileReader.openReader(input, tDSglDatumReader);
        while (t_dsgl_dyRecords.hasNext()) {
            T_dsgl record = t_dsgl_dyRecords.next();
            avroRecords.add(record);
        }

        t_dsgl_dyRecords.close();
        logger.info("succeeded to process file:" + avroFilePath + ". number of records:" + avroRecords.size());

        return avroRecords;
    }

    @Override
    public boolean isMergeOperation() {
        return false;
    }

    @Override
    public boolean isSequenceOperation() {
        return true;
    }

    @Override
    public String getHandlerKey() {
        return "T_DSGL";
    }


    public void setPreparedStatement(AvroFileRecord avroFileRecords, Long sequenceId, PreparedStatement preparedStatement) throws SQLException, ParseException {
        logger.info("going to set PreparedStatement for record:" + avroFileRecords);
        T_dsgl tDsqlRecord = (T_dsgl) avroFileRecords;
        logger.info("Need to fetch new sequence id");
        Long sqlSquenceId = sequenceNextVal.getSequenceNextVal(sequenceNextVal.SELECT_BI_NEW_Q_SQL_ID_NEXTVAL_FROM_DUAL);

        preparedStatement.setLong(1, tDsqlRecord.getClGrpId());
        preparedStatement.setLong(2, tDsqlRecord.getClId());
        preparedStatement.setLong(3, sqlSquenceId);
        preparedStatement.setString(4, tDsqlRecord.getSglPrtclCd().toString());
        preparedStatement.setString(5, tDsqlRecord.getSglFctTblCd().toString());
        preparedStatement.setTimestamp(6, fileHandlerUtil.getSqlTimeStempWithMilliseconds(tDsqlRecord.getTmstSglFirstEvt().toString()));
        preparedStatement.setTimestamp(7, fileHandlerUtil.getSqlTimeStempWithMilliseconds(tDsqlRecord.getTmstSglLastEvt().toString()));
        preparedStatement.setString(8, tDsqlRecord.getOutdat().toString());

        String creDate = tDsqlRecord.getCreDat().toString();
        String mod = tDsqlRecord.getModDat().toString();
        preparedStatement.setDate(9, fileHandlerUtil.getSqlDate(creDate));
        preparedStatement.setDate(10, fileHandlerUtil.getSqlDate(mod));

        preparedStatement.setLong(11, sequenceId);
        preparedStatement.setTimestamp(12, fileHandlerUtil.getSqlTimeStempWithMilliseconds(tDsqlRecord.getModTmst().toString()));
        preparedStatement.setLong(13, tDsqlRecord.getDkCmm());
        preparedStatement.addBatch();

    }

}
