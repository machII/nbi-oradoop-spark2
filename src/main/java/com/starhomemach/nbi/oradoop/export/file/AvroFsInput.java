package com.starhomemach.nbi.oradoop.export.file;

import org.apache.avro.file.SeekableInput;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.Path;

import java.io.Closeable;
import java.io.IOException;

public class AvroFsInput implements Closeable, SeekableInput {
    private final FSDataInputStream stream;
    private final long len;

    /** Construct given a path and a configuration. */
    public AvroFsInput(Path path, Configuration conf) throws IOException {
        this.len = path.getFileSystem(conf).getFileStatus(path).getLen();
        this.stream = path.getFileSystem(conf).open(path);
    }

    public long length() {
        return len;
    }

    public int read(byte[] b, int off, int len) throws IOException {
        return stream.read(b, off, len);
    }

    public void seek(long p) throws IOException {
        stream.seek(p);
    }

    public long tell() throws IOException {
        return stream.getPos();
    }

    public void close() throws IOException {
        stream.close();
    }
}