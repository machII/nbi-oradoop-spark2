package com.starhomemach.nbi.oradoop.export.file.filehandler;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;
import com.starhomemach.nbi.oradoop.export.file.AvroFsInput;
import com.starhomemach.nbi.oradoop.export.file.dto.AvroFileRecord;
import com.starhomemach.nbi.oradoop.export.file.dto.T_fgcc_hr;

@Component
public class TFgccHrFileHandler extends QosFileHandler {

    private final Log logger = LogFactory.getLog(getClass());

    @Autowired
    private FileHandlerUtil fileHandlerUtil;

    @Override
    public String[] getInsertQuery() {
        String INSERT_INTO_T_FGCC_HR_TMP = "INSERT INTO bi.hdp_t_fgcc_hr_tmp VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        return new String[]{INSERT_INTO_T_FGCC_HR_TMP};
    }

    private final String MERGE_T_FGCC_DY_WITH_T_FGCC_HR_TMP =
        "MERGE INTO bi.t_fgcc_dy d\n" +
        "  USING ( SELECT dk_cmm, cl_id, dk_dat, dk_dir, dk_org_snw, dk_org_hnw, dk_org_cnp, dk_pet, dk_spe, dk_sgs, dk_rat, dk_svt, MAX(dk_adt) dk_adt,\n" +
        "                 SUM(gcc_atpt) gcc_atpt, SUM(gcc_succ) gcc_succ, SUM(gcc_fail) gcc_fail, SUM(gcc_dly) gcc_dly,\n" +
        "                 SUM(gcc_uplnk_dur) gcc_uplnk_dur, SUM(gcc_uplnk_byt) gcc_uplnk_byt, SUM(gcc_uplnk_good_byt) gcc_uplnk_good_byt,\n" +
        "                 SUM(gcc_dwnlnk_dur) gcc_dwnlnk_dur, SUM(gcc_dwnlnk_byt) gcc_dwnlnk_byt, SUM(gcc_dwnlnk_good_byt) gcc_dwnlnk_good_byt,\n" +
        "                 SUM(gcc_sess_dur) gcc_sess_dur, SUM(gcc_tcp_rdtrp_tim) gcc_tcp_rdtrp_tim, SUM(gcc_tcp_rdtrp_no) gcc_tcp_rdtrp_no,\n" +
        "                 SUM(gcc_tcp_pcks) gcc_tcp_pcks, SUM(gcc_tcp_rtrns_pcks) gcc_tcp_rtrns_pcks, SUM(gcc_rdtrp_atpt) gcc_rdtrp_atpt,\n" +
        "                 SUM(gcc_dt_dur) gcc_dt_dur\n" +
        "            FROM bi.hdp_t_fgcc_hr_tmp\n" +
        "           GROUP BY dk_cmm, cl_id, dk_org_snw, dk_org_hnw, dk_org_cnp, dk_dir, dk_dat, dk_spe, dk_rat, dk_pet, dk_sgs, dk_svt ) temp\n" +
        "     ON ( d.dk_cmm = temp.dk_cmm AND\n" +
        "          d.cl_id = temp.cl_id AND\n" +
        "          d.dk_dat = temp.dk_dat AND\n" +
        "          d.dk_dir = temp.dk_dir AND\n" +
        "          d.dk_org_snw = temp.dk_org_snw AND\n" +
        "          d.dk_org_hnw = temp.dk_org_hnw AND\n" +
        "          d.dk_org_cnp = temp.dk_org_cnp AND\n" +
        "          d.dk_pet = temp.dk_pet AND\n" +
        "          d.dk_spe = temp.dk_spe AND\n" +
        "          d.dk_sgs = temp.dk_sgs AND\n" +
        "          d.dk_rat = temp.dk_rat AND\n" +
        "          d.dk_svt = temp.dk_svt )\n" +
        "  WHEN MATCHED THEN\n" +
        "    UPDATE SET d.gcc_atpt = d.gcc_atpt + temp.gcc_atpt,\n" +
        "               d.gcc_succ = d.gcc_succ + temp.gcc_succ,\n" +
        "               d.gcc_fail = d.gcc_fail + temp.gcc_fail,\n" +
        "               d.gcc_dly = d.gcc_dly + temp.gcc_dly,\n" +
        "               d.gcc_uplnk_dur = d.gcc_uplnk_dur + temp.gcc_uplnk_dur,\n" +
        "               d.gcc_uplnk_byt = d.gcc_uplnk_byt + temp.gcc_uplnk_byt,\n" +
        "               d.gcc_uplnk_good_byt = d.gcc_uplnk_good_byt + temp.gcc_uplnk_good_byt,\n" +
        "               d.gcc_dwnlnk_dur = d.gcc_dwnlnk_dur + temp.gcc_dwnlnk_dur,\n" +
        "               d.gcc_dwnlnk_byt = d.gcc_dwnlnk_byt + temp.gcc_dwnlnk_byt,\n" +
        "               d.gcc_dwnlnk_good_byt = d.gcc_dwnlnk_good_byt + temp.gcc_dwnlnk_good_byt,\n" +
        "               d.gcc_sess_dur = d.gcc_sess_dur + temp.gcc_sess_dur,\n" +
        "               d.gcc_tcp_rdtrp_tim = d.gcc_tcp_rdtrp_tim + temp.gcc_tcp_rdtrp_tim,\n" +
        "               d.gcc_tcp_rdtrp_no = d.gcc_tcp_rdtrp_no + temp.gcc_tcp_rdtrp_no,\n" +
        "               d.gcc_tcp_pcks = d.gcc_tcp_pcks + temp.gcc_tcp_pcks,\n" +
        "               d.gcc_tcp_rtrns_pcks = d.gcc_tcp_rtrns_pcks + temp.gcc_tcp_rtrns_pcks,\n" +
        "               d.gcc_rdtrp_atpt = d.gcc_rdtrp_atpt + temp.gcc_rdtrp_atpt,\n" +
        "               d.gcc_dt_dur = d.gcc_dt_dur + temp.gcc_dt_dur,\n" +
        "               d.dk_adt = temp.dk_adt\n" +
        "  WHEN NOT MATCHED THEN\n" +
        "    INSERT ( dk_cmm, cl_id, dk_dat, dk_dir, dk_org_snw, dk_org_hnw, dk_org_cnp,\n" +
        "             dk_pet,d.dk_spe, dk_rat, dk_sgs, dk_adt, gcc_atpt, gcc_succ,\n" +
        "             gcc_fail, gcc_dly, gcc_uplnk_dur, gcc_uplnk_byt, gcc_uplnk_good_byt,\n" +
        "             gcc_dwnlnk_dur, gcc_dwnlnk_byt, gcc_dwnlnk_good_byt, gcc_sess_dur, gcc_tcp_rdtrp_tim,\n" +
        "             gcc_tcp_rdtrp_no, gcc_tcp_pcks, gcc_tcp_rtrns_pcks, dk_svt, gcc_rdtrp_atpt, gcc_dt_dur)\n" +
        "      VALUES ( temp.dk_cmm, temp.cl_id, temp.dk_dat, temp.dk_dir, temp.dk_org_snw, temp.dk_org_hnw, temp.dk_org_cnp,\n" +
        "               temp.dk_pet, temp.dk_spe, temp.dk_rat, temp.dk_sgs, temp.dk_adt, temp.gcc_atpt, temp.gcc_succ,\n" +
        "               temp.gcc_fail, temp.gcc_dly, temp.gcc_uplnk_dur, temp.gcc_uplnk_byt, temp.gcc_uplnk_good_byt,\n" +
        "               temp.gcc_dwnlnk_dur, temp.gcc_dwnlnk_byt, temp.gcc_dwnlnk_good_byt, temp.gcc_sess_dur, temp.gcc_tcp_rdtrp_tim,\n" +
        "               temp.gcc_tcp_rdtrp_no, temp.gcc_tcp_pcks, temp.gcc_tcp_rtrns_pcks, temp.dk_svt, temp.gcc_rdtrp_atpt, temp.gcc_dt_dur )";

    @Override
    public String[] getMergeQuery() {
        String MERGE_T_FGCC_HR_WITH_T_FGCC_HR_TMP =
                "MERGE INTO bi.t_fgcc_hr d\n" +
                "  USING ( SELECT * FROM bi.hdp_t_fgcc_hr_tmp where dk_dat > bi.k_bi_utl.dat2dkdat(sysdate) -8 ) temp\n" +
                "     ON ( d.dk_cmm = temp.dk_cmm AND\n" +
                "          d.cl_id = temp.cl_id AND\n" +
                "          d.dk_dat = temp.dk_dat AND\n" +
                "          d.dk_tim_hr = temp.dk_tim_hr AND\n" +
                "          d.dk_dir = temp.dk_dir AND\n" +
                "          d.dk_org_snw = temp.dk_org_snw AND\n" +
                "          d.dk_org_hnw = temp.dk_org_hnw AND\n" +
                "          d.dk_org_cnp = temp.dk_org_cnp AND\n" +
                "          d.dk_pet = temp.dk_pet AND\n" +
                "          d.dk_spe = temp.dk_spe AND\n" +
                "          d.dk_sgs = temp.dk_sgs AND\n" +
                "          d.dk_rat = temp.dk_rat AND\n" +
                "          d.dk_svt = temp.dk_svt )\n" +
                "  WHEN MATCHED THEN\n" +
                "    UPDATE SET d.gcc_atpt = d.gcc_atpt + temp.gcc_atpt,\n" +
                "               d.gcc_succ = d.gcc_succ + temp.gcc_succ,\n" +
                "               d.gcc_fail = d.gcc_fail + temp.gcc_fail,\n" +
                "               d.gcc_dly = d.gcc_dly + temp.gcc_dly,\n" +
                "               d.gcc_uplnk_dur = d.gcc_uplnk_dur + temp.gcc_uplnk_dur,\n" +
                "               d.gcc_uplnk_byt = d.gcc_uplnk_byt + temp.gcc_uplnk_byt,\n" +
                "               d.gcc_uplnk_good_byt = d.gcc_uplnk_good_byt + temp.gcc_uplnk_good_byt,\n" +
                "               d.gcc_dwnlnk_dur = d.gcc_dwnlnk_dur + temp.gcc_dwnlnk_dur,\n" +
                "               d.gcc_dwnlnk_byt = d.gcc_dwnlnk_byt + temp.gcc_dwnlnk_byt,\n" +
                "               d.gcc_dwnlnk_good_byt = d.gcc_dwnlnk_good_byt + temp.gcc_dwnlnk_good_byt,\n" +
                "               d.gcc_sess_dur = d.gcc_sess_dur + temp.gcc_sess_dur,\n" +
                "               d.gcc_tcp_rdtrp_tim = d.gcc_tcp_rdtrp_tim + temp.gcc_tcp_rdtrp_tim,\n" +
                "               d.gcc_tcp_rdtrp_no = d.gcc_tcp_rdtrp_no + temp.gcc_tcp_rdtrp_no,\n" +
                "               d.gcc_tcp_pcks = d.gcc_tcp_pcks + temp.gcc_tcp_pcks,\n" +
                "               d.gcc_tcp_rtrns_pcks = d.gcc_tcp_rtrns_pcks + temp.gcc_tcp_rtrns_pcks,\n" +
                "               d.gcc_rdtrp_atpt = d.gcc_rdtrp_atpt + temp.gcc_rdtrp_atpt,\n" +
                "               d.gcc_dt_dur = d.gcc_dt_dur + temp.gcc_dt_dur,\n" +
                "               d.dk_adt = temp.dk_adt\n" +
                "  WHEN NOT MATCHED THEN\n" +
                "    INSERT ( dk_cmm, cl_id, dk_dat, dk_tim_hr, dk_dir, dk_org_snw,\n" +
                "             dk_org_hnw, dk_org_cnp, dk_pet, dk_spe, dk_rat, dk_sgs,\n" +
                "             dk_adt, gcc_atpt, gcc_succ, gcc_fail, gcc_dly, gcc_uplnk_dur,\n" +
                "             gcc_uplnk_byt, gcc_uplnk_good_byt, gcc_dwnlnk_dur, gcc_dwnlnk_byt,\n" +
                "             gcc_dwnlnk_good_byt, gcc_sess_dur, gcc_tcp_rdtrp_tim, gcc_tcp_rdtrp_no,\n" +
                "             gcc_tcp_pcks, gcc_tcp_rtrns_pcks, dk_svt, gcc_rdtrp_atpt, gcc_dt_dur )\n" +
                "      VALUES ( temp.dk_cmm, temp.cl_id, temp.dk_dat, temp.dk_tim_hr, temp.dk_dir, temp.dk_org_snw,\n" +
                "               temp.dk_org_hnw, temp.dk_org_cnp, temp.dk_pet, temp.dk_spe, temp.dk_rat, temp.dk_sgs,\n" +
                "               temp.dk_adt, temp.gcc_atpt, temp.gcc_succ, temp.gcc_fail, temp.gcc_dly, temp.gcc_uplnk_dur,\n" +
                "               temp.gcc_uplnk_byt, temp.gcc_uplnk_good_byt, temp.gcc_dwnlnk_dur, temp.gcc_dwnlnk_byt,\n" +
                "               temp.gcc_dwnlnk_good_byt, temp.gcc_sess_dur, temp.gcc_tcp_rdtrp_tim, temp.gcc_tcp_rdtrp_no,\n" +
                "               temp.gcc_tcp_pcks, temp.gcc_tcp_rtrns_pcks, temp.dk_svt, temp.gcc_rdtrp_atpt, temp.gcc_dt_dur )";

        return fileHandlerUtil.getMergeStatmentsArray(MERGE_T_FGCC_HR_WITH_T_FGCC_HR_TMP, MERGE_T_FGCC_DY_WITH_T_FGCC_HR_TMP);
    }

    @Override
    public String[] getAppendQuery() {
        String APPEND_INTO_T_FGCC_HR_FROM_T_FGCC_HR_TMP =
            "INSERT /*+ APPEND */ INTO bi.t_fgcc_hr\n" +
            "        ( dk_cmm, cl_id, dk_dat, dk_tim_hr, dk_dir, dk_org_snw, dk_org_hnw, dk_org_cnp, dk_pet, dk_spe, dk_sgs, dk_rat, dk_svt, dk_adt,\n" +
            "          gcc_atpt, gcc_succ, gcc_fail, gcc_dly, gcc_uplnk_dur, gcc_uplnk_byt, gcc_uplnk_good_byt, gcc_dwnlnk_dur, gcc_dwnlnk_byt,\n" +
            "          gcc_dwnlnk_good_byt, gcc_sess_dur, gcc_tcp_rdtrp_tim, gcc_tcp_rdtrp_no, gcc_tcp_pcks, gcc_tcp_rtrns_pcks, gcc_rdtrp_atpt, gcc_dt_dur )\n" +
            "  SELECT dk_cmm, cl_id, dk_dat, dk_tim_hr, dk_dir, dk_org_snw, dk_org_hnw, dk_org_cnp, dk_pet, dk_spe, dk_sgs, dk_rat, dk_svt, dk_adt,\n" +
            "         gcc_atpt, gcc_succ, gcc_fail, gcc_dly, gcc_uplnk_dur, gcc_uplnk_byt, gcc_uplnk_good_byt, gcc_dwnlnk_dur, gcc_dwnlnk_byt,\n" +
            "         gcc_dwnlnk_good_byt, gcc_sess_dur, gcc_tcp_rdtrp_tim, gcc_tcp_rdtrp_no, gcc_tcp_pcks, gcc_tcp_rtrns_pcks, gcc_rdtrp_atpt, gcc_dt_dur\n" +
            "    FROM bi.hdp_t_fgcc_hr_tmp";

        return fileHandlerUtil.getAppendStatmentsArray(APPEND_INTO_T_FGCC_HR_FROM_T_FGCC_HR_TMP, MERGE_T_FGCC_DY_WITH_T_FGCC_HR_TMP);
    }

    @Override
    public List<AvroFileRecord> readFileToList(String avroFilePath) throws IOException {
        List<AvroFileRecord> avroRecords = Lists.newArrayList();
        Path path = new Path(avroFilePath);
        Configuration config = new Configuration();
            org.apache.avro.file.SeekableInput input = new AvroFsInput(path, config);
            org.apache.avro.io.DatumReader<T_fgcc_hr> tFgccHrDatumReader = new org.apache.avro.specific.SpecificDatumReader<T_fgcc_hr>(T_fgcc_hr.class);
            org.apache.avro.file.FileReader<T_fgcc_hr> t_FGCC_HRRecords = org.apache.avro.file.DataFileReader.openReader(input, tFgccHrDatumReader);
            while (t_FGCC_HRRecords.hasNext()) {
                T_fgcc_hr record = t_FGCC_HRRecords.next();
                avroRecords.add(record);
            }

            t_FGCC_HRRecords.close();
            logger.info("succeeded to process file:" + avroFilePath + ". number of records:" + avroRecords.size());
        return avroRecords;
    }


    public void setPreparedStatement(AvroFileRecord avroFileRecords, Long sequenceId, PreparedStatement preparedStatement) throws SQLException {
        logger.info("going to set PreparedStatement for record:" + avroFileRecords);
        T_fgcc_hr tFgccHrRecord = (T_fgcc_hr) avroFileRecords;
        preparedStatement.setLong(1, tFgccHrRecord.getDkCmm());
        preparedStatement.setLong(2, tFgccHrRecord.getClId());
        preparedStatement.setLong(3, tFgccHrRecord.getDkDat());
        preparedStatement.setLong(4, tFgccHrRecord.getDkTimHr());
        preparedStatement.setLong(5, tFgccHrRecord.getDkDir());
        preparedStatement.setLong(6, tFgccHrRecord.getDkOrgSnw());
        preparedStatement.setLong(7, tFgccHrRecord.getDkOrgHnw());
        preparedStatement.setLong(8, tFgccHrRecord.getDkOrgCnp());
        preparedStatement.setLong(9, tFgccHrRecord.getDkPet());
        preparedStatement.setLong(10, tFgccHrRecord.getDkSpe());
        preparedStatement.setLong(11, tFgccHrRecord.getDkSgs());
        preparedStatement.setLong(12, tFgccHrRecord.getDkRat());
        preparedStatement.setLong(13, sequenceId);
        fileHandlerUtil.setPreparedStatementIfNullOrDouble(14, tFgccHrRecord.getGccAtpt(), preparedStatement);
        fileHandlerUtil.setPreparedStatementIfNullOrDouble(15, tFgccHrRecord.getGccSucc(), preparedStatement);
        fileHandlerUtil.setPreparedStatementIfNullOrDouble(16, tFgccHrRecord.getGccFail(), preparedStatement);
        fileHandlerUtil.setPreparedStatementIfNullOrDouble(17, tFgccHrRecord.getGccDly(), preparedStatement);
        fileHandlerUtil.setPreparedStatementIfNullOrDouble(18, tFgccHrRecord.getGccUplnkDur(), preparedStatement);
        fileHandlerUtil.setPreparedStatementIfNullOrDouble(19, tFgccHrRecord.getGccUplnkByt(), preparedStatement);
        fileHandlerUtil.setPreparedStatementIfNullOrDouble(20, tFgccHrRecord.getGccUplnkGoodByt(), preparedStatement);
        fileHandlerUtil.setPreparedStatementIfNullOrDouble(21, tFgccHrRecord.getGccDwnlnkDur(), preparedStatement);
        fileHandlerUtil.setPreparedStatementIfNullOrDouble(22, tFgccHrRecord.getGccDwnlnkByt(), preparedStatement);
        fileHandlerUtil.setPreparedStatementIfNullOrDouble(23, tFgccHrRecord.getGccDwnlnkGoodByt(), preparedStatement);
        fileHandlerUtil.setPreparedStatementIfNullOrDouble(24, tFgccHrRecord.getGccSessDur(), preparedStatement);
        fileHandlerUtil.setPreparedStatementIfNullOrDouble(25, tFgccHrRecord.getGccTcpRdtrpTim(), preparedStatement);
        fileHandlerUtil.setPreparedStatementIfNullOrDouble(26, tFgccHrRecord.getGccTcpRdtrpNo(), preparedStatement);
        fileHandlerUtil.setPreparedStatementIfNullOrDouble(27, tFgccHrRecord.getGccTcpPcks(), preparedStatement);
        fileHandlerUtil.setPreparedStatementIfNullOrDouble(28, tFgccHrRecord.getGccTcpRtrnsPcks(), preparedStatement);
        preparedStatement.setLong(29, tFgccHrRecord.getGccDkSvt());
        fileHandlerUtil.setPreparedStatementIfNullOrDouble(30, tFgccHrRecord.getGccRdtrpAtpt(), preparedStatement);
        fileHandlerUtil.setPreparedStatementIfNullOrDouble(31, tFgccHrRecord.getGccDtDur(), preparedStatement);

        preparedStatement.addBatch();

    }

    @Override
    public boolean isSequenceOperation() {
        return true;
    }

    @Override
    public String getHandlerKey() {
        return "T_FGCC_HR";
    }


    @Override
    public boolean isMergeOperation() {
        return true;
    }

    @Override
    public boolean isAppendOperation() {
        return false;
    }

}
