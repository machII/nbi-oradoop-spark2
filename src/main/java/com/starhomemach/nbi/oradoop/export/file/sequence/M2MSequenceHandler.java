package com.starhomemach.nbi.oradoop.export.file.sequence;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import com.starhomemach.nbi.oradoop.export.file.dto.AvroFileRecord;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Component;

/**
 * Created by tpin on 09/10/2016.
 */
@Component
public class M2MSequenceHandler extends SequenceHandler {
    public static final String SELECT_BI_NEW_Q_HADOOP_M2M_PEID_NEXTVAL_FROM_DUAL = "select  BI.Q_HADOOP_M2M_PE_ID.NEXTVAL from dual";

    Long sequenceNextVal = -1l;

    private final Log logger = LogFactory.getLog(getClass());

    @Override
    public Long getSequenceID(AvroFileRecord record) {
        if (sequenceNextVal == -1l)
        {
            sequenceNextVal = jdbcTemplate.queryForObject(SELECT_BI_NEW_Q_HADOOP_M2M_PEID_NEXTVAL_FROM_DUAL, Long.class);
            logger.info("Sequence next val is:" + sequenceNextVal);
        }
        return sequenceNextVal;

    }
}
