package com.starhomemach.nbi.oradoop.export.export;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

@Component
public class Tdsvk {

    public static final String SELECT_DK_SVK = "select DK_SVK from BI.T_DSVK where SVK_CD = ?";


    @Autowired
    private JdbcTemplate jdbcTemplate;


    private final Log logger = LogFactory.getLog(getClass());

    public Long getTdsvk(String SVK_CD) throws DataAccessException {
        List<Long> longs = jdbcTemplate.queryForList(SELECT_DK_SVK, new Object[]{SVK_CD}, Long.class);
        if(longs.size() == 1)
            return longs.get(0);
        return -1l;
    }

}
