package com.starhomemach.nbi.oradoop.export.export.QosAggregations;

import com.google.common.collect.Maps;
import com.starhomemach.nbi.oradoop.export.export.MergeRecords;
import com.starhomemach.nbi.oradoop.export.file.filehandler.FileHandler;

import com.starhomemach.nbi.oradoop.export.file.filehandler.QosFileHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.sql.*;
import java.util.List;
import java.util.Map;

@Service
public class QosAggregationsExportHandler {

    @Autowired
    List<QosAggregationsFileHandler> fileHandlers;

    @Autowired
    @Qualifier("hiveConnection")
    Connection hiveConnection;

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    public MergeRecords mergeRecords;


    String jobId;
    String user;
    String hiveJdbcUrl;
    String hiveJdbcDriver;
    String hiveUserName;
    String hivePassword;

    final String stgDbName = "stgout_db";
    private Integer batchSize;

    public void run() throws Exception {


        gerArgunments();

        Map<FileHandler, ResultSet> restultSets = Maps.newHashMap();

        Statement stmt = hiveConnection.createStatement();
        ResultSet rs = null;
        for (QosFileHandler fileHandler : fileHandlers) {

            System.out.println("Getting data from "+fileHandler.getHandlerKey()+"...");
            String query = "select * from " + stgDbName + "." + fileHandler.getHandlerKey() + " where user like '"+user+"' and wf_id like '"+jobId+"'";
            System.out.println("Executing query "+query+"...");
            stmt.execute(query);
            rs = stmt.getResultSet();

            restultSets.put(fileHandler, rs);
        }

        mergeRecords.mergeToOracleFromRs(restultSets);

    }

    private void gerArgunments() {
        QosAggregationsExportArgumentsHolder qosAggregationsExportArgumentsHolder = QosAggregationsExportArgumentsHolder.getInstance();
        jobId = qosAggregationsExportArgumentsHolder.getJobId();
        user = qosAggregationsExportArgumentsHolder.getUser();
        hiveJdbcUrl = qosAggregationsExportArgumentsHolder.getHiveJdbcUrl();
        hiveJdbcDriver = qosAggregationsExportArgumentsHolder.getHiveJdbcDriver();
        hiveUserName = qosAggregationsExportArgumentsHolder.getHiveUserName();
        hivePassword = qosAggregationsExportArgumentsHolder.getHivePassword();
        batchSize = qosAggregationsExportArgumentsHolder.getBatchSize();

        System.out.println("-----------------------------Arguments: ----------------------------");

        System.out.println(qosAggregationsExportArgumentsHolder.toString());
        System.out.println("---------------------------------------------------------------------");
    }

}