package com.starhomemach.nbi.oradoop.export.file.filehandler;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.stereotype.Component;

import com.starhomemach.nbi.oradoop.OradoopConf;

@Component
public class FileHandlerUtil {

    public static final String YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";
    public static final String YYYY_MM_DD_HH_MM_SS_SSSZ = "yyyy-MM-dd HH:mm:ss.SSSZ";
    public static final String YYYY_MM_DD_T_HH_MM = "yyyy-MM-dd'T'HH:mm";

    public void setPreparedStatementIfNullOrDouble(int index, Double value, PreparedStatement preparedStatement) throws SQLException {
        if(value == null)
        {
            preparedStatement.setNull(index, Types.DOUBLE);
            return;
        }
        preparedStatement.setDouble(index, value);
    }


    public void setPreparedStatementIfNullOrLong(int index, Long value, PreparedStatement preparedStatement) throws SQLException {
        if(value == null)
        {
            preparedStatement.setNull(index, Types.BIGINT);
            return;
        }
        preparedStatement.setLong(index, value);
    }


    public java.sql.Date getSqlDate(String date) throws ParseException {
        Date d = new SimpleDateFormat(YYYY_MM_DD_HH_MM_SS).parse(date);
        return new java.sql.Date(d.getTime());
    }

    public Timestamp getSqlTimeStemp(String date) throws ParseException {
        Date modDate = new SimpleDateFormat(YYYY_MM_DD_HH_MM_SS).parse(date);
        return new java.sql.Timestamp(modDate.getTime());
    }

    public Timestamp getSqlTimeStempWithMilliseconds(String date) throws ParseException {
        Date d = new SimpleDateFormat(YYYY_MM_DD_HH_MM_SS_SSSZ).parse(date);
        return new java.sql.Timestamp(d.getTime());
    }

    public Timestamp getSqlTSWithT(String date) throws ParseException {
        Date d = new SimpleDateFormat(YYYY_MM_DD_T_HH_MM).parse(date);
        return new java.sql.Timestamp(d.getTime());
    }

    public java.sql.Date getSqlDateWithT(String date) throws ParseException {
        Date d = new SimpleDateFormat(YYYY_MM_DD_T_HH_MM).parse(date);
        return new java.sql.Date(d.getTime());
    }

    public String[] getMergeStatmentsArray(String mergeHr, String MergeDy){
        if (OradoopConf.isRunOnlyHourly()){
            return new String[]{mergeHr};
        }
        return new String[]{mergeHr, MergeDy};
    }

    public String[] getAppendStatmentsArray(String appendHr, String appendDy){
        if (OradoopConf.isRunOnlyHourly()){
            return new String[]{appendHr};
        }
        return new String[]{appendHr, appendDy};
    }

}
