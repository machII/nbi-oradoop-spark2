package com.starhomemach.nbi.oradoop.export.Ida;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.sql.*;

@Service
class IdaDealExportHandler {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private String jobId;
    private String hiveJdbcUrl;
    private String hiveJdbcDriver;
    private String hiveUsername;
    private String hivePassword;

    private Connection hiveConnection;
    private Connection oracleConnection;

    private final String INSERT_INTO_T_DL_CALC_IMSI_CHE = "INSERT INTO " + IdaExportArgumentsHolder.getInstance().getOutSchema() + ".hdp_t_dl_calc_imsi_che\n" +
            "  ( dl_id, dk_dir, dl_cnd_ln_id, dk_dat_mo, aflt_nw_id, prtn_nw_id, imsi_cnt, nws_usg_id )\n" +
            "  VALUES ( ?, ?, ?, ?, ?, ?, ?, ? )";

    private final String INSERT_INTO_T_DL_CALC_VAL_CHE = "INSERT INTO " + IdaExportArgumentsHolder.getInstance().getOutSchema() + ".hdp_t_dl_calc_val_che\n" +
            "  ( dl_id, dk_dir, dl_cnd_ln_id, dk_dat_mo, dk_cur_smc, aflt_nw_id, prtn_nw_id,\n" +
            "    chgb_unt, chgd_unt, net_chg_smc, tx_smc, nws_usg_id, dc_id, dk_sgm_lst )\n" +
            "  VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ? / 1000000, ? / 1000000, ?, ?, ? )";

    private final String EXECUTE_FINALIZE_PROCEDURE =
            "{ call " + IdaExportArgumentsHolder.getInstance().getOutSchema() + "_api.k_dl_oradoop_api.calculateIMSIDeal( ?, ? ) }";

    void run() throws Exception {

        getArguments();

        Class.forName(hiveJdbcDriver);
        hiveConnection = DriverManager.getConnection(hiveJdbcUrl, hiveUsername, hivePassword);

        oracleConnection = jdbcTemplate.getDataSource().getConnection();
        oracleConnection.setAutoCommit(false);

        Statement stmtDeals = hiveConnection.createStatement();
        stmtDeals.execute("SELECT DISTINCT dl_id, min_dk_dat_mo FROM ida_db.t_deal WHERE wf_id = '" + jobId + "'");
        ResultSet rsDeals = stmtDeals.getResultSet();
        while (rsDeals.next()) {
            PreparedStatement imsiCountStmt = oracleConnection.prepareStatement(INSERT_INTO_T_DL_CALC_IMSI_CHE);
            PreparedStatement imsiTrafficStmt = oracleConnection.prepareStatement(INSERT_INTO_T_DL_CALC_VAL_CHE);
            PreparedStatement calculateDealStmt = oracleConnection.prepareStatement(EXECUTE_FINALIZE_PROCEDURE);
            int dealId = rsDeals.getInt("dl_id");

            Statement stmtCount = hiveConnection.createStatement();
            stmtCount.execute("SELECT * FROM ida_db.t_dl_calc_imsi WHERE wf_id = '" + IdaExportArgumentsHolder.getInstance().getJobId() + "' AND dl_id = " + dealId );
            ResultSet rsCount = stmtCount.getResultSet();

            int currentBatchSize = 0;

            while (rsCount.next()) {
                imsiCountStmt.setInt(1, dealId);
                imsiCountStmt.setInt(2, rsCount.getInt("dk_dir"));
                imsiCountStmt.setInt(3, rsCount.getInt("dl_cnd_ln_id"));
                imsiCountStmt.setInt(4, rsCount.getInt("dk_dat_mo"));
                imsiCountStmt.setLong(5, rsCount.getLong("cl_id"));
                imsiCountStmt.setInt(6, rsCount.getInt("dk_org_cnp"));
                imsiCountStmt.setLong(7, rsCount.getLong("imsi_cnt"));
                imsiCountStmt.setInt(8, rsCount.getInt("nws_usg_id"));

                currentBatchSize++;
                imsiCountStmt.addBatch();

                if (currentBatchSize >= IdaExportArgumentsHolder.getInstance().getBatchSize()) {
                    imsiCountStmt.executeBatch();
                    currentBatchSize = 0;
                }

            }

            Statement stmtTraffic = hiveConnection.createStatement();
            stmtTraffic.execute("SELECT * FROM ida_db.t_dl_calc_val WHERE wf_id = '" + IdaExportArgumentsHolder.getInstance().getJobId() + "' AND dl_id = " + dealId );
            ResultSet rsTraffic = stmtTraffic.getResultSet();

            currentBatchSize = 0;

            while (rsTraffic.next()) {
                String dkSgmLst;
                try {
                    dkSgmLst = rsTraffic.getString("dk_sgm_lst").replaceAll("[\\[\\]]", "");
                }
                catch(NullPointerException e) {
                    dkSgmLst = "0";
                }

                imsiTrafficStmt.setInt(1, dealId);
                imsiTrafficStmt.setInt(2, rsTraffic.getInt("dk_dir"));
                imsiTrafficStmt.setInt(3, rsTraffic.getInt("dl_cnd_ln_id"));
                imsiTrafficStmt.setInt(4, rsTraffic.getInt("dk_dat_mo"));
                imsiTrafficStmt.setInt(5, rsTraffic.getInt("dk_cur_smc"));
                imsiTrafficStmt.setLong(6, rsTraffic.getLong("cl_id"));
                imsiTrafficStmt.setInt(7, rsTraffic.getInt("dk_org_cnp"));
                imsiTrafficStmt.setLong(8, rsTraffic.getLong("chgb_unt"));
                imsiTrafficStmt.setLong(9, rsTraffic.getLong("chgd_unt"));
                imsiTrafficStmt.setDouble(10, rsTraffic.getDouble("net_chg_smc"));
                imsiTrafficStmt.setDouble(11, rsTraffic.getDouble("tx_smc"));
                imsiTrafficStmt.setInt(12, rsTraffic.getInt("nws_usg_id"));
                imsiTrafficStmt.setInt(13, rsTraffic.getInt("dc_id"));
                imsiTrafficStmt.setString(14, dkSgmLst);

                currentBatchSize++;
                imsiTrafficStmt.addBatch();

                if (currentBatchSize >= IdaExportArgumentsHolder.getInstance().getBatchSize()) {
                    imsiTrafficStmt.executeBatch();
                    currentBatchSize = 0;
                }
            }

            calculateDealStmt.setInt(1, dealId);
            calculateDealStmt.setInt(2, rsDeals.getInt("min_dk_dat_mo"));

            imsiCountStmt.executeBatch();
            imsiTrafficStmt.executeBatch();
            calculateDealStmt.execute();
            doCommit();
        }

        doCloseConnection();
    }

    private void getArguments() {
        IdaExportArgumentsHolder idaExportArgumentsHolder = IdaExportArgumentsHolder.getInstance();
        jobId = idaExportArgumentsHolder.getJobId();
        hiveJdbcUrl = idaExportArgumentsHolder.getHiveJdbcUrl();
        hiveJdbcDriver = idaExportArgumentsHolder.getHiveJdbcDriver();
        hiveUsername = idaExportArgumentsHolder.getUser();
        hivePassword = idaExportArgumentsHolder.getUser();

        System.out.println("-----------------------------Arguments: ----------------------------");
        System.out.println(idaExportArgumentsHolder.toString());
        System.out.println("---------------------------------------------------------------------");
    }

    private void doCommit() throws SQLException {
        oracleConnection.commit();
    }

    private void doCloseConnection() throws SQLException {
        oracleConnection.close();
        hiveConnection.close();
    }
}