package com.starhomemach.nbi.oradoop.export.file;

public enum EtlFileStatus {
    _SUCCESS("_SUCCESS"),_FAILED("_FAILED"),OTHER("OTHER");


    private String value;

    EtlFileStatus(String value){
        this.value = value;
    }

    public boolean equalValue(EtlFileStatus etlFileStatus){
        return this.value.equals(etlFileStatus.value);
    }
}
