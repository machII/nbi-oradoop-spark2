package com.starhomemach.nbi.oradoop.silentroamers;

import com.google.common.collect.Lists;
import com.starhomemach.nbi.oradoop.export.file.AvroFsInput;
import com.starhomemach.nbi.oradoop.export.file.dto.AvroFileRecord;
import com.starhomemach.nbi.oradoop.export.file.filehandler.FileHandler;
import com.starhomemach.nbi.oradoop.silentroamers.dto.T_fsrm_mo;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

/**
 * Created by kcorsia on 20/02/2017.
 */
@Component
public class TFsrmMoFileHandler extends FileHandler {

    private final Log logger = LogFactory.getLog(getClass());

    private final String INSERT_INTO_T_FSRM_MO = "DROP TABLE IF EXISTS bi.stam;";


    private final String MERGE_T_FSRM_MO_WITH_T_FSRM_TMP = "select 1 from dual";
    @Override
    public String[] getInsertQuery() {
        return new String[]{INSERT_INTO_T_FSRM_MO};
    }

    @Override
    public String[] getMergeQuery() { return new String[]{MERGE_T_FSRM_MO_WITH_T_FSRM_TMP};
    }

    @Override
    public String[] getAppendQuery() {
        return new String[0];
    }

    @Override
    public List<AvroFileRecord> readFileToList(String avroFilePath) throws IOException {
        List<AvroFileRecord> avroRecords = Lists.newArrayList();
        Path path = new Path(avroFilePath);

        Configuration config = new Configuration();

        org.apache.avro.file.SeekableInput input = new AvroFsInput(path, config);
        System.out.println();
        System.out.println("--"+path.toString());
        org.apache.avro.io.DatumReader<T_fsrm_mo> tFsrmDatumReader = new org.apache.avro.specific.SpecificDatumReader<T_fsrm_mo>(T_fsrm_mo.class);
        org.apache.avro.file.FileReader<T_fsrm_mo> t_fsrmRecords = org.apache.avro.file.DataFileReader.openReader(input, tFsrmDatumReader);

        while (t_fsrmRecords.hasNext()) {
            T_fsrm_mo record = t_fsrmRecords.next();
            avroRecords.add(record);
        }
        System.out.println("num of records: " +avroRecords.size());

        t_fsrmRecords.close();
        logger.info("succeeded to process file:" + avroFilePath + ". number of records:" + avroRecords.size());

        return avroRecords;
    }

    @Override
    public boolean isMergeOperation() {
        return true;
    }

    @Override
    public boolean isAppendOperation() {
        return false;
    }

    @Override
    public boolean isSequenceOperation() {
        return true;
    }

    @Override
    public boolean useFinalizeProcedure() { return false; }

    @Override
    public String[] getFinalizeProcedure() { return new String[]{""}; }

    @Override
    public String getHandlerKey() {
        return "T_FSRM_MO";
    }


    public void setPreparedStatement(AvroFileRecord avroFileRecords, Long sequenceId, PreparedStatement preparedStatement) throws SQLException, ParseException {

        logger.info("going to set PreparedStatement for record:" + avroFileRecords);



        preparedStatement.addBatch();
    }
}
