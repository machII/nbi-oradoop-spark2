package com.starhomemach.nbi.oradoop.silentroamers;

import com.google.common.collect.Lists;
import com.starhomemach.nbi.oradoop.export.file.AvroFsInput;
import com.starhomemach.nbi.oradoop.export.file.dto.AvroFileRecord;
import com.starhomemach.nbi.oradoop.silentroamers.dto.T_fsrm_dy;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;
import com.starhomemach.nbi.oradoop.export.file.filehandler.*;
/**
 * Created by kcorsia on 20/02/2017.
 */
@Component
public class TFsrmDyFileHandler extends FileHandler {

    private final Log logger = LogFactory.getLog(getClass());

    private final String INSERT_INTO_T_FSRM_DY = "insert into BI.T_FSRM_DY_tmp " +
            "(DK_DAT,DK_ORG_CY,CL_ID,DK_CMM,SMS_NO ,VOICE_NO \n" +
            ",DT_BYT ,TTL_SLNT_IMSI,SMS_SLNT_IMSI,VOICE_SLNT_IMSI,DT_SLNT_IMSI ,\n" +
            "TTL_ACT_IMSI ,SMS_ACT_IMSI ,VOICE_ACT_IMSI ,DT_ACT_IMSI )" +
            " values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";


    private final String MERGE_T_FSRM_DY_WITH_T_FSRM_TMP = "MERGE INTO BI.T_FSRM_DY DY \n" +
            "USING (SELECT DISTINCT * FROM bi.T_FSRM_DY_tmp) temp \n" +
            "ON (\n" +
            "DY.cl_id = temp.cl_id AND\n" +
            "DY.dk_org_cy  = temp.dk_org_cy AND\n" +
            "DY.dk_dat = temp.dk_dat AND \n" +
            "DY.dk_cmm = temp.dk_cmm)\n" +
            "WHEN MATCHED THEN UPDATE SET \n" +
            "DY.SMS_NO = temp.SMS_NO , \n" +
            "DY.VOICE_NO = temp.VOICE_NO  , \n" +
            "DY.DT_BYT = temp.DT_BYT   , \n" +
            "DY.TTL_SLNT_IMSI = temp.TTL_SLNT_IMSI  , \n" +
            "DY.SMS_SLNT_IMSI = temp.SMS_SLNT_IMSI  , \n" +
            "DY.VOICE_SLNT_IMSI = temp.VOICE_SLNT_IMSI  ,\n" +
            "DY.DT_SLNT_IMSI = temp.DT_SLNT_IMSI  ,\n" +
            "DY.TTL_ACT_IMSI = temp.TTL_ACT_IMSI  ,\n" +
            "DY.SMS_ACT_IMSI = temp.SMS_ACT_IMSI  ,\n" +
            "DY.VOICE_ACT_IMSI = temp.VOICE_ACT_IMSI  ,\n" +
            "DY.DT_ACT_IMSI = temp.DT_ACT_IMSI \n" +
            "WHEN NOT MATCHED THEN INSERT (DK_DAT,DK_ORG_CY,CL_ID,DK_CMM,SMS_NO ,VOICE_NO \n" +
            ",DT_BYT ,TTL_SLNT_IMSI,SMS_SLNT_IMSI,VOICE_SLNT_IMSI,DT_SLNT_IMSI ,\n" +
            "TTL_ACT_IMSI ,SMS_ACT_IMSI ,VOICE_ACT_IMSI ,DT_ACT_IMSI  )\n" +
            "VALUES (temp.DK_DAT,temp.DK_ORG_CY,temp.CL_ID,temp.DK_CMM,temp.SMS_NO ,temp.VOICE_NO \n" +
            ",temp.DT_BYT ,temp.TTL_SLNT_IMSI,temp.SMS_SLNT_IMSI,temp.VOICE_SLNT_IMSI,temp.DT_SLNT_IMSI ,temp.\n" +
            "TTL_ACT_IMSI ,temp.SMS_ACT_IMSI ,temp.VOICE_ACT_IMSI ,temp.DT_ACT_IMSI  )";
    @Override
    public String[] getInsertQuery() {
        System.out.println();return new String[]{INSERT_INTO_T_FSRM_DY};
    }

    @Override
    public String[] getMergeQuery() { return new String[]{MERGE_T_FSRM_DY_WITH_T_FSRM_TMP};
    }

    @Override
    public String[] getAppendQuery() {
        return new String[0];
    }

    @Override
    public List<AvroFileRecord> readFileToList(String avroFilePath) throws IOException {
        List<AvroFileRecord> avroRecords = Lists.newArrayList();
        Path path = new Path(avroFilePath);

        Configuration config = new Configuration();

        org.apache.avro.file.SeekableInput input = new AvroFsInput(path, config);
        org.apache.avro.io.DatumReader<T_fsrm_dy> tFsrmDatumReader = new org.apache.avro.specific.SpecificDatumReader<T_fsrm_dy>(T_fsrm_dy.class);
        org.apache.avro.file.FileReader<T_fsrm_dy> t_fsrmRecords = org.apache.avro.file.DataFileReader.openReader(input, tFsrmDatumReader);

        while (t_fsrmRecords.hasNext()) {
            T_fsrm_dy record = t_fsrmRecords.next();
            avroRecords.add(record);
        }

        t_fsrmRecords.close();
        logger.info("succeeded to process file:" + avroFilePath + ". number of records:" + avroRecords.size());

        return avroRecords;
    }

    @Override
    public boolean isMergeOperation() {
        return true;
    }

    @Override
    public boolean isAppendOperation() {
        return false;
    }

    @Override
    public boolean isSequenceOperation() {
        return true;
    }

    @Override
    public boolean useFinalizeProcedure() { return false; }

    @Override
    public String[] getFinalizeProcedure() { return new String[]{""}; }

    @Override
    public String getHandlerKey() {
        return "T_FSRM_DY";
    }


    public void setPreparedStatement(AvroFileRecord avroFileRecords, Long sequenceId, PreparedStatement preparedStatement) throws SQLException, ParseException {

        logger.info("going to set PreparedStatement for record:" + avroFileRecords);

        T_fsrm_dy tFSRmRecord = (T_fsrm_dy) avroFileRecords;
        preparedStatement.setLong(1, tFSRmRecord.getDkDat());
        preparedStatement.setLong(2, tFSRmRecord.getDkOrgCy());
        preparedStatement.setLong(3, tFSRmRecord.getClId());
        preparedStatement.setLong(4, 0);
        preparedStatement.setLong(5, tFSRmRecord.getNoSms());
        preparedStatement.setLong(6, tFSRmRecord.getNoVoice());
        preparedStatement.setDouble(7, tFSRmRecord.getNoData());
        preparedStatement.setLong(8, tFSRmRecord.getSilentImsis());
        preparedStatement.setLong(9, tFSRmRecord.getSmsSilentImsis());
        preparedStatement.setLong(10, tFSRmRecord.getVoiceSilentImsis());
        preparedStatement.setLong(11, tFSRmRecord.getDataSilentImsis());
        preparedStatement.setLong(12, tFSRmRecord.getActiveImsis());
        preparedStatement.setLong(13, tFSRmRecord.getSmsActiveImsis());
        preparedStatement.setLong(14, tFSRmRecord.getVoiceActiveImsis());
        preparedStatement.setLong(15, tFSRmRecord.getDataActiveImsis());

        preparedStatement.addBatch();
    }
}
