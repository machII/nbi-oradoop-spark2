package com.starhomemach.nbi.oradoop.silentroamers;

import com.starhomemach.nbi.oradoop.export.Application;
import com.starhomemach.nbi.oradoop.export.SpringConfig;
import com.starhomemach.nbi.oradoop.export.service.ProcessHdfsDirectoryService;
import com.starhomemach.nbi.oradoop.export.util.HandleProcessConfiguration;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Created by pazit shefet on 15-05-2017.
 */
public class SilentRoamersApplication {

    public static void main(String[] args) throws Exception
    {
    System.out.println("########################################################## START EXPORT TO ORACLE #############################");
        if (args.length < 4) {
            System.out.println("ERROR args should contain the following parameters 1)USER 2)SPARK-JOB-ID 3)oradoop properties file name 4)SilentRoamers HDFS location ");
            return;
        }

        // load oradoop properties file
        HandleProcessConfiguration processConfiguration =  new HandleProcessConfiguration();
        processConfiguration.setPath(HandleProcessConfiguration.STARHOME_SILENTROAMERS_SHARE_CONF);
        args[2] = processConfiguration.handleOradoopConf(args, Application.INDEX_DB_PROPERTIES_FILE);

        // activate ProcessHdfsDirectoryService
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SpringConfig.class);
        context.getBean(ProcessHdfsDirectoryService.class).run(args);

        System.out.println("########################################################## END EXPORT TO ORACLE #############################");

    }
}
