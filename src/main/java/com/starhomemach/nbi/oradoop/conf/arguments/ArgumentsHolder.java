package com.starhomemach.nbi.oradoop.conf.arguments;

import com.google.common.collect.Lists;
import lombok.Data;
import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.inf.Argument;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

@Data
public abstract class ArgumentsHolder {

    private String user;
    private String jobId;
    private Integer numPartitions=32;
    private Boolean debugMode=false;


    static ArgumentsHolder ourinstance = null;

    public static <T extends ArgumentsHolder> T getInstance(Class<T> clazz) throws IllegalAccessException, InstantiationException {
        T instance = null;
        if (ourinstance == null) {
            instance = clazz.newInstance();
            ourinstance = instance;
        }
        return instance;
    }


    public static <T extends ArgumentsHolder> T getInstance()  {

        return (T) ourinstance;
    }

    public void initArgumentsHolder(String[] args) throws Exception {

        ArgumentParser parser = ArgumentParsers.newArgumentParser("Argument holder");

        buildArgumentsParser(parser);

        parseArguments(parser, args);

    }

    private void buildArgumentsParser(ArgumentParser parser) {

        List<Field> fields = getAllFields(Lists.newArrayList(), this.getClass());

        for (Field field : fields) {
            Argument argument = parser.addArgument("--" + field.getName())
                    .type(field.getType())
                    .required(field.isAnnotationPresent(IsRequired.class));
        }

    }

    private void parseArguments(ArgumentParser parser, String[] args) throws Exception {

        try {
            Namespace namespace = parser.parseArgs(args);
            System.out.println("subscriber parser " + namespace.toString());

            List<Field> fields = getAllFields(Lists.newArrayList(), this.getClass());

            for (Field field : fields) {
                boolean isAccessibleField = field.isAccessible() ? true : false;
                if (!isAccessibleField)
                    field.setAccessible(true);

                if(namespace.get(field.getName())!=null)
                   field.set(this, field.getType().cast(namespace.get(field.getName())));

                if (!isAccessibleField)
                    field.setAccessible(false);
            }
        } catch (ArgumentParserException e) {
            System.out.println("parse Arguments failed: syntax is " + parser.toString());
            parser.handleError(e);
            throw e;
        }
    }

    private List<Field> getAllFields(List<Field> fields, Class<?> type) {

        List<Field> typeFields = getAllFields_rec(fields, type);

        Iterator<Field> i = typeFields.iterator();
        while (i.hasNext()) {
            Field field = i.next();
            if (Modifier.isStatic(field.getModifiers()) || Modifier.isFinal(field.getModifiers()))
                i.remove();
        }

        return typeFields;
    }

    private List<Field> getAllFields_rec(List<Field> fields, Class<?> type) {

        fields.addAll(Arrays.asList(type.getDeclaredFields()));
        if (type.getSuperclass() != null) {
            getAllFields(fields, type.getSuperclass());
        }
        return fields;
    }

    @Override
    public String toString() {

        List<Field> fields = getAllFields(Lists.newArrayList(), this.getClass());

        StringBuilder fieldsString = new StringBuilder();
        fieldsString.append("==================================Arguments:====================================\n");
        for (Field field : fields) {
            try {
                boolean isAccessibleField = field.isAccessible() ? true : false;
                if (!isAccessibleField)
                    field.setAccessible(true);

                fieldsString.append(field.getName());
                fieldsString.append(": ");
                fieldsString.append(String.valueOf(field.get(this)));
                fieldsString.append("\n");

                if (!isAccessibleField)
                    field.setAccessible(false);

            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        fieldsString.append("===============================================================================\n");
        return fieldsString.toString();
    }

}
