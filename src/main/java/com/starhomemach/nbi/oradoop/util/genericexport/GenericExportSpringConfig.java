package com.starhomemach.nbi.oradoop.util.genericexport;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;


@Configuration
@ComponentScan(basePackages = {"com.starhomemach.nbi.oradoop.util.genericexport"})
public class GenericExportSpringConfig {

    }
