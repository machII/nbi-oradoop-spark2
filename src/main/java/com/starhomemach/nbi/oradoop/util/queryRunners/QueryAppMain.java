package com.starhomemach.nbi.oradoop.util.queryRunners;

import com.starhomemach.nbi.oradoop.OradoopConf;
import org.apache.hadoop.conf.Configuration;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Created by tpin on 19/12/2016.
 */
public class QueryAppMain {
    public static void main(String[] args) throws Exception
    {
        System.out.println("################################################ START ####################################################");
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(QueryAppSpringConfig.class);

        Configuration conf = new OradoopConf().getConf();
        int count = Integer.parseInt(args[2]);
        context.getBean(QueryAppHandler.class).runQuery(args[0],
                args[1],
                count,
                args,
                args.length > 3 + count ? args[3 + count] : null,
                args.length > 3 + count ? args[4 + count] : null );
        System.out.println("################################################ END ####################################################");
    }

}
