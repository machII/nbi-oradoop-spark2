package com.starhomemach.nbi.oradoop.util.spark;

import lombok.Data;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataType;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Data
public class TableStructureUtil {

    @Autowired
    SparkSession sparkSession;

    @Data
    public
    class Table {
        public String dbName;
        public String tableName;

        List<StructField> columns = new ArrayList();
        List<StructField> partitions = new ArrayList();

        public void processNewColumn(Row r) {

            String colName = r.getString(0);
            String colType = r.getString(1);
            StructField colStruct = DataTypes.createStructField(colName, getDataTypes(colType), true);

            if (columns.contains(colStruct)) {
                columns.remove(colStruct);
                partitions.add(colStruct);
            } else {
                columns.add(colStruct);
            }
        }
    }

    public Table getTableStructure(String dbName, String tableName) {

        Table table = new Table();
        table.setDbName(dbName);
        table.setTableName(tableName);
        Dataset<Row> describeTable = sparkSession.sql("describe " + dbName + "." + tableName);
        List<Row> rows = describeTable.collectAsList();
        rows.stream().filter(row -> !row.getString(0).startsWith("#")).forEach(row -> table.processNewColumn(row));
        return table;
    }



    public DataType getDataTypes(String type) {
        if (!type.isEmpty()) {
            if (type.equals("string")) {
                return DataTypes.StringType;
            } else if (type.equals("smallint")) {
                return DataTypes.ShortType;
            } else if (type.equals("bigint")) {
                return DataTypes.LongType;
            } else if (type.equals("int")) {
                return DataTypes.IntegerType;
            } else if (type.equals("timestamp")) {
                return DataTypes.TimestampType;
            } else if (type.equals("double")) {
                return DataTypes.DoubleType;
            }
        }
        System.out.println("Unknown type:" + type);
        return null;
    }

}
