package com.starhomemach.nbi.oradoop.util;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Table;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.Serializable;

/**
 * Created by yamar on 08/05/2017.
 */
@Component
@Windows(true)
public class HbaseDevUtil implements HbaseClient ,Serializable {

    Connection connection;

    Configuration config;

    @Override
    public boolean connectToHbase() {

        try {
            connection = ConnectionFactory.createConnection(config);

        } catch (IOException e) {
            System.err.println("FAILED TO CONNECT TO HBASE DEV!!!!");
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public boolean isTableExist(String tableName) {
        TableName tname = TableName.valueOf(tableName);
        boolean found;
        try {
            found = connection.getAdmin().tableExists(tname);
        } catch (IOException e) {
            System.err.println("Failed to check if table is in Hbase ");
            e.printStackTrace();
            return false;
        }
        return found;
    }

    @Override
    public Table getTable(String tableName) {
        TableName tname = TableName.valueOf(tableName);
        Table table= null;
        try {
              table = connection.getTable(tname);
        } catch (IOException e) {
            e.printStackTrace();
            System.err.println("FAILED TO GET TABLE");
        }
        return table;
    }

    @Override
    public void closeConnection() {
        if(connection != null){
            try {
                connection.close();
            } catch (IOException e) {
                System.err.println("Err on connection shutdown");
                e.printStackTrace();
            }
        }
    }
    @PostConstruct
    public void setHbaseConfig(){
        config = HBaseConfiguration.create();
        config.set("hbase.zookeeper.quorum", "isr-poc-1-hdf-1.starhome.com");
        config.set("hbase.master", "isr-poc-1-hdf-1.starhome.com:60000");
        config.set("hbase.zookeeper.property.clientport", "2181");
        config.set("zookeeper.znode.parent", "/hbase");
    }


    public Configuration getConfig() {
        return config;
    }



    public Connection getConnection() {
        return connection;
    }

}
