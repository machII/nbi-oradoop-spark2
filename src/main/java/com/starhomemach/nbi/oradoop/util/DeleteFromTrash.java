package com.starhomemach.nbi.oradoop.util;

import com.starhomemach.nbi.oradoop.OradoopConf;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * Created by gtama on 15/06/2017.
 */

@Component
public class DeleteFromTrash {

    public static void run(String user, String containigString) {
        try {
            System.out.println("Starting delete trash folder for user: " + user);
            System.out.println("containing: " + containigString);
            Configuration conf = new OradoopConf().getConf();
            FileSystem fileSystem = FileSystem.newInstance(conf);
            String trashPath = "/user/" + user + "/.Trash";
            FileStatus currentFile = fileSystem.getFileStatus(new Path(trashPath));
            deleteFilesFromTrashRec(containigString, fileSystem, currentFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void deleteFilesFromTrashRec(String containigString, FileSystem fileSystem, FileStatus dirTrash) {

        String currentPath = dirTrash.getPath().toString();
//        String spacecStr = String.format("%" + spaces + "s", "");
            System.out.println("scanning: " + currentPath);

        try {
            if (currentPath.contains(containigString.trim())) {
                System.out.println("Deleting : " + currentPath);
                fileSystem.delete(dirTrash.getPath(), true);

            } else {
                if (dirTrash.isDirectory()) {
                    FileStatus[] files = fileSystem.listStatus(new Path(currentPath));
                    for (FileStatus file : files) {
                        deleteFilesFromTrashRec(containigString, fileSystem, file);
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
