package com.starhomemach.nbi.oradoop.util;

import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.oozie.client.OozieClient;
import org.apache.oozie.client.OozieClientException;
import org.apache.oozie.client.WorkflowJob;
import org.apache.hadoop.conf.Configuration;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.util.Properties;

/**
 * Created by tpin on 16/02/2017.
 */
public class OozieRunner {

    private static Configuration conf1;

    public static Properties getPropertiesFile(String nameNodeUrl, String pathStr) throws IOException {
        System.out.println("Going to load properties file from: " + pathStr);
        Properties properties = new Properties();
        Path path = new Path(pathStr);

        Configuration conf1 = new Configuration();
        conf1.set("fs.defaultFS", nameNodeUrl);
        conf1.set("fs.hdfs.impl", "org.apache.hadoop.hdfs.DistributedFileSystem");

        FileSystem fileSystem = path.getFileSystem(conf1);
        FSDataInputStream schema = fileSystem.open(path);
        properties.load(schema);
        return properties;
    }

    public static void main(String[] args) throws OozieClientException, IOException {

        if (args.length < 4 || args.length > 5)
        {
            System.out.println("java com.starhomemach.nbi.oradoop.util.OozieRunner <oozieUrl> <NameNode Url> <WF App Path> <User> [waitToEnd=true]");
            return;
        }

        OozieClient wc = new OozieClient(args[0]);
        boolean wait = args.length == 5 ? Boolean.getBoolean(args[4]): true;

        Properties conf = getPropertiesFile(args[1], args[1] + args[2] + "/job.properties");
        conf.setProperty(OozieClient.APP_PATH, args[1] + args[2] + "/workflow.xml");
        conf.setProperty("user.name",args[3]);


        // submit and start the workflow job
        try {
            String jobId = wc.run(conf);
            System.out.println("Workflow job ["+jobId+"] submitted");

            //wait until the workflow job finishes printing the status every 10 seconds
            while (wc.getJobInfo(jobId).getStatus() == WorkflowJob.Status.RUNNING && wait) {
                System.out.println("Workflow job ["+jobId+"] running ...");
                Thread.sleep(10 * 1000);
            }
            System.out.println("Workflow job ["+wc.getJobInfo(jobId)+"]completed or you decided not to wait");
         }
         catch(Exception e){
            System.out.println("Workflow not submitted"+e);
         }
    }
}


