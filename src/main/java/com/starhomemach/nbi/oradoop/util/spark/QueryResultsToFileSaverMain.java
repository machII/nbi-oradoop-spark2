package com.starhomemach.nbi.oradoop.util.spark;

import com.starhomemach.nbi.oradoop.export.SpringConfig;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Created by tpin on 19/12/2016.
 */

public class QueryResultsToFileSaverMain {


    public static void main(String[] args) throws Exception
    {
        System.out.println("################################################ START ####################################################");

        QueryResultsToFileSaverArgumentsHolder queryResultsToFileSaverArgumentsHolder = QueryResultsToFileSaverArgumentsHolder.getInstance(QueryResultsToFileSaverArgumentsHolder.class);
        queryResultsToFileSaverArgumentsHolder.initArgumentsHolder(args);

        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();

        context.register(SparkSpringConfig.class);

        context.refresh();

        context.getBean(QueryResultsToFileSaverHandler.class).run();

        System.out.println("################################################ End ####################################################");


    }


    @Test
    public void replaceRegex() {
        String fullFilePath = "gabi/testing/20203003/fff.csv";

        String fileDirPath = fullFilePath.substring(0, fullFilePath.lastIndexOf("/"));
        String fileName = fullFilePath.substring(fullFilePath.lastIndexOf("/")+1);

        System.out.println(fileDirPath);
        System.out.println(fileName);
    }

}

