package com.starhomemach.nbi.oradoop.util;

import com.starhomemach.nbi.oradoop.OradoopConf;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.PathFilter;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatterBuilder;
import org.joda.time.format.DateTimeParser;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.time.ZoneId;
import java.util.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * Created by tpin on 01/08/2016.
 */
@Component
public class GeneralUtils {
    // DK_DAT first day
    final private  static  LocalDate pInitialDate = LocalDate.of(1989,12,31);
    final public static String DK_TIM_HR_PARTITION_PREFIX="dk_tim_hr";
    final public static String DK_DAT_PARTITION_PREFIX="dk_dat";
    final public static String CL_ID_PARTITION_PREFIX="cl_id";

    private final Log logger = LogFactory.getLog(getClass());

    public static final String rawTimeFormat = "yyyy/MM/dd HH:mm:ss.SSS";

    public LocalDateTime toLocalDateTime(String time) {
        DateTimeParser[] parsers = {DateTimeFormat.forPattern(rawTimeFormat).getParser()};
        org.joda.time.format.DateTimeFormatter formatter = new DateTimeFormatterBuilder().append(null, parsers).toFormatter();
        DateTime dateTime = formatter.parseDateTime(time);
        org.joda.time.LocalDateTime localDateTime = dateTime.toLocalDateTime();
        return LocalDateTime.ofInstant(localDateTime.toDate().toInstant(), ZoneId.systemDefault());
    }

    public LocalDate getInitialDay()
    {
        return pInitialDate;
    }

    public long calculateDK_DAT(LocalDate vToDate)
    {
        return vToDate.toEpochDay() - pInitialDate.toEpochDay();
    }

    public long calculateDK_DAT(Date vToDate)
    {
        return vToDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate().toEpochDay() - pInitialDate.toEpochDay();
    }

    public void dropHourlyPartition( FileSystem hdfs, String vTableDailyPartitionPath, int fromHour, int toHour) throws IOException
    {
        for (int i = fromHour; i <=  toHour; i++)
        {
            Path pathToDelete =new Path(new StringBuffer(vTableDailyPartitionPath).append("/").append(DK_TIM_HR_PARTITION_PREFIX).append("=").append(i).toString());

            if (hdfs.exists(pathToDelete) && hdfs.isDirectory(pathToDelete)) {
                logger.info("About to delete " + pathToDelete);
                hdfs.delete(pathToDelete, true);
                logger.info("Path " + pathToDelete + " Deleted!");
            }
        }
    }

    public void dropDailyPartitionsRange( FileSystem hdfs, String vTablePath, LocalDateTime fromDateTime, LocalDateTime toDateTime) throws IOException
    {
        int firstHour = fromDateTime.getHour()+1;
        int lastHour = toDateTime.getHour()==0 ? 24 : toDateTime.getHour();
        long fromDK_DAT = calculateDK_DAT(fromDateTime.toLocalDate());
        long toDK_DAT = calculateDK_DAT(toDateTime.toLocalDate());
        toDK_DAT=toDateTime.getHour()==0 ? toDK_DAT-1 : toDK_DAT;

        logger.info("Going to delete from DkDat " + fromDK_DAT + " Hour " + firstHour + ":00 to DkDat " + toDK_DAT + " Hour " + lastHour + ":00");
        for (long i=fromDK_DAT; i<=toDK_DAT; i++)
        {
            int deleteFromHour = i==fromDK_DAT ? firstHour : 1;
            int deleteToHour = i==toDK_DAT ? lastHour :24;

            String dailyPath=new StringBuffer(vTablePath).append("/").append(DK_DAT_PARTITION_PREFIX).append("=").append(i).toString();
            logger.info("Going to delete daily partition of DkDat " + i + " From Hour " + deleteFromHour + ":00 to Hour " + deleteToHour + ":00");
            dropHourlyPartition(hdfs,dailyPath,deleteFromHour,deleteToHour);
            logger.info("Deleted daily partition");
        }

        logger.info("Delete is Finished");
    }

    public void dropDailyPartitionsRangeForAllClients(FileSystem hdfs, String vTablePath, LocalDateTime fromDateTime, LocalDateTime toDateTime) throws IOException
    {
        List<FileStatus> list = Arrays.asList(
        hdfs.listStatus(new Path(vTablePath), new PathFilter() {
            @Override
            public boolean accept(Path path) {
                return path.getName().contains(CL_ID_PARTITION_PREFIX);
            }
        }));

        for (FileStatus fileStatus : list)
            dropDailyPartitionsRange(hdfs, vTablePath + "/" + fileStatus.getPath().getName(), fromDateTime, toDateTime);
    }

    public FileStatus[] getClientDirectories(FileSystem hdfs, String vTablePath) throws IOException
    {
        FileStatus[] fileStatus = hdfs.listStatus(new Path(vTablePath), new PathFilter() {
            @Override
            public boolean accept(Path path) {
                return path.getName().contains(CL_ID_PARTITION_PREFIX);
            }
        });
        return fileStatus;
    }

    public Integer[] getClientIdListFromFileList(FileStatus[] fileStatus) throws IOException
    {
        List<Integer> clientList = new ArrayList();

        for (FileStatus status : fileStatus) {
            clientList.add(Integer.valueOf(status.getPath().getName().toString().replace(CL_ID_PARTITION_PREFIX + "=", "")));
        }

        return clientList.stream().toArray(Integer[]::new);
    }
}
