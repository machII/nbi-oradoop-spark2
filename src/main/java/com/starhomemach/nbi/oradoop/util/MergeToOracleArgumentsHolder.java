package com.starhomemach.nbi.oradoop.util;

import com.starhomemach.nbi.oradoop.conf.arguments.ArgumentsHolder;
import com.starhomemach.nbi.oradoop.conf.arguments.IsRequired;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MergeToOracleArgumentsHolder extends ArgumentsHolder
{

    private static MergeToOracleArgumentsHolder ourInstance =null;

    public static MergeToOracleArgumentsHolder getInstance() {
        if(ourInstance==null)
            ourInstance=new MergeToOracleArgumentsHolder();

        return ourInstance;
    }

    @IsRequired
    private String sourceJdbcUrl;
    private String sourceJdbcDriver;
    private String sourceUser;
    private String  sourcePassword;


    private String sourceQuery;

    @IsRequired
    private String destinationJdbcUrl;
    private String destinationJdbcDriver;
    private String destinationUser;
    private String destinationPassword;
    private String destinationTable;

    private String mergeSqlQuery;

    private int batcSize=200;
}


