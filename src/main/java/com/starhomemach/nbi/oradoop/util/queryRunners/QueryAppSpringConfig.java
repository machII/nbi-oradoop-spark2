package com.starhomemach.nbi.oradoop.util.queryRunners;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;


@Configuration
@ComponentScan(basePackages = {"com.starhomemach.nbi.oradoop.util.queryRunners"})
public class QueryAppSpringConfig {

}
