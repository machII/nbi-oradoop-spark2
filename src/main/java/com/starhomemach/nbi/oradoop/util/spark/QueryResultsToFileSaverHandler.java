package com.starhomemach.nbi.oradoop.util.spark;

import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.storage.StorageLevel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static org.apache.spark.sql.functions.*;
import static org.apache.spark.sql.types.DataTypes.DoubleType;
import static org.apache.spark.sql.types.DataTypes.FloatType;

@Service
public class QueryResultsToFileSaverHandler {

    private QueryResultsToFileSaverArgumentsHolder arguments;
    private String query;
    private String dirPath;
    private String fileName;
    private String fileFormat;
    private String csvDelimiter;
    private String headers;

    @Autowired
    SparkSession sparkSession;

    @Autowired
    private org.apache.hadoop.conf.Configuration hadoopConf;

    public void run() throws IOException {

        initArguments();

        if (StringUtils.isNotBlank(arguments.getSpakrConfProperties())) {
            List<String> sparkConfProperties = Arrays.asList(arguments.getSpakrConfProperties().split(arguments.getSpakrConfPropertiesDelimiter()));
            for (String property : sparkConfProperties) {
                sparkSession.conf().set(property.split("=")[0], property.split("=")[1]);
            }
        }

        String tempDir = dirPath + "/" + arguments.getJobId();

        System.out.println("Executing:\n" + query);
        Dataset<Row> dfToSave = sparkSession.sql(query).coalesce(arguments.getNumPartitions()).persist(StorageLevel.MEMORY_AND_DISK());

        if (dfToSave.count() > 0) {
            System.out.println("\nSaving results to: " + tempDir);

            for (StructField col : dfToSave.schema().fields()) {
                System.out.println("Colums: " + col.name() + " Type:" + col.dataType());
                if (col.dataType().equals(DoubleType) || col.dataType().equals(FloatType) || col.dataType().getClass().getSimpleName().equals("DecimalType")) {
                    System.out.println("Handling " + col.dataType() + " columm:" + col.name());
                    dfToSave = dfToSave.withColumn(col.name(), format_number(dfToSave.col(col.name()), arguments.getNumberOfDigitsAfterDecimalDot()).cast("decimal(38,10)"));


                    dfToSave = dfToSave.withColumn(col.name(), when(col(col.name()).equalTo(lit(0)), lit(0)).otherwise(regexp_replace(col(col.name()), "()\\.0+$|(\\..+?)0+$", "$2")));
                }
            }

            dfToSave.coalesce(1)
                    .write().format(fileFormat)
                    .mode(SaveMode.Overwrite)
                    .option("delimiter", csvDelimiter)
                    .option("emptyValue", "")
                    .option("header", headers)
                    .save(tempDir);

            renameFile(tempDir);
        }
    }

    private void renameFile(String tempDir) throws IOException {


        Path path = new Path(tempDir);
        FileSystem fileSystem = path.getFileSystem(hadoopConf);
        String existingFileName = fileSystem.globStatus(new Path(tempDir + "/part*"))[0].getPath().getName();

        Path newPath = new Path(dirPath + "/" + fileName);

        System.out.println("\nChecking if file: " + newPath + " already exists...");
        // if new file is already exist in dir (can be when reprocessing fl table record with error indication) delete it
        if (fileSystem.exists(newPath)) {
            System.out.println("File already exists. Deleting it...");
            fileSystem.delete(newPath, false);
        }

        System.out.println("\nMoving file: " + tempDir + "/" + existingFileName + " to: " + dirPath + "/" + fileName + "...");
        fileSystem.rename(new Path(tempDir + "/" + existingFileName), newPath);

        System.out.println("\nDeleting temp dir: " + tempDir + "...");

        fileSystem.delete(new Path(tempDir), true);
    }

    private void initArguments() {
        arguments = QueryResultsToFileSaverArgumentsHolder.getInstance();
        System.out.println(arguments);
        query = arguments.getQuery();
        String fullFilePath = arguments.getFilePath();

        dirPath = fullFilePath.substring(0, fullFilePath.lastIndexOf("/"));
        fileName = fullFilePath.substring(fullFilePath.lastIndexOf("/") + 1);

        fileFormat = arguments.getFileFormat();
        csvDelimiter = arguments.getCsvDelimiter();
        headers = arguments.getHeaders();
    }
}
