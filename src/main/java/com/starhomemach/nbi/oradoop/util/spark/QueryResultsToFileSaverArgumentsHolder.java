package com.starhomemach.nbi.oradoop.util.spark;

import com.starhomemach.nbi.oradoop.conf.arguments.ArgumentsHolder;
import com.starhomemach.nbi.oradoop.conf.arguments.IsRequired;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class QueryResultsToFileSaverArgumentsHolder extends ArgumentsHolder
{


    @IsRequired
    private String query;

    @IsRequired
    private String filePath;

    private String fileFormat="com.databricks.spark.csv";

    private String csvDelimiter=",";

    private String headers="false";

    private String spakrConfProperties ="";

    private String spakrConfPropertiesDelimiter ="";

    private Integer numberOfDigitsAfterDecimalDot = 10;

}