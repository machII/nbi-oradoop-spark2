package com.starhomemach.nbi.oradoop.util;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.client.Table;

/**
 * Created by yamar on 08/05/2017.
 */

public interface HbaseClient
{
    boolean connectToHbase();

    boolean isTableExist(String tableName);

    Table   getTable(String tableName);

    void closeConnection();

    Configuration getConfig();



}
