package com.starhomemach.nbi.oradoop.util.spark;

import org.apache.commons.lang.StringUtils;
import org.apache.directory.api.util.Strings;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import scala.collection.JavaConverters;
import scala.collection.Seq;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class QueryResultsToTableSaverHandler {

    private QueryResultsToTableSaverArgumentsHolder arguments;
    private String query;
    private String table;
    private List<Column> partitionColums;

    @Autowired
    SparkSession sparkSession;

    @Autowired
    TableStructureUtil tableStructureUtil;

    @Autowired
    DataSaver dataSaver;
    private Boolean overwrite;

    public void run() throws IOException {

        initArguments();

        if (StringUtils.isNotBlank(arguments.getSpakrConfProperties())) {
            List<String> sparkConfProperties = Arrays.asList(arguments.getSpakrConfProperties().split(arguments.getSpakrConfPropertiesDelimiter()));
            for (String property : sparkConfProperties) {
                sparkSession.conf().set(property.split("=")[0], property.split("=")[1]);
            }
        }

        System.out.println("Executing:\n" + query);
        Dataset<Row> dfToSave = sparkSession.sql(query);

        if (partitionColums != null && partitionColums.size() > 0) {
            System.out.println("Repartition data by columns: " + partitionColums);
            Seq columnSeq = JavaConverters.asScalaIteratorConverter(partitionColums.iterator()).asScala().toSeq();
            dfToSave = dfToSave.repartition(columnSeq);
        }

        else if (arguments.getNumPartitions()>0) {
            System.out.println("Repartition data to: " + arguments.getNumPartitions() + " partitions");
           dfToSave = dfToSave.coalesce(arguments.getNumPartitions());
        }

        if (arguments.getDebugMode()) {
            System.out.println("Dataframe size: " + dfToSave.count());
            dfToSave.show();
        }
        TableStructureUtil.Table destTableStruct = tableStructureUtil.getTableStructure(table.split("\\.")[0], table.split("\\.")[1]);
        String destTablePartitions = destTableStruct.getPartitions().stream().map(structField -> structField.name()).collect(Collectors.joining(","));
        System.out.println("Destination table partitions: " + destTablePartitions);

        System.out.println("\nSaving results to: " + table);

        String[] partitions = Strings.isEmpty(destTablePartitions) ? null : destTablePartitions.split(",");
        String dbName = table.split("\\.")[0];
        String tblName = table.split("\\.")[1];
        if(overwrite) {
            dataSaver.saveDataOverwrite(dfToSave, partitions, dbName,tblName);
        }
        else {
            dataSaver.saveDataAppend(dfToSave, partitions, dbName,tblName);
        }

    }


    private void initArguments() {
        arguments = QueryResultsToTableSaverArgumentsHolder.getInstance();
        System.out.println(arguments);

        query = arguments.getSourceQuery();
        table = arguments.getDestinationTable();

        overwrite = arguments.getOverWrite();

        if (!Strings.isEmpty(arguments.getPartitionMidDataframeBy().trim())) {
            partitionColums = new ArrayList<>();
            Arrays.stream(arguments.getPartitionMidDataframeBy().split(",")).forEach(colName -> partitionColums.add(new Column((colName))));
        }
    }
}
