package com.starhomemach.nbi.oradoop.util;

import com.starhomemach.nbi.oradoop.OradoopConf;
import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Service
public class MergeToOracleRunner {

    MergeToOracleArgumentsHolder arguments = MergeToOracleArgumentsHolder.getInstance();
    private String sourceJdbcUrl;
    private String sourceJdbcDriver;
    private String sourceUser;
    private String sourcePassword;
    private String sourceQuery;

    private String destinationJdbcUrl;
    private String destinationJdbcDriver;
    private String destinationUser;
    private String destinationPassword;

    private String destinationTable;
    private String mergeSqlQuery;

    private int batchSize;

    Connection sourceConnection;
    Connection destinationConnection;

    public void run() throws Exception {

        getArguments();

        sourceConnection = getConnection(sourceJdbcUrl, sourceJdbcDriver, sourceUser, sourcePassword);
        destinationConnection = getConnection(destinationJdbcUrl, destinationJdbcDriver, destinationUser, destinationPassword);
        destinationConnection.setAutoCommit(false);

        ResultSet sourceRs = executeQuery(sourceConnection, sourceQuery);

        PreparedStatement insertQueryPreparedStatement = prepareInsertQueryStatement(destinationConnection, sourceRs);

        executeInsertQueryStatement(sourceRs, insertQueryPreparedStatement);

        insertQueryPreparedStatement.executeBatch();

        if(StringUtils.isNotBlank(mergeSqlQuery)) {
            executeQuery(destinationConnection, mergeSqlQuery);
        }

        destinationConnection.commit();

        sourceConnection.close();
        destinationConnection.close();
    }

    private void getArguments() {
        sourceJdbcUrl = arguments.getSourceJdbcUrl();
        sourceJdbcDriver = arguments.getSourceJdbcDriver();
        sourceUser = arguments.getSourceUser();
        sourcePassword = arguments.getSourcePassword();
        sourceQuery = arguments.getSourceQuery();

        destinationJdbcUrl = arguments.getDestinationJdbcUrl();
        destinationJdbcDriver = arguments.getDestinationJdbcDriver();
        destinationUser = arguments.getDestinationUser();
        destinationPassword = arguments.getDestinationPassword();

        destinationTable = arguments.getDestinationTable();
        mergeSqlQuery = arguments.getMergeSqlQuery();

        batchSize = arguments.getBatcSize();

        System.out.println(arguments.toString());
    }

    private ResultSet executeQuery(Connection con, String query) throws SQLException {
        Statement sourceStmt = con.createStatement();
        System.out.println("Excecuting query:-------------------");
        System.out.println(query);

        return sourceStmt.executeQuery(query);
    }

    private PreparedStatement prepareInsertQueryStatement(Connection con, ResultSet rs) throws Exception {
        StringBuilder insertQueryString = new StringBuilder("insert into " + destinationTable);
        ResultSetMetaData sourceRsMetaData = rs.getMetaData();
        int numOfColumns = sourceRsMetaData.getColumnCount();
        List<String> columnsNames = new ArrayList<String>();
        for (int i = 1; i <= numOfColumns; i++) {
            columnsNames.add(sourceRsMetaData.getColumnName(i));
        }

        insertQueryString.append("(" + String.join(",", columnsNames) + ")").append(" values (").append(String.join(",", StringUtils.repeat("?", numOfColumns).split(""))).append(")");
        return con.prepareStatement(insertQueryString.toString());
    }


    private void executeInsertQueryStatement(ResultSet rs, PreparedStatement pstmt) throws Exception {

        int totalRowsSize=0;
        int currentBatchSize = 0;

        while (rs.next()) {
            totalRowsSize++;
            setInsertPreparedStatment(rs,pstmt);
            currentBatchSize++;
            pstmt.addBatch();

            if (currentBatchSize >= batchSize) {
                pstmt.executeBatch();
                currentBatchSize = 0;
            }
        }
        pstmt.executeBatch();
        System.out.println("Total rows: " + totalRowsSize);
    }

    private void setInsertPreparedStatment(ResultSet rs, PreparedStatement pstmt) throws Exception {
        ResultSetMetaData rsMetaData = rs.getMetaData();
        int numOfColumns = rsMetaData.getColumnCount();
        for (int i = 1; i <= numOfColumns; i++) {
            String columnType = rsMetaData.getColumnTypeName(i).toLowerCase();
            if (columnType.contains("int"))
                pstmt.setInt(i, rs.getInt(i));

            else if (columnType.contains("string") || columnType.contains("varchar"))
                pstmt.setString(i, rs.getString(i));

            else if (columnType.contains("long"))
                pstmt.setLong(i, rs.getLong(i));

            else if (columnType.contains("double"))
                pstmt.setDouble(i, rs.getDouble(i));

            else if (columnType.contains("float"))
                pstmt.setFloat(i, rs.getFloat(i));

            else if (columnType.contains("bool"))
                pstmt.setBoolean(i, rs.getBoolean(i));

            else if (columnType.contains("byte"))
                pstmt.setByte(i, rs.getByte(i));

            else if (columnType.contains("timestamp"))
                pstmt.setTimestamp(i, rs.getTimestamp(i));

            else if (columnType.contains("time"))
                pstmt.setTime(i, rs.getTime(i));

            else if (columnType.contains("date"))
                pstmt.setDate(i, rs.getDate(i));

            else throw (new Exception("Couldn't find field data type."));
        }
    }

    private Connection getConnection(String jdbcUrl, String jdbcDriver, String user, String password) throws ClassNotFoundException, SQLException {
        System.out.println("Trying to load connection:" + jdbcUrl + "," + jdbcDriver + "," + user + "," + password);
        if (StringUtils.isNotBlank(jdbcDriver)) {
            System.out.println("Loading source class driver: " + jdbcDriver);
            Class.forName(jdbcDriver);
            System.out.println("Success loading class driver: " + jdbcDriver);
        }

        System.out.println("Getting connection: " + jdbcUrl + "....");
        Connection connection = DriverManager.getConnection(jdbcUrl, user, password);
        System.out.println("Success getting connection: " + jdbcUrl + "....");

        return connection;
    }

    public String getQueryFromFile(String filePath, String[] params) throws IOException {

        System.out.println("Starting parse file: " + filePath);
        StringBuilder queriesBuffers = new StringBuilder();

        BufferedReader br = null;

        try {
            Configuration conf = new OradoopConf().getConf();
            FileSystem fileSystem = FileSystem.newInstance(conf);

            br = new BufferedReader(new InputStreamReader(fileSystem.open(new Path(filePath))));

            String sCurrentLine;

            int rows = 0;
            while ((sCurrentLine = br.readLine()) != null) {
                queriesBuffers.append(sCurrentLine);
                queriesBuffers.append("\n");
                rows++;
            }

            System.out.println("Total of: " + rows + " rows.");
        } finally {

            try {

                if (br != null)
                    br.close();

            } catch (IOException ex) {

                ex.printStackTrace();

            }

        }
        String query = this.myFormat(queriesBuffers.toString(), params);
        return query;
    }

    private String myFormat(String s, String[] params) {
        for (int i = 0; i < params.length; i++) {
            s = s.replaceFirst("%s", params[i]);
        }
        return s;
    }


}