package com.starhomemach.nbi.oradoop.util.generic;


import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class SparkJdbcConfigDetails implements Serializable {
  @JsonProperty("url")
  private String url;

  @JsonProperty("user")
  private String jdbcUserName;

  @JsonProperty("password")
  private String jdbcPassword;

  @JsonProperty("driver")
  private String JdbcDriver;

  @JsonProperty("fetchsize")
  private Integer fetchSize = 100000;

  @JsonProperty("batchsize")
  private Integer batchSize = 100000;

}
