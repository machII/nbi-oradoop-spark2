package com.starhomemach.nbi.oradoop.util.queryRunners;

import com.starhomemach.nbi.oradoop.OradoopConf;
import com.facebook.presto.jdbc.PrestoConnection;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.springframework.stereotype.Service;

import java.io.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Created by tpin on 19/12/2016.
 */
@Service
public class QueryAppAnySourceHandler {

    private final Log logger = LogFactory.getLog(getClass());

    QueryRunnerArgumentsHolder queryRunnerArgumentsHolder = QueryRunnerArgumentsHolder.getInstance();
    String jdbcDriver;
    String jdbcUrl;
    String userName;
    String password;
    boolean isQueryFromFile;
    String filePath;
    String queryString;
    String queryParams;
    String queryParamsByName;
    boolean setAutoCommitToFalse;
    boolean printAsTableToConsole;
    String charToReplace;
    String replaceWith;
    boolean runLastResult;
    String queryDelimiter;

    static String parameterSign = "%";

    public void run() throws Exception {
        getArgumnets();
        String[] queries = getQueries();
        Connection con = getConnection();
        runQueries(con, queries);
    }

    private String useResultsFromPreviousQueriesResults(String query, Properties properties) {
        String newQuery = query;

        for (Object key : properties.keySet()) {
            newQuery = newQuery.replace("[" + key.toString() + "]", properties.get(key).toString());
        }
        return newQuery;
    }

    private void runQueries(Connection con, String[] queries) throws IOException, SQLException {


        String propertiesFilePath = System.getProperty("oozie.action.output.properties");
        OutputStream outputStream = new FileOutputStream(propertiesFilePath);
        Properties properties = new Properties();
        List<String> lastResults = null;
        int i = 0;

        try {
            for (String query : queries) {
                if (!query.trim().isEmpty()) {
                    query = replaceIfNeeded(query);

                    query = useResultsFromPreviousQueriesResults(query, properties);

                    if (query.trim().toLowerCase().startsWith("select") || query.trim().toLowerCase().startsWith("with"))
                        if (!printAsTableToConsole)
                            lastResults = runQuery(i++, con, query, properties);
                        else
                            runQueryAndPrintAsTable(con, query);
                    else if (query.trim().startsWith("set session")) {
                        System.out.println("\nExecuting:" + query);
                        String propertyName = query.replace("set session", "").split("=")[0].trim();
                        String propertyValue = query.replace("set session", "").split("=")[1].trim();
                        ((PrestoConnection) con).setSessionProperty(propertyName, propertyValue);
                    } else
                        runUpdate(con, query);
                }
            }
            if (runLastResult) {
                if (lastResults != null) {
                    for (String lastResult : lastResults) {
                        runUpdate(con, lastResult);
                    }
                }
            }
            properties.store(outputStream, "");
            outputStream.close();
        }
        catch (Exception e) {
            logger.error("Got exception while running queries " + queries, e);
            e.printStackTrace();
            throw e;
        }
    }

    private String replaceIfNeeded(String query) {

        if (charToReplace != null) {
            System.out.println(charToReplace);
            System.out.println(replaceWith);
            System.out.println(query);
            return query.replaceAll(charToReplace, replaceWith);
        }
        return query;
    }

    private Connection getConnection() throws ClassNotFoundException, SQLException {
        Connection connection;
        if (jdbcDriver != null && !jdbcDriver.trim().equals("")) {
            System.out.println("Loading class: " + jdbcDriver);
            Class.forName(jdbcDriver);
            System.out.println("Success loading class: " + jdbcDriver);
        }

        System.out.println("Getting connecting: " + jdbcUrl);
        connection = DriverManager.getConnection(jdbcUrl, userName, password);
        System.out.println("Success getting connecting: " + jdbcUrl);

        if (setAutoCommitToFalse) {
            System.out.println("Setting autocommit to false");
            connection.setAutoCommit(false);
        }

        return connection;
    }

    private String[] getQueries() throws IOException {
        String[] queries;
        if (isQueryFromFile) {
            System.out.println("Starting to parse file...");
            if (queryParamsByName != null && queryParamsByName.length() > 0)
                queries = this.parseFile(filePath, queryParamsByName.split(";"), true);
            else
                queries = this.parseFile(filePath, queryParams.split(","), false);
            System.out.println("Total queries after parsing: " + queries.length);
        } else {
            queries = queryString.split(queryDelimiter);
        }
        return queries;
    }

    private void getArgumnets() {
        jdbcDriver = queryRunnerArgumentsHolder.getJdbcDriver();
        jdbcUrl = queryRunnerArgumentsHolder.getJdbcUrl();
        userName = queryRunnerArgumentsHolder.getUserName();
        password = queryRunnerArgumentsHolder.getPassword();
        isQueryFromFile = queryRunnerArgumentsHolder.isQueryFromFile();
        filePath = queryRunnerArgumentsHolder.getFilePath();
        queryString = queryRunnerArgumentsHolder.getQueryString();
        queryParams = queryRunnerArgumentsHolder.getQueriesParams();
        queryParamsByName = queryRunnerArgumentsHolder.getQueriesParamsByName();
        setAutoCommitToFalse = queryRunnerArgumentsHolder.getIsSetAutoCommitToFalse();
        printAsTableToConsole = queryRunnerArgumentsHolder.isPrintAsTableToConsole();
        replaceWith = queryRunnerArgumentsHolder.getReplaceWith();
        charToReplace = queryRunnerArgumentsHolder.getCharToReplace();
        runLastResult = queryRunnerArgumentsHolder.isRunLastResult();
        queryDelimiter = queryRunnerArgumentsHolder.getQueryDelimiter();

        System.out.println("jdbcDriver: " + jdbcDriver);
        System.out.println("jdbcUrl: " + jdbcUrl);
        System.out.println("userName: " + userName);
        System.out.println("password: " + password);

        if (isQueryFromFile) {
            System.out.println("isQueryFromFile: " + isQueryFromFile);
            System.out.println("filePath: " + filePath);
        } else {
            System.out.println("queryString: " + queryString);
        }
        System.out.println("query params: " + queryParams);
        System.out.println("query params by name: " + queryParamsByName);
        System.out.println("print as table to console: " + printAsTableToConsole);

        if (setAutoCommitToFalse)
            System.out.println("setAutoCommitToFalse: " + setAutoCommitToFalse);

        System.out.println("--------------------------------------------------------------------------------------------------------");

    }

    public String[] parseFile(String filePath, String[] params, boolean paramsByName) throws IOException {
        System.out.println("Parsing file: " + filePath);
        System.out.println("params: " + String.join(",", params));
        StringBuilder queriesBuffers = null;
        BufferedReader br = null;
        int i = 0;
        try {

            Configuration conf = new OradoopConf().getConf();

            FileSystem fileSystem = FileSystem.newInstance(conf);

            queriesBuffers = new StringBuilder();

            br = new BufferedReader(new InputStreamReader(fileSystem.open(new Path(filePath))));

            String currentLine;

            while ((currentLine = br.readLine()) != null) {
                queriesBuffers.append(currentLine);
                queriesBuffers.append("\n");
                i++;
            }

        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        } finally {

            try {

                if (br != null)
                    br.close();

            } catch (IOException ex) {

                ex.printStackTrace();

            }

        }

        System.out.println("File size: " + i + " lines.");
        String queries = queriesBuffers.toString();
        if (params != null && params.length > 0)
            queries = plantParamsInQueries(queriesBuffers.toString(), params, paramsByName);
        String[] finalQueries = queries.split(queryDelimiter);
        System.out.println("Total num of queries from file: " + finalQueries.length);
        return finalQueries;
    }

    private String plantParamsInQueries(String s, String[] params, boolean paramsByName) {
        if (paramsByName) {
            for (int i = 0; i < params.length; i++) {
                String[] keyValue = params[i].split("=");
                s = s.replaceAll(parameterSign + keyValue[0] + parameterSign, keyValue[1]);
            }
        } else {
            for (int i = 0; i < params.length; i++) {
                s = s.replaceFirst(parameterSign + "s", params[i]);
            }
        }
        return s;
    }

    private void runUpdate(Connection con, String sqlQurey) throws SQLException {
        System.out.println("\nExecuting:" + sqlQurey);

        Statement stmnt = con.createStatement();
        stmnt.executeUpdate(sqlQurey);
        System.out.println("-----------------------------------");
    }


    private List<String> runQuery(int queryNum, Connection con, String sqlQurey, Properties properties) throws SQLException {
        System.out.println("Executing:\n" + sqlQurey);

        ResultSet resultSet = null;

        List<String> results = new ArrayList<>();
        Statement stmnt = con.createStatement();
        resultSet = stmnt.executeQuery(sqlQurey);

        ResultSetMetaData metadata = resultSet.getMetaData();
        int columnCount = metadata.getColumnCount();

        int rowCnt = 0;
        while (resultSet.next()) {
            for (int i = 1; i <= columnCount; i++) {
                properties.setProperty(queryNum + "-" + rowCnt + "-" + i, resultSet.getString(i) == null ? "" : resultSet.getString(i));
                results.add(resultSet.getString(i));
                System.out.println("param " + queryNum + "-" + rowCnt + "-" + i + "=" + resultSet.getString(i));
            }
            System.out.println();
            rowCnt++;

            System.out.println("Total of: " + rowCnt + " rows.");
            System.out.println("------------------------------------------------------------------------------------------------");
        }

        return results;
    }

    private ResultSet runQueryAndPrintAsTable(Connection con, String sqlQurey) throws SQLException, IOException {
        System.out.println("Executing:\n" + sqlQurey);

        Statement stmnt = con.createStatement();
        ResultSet rs = stmnt.executeQuery(sqlQurey);

        ResultSetMetaData resultSetMetaData = rs.getMetaData();

        int numberOfColumns = resultSetMetaData.getColumnCount();

        List<String[]> rsData = new ArrayList<String[]>();

        String[] headers = new String[numberOfColumns];

        for (int i = 1; i <= numberOfColumns; i++) {
            String columnName = resultSetMetaData.getColumnName(i);
            headers[i - 1] = columnName;
        }
        rsData.add(headers);

        while (rs.next()) {
            String[] currentLine = new String[numberOfColumns];
            for (int i = 1; i <= numberOfColumns; i++) {
                String columnValue = rs.getString(resultSetMetaData.getColumnName(i));
                currentLine[i - 1] = columnValue == null ? "NULL" : columnValue;
            }
            rsData.add(currentLine);
        }

        // find max length of each line
        int[] linesLens = new int[numberOfColumns];

        for (int i = 0; i < numberOfColumns; i++) {
            linesLens[i] = 0;
            for (int j = 0; j < rsData.size(); j++) {
                if (rsData.get(j)[i].length() > linesLens[i])
                    linesLens[i] = rsData.get(j)[i].length();
            }
        }

        // printing pretty table
        int totalLineLen = 0;

        for (int i = 0; i < linesLens.length; i++)
            totalLineLen = totalLineLen + linesLens[i] + 3;

        System.out.print("  ");
        for (int i = 0; i < totalLineLen; i++)
            System.out.print("-");
        System.out.println();

        for (int i = 0; i < rsData.size(); i++) {
            for (int j = 0; j < numberOfColumns; j++) {
                String currentVal = rsData.get(i)[j];
                int max_len = linesLens[j];
                System.out.print(" | ");

                for (int spaces = 0; spaces < max_len - currentVal.length(); spaces++)
                    System.out.print(" ");
                System.out.print(currentVal);

            }
            System.out.println("  |");

            System.out.print("  ");
            for (int l = 0; l < totalLineLen; l++)
                System.out.print("-");
            System.out.println();
        }

        return rs;
    }

}
