//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.starhomemach.nbi.oradoop.util;

import com.starhomemach.nbi.oradoop.dimension.metadata.HcatQuery;
import com.starhomemach.nbi.oradoop.export.SpringConfig;
import java.io.FileOutputStream;
import java.util.Properties;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Service;

@Service
public class HCatUtil {
    public HCatUtil() {
    }

    public  static void main(String[] args) throws Exception {
        System.out.println("################################################ START ####################################################");
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(new Class[]{SpringConfig.class});
        String retValPart = ((HcatQuery)context.getBean(HcatQuery.class)).query(new String[]{args[0], args[1], args[2], "PARTCOLS"});
        System.out.println("PARTITION KEYS: " + retValPart);
        String retValPartNoDT = ((HcatQuery)context.getBean(HcatQuery.class)).query(new String[]{args[0], args[1], args[2], "PARTCOLS", "NO_DATATYPE"});
        System.out.println("PARTITION KEYS: " + retValPartNoDT);
        String retValEx = ((HcatQuery)context.getBean(HcatQuery.class)).getColumnsWithExclude(new String[]{args[0], args[1], args[2], "COLUMNS", "cre_dat,mod_dat"});
        System.out.println("ALL COLUMNS (NO PARTITION KEY OR CRE_DAT OR MOD_DAT): " + retValEx);
        String retVal = ((HcatQuery)context.getBean(HcatQuery.class)).query(new String[]{args[0], args[1], args[2], "COLUMNS"});
        System.out.println("ALL COLUMNS(NO PARTITION KEY): " + retVal);
        String filePath = System.getProperty("oozie.action.output.properties");
        if(filePath != null) {
            FileOutputStream outputStream = new FileOutputStream(filePath);
            Properties p = new Properties();
            p.setProperty("COLUMNS_EX", retValEx);
            p.setProperty("COLUMNS", retVal);
            p.setProperty("PARTCOLS", retValPart);
            p.setProperty("PARTCOLS_NO_DT", retValPartNoDT);
            p.setProperty("IS_PART", retValPart.length() > 0?"1":"0");
            p.setProperty("PART_STMT", retValPart.length() > 0?"PARTITIONED BY(" + retValPart + ")":"");
            p.setProperty("PART_STMT2", retValPart.length() > 0?"PARTITION (" + retValPartNoDT + ")":"");
            p.store(outputStream, "");
            outputStream.close();
        }

        System.out.println("################################################ END ####################################################");
    }
}
