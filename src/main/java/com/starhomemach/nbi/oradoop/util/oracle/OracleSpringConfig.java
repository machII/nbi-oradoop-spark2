package com.starhomemach.nbi.oradoop.util.oracle;

import com.starhomemach.nbi.oradoop.export.util.OradoopProcessArgs;
import oracle.jdbc.pool.OracleConnectionPoolDataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;

import java.sql.SQLException;


@Configuration
@ComponentScan(basePackages = {"com.starhomemach.nbi.oradoop.util.oracle"})
public class OracleSpringConfig {

    @Bean
    @Qualifier("oracleConnectionPoolDataSource")
    public OracleConnectionPoolDataSource oracleConnectionPoolDataSource() {

        OracleConnectionPoolDataSource oracleConnectionPoolDataSource = null;
        try {
            oracleConnectionPoolDataSource = new OracleConnectionPoolDataSource();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        OradoopProcessArgs oradoopProcessArgs = OradoopProcessArgs.getInstance();
        oracleConnectionPoolDataSource.setURL(oradoopProcessArgs.getOracleDburl());
        oracleConnectionPoolDataSource.setUser(oradoopProcessArgs.getUser());
        oracleConnectionPoolDataSource.setPassword(oradoopProcessArgs.getPassword());
        return oracleConnectionPoolDataSource;
    }

    @Bean
    public JdbcTemplate jdbcTemplate() {
        return new JdbcTemplate(oracleConnectionPoolDataSource());
    }

}
