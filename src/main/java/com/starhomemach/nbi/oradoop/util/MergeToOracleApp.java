package com.starhomemach.nbi.oradoop.util;

import com.starhomemach.nbi.oradoop.export.SpringConfig;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Service;

@Service
public class MergeToOracleApp {
    public static void main(String[] args) throws Exception {
        System.out.println("################################################ START ####################################################");

        MergeToOracleArgumentsHolder mergeToOracleArgumentsHolder = MergeToOracleArgumentsHolder.getInstance(MergeToOracleArgumentsHolder.class);
        mergeToOracleArgumentsHolder.initArgumentsHolder(args);
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SpringConfig.class);
        context.getBean(MergeToOracleRunner.class).run();

    }


}
