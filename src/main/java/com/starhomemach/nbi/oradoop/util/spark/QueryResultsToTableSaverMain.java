package com.starhomemach.nbi.oradoop.util.spark;

import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Created by tpin on 19/12/2016.
 */

public class QueryResultsToTableSaverMain {


    public static void main(String[] args) throws Exception
    {
        System.out.println("################################################ START ####################################################");

        QueryResultsToTableSaverArgumentsHolder queryResultsToTableSaverArgumentsHolder = QueryResultsToTableSaverArgumentsHolder.getInstance(QueryResultsToTableSaverArgumentsHolder.class);
        queryResultsToTableSaverArgumentsHolder.initArgumentsHolder(args);

        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();

        context.register(SparkSpringConfig.class);

        context.refresh();

        context.getBean(QueryResultsToTableSaverHandler.class).run();

        System.out.println("################################################ End ####################################################");


    }


    @Test
    public void replaceRegex() {
        String fullFilePath = "gabi/testing/20203003/fff.csv";

        String fileDirPath = fullFilePath.substring(0, fullFilePath.lastIndexOf("/"));
        String fileName = fullFilePath.substring(fullFilePath.lastIndexOf("/")+1);

        System.out.println(fileDirPath);
        System.out.println(fileName);
    }

}

