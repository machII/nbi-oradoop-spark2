package com.starhomemach.nbi.oradoop.util.spark;

import com.starhomemach.nbi.oradoop.util.spark.udaf.BitWiseOrAgg;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataTypes;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;


@Configuration
@ComponentScan(basePackages = {"com.starhomemach.nbi.oradoop.util.spark"})
public class SparkSpringConfig {

    SparkSession sparkSession;

    @Bean
    public SparkSession sparkSession() {

        sparkSession = SparkSession.builder().appName("tomiaNbi").enableHiveSupport().getOrCreate();

        System.out.println("Sparksession:" + sparkSession == null ? "Sparksession is null " : "Spark session has reached :)");
        sparkSession.conf().set("hive.exec.dynamic.partition", "true");
        sparkSession.conf().set("hive.exec.dynamic.partition.mode", "nonstrict");
        sparkSession.conf().set("spark.sql.hive.caseSensitiveInferenceMode", "NEVER_INFER");

        registerUdfs();


        return sparkSession;
    }

    private void registerUdfs() {
        sparkSession.udf().register("bitWiseOrAgg", new BitWiseOrAgg());
        sparkSession.udf().register("bitCount", (Long number) -> {return number==null?0:Long.bitCount(number);}, DataTypes.IntegerType);
    }

    @Bean
    public org.apache.hadoop.conf.Configuration conf() {
        org.apache.hadoop.conf.Configuration conf = sparkSession.sparkContext().hadoopConfiguration();
        return conf;
    }

}
