package com.starhomemach.nbi.oradoop.util;

import com.starhomemach.nbi.oradoop.datacompaction.FetchDatesHandler;
import com.starhomemach.nbi.oradoop.export.Application;
import com.starhomemach.nbi.oradoop.export.SpringConfig;
import com.starhomemach.nbi.oradoop.export.util.HandleProcessConfiguration;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Created by gtama on 15/06/2017.
 */
public class DeleteFromTrashMain {

    public static void main(String[] args) throws Exception {

        System.out.println("########################### STARTING TO DELETE FROM TRASH ####################################");
        if (args.length < 2) {
            System.out.println("ERROR args should contain the following parameters 1)USER 2)ContainigString");
            return;
        }
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SpringConfig.class);
        context.getBean(DeleteFromTrash.class).run(args[0],args[1]);
        System.out.println("########################### FINISHED DELETE FROM TRASH ####################################");
    }
}
