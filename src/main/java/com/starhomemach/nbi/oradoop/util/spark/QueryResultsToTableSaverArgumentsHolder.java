package com.starhomemach.nbi.oradoop.util.spark;

import com.starhomemach.nbi.oradoop.conf.arguments.ArgumentsHolder;
import com.starhomemach.nbi.oradoop.conf.arguments.IsRequired;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class QueryResultsToTableSaverArgumentsHolder extends ArgumentsHolder {


    @IsRequired
    private String sourceQuery;

    @IsRequired
    private String destinationTable;

    private String partitionMidDataframeBy = "";

    private String spakrConfProperties = "";

    private String spakrConfPropertiesDelimiter = "";

    private Boolean overWrite =false;


}