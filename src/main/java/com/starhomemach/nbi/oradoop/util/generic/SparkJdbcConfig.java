package com.starhomemach.nbi.oradoop.util.generic;


import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class SparkJdbcConfig implements Serializable {
  private SparkJdbcConfigDetails sparkJdbcConfigDetails;

}
