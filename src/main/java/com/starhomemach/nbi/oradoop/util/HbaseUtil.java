package com.starhomemach.nbi.oradoop.util;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Table;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;

/**
 * Created by yamar on 08/05/2017.
 */
@Component
@Windows(false)
public class HbaseUtil implements HbaseClient {

    Configuration config;

    Connection connection;

    @Override
    public boolean connectToHbase()
    {
        try {
            connection = ConnectionFactory.createConnection(config);

        } catch (IOException e) {
            System.err.println("FAILED TO CONNECT TO HBASE!!!!");
            e.printStackTrace();
            throw  new RuntimeException();
        }
        return true;
    }

    @Override
    public boolean isTableExist(String tableName) {
        TableName tname = TableName.valueOf(tableName);
        boolean found;
        try {
            found = connection.getAdmin().tableExists(tname);
        } catch (IOException e) {
            System.err.println("Failed to check if table is in Hbase ");
            e.printStackTrace();
            return false;
        }
        return found;
    }

    @Override
    public Table getTable(String tableName) {
        TableName tname = TableName.valueOf(tableName);
        Table table= null;
        try {
            table = connection.getTable(tname);
        } catch (IOException e) {
            e.printStackTrace();
            System.err.println("FAILED TO GET TABLE");
        }
        return table;

    }

    @Override
    public void closeConnection()
    {
        if(connection != null){
            try {
                connection.close();
            } catch (IOException e) {
                System.err.println("Err on connection shutdown");
                e.printStackTrace();
            }
        }
    }

    @PostConstruct
    public void setHbaseConfig(){
        config = HBaseConfiguration.create();
        //TODO check if needed to take from config
        config.addResource(new Path("/etc/hbase/conf/hbase-site.xml"));
        config.addResource(new Path("/etc/hbase/conf/core-site.xml"));
    }


    @Override
    public Configuration getConfig() {
        return config;
    }
}
