package com.starhomemach.nbi.oradoop.util.genericexport;

import com.starhomemach.nbi.oradoop.commons.CommonSpringConfig;
import com.starhomemach.nbi.oradoop.export.util.OradoopProcessArgs;
import com.starhomemach.nbi.oradoop.util.oracle.OracleSpringConfig;
import com.starhomemach.nbi.oradoop.util.spark.SparkSpringConfig;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Created by Maor Blumenfeld on 05/2021.
 */

public class GenericExportToFileSaverMain
{
    public static void main(String[] args) throws Exception
    {
        System.out.println("################################################ START ####################################################");

        GenericExportToFileSaverArgumentsHolder genericExportToFileSaverArgumentsHolder = GenericExportToFileSaverArgumentsHolder.getInstance(GenericExportToFileSaverArgumentsHolder.class);
        genericExportToFileSaverArgumentsHolder.initArgumentsHolder(args);

        OradoopProcessArgs.getInstance().oracleDbUrl(genericExportToFileSaverArgumentsHolder.getOracleUrl())
                                        .user(genericExportToFileSaverArgumentsHolder.getOracleUser())
                                        .password(genericExportToFileSaverArgumentsHolder.getOraclePassword());

        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(GenericExportSpringConfig.class, SparkSpringConfig.class, OracleSpringConfig.class, CommonSpringConfig.class);


        context.getBean(GenericExportToFileSaverHandler.class).run();

        System.out.println("################################################ End ####################################################");


    }
}

