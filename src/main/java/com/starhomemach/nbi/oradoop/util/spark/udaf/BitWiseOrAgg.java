package com.starhomemach.nbi.oradoop.util.spark.udaf;

import org.apache.spark.sql.Row;
import org.apache.spark.sql.expressions.MutableAggregationBuffer;
import org.apache.spark.sql.expressions.UserDefinedAggregateFunction;
import org.apache.spark.sql.types.DataType;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;

import java.util.ArrayList;
import java.util.List;

/**
 * An example {@link UserDefinedAggregateFunction} to calculate the sum of a
 * {@link org.apache.spark.sql.types.LongType} column.
 */
public class BitWiseOrAgg extends UserDefinedAggregateFunction {

	private StructType inputDataType;

	private StructType bufferSchema;

	private DataType returnDataType;

	public BitWiseOrAgg() {
		List<StructField> inputFields = new ArrayList<>();
		inputFields.add(DataTypes.createStructField("inputLong", DataTypes.LongType, true));
		inputDataType = DataTypes.createStructType(inputFields);

		List<StructField> bufferFields = new ArrayList<>();
		bufferFields.add(DataTypes.createStructField("bufferLong", DataTypes.LongType, true));
		bufferSchema = DataTypes.createStructType(bufferFields);

		returnDataType = DataTypes.LongType;
	}

	@Override public StructType inputSchema() {
		return inputDataType;
	}

	@Override public StructType bufferSchema() {
		return bufferSchema;
	}

	@Override public DataType dataType() {
		return returnDataType;
	}

	@Override public boolean deterministic() {
		return true;
	}

	@Override public void initialize(MutableAggregationBuffer buffer) {
		// The initial value of the sum is null.
		buffer.update(0, null);
	}

	@Override public void update(MutableAggregationBuffer buffer, Row input) {
		// This input Row only has a single column storing the input value in Long.
		// We only update the buffer when the input value is not null.
		if (!input.isNullAt(0)) {
			if (buffer.isNullAt(0)) {
				// If the buffer value (the intermediate result of the sum) is still null,
				// we set the input value to the buffer.
				buffer.update(0, input.getLong(0));
			} else {
				// Otherwise, we add the input value to the buffer value.
				Long newValue =  buffer.getLong(0) | input.getLong(0) ;
				buffer.update(0, newValue);
			}
		}
	}

	@Override public void merge(MutableAggregationBuffer buffer1, Row buffer2) {
		// buffer1 and buffer2 have the same structure.
		// We only update the buffer1 when the input buffer2's value is not null.
		if (!buffer2.isNullAt(0)) {
			if (buffer1.isNullAt(0)) {
				// If the buffer value (intermediate result of the sum) is still null,
				// we set the it as the input buffer's value.
				buffer1.update(0, buffer2.getLong(0));
			} else {
				// Otherwise, we add the input buffer's value (buffer1) to the mutable
				// buffer's value (buffer2).
				Long newValue = buffer2.getLong(0) | buffer1.getLong(0);
				buffer1.update(0, newValue);
			}
		}
	}

	@Override public Object evaluate(Row buffer) {
		if (buffer.isNullAt(0)) {
			// If the buffer value is still null, we return null.
			return null;
		} else {
			// Otherwise, the intermediate sum is the final result.
			return buffer.getLong(0);
		}

	}
}