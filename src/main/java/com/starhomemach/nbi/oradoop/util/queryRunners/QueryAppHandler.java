package com.starhomemach.nbi.oradoop.util.queryRunners;

import org.springframework.stereotype.Service;

import java.io.FileOutputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;

/**
 * Created by tpin on 19/12/2016.
 */
@Service
public class QueryAppHandler {
    public void runQuery(String jdbcDriver, String jdbcUrl, int stmtCnt, String[] sqlStmt, String userName, String password) throws Exception
    {
        System.out.println("Using JDBC driver: " + jdbcDriver);
        System.out.println("Using Connection URL: " + jdbcUrl);
        System.out.println("Username: " + ( userName == null ? "N/A" : userName ));
        System.out.println("Statement Count: " + stmtCnt);

        Connection con = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            Class.forName(jdbcDriver);

            // if no username given then try connecting without
            if (userName == null)
                con = DriverManager.getConnection(jdbcUrl);
            else
                con = DriverManager.getConnection(jdbcUrl, userName, password);
            con.setAutoCommit(false);
            stmt = con.createStatement();

            String filePath = System.getProperty("oozie.action.output.properties");
            if (filePath != null) {
                OutputStream outputStream = new FileOutputStream(filePath);
                Properties p = new Properties();


                for (int i = 0; i < stmtCnt; i++) {
                    System.out.println("Running Query: " + sqlStmt[i + 3]);
                    boolean isQuery = stmt.execute(sqlStmt[i + 3]);

                    if (isQuery) {
                        rs = stmt.getResultSet();
                        int rowCnt = 0;
                        while (rs.next()) {
                            for (int j = 1; j <= rs.getMetaData().getColumnCount(); j++) {
                                p.setProperty(i + "-" + rowCnt + "-" + Integer.toString(j), rs.getString(j) == null ? "" : rs.getString(j));
                                System.out.println("param " + i + "-" + rowCnt + "-" + Integer.toString(j) + "=" + rs.getString(j));
                            }
                            rowCnt++;
                        }
                    } else {
                        int count = stmt.getUpdateCount();
                        System.out.println("SQL inserted/updated/deleted " + count + " rows.");
                    }
                }

                p.store(outputStream, "");
                outputStream.close();
            } else {
                throw new RuntimeException("oozie.action.output.properties System property not defined");
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            try {
                if (rs != null)
                    rs.close();
                if (stmt != null)
                    stmt.close();
                if (con != null)
                    con.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
