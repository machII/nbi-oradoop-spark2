package com.starhomemach.nbi.oradoop.util.genericexport;

import com.starhomemach.nbi.oradoop.conf.arguments.ArgumentsHolder;
import com.starhomemach.nbi.oradoop.conf.arguments.IsRequired;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GenericExportToFileSaverArgumentsHolder extends ArgumentsHolder
{
    @IsRequired
    private String oracleUrl;

    @IsRequired
    private String oracleUser;

    @IsRequired
    private String oraclePassword;

    @IsRequired
    private Long expJobQueId;

    private String fileFormat="com.databricks.spark.csv";

    private Integer numberOfDigitsAfterDecimalDot = 10;
}