package com.starhomemach.nbi.oradoop.util.queryRunners;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Service;

/**
 * Created by tpin on 19/12/2016.
 */
@Service
public class QueryAppAnySourceApp {
    public static void main(String[] args) throws Exception
    {
        System.out.println("################################################ START ####################################################");

        QueryRunnerArgumentsHolder queryRunnerArgumentsHolder = QueryRunnerArgumentsHolder.getInstance();
        queryRunnerArgumentsHolder.initQureyRunnerArgumentsHolder(args);

        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(QueryAppSpringConfig.class);
        context.getBean(QueryAppAnySourceHandler.class).run();

    }



}
