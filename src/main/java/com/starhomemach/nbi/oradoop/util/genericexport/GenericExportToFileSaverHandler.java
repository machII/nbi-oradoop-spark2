package com.starhomemach.nbi.oradoop.util.genericexport;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.starhomemach.nbi.oradoop.util.generic.SparkJdbcConfigDetails;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.storage.StorageLevel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.apache.spark.sql.functions.*;
import static org.apache.spark.sql.types.DataTypes.DoubleType;
import static org.apache.spark.sql.types.DataTypes.FloatType;

@Service
public class GenericExportToFileSaverHandler
{
    private GenericExportToFileSaverArgumentsHolder arguments;
    private Long expJobQueId;
    private String query;
    private String queryFilePath;
    private String dirPath;
    private String fileName;
    private String fileFormat;
    private String csvDelimiter;
    private String headers;
    private String headerVal;
    private String sparkSessionConf;

    @Autowired
    SparkSession sparkSession;

    @Autowired
    private Configuration hadoopConf;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private LocalDateTime etlStartTime;

    private Connection oracleConnection;
    private String expJobArgs;
    private String tenantId;
    private HashMap<String, String> expJobArgsMap;
    private Pattern patternRegExpTemplateVariable;
    private String sparkJdbcDetails;
    SparkJdbcConfigDetails sparkJdbcConfigDetails = null;

    private final static String SELECT_T_EXP_JOB_QUEUE = "select TENANT_ID, EXP_JOB_ARGS, JDBC_CONF" +
            " from BI_UTILS.T_EXP_JOB_QUEUE"+
            " where EXP_JOB_QUE_ID = ?";

    private final static String UPDATE_EXP_JOB_QUE_STS = "UPDATE BI_UTILS.T_EXP_JOB_QUEUE set EXP_JOB_QUE_STS = ?" +
            " where EXP_JOB_QUE_ID = ?";

    private final static String UPDATE_EXP_JOB_PARAMS = "UPDATE BI_UTILS.T_EXP_JOB_QUEUE set EXP_JOB_OUTPUT = ?, EXP_JOB_QUE_STS = ?" +
            " where EXP_JOB_QUE_ID = ?";

    private final static String EXP_JOB_QUE_STS_EX_JOB_EXECUTION_STARTED="EX";
    private final static String EXP_JOB_QUE_STS_DN_JOB_IS_FINISHED="DN";
    private final static String EXP_JOB_QUE_STS_FP_JOB_FAILED="FP";

    private final static String EXD_TODAY_DT="EXD_TODAY_DT";
    private final static String EXD_TODAY_INT="EXD_TODAY_INT";
    private final static String EXD_PROC_TS="EXD_PROC_TS";

    private final static String EXD_TODAY_DT_FORMAT="yyyy-MM-dd";
    private final static String EXD_TODAY_INT_FORMAT="yyyyMMdd";
    private final static String EXD_PROC_TS_FORMAT="yyyyMMddhhmmss";

    private final static String EXP_TPL_STR_BEGIN = "EXP_TPL_STR_BEGIN";
    private final static String EXP_TPL_STR_END = "EXP_TPL_STR_END";
    private final static String EXP_TPL_STR_BEGIN_DEFAULT = "{{";
    private final static String EXP_TPL_STR_END_DEFAULT = "}}";

    private final static String EXP_SQL_TPL_QUERY = "EXP_SQL_TPL_QUERY";
    private final static String EXP_SQL_TPL_FILE = "EXP_SQL_TPL_FILE";
    private final static String EXP_OUT_DIR = "EXP_OUT_DIR";
    private final static String EXP_OUT_FILE_NAME = "EXP_OUT_FILE_NAME";
    private final static String EXP_CSV_DELIMITER = "EXP_CSV_DELIMITER";
    private final static String EXP_CSV_HEADER = "EXP_CSV_HEADER";
    private final static String EXP_CSV_HEADER_VAL = "EXP_CSV_HEADER_VAL";
    private final static String SPARK_SESSION_CONF = "SPARK_SESSION_CONF";

    private final static String DEFAULT_TENANT_ID = "TOMIA";


    public void run() throws IOException, SQLException
    {
        try
        {
            initArguments();

            openConnection();

            updateExpJobQueSts(EXP_JOB_QUE_STS_EX_JOB_EXECUTION_STARTED);

            fetchDataFromExpJobQueTable();

            addDateParamsToExpJobArgsMap();

            exchangeTemplateVariables();

            readAndValidateExpJobArgs();

            readJdbcConfDetails();

            readQueryFromParamOrFile();

            executeQueryAndWriteToFile();

            handleProcessingSuccess();
        }
        catch (Exception exception)
        {
            handleProcessingError(exception);
            throw exception;
        }
        finally
        {
            closeConnection();
        }
    }

    private void initArguments()
    {
        arguments = GenericExportToFileSaverArgumentsHolder.getInstance();
        System.out.println(arguments);

        expJobQueId = arguments.getExpJobQueId();

        fileFormat = arguments.getFileFormat();
    }

    private void openConnection() throws SQLException
    {
        oracleConnection = jdbcTemplate.getDataSource().getConnection();
        oracleConnection.setAutoCommit(false);
    }

    private void closeConnection() throws SQLException
    {
        if (oracleConnection != null)
            oracleConnection.close();
    }

    private void updateExpJobQueSts(String status) throws SQLException
    {
        PreparedStatement updateExpJobQueStsStmt = null;

        updateExpJobQueStsStmt = oracleConnection.prepareStatement(UPDATE_EXP_JOB_QUE_STS);
        updateExpJobQueStsStmt.setString(1, status);
        updateExpJobQueStsStmt.setLong(2, expJobQueId);


        System.out.println("execute update: " + UPDATE_EXP_JOB_QUE_STS);
        System.out.println("param EXP_JOB_QUE_STS: " + status);
        System.out.println("param EXP_JOB_QUE_ID: " + expJobQueId);

        updateExpJobQueStsStmt.executeUpdate();

        updateExpJobQueStsStmt.close();
        oracleConnection.commit();
    }

    private void updateExpJobParams(String status) throws SQLException
    {
        PreparedStatement updateExpJobParamsStmt = null;

        String generatedFileFullPath = dirPath + "/" + fileName;

        updateExpJobParamsStmt = oracleConnection.prepareStatement(UPDATE_EXP_JOB_PARAMS);
        updateExpJobParamsStmt.setString(1, generatedFileFullPath);
        updateExpJobParamsStmt.setString(2, status);
        updateExpJobParamsStmt.setLong(3, expJobQueId);


        System.out.println("execute update: " + UPDATE_EXP_JOB_PARAMS);
        System.out.println("param EXP_JOB_OUTPUT: " + generatedFileFullPath);
        System.out.println("param EXP_JOB_QUE_STS: " + status);
        System.out.println("param EXP_JOB_QUE_ID: " + expJobQueId);

        updateExpJobParamsStmt.executeUpdate();

        updateExpJobParamsStmt.close();
        oracleConnection.commit();
    }

    private void fetchDataFromExpJobQueTable() throws IOException, SQLException
    {
        PreparedStatement fetchExpJobQueStmt = oracleConnection.prepareStatement(SELECT_T_EXP_JOB_QUEUE);
        fetchExpJobQueStmt.setLong(1, expJobQueId);

        System.out.println("execute query:  " + SELECT_T_EXP_JOB_QUEUE);
        System.out.println("param expJobQueId: " + expJobQueId);

        ResultSet rs = fetchExpJobQueStmt.executeQuery();

        if (!rs.next())
        {
            System.out.println("Error: no data found in BI_UTILS.T_EXP_JOB_QUEUE for EXP_JOB_QUE_ID: " + expJobQueId);
            rs.close();
            fetchExpJobQueStmt.close();
            throw new IOException("Error: no data found in BI_UTILS.T_EXP_JOB_QUEUE for EXP_JOB_QUE_ID: " + expJobQueId);
        }

        expJobArgs = rs.getString("EXP_JOB_ARGS");
        tenantId = rs.getString("TENANT_ID");
        sparkJdbcDetails = rs.getString("JDBC_CONF");

        System.out.println("EXP_JOB_ARGS:  " + expJobArgs);
        System.out.println("TENANT_ID:  " + tenantId);
        System.out.println("JDBC_CONF:  " + sparkJdbcDetails);

        rs.close();
        fetchExpJobQueStmt.close();

        if (StringUtils.isEmpty(expJobArgs))
        {
            System.out.println("Error: T_EXP_JOB_QUEUE.EXP_JOB_ARGS has no value for EXP_JOB_QUE_ID: " + expJobQueId);
            throw new IOException("Error: T_EXP_JOB_QUEUE.EXP_JOB_ARGS has no value for EXP_JOB_QUE_ID: " + expJobQueId);
        }

        // convert EXP_JOB_ARGS json string to hash map
        expJobArgsMap = new Gson().fromJson(expJobArgs, HashMap.class);

        System.out.println("expJobArgsMap: " + expJobArgsMap.toString());
    }

    private void readAndValidateExpJobArgs() throws IOException
    {
        query = expJobArgsMap.get(EXP_SQL_TPL_QUERY);

        if (StringUtils.isEmpty(query))
        {
            queryFilePath = expJobArgsMap.get(EXP_SQL_TPL_FILE);
        }

        if (StringUtils.isEmpty(query) && StringUtils.isEmpty(queryFilePath))
        {
            System.out.println("Error: No query defined in EXP_SQL_TPL_QUERY and EXP_SQL_TPL_FILE. Please check T_EXP_JOB_QUEUE.EXP_JOB_ARGS for EXP_JOB_QUE_ID: " + expJobQueId);
            throw new IOException("Error: No query defined in EXP_SQL_TPL_QUERY and EXP_SQL_TPL_FILE. Please check T_EXP_JOB_QUEUE.EXP_JOB_ARGS for EXP_JOB_QUE_ID: " + expJobQueId);
        }

        dirPath = expJobArgsMap.get(EXP_OUT_DIR);

        if (StringUtils.isEmpty(dirPath))
        {
            System.out.println("Error: No output directory defined in EXP_OUT_DIR. Please check T_EXP_JOB_QUEUE.EXP_JOB_ARGS for EXP_JOB_QUE_ID: " + expJobQueId);
            throw new IOException("Error: No output directory defined in EXP_OUT_DIR. Please check T_EXP_JOB_QUEUE.EXP_JOB_ARGS for EXP_JOB_QUE_ID: " + expJobQueId);
        }

        fileName = expJobArgsMap.get(EXP_OUT_FILE_NAME);

        if (StringUtils.isEmpty(fileName))
        {
            System.out.println("Error: No file name defined in EXP_OUT_FILE_NAME. Please check T_EXP_JOB_QUEUE.EXP_JOB_ARGS for EXP_JOB_QUE_ID: " + expJobQueId);
            throw new IOException("Error: No file name defined in EXP_OUT_FILE_NAME. Please check T_EXP_JOB_QUEUE.EXP_JOB_ARGS for EXP_JOB_QUE_ID: " + expJobQueId);
        }

        csvDelimiter = expJobArgsMap.get(EXP_CSV_DELIMITER);

        if (StringUtils.isEmpty(csvDelimiter))
        {
            System.out.println("Error: No delimiter defined in EXP_CSV_DELIMITER. Please check T_EXP_JOB_QUEUE.EXP_JOB_ARGS for EXP_JOB_QUE_ID: " + expJobQueId);
            throw new IOException("Error: No delimiter defined in EXP_CSV_DELIMITER. Please check T_EXP_JOB_QUEUE.EXP_JOB_ARGS for EXP_JOB_QUE_ID: " + expJobQueId);
        }

        headers = expJobArgsMap.get(EXP_CSV_HEADER);

        if (StringUtils.isEmpty(headers))
        {
            System.out.println("Error: No csv header defined in EXP_CSV_HEADER. Please check T_EXP_JOB_QUEUE.EXP_JOB_ARGS for EXP_JOB_QUE_ID: " + expJobQueId);
            throw new IOException("Error: No csv header defined in EXP_CSV_HEADER. Please check T_EXP_JOB_QUEUE.EXP_JOB_ARGS for EXP_JOB_QUE_ID: " + expJobQueId);
        }

        headerVal = expJobArgsMap.get(EXP_CSV_HEADER_VAL);

        sparkSessionConf = expJobArgsMap.get(SPARK_SESSION_CONF);
        if(!StringUtils.isEmpty(sparkSessionConf))
        {
            System.out.println("SPARK_SESSION_CONF is defined: " + sparkSessionConf);
            //Set sparkSession
            sparkSession.sql(sparkSessionConf);
        }
    }

    private void readJdbcConfDetails() throws IOException {
        if (!StringUtils.isEmpty(sparkJdbcDetails))
        {
            sparkJdbcConfigDetails = new ObjectMapper().readValue(sparkJdbcDetails, SparkJdbcConfigDetails.class);
        }
    }

    private void addDateParamsToExpJobArgsMap()
    {
        DateTimeFormatter timeFormatter;

        timeFormatter = DateTimeFormatter.ofPattern(EXD_TODAY_DT_FORMAT);
        expJobArgsMap.put(EXD_TODAY_DT, etlStartTime.format(timeFormatter));

        System.out.println("EXD_TODAY_DT value:  " + expJobArgsMap.get(EXD_TODAY_DT));

        timeFormatter = DateTimeFormatter.ofPattern(EXD_TODAY_INT_FORMAT);
        expJobArgsMap.put(EXD_TODAY_INT, etlStartTime.format(timeFormatter));

        System.out.println("EXD_TODAY_INT value:  " + expJobArgsMap.get(EXD_TODAY_INT));

        timeFormatter = DateTimeFormatter.ofPattern(EXD_PROC_TS_FORMAT);
        expJobArgsMap.put(EXD_PROC_TS, etlStartTime.format(timeFormatter));

        System.out.println("EXD_PROC_TS value:  " + expJobArgsMap.get(EXD_PROC_TS));
    }

    private void exchangeTemplateVariables() throws IOException
    {
        String templateVariableBegin;
        String templateVariableEnd;
        String exchangedLine;
        String currentEntryValue;

        templateVariableBegin = expJobArgsMap.get(EXP_TPL_STR_BEGIN);

        if (StringUtils.isEmpty(templateVariableBegin))
        {
            templateVariableBegin = EXP_TPL_STR_BEGIN_DEFAULT;
        }

        templateVariableEnd = expJobArgsMap.get(EXP_TPL_STR_END);

        if (StringUtils.isEmpty(templateVariableEnd))
        {
            templateVariableEnd = EXP_TPL_STR_END_DEFAULT;
        }

        String regExpSearchForTemplateVariable = Pattern.quote(templateVariableBegin) + "(.+?)" + Pattern.quote(templateVariableEnd);
        patternRegExpTemplateVariable = Pattern.compile(regExpSearchForTemplateVariable);

        for (Map.Entry<String, String> entry : expJobArgsMap.entrySet())
        {
            currentEntryValue = entry.getValue();
            exchangedLine = exchangeTemplateVariablesInSingleLine(currentEntryValue);
            if (!exchangedLine.equals(currentEntryValue))
            {
                entry.setValue(exchangedLine);
                System.out.println("Line: " + currentEntryValue + " ==> exchanged to: " + exchangedLine);
            }
        }

        System.out.println("expJobArgsMap after exchange template variables: " + expJobArgsMap.toString());
    }

    private String exchangeTemplateVariablesInSingleLine(String line) throws IOException
    {
        String currentMatchParamName;
        String currentParamValueInMap;
        Matcher matcher = patternRegExpTemplateVariable.matcher(line);

        int index = 0;
        while(matcher.find(index))
        {
            // get only parameter name without begin "{{" and end...
            currentMatchParamName = matcher.group(1);
            currentParamValueInMap = expJobArgsMap.get(currentMatchParamName);
            if (currentParamValueInMap == null)
            {
                System.out.println("Error: parameter: " + currentMatchParamName + " has no value. Please check T_EXP_JOB_QUEUE.EXP_JOB_ARGS for EXP_JOB_QUE_ID: " + expJobQueId);
                throw new IOException("Error: parameter: " + currentMatchParamName + " has no value. Please check T_EXP_JOB_QUEUE.EXP_JOB_ARGS for EXP_JOB_QUE_ID: " + expJobQueId);
            }

            line = line.replaceAll(Pattern.quote(matcher.group()), currentParamValueInMap);
            index = matcher.end();
        }

        return line;
    }

    private void readQueryFromParamOrFile() throws IOException
    {
        if (!StringUtils.isEmpty(query))
        {
            System.out.println("query read from EXP_SQL_TPL_QUERY param: " + query);
            return;
        }

        query = readQueryFromFile();

        if (StringUtils.isEmpty(query))
        {
            System.out.println("Error: No query found in EXP_SQL_TPL_QUERY and input file: " + queryFilePath + " for EXP_JOB_QUE_ID: " + expJobQueId);
            throw new IOException("Error: No query found in EXP_SQL_TPL_QUERY and input file: " + queryFilePath + " for EXP_JOB_QUE_ID: " + expJobQueId);
        }
    }

    private String readQueryFromFile() throws IOException
    {
        System.out.println("Read query from file: " + queryFilePath);

        String queryFromFile;
        Path path = new Path(queryFilePath);
        Configuration conf = sparkSession.sparkContext().hadoopConfiguration();
        FileSystem fileSystem = null;
        fileSystem = path.getFileSystem(conf);

        // Check if input file is valid
        if (!fileSystem.exists(path))
        {
            System.out.println("Query input file not found: " + queryFilePath + " for EXP_JOB_QUE_ID: " + expJobQueId);
            throw new IOException("Query input file not found: " + queryFilePath + " for EXP_JOB_QUE_ID: " + expJobQueId);
        }

        FSDataInputStream inputStream = fileSystem.open(path);

        queryFromFile = IOUtils.toString(inputStream, StandardCharsets.UTF_8);

        System.out.println("Query from file: " + queryFromFile);

        // exchange template variables in query read from file
        queryFromFile = exchangeTemplateVariablesInSingleLine(queryFromFile);

        System.out.println("Query after exchange template variables: " + queryFromFile);

        return queryFromFile;
    }

    private void executeQueryAndWriteToFile() throws IOException
    {
        Dataset<Row> dfToSave;
        int jdbcLoad;
        String connName;
        String tempDir = dirPath + "/" + arguments.getJobId();

        System.out.println("Executing query: " + query);

        if (sparkJdbcConfigDetails != null){
            jdbcLoad = 1;
            connName = sparkJdbcConfigDetails.getUrl().split(":")[1];
            System.out.println("connName: " + connName);
            if (connName.equals("trino"))
            {
                dfToSave = sparkSession.read().format("jdbc")
                    .option("url", sparkJdbcConfigDetails.getUrl())
                    .option("dbtable", "(" + query + ") subquery")
                    .option("user", sparkJdbcConfigDetails.getJdbcUserName())
                    .option("password", sparkJdbcConfigDetails.getJdbcPassword())
                    .option("driver", sparkJdbcConfigDetails.getJdbcDriver())
                    .load().coalesce(arguments.getNumPartitions())
                    .persist(StorageLevel.MEMORY_AND_DISK());
            }
            else
            {
                dfToSave = sparkSession.read().format("jdbc")
                    .option("url", sparkJdbcConfigDetails.getUrl())
                    .option("dbtable", "(" + query + ") subquery")
                    .option("user", sparkJdbcConfigDetails.getJdbcUserName())
                    .option("password", sparkJdbcConfigDetails.getJdbcPassword())
                    .option("driver", sparkJdbcConfigDetails.getJdbcDriver())
                    .option("fetchsize", sparkJdbcConfigDetails.getFetchSize())
                    .option("batchsize", sparkJdbcConfigDetails.getBatchSize())
                    .load().coalesce(arguments.getNumPartitions())
                    .persist(StorageLevel.MEMORY_AND_DISK());
            }
        }
        else {
            jdbcLoad = 0;
            dfToSave = sparkSession.sql(query).coalesce(arguments.getNumPartitions())
                .persist(StorageLevel.MEMORY_AND_DISK());
        }

        long numRowsInDF = dfToSave.count();

        System.out.println("num rows in dataset: " + numRowsInDF);

        if (numRowsInDF == 0)
        {
            System.out.println("Executing query returned no results ==> not writing output file");
            return;
        }

        System.out.println("\nSaving results to: " + tempDir);

        if (jdbcLoad == 0) {
            for (StructField col : dfToSave.schema().fields()) {
                System.out.println("Colums: " + col.name() + " Type:" + col.dataType());
                if (col.dataType().equals(DoubleType) || col.dataType().equals(FloatType) || col
                    .dataType().getClass().getSimpleName().equals("DecimalType")) {
                    System.out.println("Handling " + col.dataType() + " columm:" + col.name());
                    dfToSave = dfToSave.withColumn(col.name(),
                        format_number(dfToSave.col(col.name()),
                            arguments.getNumberOfDigitsAfterDecimalDot()).cast("decimal(38,10)"));

                    dfToSave = dfToSave.withColumn(col.name(),
                        when(col(col.name()).equalTo(lit(0)), lit(0)).otherwise(
                            regexp_replace(col(col.name()), "()\\.0+$|(\\..+?)0+$", "$2")));
                }
            }
        }

        if (!StringUtils.isEmpty(headerVal))
        {
            if (!StringUtils.isEmpty(headers) && headers.equalsIgnoreCase("true"))
            {
                System.out.println("set header values: " + headerVal);
                String[] csvHeaderColumns = headerVal.split(Pattern.quote(csvDelimiter));
                dfToSave = dfToSave.toDF(csvHeaderColumns);
            }
            else
            {
                System.out.println("not setting header values as EXP_CSV_HEADER param is: " + headers);
            }
        }
        else
        {
            System.out.println("not setting header values as EXP_CSV_HEADER_VAL param is has no value");
        }

        dfToSave.coalesce(1)
                .write().format(fileFormat)
                .mode(SaveMode.Overwrite)
                .option("delimiter", csvDelimiter)
                .option("emptyValue", "")
                .option("header", headers)
                .save(tempDir);

        renameFile(tempDir);
    }

    private void renameFile(String tempDir) throws IOException
    {
        Path path = new Path(tempDir);
        FileSystem fileSystem = path.getFileSystem(hadoopConf);
        String existingFileName = fileSystem.globStatus(new Path(tempDir + "/part*"))[0].getPath().getName();

        Path newPath = new Path(dirPath + "/" + fileName);

        System.out.println("\nChecking if file: " + newPath + " already exists...");
        // if new file is already exist in dir (can be when reprocessing fl table record with error indication) delete it
        if (fileSystem.exists(newPath)) {
            System.out.println("File already exists. Deleting it...");
            fileSystem.delete(newPath, false);
        }

        System.out.println("\nMoving file: " + tempDir + "/" + existingFileName + " to: " + dirPath + "/" + fileName + "...");
        fileSystem.rename(new Path(tempDir + "/" + existingFileName), newPath);

        System.out.println("\nDeleting temp dir: " + tempDir + "...");

        fileSystem.delete(new Path(tempDir), true);
    }

    private void handleProcessingSuccess() throws SQLException
    {
        updateExpJobParams(EXP_JOB_QUE_STS_DN_JOB_IS_FINISHED);
    }

    private void handleProcessingError(Exception exception) throws SQLException
    {
        insertEntryToNotificationTable(exception);
        updateExpJobQueSts(EXP_JOB_QUE_STS_FP_JOB_FAILED);
    }

    private void insertEntryToNotificationTable(Exception exception) throws SQLException
    {
        PreparedStatement notificationStmt = null;
        String notificationSql = "INSERT INTO BI_UTILS.T_NTFY(NTFY_ID, TENANT_ID, PRD_CODE, MOD_CODE, NTFY_DLVR_TYP, DLVR_CONF_REF_ID, LAST_STS_DT, MSG_SUB, MSG_BODY, ERR_MSG) " +
                "VALUES (t_ntfy_ntfy_id_seq.nextval, ?, 'Generic-Extract-Service', 'gen-ext-etl', 'E', 1, sysdate, 'Generic extract ETL failed', ?, ?)";

        try
        {
            notificationStmt = oracleConnection.prepareStatement(notificationSql);
            notificationStmt.setString(1, tenantId!=null ? tenantId : DEFAULT_TENANT_ID);
            notificationStmt.setString(2, exception.getMessage());
            notificationStmt.setString(3, ExceptionUtils.getStackTrace(exception));

            System.out.println("execute query:  " + notificationSql);

            notificationStmt.executeUpdate();
        }
        catch (Exception e)
        {
            System.out.println("Got exception during inserting notification entry to BI_UTILS.T_NTFY for exception: " + exception.getMessage());
        }
        finally
        {
            if (notificationStmt != null)
                notificationStmt.close();
        }
    }
}
