package com.starhomemach.nbi.oradoop.util.spark;


import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DataSaver {


    public static final String TEMP_DATA_APPEND = "TEMP_DATA_APPEND";
    @Autowired
    SparkSession sparkSession;


    public void saveDataAppend(Dataset<Row> dataFrame, String[] partitionedBy, String dbName, String tableName) {
        dataFrame.createOrReplaceTempView("TEMP_DATA_APPEND");
        sparkSession.conf().set("hive.exec.dynamic.partition", "true");
        sparkSession.conf().set("hive.exec.dynamic.partition.mode", "nonstrict");
        String partitionClause = partitionedBy==null? "" : " PARTITION (" + String.join(",", partitionedBy) + ")";
        String SQL = "INSERT INTO " + dbName + "." + tableName + partitionClause +" SELECT * FROM TEMP_DATA_APPEND TEMP_DATA";
        System.out.println("Going to Run: " + SQL);
        sparkSession.sql(SQL);
    }

    public void saveDataOverwrite(Dataset<Row> dataFrame, String[] partitionedBy, String dbName, String tableName) {
        dataFrame.createOrReplaceTempView("TEMP_DATA_APPEND");
        sparkSession.conf().set("hive.exec.dynamic.partition", "true");
        sparkSession.conf().set("hive.exec.dynamic.partition.mode", "nonstrict");
        String partitionClause = partitionedBy==null? "" : " PARTITION (" + String.join(",", partitionedBy) + ")";
        String SQL = "INSERT OVERWRITE TABLE " + dbName + "." + tableName + partitionClause +" SELECT * FROM TEMP_DATA_APPEND TEMP_DATA";
        System.out.println("Going to Run: " + SQL);
        sparkSession.sql(SQL);
    }


    public void saveAndSortDataAppend(Dataset<Row> dataFrame, String[] partitionedBy, String dbName, String tableName, String[] sortByFields) {
        dataFrame.createOrReplaceTempView("TEMP_DATA_APPEND");
        sparkSession.conf().set("hive.exec.dynamic.partition", "true");
        sparkSession.conf().set("hive.exec.dynamic.partition.mode", "nonstrict");
        String SQL = "INSERT INTO " + dbName + "." + tableName + " PARTITION (" + String.join(",", partitionedBy) + ") SELECT * FROM TEMP_DATA_APPEND TEMP_DATA sort by " + String.join(",", partitionedBy) + "," + String.join(",", sortByFields);
        System.out.println("Going to Run: " + SQL);
        sparkSession.sql(SQL);
    }

    public void saveAndSortDataOverwrite(Dataset<Row> dataFrame, String[] partitionedBy, String dbName, String tableName, String[] sortByFields) {
        dataFrame.createOrReplaceTempView("TEMP_DATA_APPEND");
        sparkSession.conf().set("hive.exec.dynamic.partition", "true");
        sparkSession.conf().set("hive.exec.dynamic.partition.mode", "nonstrict");
        String SQL = "INSERT OVERWRITE TABLE " + dbName + "." + tableName + " PARTITION (" + String.join(",", partitionedBy) + ") SELECT * FROM TEMP_DATA_OVERWRITE sort by " + String.join(",", partitionedBy) + "," + String.join(",", sortByFields);
        System.out.println("Going to Run: " + SQL);
        sparkSession.sql(SQL);
    }
}

