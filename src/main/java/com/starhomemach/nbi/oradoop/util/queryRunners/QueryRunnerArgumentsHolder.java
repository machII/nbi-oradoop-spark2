package com.starhomemach.nbi.oradoop.util.queryRunners;

import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;


public class QueryRunnerArgumentsHolder
{
    private static QueryRunnerArgumentsHolder ourInstance =null;

    public static QueryRunnerArgumentsHolder getInstance() {
        if(ourInstance==null)
            ourInstance=new QueryRunnerArgumentsHolder();

        return ourInstance;
    }

    private QueryRunnerArgumentsHolder(){}

    public static final String PREFIX="--";
    public static final String JDBC_DRIVER ="jdbcDriver";
    public static final String JDBC_URL ="jdbcUrl";
    public static final String USER_NAME ="userName";
    public static final String PASSWORD ="password";
    public static final String IS_QUERY_FROM_FILE ="isQueryFromFile";
    public static final String FILE_PATH ="filePath";
    public static final String QUERY_STRING ="queryString";
    public static final String QUERIES_PARAMS ="queriesParams";
    public static final String QUERIES_PARAMS_BY_NAME ="queriesParamsByName";
    public static final String PRINT_AS_TABLE_TO_CONSOLE ="printAsTableToConsole";
    public static final String SET_AUTO_COMMIT_TO_FALSE ="setAutoCommitToFalse";
    public static final String CHAR_TO_REPLACE ="charToReplace";
    public static final String REPLACE_WITH ="replaceWith";
    public static final String RUN_LAST_RESULT ="runLastResult";
    public static final String QUERY_DELIMITER ="queryDelimiter";

    private Namespace params;



    public String getJdbcDriver() {
        return jdbcDriver;
    }

    public void setJdbcDriver(String jdbcDriver) {
        this.jdbcDriver = jdbcDriver;
    }

    public String getJdbcUrl() {
        return jdbcUrl;
    }

    public void setJdbcUrl(String jdbcUrl) {
        this.jdbcUrl = jdbcUrl;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isQueryFromFile() {
        return isQueryFromFile;
    }

    public void setIsQueryFromFile(boolean queryFromFile) {
        isQueryFromFile = queryFromFile;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getQueriesParams() {
        return queries_params;
    }

    public void setQueriesParams(String queries_params) {
        this.queries_params = queries_params;
    }

    public String getQueriesParamsByName() {
        return queries_params_by_name;
    }

    public void setQueriesParamsByName(String queries_params_by_name) {
        this.queries_params_by_name = queries_params_by_name;
    }

    public boolean getIsSetAutoCommitToFalse() {
        return setAutoCommitToFalse;
    }

    public void setSetAutoCommitToFalse(boolean setAutoCommitToFalse) {
        this.setAutoCommitToFalse = setAutoCommitToFalse;
    }

    public String getQueryString() {
        return queryString;
    }

    public void setQueryString(String queryString) {
        this.queryString = queryString;
    }

    private String jdbcDriver;
    private String jdbcUrl;
    private String userName;
    private String  password;
    private boolean isQueryFromFile = false;
    private String filePath;
    private String queryString;
    private String queries_params = "";
    private String queries_params_by_name = "";
    private String charToReplace;
    private String replaceWith;
    private boolean runLastResult = false;
    private String queryDelimiter;

    public boolean isRunLastResult() {
        return runLastResult;
    }

    public void setRunLastResult(boolean runLastResult) {
        this.runLastResult = runLastResult;
    }

    public String getCharToReplace() {
        return charToReplace;
    }

    public void setCharToReplace(String charToReplace) {
        this.charToReplace = charToReplace;
    }

    public String getReplaceWith() {
        return replaceWith;
    }

    public void setReplaceWith(String replaceWith) {
        this.replaceWith = replaceWith;
    }

    public boolean isPrintAsTableToConsole() {
        return printAsTableToConsole;
    }

    public void setPrintAsTableToConsole(boolean printAsTableToConsole) {
        this.printAsTableToConsole = printAsTableToConsole;
    }

    public String getQueryDelimiter() {
        return queryDelimiter;
    }

    public void setQueryDelimiter(String queryDelimiter) {
        this.queryDelimiter = queryDelimiter;
    }

    private boolean printAsTableToConsole=false;
    private boolean setAutoCommitToFalse=false;

    private ArgumentParser parser;

    public static String[] args=null;



    public void initQureyRunnerArgumentsHolder(String[] args) throws Exception {

        parser = ArgumentParsers.newArgumentParser("query runner")
                .description("Process query runner arguments.");

        buildArgumentsParser();

        parseArguments(parser, args);
    }

    private void buildArgumentsParser()  {
        parser.addArgument(PREFIX + JDBC_DRIVER)
                .type(String.class)
                .required(false);

        parser.addArgument(PREFIX + JDBC_URL)
                .type(String.class)
                .required(false);

        parser.addArgument(PREFIX + USER_NAME)
                .type(String.class)
                .required(true);

        parser.addArgument(PREFIX + PASSWORD)
                .type(String.class)
                .required(false);

        parser.addArgument(PREFIX + IS_QUERY_FROM_FILE)
                .type(Boolean.class)
                .required(false)
                .setDefault(false);

        parser.addArgument(PREFIX + FILE_PATH)
                .type(String.class)
                .required(false);

        parser.addArgument(PREFIX + QUERY_STRING)
                .type(String.class)
                .required(false);

        parser.addArgument(PREFIX + QUERIES_PARAMS)
                .type(String.class)
                .required(false)
        .setDefault(getQueriesParams());

        parser.addArgument(PREFIX + QUERIES_PARAMS_BY_NAME)
                .type(String.class)
                .required(false)
        .setDefault(getQueriesParamsByName());


        parser.addArgument(PREFIX + PRINT_AS_TABLE_TO_CONSOLE)
                .type(Boolean.class)
                .required(false)
                .setDefault(false);

        parser.addArgument(PREFIX + SET_AUTO_COMMIT_TO_FALSE)
                .type(Boolean.class)
                .required(false)
                .setDefault(getIsSetAutoCommitToFalse());

        parser.addArgument(PREFIX +  CHAR_TO_REPLACE)
                .type(String.class)
                .required(false);

        parser.addArgument(PREFIX + REPLACE_WITH)
                .type(String.class)
                .required(false);

        parser.addArgument(PREFIX + RUN_LAST_RESULT)
                .type(Boolean.class)
                .required(false)
                .setDefault(false);

        parser.addArgument(PREFIX + QUERY_DELIMITER)
                .type(String.class)
                .required(false)
                .setDefault(";");
    }

    private void parseArguments(ArgumentParser parser, String[] args) throws Exception {

        try {

                params = parser.parseArgs(args);

            if (params.get(JDBC_DRIVER) != null)
                this.setJdbcDriver(params.get(JDBC_DRIVER));

            if (params.get(JDBC_URL) != null)
                this.setJdbcUrl(params.get(JDBC_URL));

            if (params.get(USER_NAME) != null)
                this.setUserName(params.get(USER_NAME));

            if (params.get(PASSWORD) != null)
                this.setPassword(params.get(PASSWORD));

            if (params.get(IS_QUERY_FROM_FILE) != null)
                this.setIsQueryFromFile(params.get(IS_QUERY_FROM_FILE));

            if (params.get(FILE_PATH) != null)
                this.setFilePath(params.get(FILE_PATH));

            if (params.get(QUERY_STRING) != null)
                this.setQueryString(params.get(QUERY_STRING));

            if (params.get(QUERIES_PARAMS) != null)
                this.setQueriesParams(params.get(QUERIES_PARAMS));

            if (params.get(QUERIES_PARAMS_BY_NAME) != null)
                this.setQueriesParamsByName(params.get(QUERIES_PARAMS_BY_NAME));

            if (params.get(PRINT_AS_TABLE_TO_CONSOLE) != null)
                this.setPrintAsTableToConsole(params.get(PRINT_AS_TABLE_TO_CONSOLE));

            if (params.get(SET_AUTO_COMMIT_TO_FALSE) != null)
                this.setSetAutoCommitToFalse(params.get(SET_AUTO_COMMIT_TO_FALSE));

            if(params.get(CHAR_TO_REPLACE) != null){
                this.setCharToReplace(params.get(CHAR_TO_REPLACE));
            }

            if(params.get(REPLACE_WITH) != null){
                this.setReplaceWith(params.get(REPLACE_WITH));
            }

            if(params.get(RUN_LAST_RESULT) != null){
                this.setRunLastResult(params.get(RUN_LAST_RESULT));
            }

            if(params.get(QUERY_DELIMITER) != null){
                this.setQueryDelimiter(params.get(QUERY_DELIMITER));
            }

            System.out.println("query runner parser " + params.toString());

        }
        catch (ArgumentParserException e) {
            System.out.println("parse Arguments failed: syntax is " + parser.toString());
            parser.handleError(e);
        }
    }

}


