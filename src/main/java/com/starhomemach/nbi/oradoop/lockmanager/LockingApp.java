package com.starhomemach.nbi.oradoop.lockmanager;

import com.starhomemach.nbi.oradoop.export.SpringConfig;
import org.apache.zookeeper.CreateMode;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Properties;

/**
 * Created by tpin on 09/04/2017.
 */
public class LockingApp {
    public static void main(String[] args) throws Exception
    {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SpringConfig.class, LockMngrSpringConfig.class);
        LockManager lm = context.getBean(LockManager.class);
        System.out.println("create lock manager");

        if (args[0].equalsIgnoreCase("LOCK"))
        {
            if (args.length==3)
            {
                System.out.println("try to acquire lock");
                String lockKey = lm.acquireLock(args[1],args[2], CreateMode.PERSISTENT_SEQUENTIAL);
                System.out.println("acquired lock " + lockKey);
                String filePath = System.getProperty("oozie.action.output.properties");
                if (filePath != null) {
                    OutputStream outputStream = new FileOutputStream(filePath);
                    Properties p = new Properties();
                    p.setProperty("LOCK_KEY", lockKey);
                    p.store(outputStream, "");
                    outputStream.close();
                }
            }
            else usage();
        }
        else if (args[0].equalsIgnoreCase("UNLOCK"))
        {
            if (args.length == 2) {
                System.out.println("releasing lock " + args[1]);
                lm.releaseLock(args[1]);
                System.out.println("released lock " + args[1]);
            }
            else usage();
        }
        else usage();
    }

    public static void usage()
    {
        System.out.println("Usage [LOCK|UNLOCK] {[OWNER] [CONTEXT_NAME]} {[LOCK_KEY]}");
    }
}
