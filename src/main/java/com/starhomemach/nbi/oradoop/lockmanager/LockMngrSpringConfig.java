package com.starhomemach.nbi.oradoop.lockmanager;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;


@Configuration
@ComponentScan(basePackages = {"com.starhomemach.nbi.oradoop.lockmanager"})
public class LockMngrSpringConfig {

}
