package com.starhomemach.nbi.oradoop.lockmanager;

import com.starhomemach.nbi.oradoop.OradoopConf;
import com.starhomemach.nbi.oradoop.export.SpringConfig;
import com.starhomemach.nbi.oradoop.export.service.ProcessHdfsDirectoryService;
import com.starhomemach.nbi.oradoop.export.util.HandleProcessConfiguration;
import com.starhomemach.nbi.oradoop.export.util.OradoopProcessArgs;
import org.apache.zookeeper.*;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;
import java.util.function.ObjDoubleConsumer;

/**
 * Created by tpin on 09/04/2017.
 */
@Component
public class LockManager {
    public static final String baseProductPath = "/starhome";
    public static final String baseLockNodePath = baseProductPath + "/lock_node";
    private static final String STARHOME_COMMON_SHARE_CONF = "/starhome/common/conf/";
    private static final String ZOOKEEPER_PROPERTIES = "zookeeper.properties";
    private static  String ZOOKEEPER_HOST = "zookeeper.host";
    private static ZooKeeper zkCl;
    private final CountDownLatch connectedSignal = new CountDownLatch(1);

    @PostConstruct
    public void init() throws IOException,InterruptedException,KeeperException
    {
        Properties zookeeperProperties = HandleProcessConfiguration.getPropertiesFile(STARHOME_COMMON_SHARE_CONF + ZOOKEEPER_PROPERTIES);
        String zookeeperHost = zookeeperProperties.getProperty(ZOOKEEPER_HOST);

        zkCl = new ZooKeeper(zookeeperHost, 5000, new Watcher() {

            public void process(WatchedEvent we) {

                if (we.getState() == Event.KeeperState.SyncConnected) {
                    connectedSignal.countDown();
                }
            }
        });

        connectedSignal.await();

        try {
            // check if base folder exists
            if (zkCl.exists(baseProductPath, false) == null) {
                zkCl.create(baseProductPath, null, ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);
            }

            // check if locknode exists
            if (zkCl.exists(baseLockNodePath, false) == null) {
                zkCl.create(baseLockNodePath, null, ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);
            }
        } catch (KeeperException e) {
            if (e.code() != KeeperException.Code.NODEEXISTS)
                throw e;
        }

    }

    public String acquireLock(String owner , String contextName, CreateMode creMode) throws InterruptedException, KeeperException {
        String lockPath = baseLockNodePath + "/" + owner + "/" + contextName;

        // check if owner folder was already created
        if (zkCl.exists(baseLockNodePath + "/" + owner , false) == null) {
            zkCl.create(baseLockNodePath + "/" + owner, null, ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);
        }

        // check if context name is used already
        if (zkCl.exists(baseLockNodePath + "/" + owner + "/" + contextName , false) == null) {
            zkCl.create(baseLockNodePath + "/" + owner + "/" + contextName, null, ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);
        }

        // create a lock file
        String lockKey = zkCl.create(lockPath + "/lock-", new byte[0], ZooDefs.Ids.OPEN_ACL_UNSAFE, creMode);

        // wait to acquire lock
        final Object lock = new Object();
        synchronized(lock) {
            while(true) {
                List<String> nodes = zkCl.getChildren(lockPath, new Watcher() {

                    public void process(WatchedEvent event) {
                        synchronized (lock) {
                            lock.notifyAll();
                        }
                    }
                });
                Collections.sort(nodes); // ZooKeeper node names can be sorted lexographically
                if (lockKey.endsWith(nodes.get(0)))
                    return lockKey;
                else
                    lock.wait();

            }
        }
    }

    public void releaseLock(String lockKey) throws KeeperException, InterruptedException {
        zkCl.delete(lockKey, -1);
    }

    @PreDestroy
    public void close() throws InterruptedException {
        zkCl.close();
    }
}
