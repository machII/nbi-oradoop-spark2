package com.starhomemach.nbi.oradoop;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.springframework.stereotype.Component;

@Component
public class OradoopConf {

    public Configuration getConf() throws IOException {
        Configuration conf = new Configuration();
        conf.addResource(new Path("/etc/hadoop/conf/core-site.xml"));
        conf.addResource(new Path("/etc/hadoop/conf/hdfs-site.xml"));
        conf.addResource(new Path("/etc/hadoop/conf/mapred-site.xml"));
        conf.addResource(new Path("/etc/hive/conf/hive-site.xml"));

        return conf;
    }

    public static boolean isRunOnlyHourly() {
        return OradoopConf.runOnlyHourly;
    }

    public void setRunOnlyHourly(boolean runOnlyHourly) {
        OradoopConf.runOnlyHourly = runOnlyHourly;
    }

    private static boolean runOnlyHourly = false;


}
