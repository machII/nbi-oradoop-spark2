package com.starhomemach.nbi.oradoop.dimension;

import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;

public class DimensionFullArgumentsHolder
{
    private static DimensionFullArgumentsHolder ourInstance = null;

    public static DimensionFullArgumentsHolder getInstance() {
        if(ourInstance == null)
            ourInstance = new DimensionFullArgumentsHolder();

        return ourInstance;
    }

    private DimensionFullArgumentsHolder(){}

    private static final String PREFIX = "--";
    private static final String OOZIE_URL = "oozieUrl";
    private static final String PARENT_WF = "parentWf";
    private static final String SUB_WORKFLOW_PATH = "subWorkflowPath";
    private static final String WF_USER = "wfUser";
    private static final String TABLES_LIST = "tablesList";
    private static final String BATCH_SIZE = "batchSize";
    private static final String SLEEP_SECONDS = "sleepSeconds";
    private static final String REFRESH_GROUP = "refreshGroup";
    private static final String HIVE_JDBC_DRIVER = "hiveJdbcDriver";
    private static final String HIVE_URL = "hiveUrl";
    private static final String ORADOOP_VERSION = "oradoopVersion";

    private String hiveJdbcDriver;
    private String hiveUrl;
    private String oozieUrl;
    private String parentWf;
    private String subWorkflowPath;
    private String wfUser;
    private String tablesList = "";
    private String refreshGroup = "";
    private String oradoopVersion = "";
    private int batchSize = 1;
    private int sleepSeconds = 30;

    String getHiveJdbcDriver() {
        return hiveJdbcDriver;
    }

    private void setHiveJdbcDriver(String hiveJdbcDriver) {
        this.hiveJdbcDriver = hiveJdbcDriver;
    }

    String getHiveUrl() {
        return hiveUrl;
    }

    private void setHiveUrl(String hiveUrl) {
        this.hiveUrl = hiveUrl;
    }

    String getOozieUrl() {
        return oozieUrl;
    }

    private void setOozieUrl(String oozieUrl) {
        this.oozieUrl = oozieUrl;
    }

    String getParentWf() {
        return parentWf;
    }

    private void setParentWf(String parentWf) {
        this.parentWf = parentWf;
    }

    String getSubWorkflowPath() {
        return subWorkflowPath;
    }

    private void setSubWorkflowPath(String subWorkflowPath) {
        this.subWorkflowPath = subWorkflowPath;
    }

    String getWfUser() {
        return wfUser;
    }

    private void setWfUser(String wfUser) {
        this.wfUser = wfUser;
    }

    String getRefreshGroup() {
        return refreshGroup;
    }

    private void setRefreshGroup(String refreshGroup) {
        this.refreshGroup = refreshGroup;
    }

    boolean isRefreshGroupSet() {
        return refreshGroup.length() > 0;
    }

    String getOradoopVersion() {
        return oradoopVersion;
    }

    private void setOradoopVersion(String oradoopVersion) {
        this.oradoopVersion = oradoopVersion;
    }

    boolean isOradoopVersionSet() {
        return oradoopVersion.length() > 0;
    }

    String getTablesList() {
        return tablesList;
    }

    private void setTablesList(String tablesList) {
        this.tablesList = tablesList;
    }

    boolean isTableListSet() {
        return tablesList.length() > 0;
    }

    int getBatchSize() {
        return batchSize;
    }

    private void setBatchSize(int batchSize) {
        this.batchSize = batchSize;
    }

    int getSleepSeconds() {
        return sleepSeconds;
    }

    private void setSleepSeconds(int sleepSeconds) {
        this.sleepSeconds = sleepSeconds;
    }


    private ArgumentParser parser;

    public static String[] args = null;

    void initDimensionFullArgumentsHolder(String[] args) {

        parser = ArgumentParsers.newArgumentParser("query runner")
                .description("Process query runner arguments.");

        buildArgumentsParser();

        parseArguments(parser, args);
    }

    private void buildArgumentsParser()  {
        parser.addArgument(PREFIX + HIVE_JDBC_DRIVER)
                .type(String.class)
                .required(true);

        parser.addArgument(PREFIX + HIVE_URL)
                .type(String.class)
                .required(true);

        parser.addArgument(PREFIX + OOZIE_URL)
                .type(String.class)
                .required(true);

        parser.addArgument(PREFIX + PARENT_WF)
                .type(String.class)
                .required(true);

        parser.addArgument(PREFIX + WF_USER)
                .type(String.class)
                .required(true);

        parser.addArgument(PREFIX + ORADOOP_VERSION)
                .type(String.class)
                .required(true);

        parser.addArgument(PREFIX + REFRESH_GROUP)
                .type(String.class)
                .required(false);

        parser.addArgument(PREFIX + TABLES_LIST)
                .type(String.class)
                .required(false);

        parser.addArgument(PREFIX + SUB_WORKFLOW_PATH)
                .type(String.class)
                .required(true);

        parser.addArgument(PREFIX + BATCH_SIZE)
                .type(Integer.class)
                .required(false);

        parser.addArgument(PREFIX + SLEEP_SECONDS)
                .type(Integer.class)
                .required(false);
    }

    private void parseArguments(ArgumentParser parser, String[] args) {

        try {
            Namespace params = parser.parseArgs(args);

            if (params.get(PARENT_WF) != null)
                this.setParentWf(params.get(PARENT_WF));

            if (params.get(HIVE_JDBC_DRIVER) != null)
                this.setHiveJdbcDriver(params.get(HIVE_JDBC_DRIVER));

            if (params.get(HIVE_URL) != null)
                this.setHiveUrl(params.get(HIVE_URL));

            if (params.get(OOZIE_URL) != null)
                this.setOozieUrl(params.get(OOZIE_URL));

            if (params.get(REFRESH_GROUP) != null)
                this.setRefreshGroup(params.get(REFRESH_GROUP));

            if (params.get(ORADOOP_VERSION) != null)
                this.setOradoopVersion(params.get(ORADOOP_VERSION));

            if (params.get(TABLES_LIST) != null)
                this.setTablesList(params.get(TABLES_LIST));

            if (params.get(WF_USER) != null)
                this.setWfUser(params.get(WF_USER));

            if (params.get(SUB_WORKFLOW_PATH) != null)
                this.setSubWorkflowPath(params.get(SUB_WORKFLOW_PATH));

            if (params.get(BATCH_SIZE) != null)
                this.setBatchSize(params.get(BATCH_SIZE));

            if (params.get(SLEEP_SECONDS) != null)
                this.setSleepSeconds(params.get(SLEEP_SECONDS));

            System.out.println("query runner parser " + params.toString());

        }
        catch (ArgumentParserException e) {
            System.out.println("parse Arguments failed: syntax is " + parser.toString());
            parser.handleError(e);
        }
    }

}


