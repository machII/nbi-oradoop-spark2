package com.starhomemach.nbi.oradoop.dimension;

import com.starhomemach.nbi.oradoop.dataretention.PartitionKey;
import com.starhomemach.nbi.oradoop.dataretention.date.DateService;
import com.starhomemach.nbi.oradoop.dimension.metadata.TableMetadata;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tpin on 08/01/2017.
 */
public abstract class DimensionHandler {
    protected String databaseHomeDir="/starhome/common/data";
    public final String DIM_DB = "dimension_db";
    public final String STGOUT_DB = "stgout_db";
    protected String columnList;
    protected List<String> pkList;

    public abstract String getHandlerKey();
    public abstract String getExtTableSql(TableMetadata md, int isPartitioned, String partCols , PartitionKey dateService)  ;

    protected String getTmpTableName(String tableCode)
    {
        return STGOUT_DB + "." +  tableCode + "_TMP";
    }

    protected String getDeltaTableName(String tableCode)
    {
        return STGOUT_DB + "." +  tableCode + "_DEL";
    }
}
