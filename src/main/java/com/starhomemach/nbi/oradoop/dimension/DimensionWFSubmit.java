package com.starhomemach.nbi.oradoop.dimension;

import com.starhomemach.nbi.oradoop.export.util.HandleProcessConfiguration;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.oozie.client.OozieClient;
import org.apache.oozie.client.WorkflowJob;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.StringTokenizer;

/**
 * Created by tpin on 12/01/2017.
 */
@Service
public class DimensionWFSubmit {

    private final Log logger = LogFactory.getLog(getClass());
    public void submitDimLoadWF(String oozieUrl, String parentWf, String dimWfPath, String tableCodeListStr, String wfListStr, String username, int batchSize) throws Exception
    {
        HashSet<String> activeJobs =new HashSet<String>(10);
        HandleProcessConfiguration oradoopConf = new HandleProcessConfiguration();
        OozieClient wc = new OozieClient(oozieUrl);

        logger.info("Setting properties from " + dimWfPath+"/job.properties");

        // create a workflow job configuration and set the workflow application path
        Properties conf = oradoopConf.getPropertiesFile(dimWfPath+"/job.properties");

        // setting workflow parameters
        conf.setProperty(OozieClient.APP_PATH, dimWfPath);
        logger.info("Setting App path from " + dimWfPath);
        conf.setProperty("parentWfID", parentWf);
        logger.info("Setting parentWFid " + parentWf);
        conf.setProperty("wf_ids", wfListStr);
        logger.info("List of source wfs that arrived to staging " + wfListStr);
        conf.setProperty("user.name",username);
        conf.setProperty("user name ", username);

        StringTokenizer strTk = new StringTokenizer(tableCodeListStr,",");

        // continue to spawn jobs as long as we have a dim ready
        while (strTk.hasMoreElements())
        {
            //submit dim and add to active queue
            Properties wfConf = (Properties)(conf.clone());
            String tableCode = strTk.nextToken();
            conf.setProperty("table_code", tableCode);
            logger.info("Adding table_code " + tableCode);
            String wfId = wc.run(conf);
            activeJobs.add(wfId);
            logger.info("Job Submitted : " +  wfId);

            logger.info("Has " + activeJobs.size() + " jobs in active queue ");

            // as long as the queue is full of there are no dims to load but queue isn't empty, go to sleep and check if queue is empty
            while (activeJobs.size() == batchSize || (!strTk.hasMoreElements() && activeJobs.size()>0))
            {
                logger.info("Reached limit, going to sleep");
                Thread.sleep(30*1000);
                logger.info("Going to update list");

                // update queue
                updateSetWithFinishedJobs(wc,activeJobs);
                logger.info("Has " + activeJobs.size() + " jobs in active queue ");
            }
        }

    }

    // updates queue after querying the oozie if jobs are done
    public void updateSetWithFinishedJobs(OozieClient wc, HashSet<String> activeJobs) throws Exception
    {
        String filterList = "";
        boolean first = true;
        for (String jobId:activeJobs)
        {
            if (first)
                filterList = OozieClient.FILTER_ID + "=" +  jobId;
            else
                filterList = filterList + ";" + OozieClient.FILTER_ID + "=" +  jobId;
            first=false;
        }

        /*String filter = OozieClient.FILTER_STATUS + "=" + WorkflowJob.Status.FAILED + ";"+ OozieClient.FILTER_STATUS + "=" + WorkflowJob.Status.KILLED + ";" +
                        OozieClient.FILTER_STATUS + "=" + WorkflowJob.Status.SUCCEEDED + ";" + filterList;*/
        String filter = OozieClient.FILTER_STATUS + "=" + WorkflowJob.Status.SUCCEEDED + ";" + filterList;

        logger.info("Querying oozie with filter " + filter);
        List<WorkflowJob> wfList = wc.getJobsInfo(filter);

        // remove done jobs
        for (WorkflowJob job : wfList) {
            if (job.getStatus()==WorkflowJob.Status.SUCCEEDED)
                logger.info(job.getId() + " is Done.");
            else
            {
                logger.info(job.getId() + " is in " + job.getStatus() + "State, please fix and rerun from oozie scheduler");
            }

            logger.info("Removing " + job.getId() + " from active queue");
            activeJobs.remove(job.getId());
        }

    }
}
