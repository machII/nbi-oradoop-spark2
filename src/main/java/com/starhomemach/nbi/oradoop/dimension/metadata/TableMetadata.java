package com.starhomemach.nbi.oradoop.dimension.metadata;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.*;

/**
 * Created by tpin on 09/01/2017.
 */
public abstract class TableMetadata {

    private String tabCode = null;
    protected String tabName = null;
    protected String tabOwn = null;
    protected ArrayList<String> columnNames = new ArrayList<String>();
    protected ArrayList<String> partColumnNames = new ArrayList<String>();
    protected Map<String,String> columnTypes = new HashMap<String,String>();
    protected ArrayList<String> pkCols = new ArrayList<String>();
    protected String tableStorage = null;

    public String getTableStorage() {
        return tableStorage;
    }

    public String getTabOwn() {
        return tabOwn;
    }

    public Map<String,String> getColumnTypes() {
        return columnTypes;
    }

    public ArrayList<String> getPartitionColumnNames() {
        return partColumnNames;
    }

    public void setPartitionColumnNames(ArrayList<String> partColumnNames) {
        this.partColumnNames = partColumnNames;
    }

    public TableMetadata(String tableOwner, String tableCode)
    {
        this.tabCode=tableCode;
        this.tabOwn=tableOwner;
    }

    public abstract boolean initMetadata(Connection conn) throws SQLException;

    public String getTableCode() {
        return tabCode;
    }

    public String getTableName() {
        return tabName;
    }

    public ArrayList<String> getColumnNames() {
        return columnNames;
    }

    public String getColumnNamesStr(){
        boolean first=true;
        String ret="";
        for (String col:columnNames) {
            ret = ret + (first?"":",") + col;
            first=false;
        }
        return ret;
    }

    public String getColumnNamesStrExclude(Set columns){
        boolean first=true;
        String ret="";
        for (String col:columnNames) {
            if (!columns.contains(col.toLowerCase()) && !columns.contains(col.toUpperCase())) {
                ret = ret + (first ? "" : ",") + col;
                first = false;
            }
        }
        return ret;
    }

    public String getColumnNameTypesStr(){
        boolean first=true;
        String ret="";
        for (String col:columnNames) {
            ret = ret + (first?"":",") + col + " " + columnTypes.get(col);
            first=false;
        }
        return ret;
    }

    public ArrayList<String> getPkCols() {
        return pkCols;
    }

    public String getPkColumnNamesStr(){
        boolean first=true;
        String ret="";
        for (String col:pkCols) {
            ret = ret + (first?"":",") + col;
            first=false;
        }
        return ret;
    }
}
