package com.starhomemach.nbi.oradoop.dimension;

import com.starhomemach.nbi.oradoop.dataretention.PartitionKey;
import com.starhomemach.nbi.oradoop.dataretention.PartitionKeyFactory;
import com.starhomemach.nbi.oradoop.dataretention.date.DateService;
import com.starhomemach.nbi.oradoop.dataretention.date.DateServiceFactory;
import com.starhomemach.nbi.oradoop.dimension.metadata.OracleEtlTableMetadata;
import com.starhomemach.nbi.oradoop.dimension.metadata.TableMetadata;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Service;

import java.sql.*;

/**
 * Created by tpin on 11/01/2017.
 */
@Service
public class DimensionLoader {
    private final Log logger = LogFactory.getLog(getClass());

    public void loadDim(AnnotationConfigApplicationContext context, String impalaJdbcUrl, String impalaJdbcDriver, String oracleJdbcUrl,
                        String oracleJdbcDriver, String oracleUsername, String oraclePassword, String tableCode, int isPartitioned, String partCols ) throws Exception
    {
        Class.forName(impalaJdbcDriver);
        Class.forName(oracleJdbcDriver);

        Connection oraConn = null;
        Connection impConn = null;
        PreparedStatement stmt = null;

        try {
            logger.info("connecting to oracle and impala");
            oraConn = DriverManager.getConnection(oracleJdbcUrl, oracleUsername, oraclePassword);
            impConn = DriverManager.getConnection(impalaJdbcUrl,"impala","impala");
            Statement setSession = impConn.createStatement();
            setSession.execute("set yarn.app.mapreduce.am.resource.mb=4096");
            setSession.execute("set spark.driver.memory=4294967296");
            setSession.execute("set mapreduce.reduce.memory.mb=6144");
            setSession.execute("set mapreduce.reduce.java.opts=-Djava.net.preferIPv4Stack=true -Xmx4294967296");
            setSession.execute("set mapreduce.map.memory.mb=4096");
            setSession.execute("set hive.execution.engine=spark");
            setSession.execute("set hive.exec.dynamic.partition.mode=nostrict");
            setSession.execute("set mapred.job.queue.name=root.dim.batch");
            setSession.execute("set hive.optimize.sort.dynamic.partition=true");
            setSession.execute("set hive.exec.max.dynamic.partitions=3000");
            setSession.execute("set hive.exec.max.dynamic.partitions.pernode=3000");
            setSession.execute("set spark.executor.instances=10");
            setSession.execute("set spark.executor.memory=30g");
            setSession.execute("set spark.executor.cores=3");


            TableMetadata md = new OracleEtlTableMetadata(tableCode);
            md.initMetadata(oraConn);
            logger.info("Metadata loaded");
            DimensionHandler handler = context.getBean(DimensionHandlerFactory.class).getDimensionHandler("SIMPLE");
            PartitionKey partitionKey = context.getBean(PartitionKeyFactory.class).getPartitionKey(partCols!=null?partCols.trim().toUpperCase():"");
            String insertSql = handler.getExtTableSql(md,isPartitioned,partCols,partitionKey);
            logger.info("insert about to run:");
            logger.info(insertSql);
            stmt = impConn.prepareStatement(insertSql);
            stmt.execute();
            logger.info("Insert Done");
        }
        catch (SQLException e)
        {
            throw e;
        }
        finally {
            if (oraConn != null)
                oraConn.close();
            if (impConn != null)
                impConn.close();
        }
    }
}
