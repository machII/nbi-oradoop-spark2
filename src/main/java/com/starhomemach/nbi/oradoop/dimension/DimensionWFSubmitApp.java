package com.starhomemach.nbi.oradoop.dimension;

import com.starhomemach.nbi.oradoop.export.SpringConfig;
import org.apache.commons.beanutils.converters.IntegerArrayConverter;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Created by tpin on 12/01/2017.
 */
public class DimensionWFSubmitApp {
    public static void main(String[] args) throws Exception
    {
        System.out.println("################################################ START ####################################################");
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SpringConfig.class);

        context.getBean(DimensionWFSubmit.class).submitDimLoadWF(args[0],
                args[1],
                args[2],
                args[3],
                args[4],
                args[5],
                Integer.parseInt(args[6]));
        System.out.println("################################################ END ####################################################");
    }
}
