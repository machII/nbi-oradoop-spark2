package com.starhomemach.nbi.oradoop.dimension.metadata;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.sql.*;

/**
 * Created by tpin on 09/01/2017.
 */
public class OracleEtlTableMetadata extends TableMetadata {
    private final Log logger = LogFactory.getLog(getClass());

    public OracleEtlTableMetadata(String tableCode) {
        super(null,tableCode);
    }


    @Override
    public boolean initMetadata(Connection conn) throws SQLException {
        String sql="select DK_CLMN,RLS_POL,cl_id_pk,CL_CLMN_CD,TBL_NM_BI from MD.V_TBL t where   upper(TBL_CD) =upper(?)";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setString(1,this.getTableCode());
        logger.info("execute: " + sql);
        logger.info("bind var: " + this.getTableCode());
        ResultSet rs = stmt.executeQuery();

        boolean initSuccess = false;

        while (rs.next())
        {
            initSuccess=true;

            tabName = rs.getString(5);

            // add pk column
            pkCols.add(rs.getString(1).toLowerCase());
            logger.info("pk column added: " + rs.getString(1));
            columnNames.add(rs.getString(1).toLowerCase());
            logger.info("column added: " + rs.getString(1));

            logger.info("Rls policy: " + rs.getShort(2));
            logger.info("cl id column: " + rs.getString(3));

            // check if rls policy enabled and the client id is pk then add column to pk columns
            if (rs.getShort(2)==1 && rs.getString(3).equalsIgnoreCase("Y")) {
                pkCols.add(rs.getString(4).toLowerCase());
                logger.info("pk column added: " + rs.getString(4));
            }

            if (rs.getString(4) != null) {
                columnNames.add(rs.getString(4).toLowerCase());
                logger.info("column added: " + rs.getString(4));
            }
        }

        stmt.close();
        rs.close();

        sql="   SELECT ATTR_CLMN_CD clmn_cd\n" +
                "   FROM MD.T_ATTR\n" +
                "   WHERE MD.T_ATTR.DIM_TBL_CD = upper(?)\n" +
                "   UNION ALL\n" +
                "   SELECT 'R'||RD.DK_CLMN||NVL2(R.REF_RL,'_'||R.REF_RL,'') clmn_cd\n" +
                "   FROM MD.T_RDK R,MD.T_DIM_TBL RD\n" +
                "   WHERE R.DIM_TBL_CD = upper(?)\n" +
                "   AND RD.DIM_TBL_CD = R.REF_DIM_TBL_CD\n" +
                "   AND R.RDK_EXST = 'Y'\n" +
                "   UNION ALL\n" +
                "   SELECT DK_CLMN clmn_cd\n" +
                "   FROM MD.T_DIM_TBL\n" +
                "   START WITH PRNT_DIM_TBL_CD = upper(?)\n" +
                "   CONNECT BY PRNT_DIM_TBL_CD = PRIOR DIM_TBL_CD\n" +
                "   UNION ALL\n" +
                "   SELECT 'OUTDAT' clmn_cd\n" +
                "   FROM DUAL\n" +
                "   UNION ALL\n" +
                "   SELECT 'MOD_TMST' clmn_cd\n" +
                "   FROM DUAL\n" +
                "   UNION ALL\n" +
                "   SELECT 'ADT_ID' clmn_cd\n" +
                "   FROM DUAL\n";
        stmt = conn.prepareStatement(sql);
        stmt.setString(1,this.getTableCode());
        stmt.setString(2,this.getTableCode());
        stmt.setString(3,this.getTableCode());

        logger.info("execute: " + sql);
        logger.info("bind var1: " + this.getTableCode());
        logger.info("bind var2: " + this.getTableCode());
        logger.info("bind var3: " + this.getTableCode());

        rs = stmt.executeQuery();

        while (rs.next()) {
            logger.info("column added: " + rs.getString(1));
            columnNames.add(rs.getString(1).toLowerCase());
        }

        stmt.close();
        rs.close();

        return initSuccess;
    }
}
