package com.starhomemach.nbi.oradoop.dimension;

import com.starhomemach.nbi.oradoop.export.util.HandleProcessConfiguration;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.oozie.client.OozieClient;
import org.apache.oozie.client.WorkflowJob;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;

@Service
class DimensionFullWFSubmit {

    private final Log logger = LogFactory.getLog(getClass());

    private DimensionFullArgumentsHolder arguments = DimensionFullArgumentsHolder.getInstance();

    private static String tableListPlaceholder = "{{tableList}}";
    private static String refreshGroupPlaceholder = "{{refreshGroup}}";

    private static String GET_REFRESH_GROUP_FILTER = "WHERE refresh_group = '" + refreshGroupPlaceholder + "'";
    private static String GET_TABLE_LIST_FILTER = "WHERE concat_ws('.', hive_database_name, hive_table_name) IN ('" + tableListPlaceholder + "')";

    private HashSet<String> activeJobs;
    private OozieClient wc;

    void submitDimFullLoadWF() throws Exception
    {
        Connection con = null;
        Statement stmt = null;
        ResultSet rs = null;
        String sqlStmt = "SELECT hive_database_name, hive_table_name, load_type, source_database_name, sqoop_arguments\n" +
                    "  FROM metadata_db.t_table_source\n";

        if (arguments.isRefreshGroupSet())
            sqlStmt = sqlStmt + GET_REFRESH_GROUP_FILTER.replace(refreshGroupPlaceholder, arguments.getRefreshGroup());

        if (arguments.isTableListSet())
            sqlStmt = sqlStmt + GET_TABLE_LIST_FILTER.replace(tableListPlaceholder, arguments.getTablesList().replace(",", "','"));

        try {
            Class.forName(arguments.getHiveJdbcDriver());

            con = DriverManager.getConnection(arguments.getHiveUrl(), arguments.getWfUser(), arguments.getWfUser());
            con.setAutoCommit(false);
            stmt = con.createStatement();

            System.out.println("Running Query: " + sqlStmt);
            stmt.execute(sqlStmt);

            activeJobs = new HashSet<>(10);
            HandleProcessConfiguration oradoopConf = new HandleProcessConfiguration();
            wc = new OozieClient(arguments.getOozieUrl());

            logger.info("Setting properties from " + arguments.getSubWorkflowPath() + "/job.properties");

            // create a workflow job configuration and set the workflow application path
            Properties conf = HandleProcessConfiguration.getPropertiesFile(arguments.getSubWorkflowPath() + "/job.properties");

            Properties dbConf = HandleProcessConfiguration.handleDBConf();

            // setting workflow parameters
            conf.setProperty(OozieClient.APP_PATH, arguments.getSubWorkflowPath());
            conf.setProperty("parentWfID", arguments.getParentWf());
            conf.setProperty("user.name", arguments.getWfUser());
            conf.setProperty("user name ", arguments.getWfUser());
            conf.setProperty("oradoopVersion", arguments.getOradoopVersion());
            if (arguments.isRefreshGroupSet())
                conf.setProperty("refreshGroup", arguments.getRefreshGroup());
            else
                conf.setProperty("refreshGroup", "-");
            conf.setProperty("jdbcUrlHive", arguments.getHiveUrl());

            rs = stmt.getResultSet();
            while (rs.next()) {
                //submit dim and add to active queue
                conf.setProperty("hiveDatabaseName", rs.getString("hive_database_name"));
                conf.setProperty("hiveTableName", rs.getString("hive_table_name"));
                conf.setProperty("loadType", rs.getString("load_type"));
                String sourceDbName = rs.getString("source_database_name" );
                conf.setProperty("sqoopArguments", "import --username " + dbConf.getProperty(sourceDbName + ".username") + " --password " + dbConf.getProperty(sourceDbName + ".password") + " --connect " + dbConf.getProperty(sourceDbName + ".jdbcUrl") + " " + rs.getString("sqoop_arguments"));
                String wfId = wc.run(conf);
                activeJobs.add(wfId);
                logger.info("Job Submitted : " +  wfId);

                logger.info("Has " + activeJobs.size() + " jobs in active queue ");

                // as long as the queue is full of there are no dims to load but queue isn't empty, go to sleep and check if queue is empty
                while (activeJobs.size() == arguments.getBatchSize())
                {
                    sleep();
                }
            }

            while (activeJobs.size() > 0)
            {
                sleep();
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            try {
                if (rs != null)
                    rs.close();
                if (stmt != null)
                    stmt.close();
                if (con != null)
                    con.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void sleep () throws Exception
    {
        logger.info("Reached limit, going to sleep");
        Thread.sleep(arguments.getSleepSeconds() * 1000);
        logger.info("Going to update list");

        // update queue
        updateSetWithFinishedJobs(wc, activeJobs);
        logger.info("Has " + activeJobs.size() + " jobs in active queue ");
    }

    private void updateSetWithFinishedJobs(OozieClient wc, HashSet<String> activeJobs) throws Exception
    {
        StringBuilder filterList = new StringBuilder();
        boolean first = true;
        for (String jobId:activeJobs)
        {
            if (first)
                filterList = new StringBuilder(OozieClient.FILTER_ID + "=" + jobId);
            else
                filterList.append(";").append(OozieClient.FILTER_ID).append("=").append(jobId);
            first = false;
        }

        logger.info("Querying oozie with filter " + filterList);
        List<WorkflowJob> wfList = wc.getJobsInfo(filterList.toString());

        // remove done jobs
        for (WorkflowJob job : wfList) {
            if (job.getStatus() == WorkflowJob.Status.SUCCEEDED)
                logger.info(job.getId() + " is Done.");
            else
                logger.info(job.getId() + " is in " + job.getStatus() + " state, please fix and rerun from oozie scheduler");

            if (job.getStatus() != WorkflowJob.Status.PREP && job.getStatus() != WorkflowJob.Status.RUNNING && job.getStatus() != WorkflowJob.Status.SUSPENDED) {
                logger.info("Removing " + job.getId() + " from active queue");
                activeJobs.remove(job.getId());
            }
        }

    }
}
