package com.starhomemach.nbi.oradoop.dimension;

import com.starhomemach.nbi.oradoop.export.SpringConfig;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Created by tpin on 11/01/2017.
 */
public class DimensionApp {
    public static void main(String[] args) throws Exception
    {
        System.out.println("################################################ START ####################################################");
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SpringConfig.class);

        int isPart = Integer.parseInt(args[7]);
        context.getBean(DimensionLoader.class).loadDim(context,args[0],
                args[1],
                args[2],
                args[3],
                args[4],
                args[5],
                args[6],
                Integer.parseInt(args[7]),
                isPart==1?args[8]:"");
        System.out.println("################################################ END ####################################################");
    }
}
