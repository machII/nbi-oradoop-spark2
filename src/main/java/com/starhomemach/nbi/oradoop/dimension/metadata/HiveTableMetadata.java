package com.starhomemach.nbi.oradoop.dimension.metadata;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by tpin on 11/01/2017.
 */
public class HiveTableMetadata extends TableMetadata {

    public HiveTableMetadata(String tableOwner,String tableCode) {
        super(tableOwner,tableCode);
        this.tabName=tableCode;
    }

    @Override
    public boolean initMetadata(Connection conn) throws SQLException {
        String sql="select s.\"LOCATION\" \n" +
                   " from \"DBS\" d \n" +
                   " join \"TBLS\" t on d.\"DB_ID\"=t.\"DB_ID\" and upper(d.\"NAME\")=upper(?) \n" +
                   " join \"SDS\" s on t.\"SD_ID\" = s.\"SD_ID\" and upper(t.\"TBL_NAME\")=upper(?) ";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setString(1,this.tabOwn);
        stmt.setString(2,this.tabName);
        ResultSet rs = stmt.executeQuery();
        boolean init = false;

        while (rs.next()){
            this.tableStorage = rs.getString(1);
            init=true;
        }

        stmt.close();
        rs.close();

        // get column names and types
        sql="select c.\"COLUMN_NAME\",c.\"TYPE_NAME\" \n" +
                " from \"DBS\" d join \"TBLS\" t on d.\"DB_ID\"=t.\"DB_ID\" and upper(d.\"NAME\")=upper(?) \n" +
                " join \"SDS\" s on t.\"SD_ID\" = s.\"SD_ID\" and upper(t.\"TBL_NAME\")=upper(?) \n" +
                " join \"COLUMNS_V2\" c on s.\"CD_ID\"=c.\"CD_ID\" order by c.\"INTEGER_IDX\" asc";
        stmt = conn.prepareStatement(sql);
        stmt.setString(1,this.tabOwn);
        stmt.setString(2,this.tabName);
        rs = stmt.executeQuery();

        while (rs.next()) {
            this.columnNames.add(rs.getString(1));
            this.columnTypes.put(rs.getString(1),rs.getString(2));
        }

        sql="SELECT \"PKEY_NAME\", FROM \"DBS\" D,\"TBLS\" A,\"PARTITION_KEYS\" B \n" +
                "WHERE A.\"DB_ID\"=D.\"DB_ID\" AND A.\"TBL_ID\"=B.\"TBL_ID\" AND \n" +
                "UPPER(D.\"NAME\")=UPPER(?) \n" +
                "UPPER(\"TBL_NAME\") = UPPER(?) AND \n" +
                "ORDER BY \"INTEGER_IDX\" ASC";
        stmt = conn.prepareStatement(sql);
        stmt.setString(1,this.tabOwn);
        stmt.setString(2,this.tabName);
        rs = stmt.executeQuery();

        while (rs.next()) {
            this.partColumnNames.add(rs.getString(1));
        }

        return init;
    }
}
