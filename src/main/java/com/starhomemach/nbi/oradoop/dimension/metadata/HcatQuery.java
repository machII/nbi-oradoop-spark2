package com.starhomemach.nbi.oradoop.dimension.metadata;

import com.starhomemach.nbi.oradoop.OradoopConf;
import org.apache.hadoop.hive.metastore.api.Order;
import org.apache.hive.hcatalog.api.HCatClient;
import org.apache.hive.hcatalog.api.HCatClientHMSImpl;
import org.apache.hive.hcatalog.api.HCatTable;
import org.apache.hive.hcatalog.data.schema.HCatFieldSchema;
import org.apache.hive.hcatalog.data.schema.HCatSchema;
import org.springframework.stereotype.Service;

import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Properties;

/**
 * Created by tpin on 05/03/2017.
 */
@Service
public class HcatQuery {
    public String query(String[] args) throws Exception {
        if (args.length < 4) {
            System.out.println("Usage: [DBNAME] [OBJECT_TYPE: TABLE|DB] [OBJECT_NAME] [REQUEST: COLUMNS|PARTCOLS|LOCATION] [OPTIONS:NO_DATATYPE]");
            return null;
        }

        HCatClient hcatClient = HCatClientHMSImpl.create(new OradoopConf().getConf());
        String retVal = "";
        System.out.println("Object Type: " + args[1]);
        if (args[1].equalsIgnoreCase("TABLE")) {
            HCatTable tableProp = hcatClient.getTable(args[0], args[2]);
            System.out.println("Got table props " + tableProp);
            System.out.println("requested " + args[3]);
            if (args[3].equalsIgnoreCase("LOCATION")) {
                retVal = tableProp.getLocation();
                System.out.println("got " + retVal);
            } else if (args[3].equalsIgnoreCase("COLUMNS")) {
                List<HCatFieldSchema> cols = tableProp.getCols();
                boolean first = true;
                for (HCatFieldSchema field : cols) {
                    System.out.println("got " + field.getName() + " " + field.getTypeString());
                    if (first) {
                        retVal = retVal + field.getName() + " " + field.getTypeString();
                        first = false;
                    } else {
                        retVal = retVal + "," + field.getName() + " " + field.getTypeString();
                    }
                }

                System.out.println("return " + retVal);
            } else if (args[3].equalsIgnoreCase("PARTCOLS")) {
                List<HCatFieldSchema> cols = tableProp.getPartCols();
                boolean first = true;
                for (HCatFieldSchema field : cols) {
                    System.out.println("got " + field.getName() + " " + field.getTypeString());
                    if (first) {
                        retVal = retVal + field.getName();
                        if (!(args.length==5 && args[4].equalsIgnoreCase("NO_DATATYPE")))
                        {
                            retVal = retVal + " " + field.getTypeString();
                        }
                        first = false;
                    } else {
                        retVal = retVal + "," + field.getName();
                        if (!(args.length==5 && args[4].equalsIgnoreCase("NO_DATATYPE")))
                        {
                            retVal = retVal + " " + field.getTypeString();
                        }
                    }
                }

                System.out.println("return " + retVal);
            }
        }
        return retVal;
    }

    public String getColumnsWithExclude(String args[]) throws Exception
    {
        if (args.length < 5) {
            System.out.println("Usage: [DBNAME] [OBJECT_TYPE: TABLE|DB] [OBJECT_NAME] [REQUEST: COLUMNS] [EXCLUDE_COLUMNS]");
            return null;
        }
        String cols = query(args).toUpperCase();
        System.out.println("Columns: " + cols);

        String[] colEx = args[4].split(",");
        for (int i = 0; i < colEx.length; i++) {
            System.out.println("Removing " + colEx[i]);
            int loc = cols.indexOf(colEx[i].toUpperCase());
            loc = loc!=-1?loc:cols.length();
            System.out.println("Location found of start column index " + loc);
            int endLoc = cols.indexOf(",",loc);

            if (endLoc != -1)
                endLoc = endLoc+1;
            else {
                endLoc=cols.length();
                loc = loc-1;
            }

            System.out.println("Location found of end column index " + endLoc);
            cols = cols.replaceAll(cols.subSequence(loc,endLoc).toString(),"");
            System.out.println("After replacement " + cols);
        }
        System.out.println("After Exclude " + cols);

        return cols;
    }

    public static void main (String[] args) throws Exception
    {
        HcatQuery q = new HcatQuery();
        q.getColumnsWithExclude(args);
    }
}
