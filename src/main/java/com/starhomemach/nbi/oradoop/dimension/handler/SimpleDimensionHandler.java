package com.starhomemach.nbi.oradoop.dimension.handler;

import com.starhomemach.nbi.oradoop.dataretention.CustomerPartitionKey;
import com.starhomemach.nbi.oradoop.dataretention.PartitionKey;
import com.starhomemach.nbi.oradoop.dataretention.date.DateService;
import com.starhomemach.nbi.oradoop.dataretention.date.DkDatMoService;
import com.starhomemach.nbi.oradoop.dimension.DimensionHandler;
import com.starhomemach.nbi.oradoop.dimension.metadata.OracleEtlTableMetadata;
import com.starhomemach.nbi.oradoop.dimension.metadata.TableMetadata;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.HashSet;


/**
 * Created by tpin on 08/01/2017.
 */
@Component
public class SimpleDimensionHandler extends DimensionHandler {
    private final Log logger = LogFactory.getLog(getClass());



    @Override
    public String getHandlerKey() {
        return "SIMPLE";
    }

    @Override
    public String getExtTableSql(TableMetadata md,int isPartitioned, String partCol, PartitionKey partitionKey){

        String lastVerSql = getlastVerPerKeySql(md);
        String partColSql = "";
        String partColStmt = "";
        String partColSpec = "";
        HashSet excludeList = new HashSet();
        if (isPartitioned >0) {
            partColSql = "," + partitionKey.getStmtCalaulation() + " " + partCol ;
            partColStmt =" PARTITION("+ partCol + ") ";
            partColSpec ="," + partCol;
            excludeList.add(partCol);
        }

        String sql = " INSERT into  " + getTmpTableName(md.getTableName()) + partColStmt + "(" + md.getColumnNamesStrExclude(excludeList) + ",mod_tp,cre_dat,mod_dat" + partColSpec + ")\n" +
                     " SELECT " + md.getColumnNamesStrExclude(excludeList) + ",mod_tp,cre_dat,mod_dat" + partColSql + " FROM \n" +
                     " (SELECT " + md.getColumnNamesStr() + ",mod_tp,cre_dat,mod_dat, ROW_NUMBER () OVER (PARTITION BY " + md.getPkColumnNamesStr() + " ORDER BY mod_tmst DESC) mod_tmst_rank FROM \n" +
                     " (SELECT " + md.getColumnNamesStr() + ",mod_tp,cre_dat,mod_dat FROM " + DIM_DB + "." + md.getTableName() + " a\n" +
                     " WHERE NOT EXISTS (SELECT 1 FROM " + lastVerSql + " b WHERE " + getJoinStmt(md) + " and b.mod_tp in ('D'))\n" +
                     " UNION ALL \n" +
                     " SELECT " + md.getColumnNamesStr() + ",mod_tp,current_timestamp as cre_dat,current_timestamp as mod_dat FROM " +  lastVerSql + " b WHERE b.mod_tp in ('U','I')) del_plus_full) ranked_tab \n" +
                     " WHERE mod_tmst_rank = 1 \n";
        return sql;
    }

    private String getlastVerPerKeySql(TableMetadata md)
    {
        String sql = " (SELECT " + md.getColumnNamesStr() + ",mod_tp FROM \n" +
                     " (SELECT " + md.getColumnNamesStr() + ",mod_tp, ROW_NUMBER () OVER (PARTITION BY " + md.getPkColumnNamesStr() + " ORDER BY mod_tmst DESC) mod_tmst_rank \n" +
                     " FROM " + getDeltaTableName(md.getTableName()) + ") t \n" +
                     " WHERE mod_tmst_rank = 1) \n";
        return sql;
    }

    private String getJoinStmt(TableMetadata md)
    {
        boolean first=true;
        String ret="";
        for (String col:md.getPkCols()) {
            ret = ret + (first?"":" and ") + "a." + col + "=b." + col + " ";
            first=false;
        }
        return ret;
    }

    public static void main(String[] args) throws Exception
    {
        Class.forName("oracle.jdbc.driver.OracleDriver");
        int isPartitioned=1;
        String partCols="CL_ID";
        Connection conn = null;
        try {
            conn = DriverManager.getConnection("jdbc:oracle:thin:@10.30.40.13:1521:BIDG1", "oradoop", "oradoop");
            OracleEtlTableMetadata md = new OracleEtlTableMetadata("TAF");
            md.initMetadata(conn);
            SimpleDimensionHandler hand = new SimpleDimensionHandler();
            String sql = hand.getExtTableSql(md, isPartitioned,  partCols,new CustomerPartitionKey());
            System.out.println(sql);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        finally {
            if (conn != null)
                conn.close();
        }
    }
}
