package com.starhomemach.nbi.oradoop.dimension;

import com.starhomemach.nbi.oradoop.export.SpringConfig;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class DimensionFullWFSubmitApp {
    public static void main(String[] args) throws Exception
    {
        System.out.println("################################################ START ####################################################");
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SpringConfig.class);

        DimensionFullArgumentsHolder arguments = DimensionFullArgumentsHolder.getInstance();
        arguments.initDimensionFullArgumentsHolder(args);

        context.getBean(DimensionFullWFSubmit.class).submitDimFullLoadWF();
        System.out.println("################################################ END ####################################################");
    }
}
