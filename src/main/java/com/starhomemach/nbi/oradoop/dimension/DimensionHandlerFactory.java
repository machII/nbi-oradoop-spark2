package com.starhomemach.nbi.oradoop.dimension;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

/**
 * Created by tpin on 08/01/2017.
 */
@Service
public class DimensionHandlerFactory {
    private final Log logger = LogFactory.getLog(getClass());

    @Autowired
    private List<DimensionHandler> listOfHandlers;

    static private HashMap<String,DimensionHandler> handlerHashMap;

    @PostConstruct
    public void init()
    {
        handlerHashMap = new HashMap<String,DimensionHandler>();
        for(DimensionHandler dimensionHandler: listOfHandlers)
        {
            handlerHashMap.put(dimensionHandler.getHandlerKey(),dimensionHandler);
        }
    }

    public DimensionHandler getDimensionHandler(String retHandler) throws IOException {
        try {
            return handlerHashMap.get(retHandler);
        }catch(Exception e){
            logger.error("Failed to fetch the Dimension Handler:" + retHandler + ". The exception is:" + e.getMessage() + " .going to throw exception");
            throw e;
        }


    }



}
