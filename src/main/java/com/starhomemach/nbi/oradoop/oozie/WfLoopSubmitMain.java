package com.starhomemach.nbi.oradoop.oozie;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class WfLoopSubmitMain {
    public static void main(String[] args) throws Exception
    {
        System.out.println("################################################ START ####################################################");
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(OozieSpringConfig.class);

        WfLoopSubmitArgumentsHolder arguments = WfLoopSubmitArgumentsHolder.getInstance(WfLoopSubmitArgumentsHolder.class);
        arguments.initArgumentsHolder(args);

        context.getBean(WfLoopSubmitHandler.class).run();
        System.out.println("################################################ END ####################################################");
    }
}
