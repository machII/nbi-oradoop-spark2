package com.starhomemach.nbi.oradoop.oozie;

import com.starhomemach.nbi.oradoop.export.util.HandleProcessConfiguration;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.directory.api.util.Strings;
import org.apache.oozie.client.OozieClient;
import org.apache.oozie.client.WorkflowJob;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Properties;

@Service
public class WfLoopSubmitHandler {

    private WfLoopSubmitArgumentsHolder arguments;
    private final Log logger = LogFactory.getLog(getClass());

    private int i =1;
    private HashSet<String> activeJobs;
    private OozieClient oozieClient;

    public void run() throws Exception {
        try {

            arguments = WfLoopSubmitArgumentsHolder.getInstance();
            String parametersDelimiter = arguments.getParametersDelimiter();

            String paramName1 = arguments.getRepeatingParam1Name();
            String[] params1 = arguments.getRepeatingParam1Values().split(parametersDelimiter);

            String paramName2 = arguments.getRepeatingParam2Name();
            String[] params2 = arguments.getRepeatingParam2Values().split(parametersDelimiter);

            String paramName3 = arguments.getRepeatingParam3Name();
            String[] params3 = arguments.getRepeatingParam3Values().split(parametersDelimiter);

            String paramName4 = arguments.getRepeatingParam4Name();
            String[] params4 = arguments.getRepeatingParam4Values().split(parametersDelimiter);

            String paramName5 = arguments.getRepeatingParam5Name();
            String[] params5 = arguments.getRepeatingParam5Values().split(parametersDelimiter);

            String paramName6 = arguments.getRepeatingParam6Name();
            String[] params6 = arguments.getRepeatingParam6Values().split(parametersDelimiter);

            activeJobs = new HashSet<>(10);

            oozieClient = new OozieClient(arguments.getOozieUrl());

            System.out.println("Setting properties from " + arguments.getSubWorkflowPath() + "/job.properties");

            // create a workflow job configuration and set the workflow application path

            Properties conf = HandleProcessConfiguration.getPropertiesFile(arguments.getSubWorkflowPath() + "/job.properties");

            // setting workflow parameters
            conf.setProperty(OozieClient.APP_PATH, arguments.getSubWorkflowPath());
            conf.setProperty("parentWfID", arguments.getParentWfId());
            conf.setProperty("user.name", arguments.getUser());

            for (int i = 0; i < params1.length; i++) {
                //submit dim and add to active queue

                conf.setProperty(paramName1, params1[i]);

                if (Strings.isNotEmpty(paramName2))
                    conf.setProperty(paramName2, params2[i]);

                if (Strings.isNotEmpty(paramName3))
                    conf.setProperty(paramName3, params3[i]);

                if (Strings.isNotEmpty(paramName4))
                    conf.setProperty(paramName4, params4[i]);

                if (Strings.isNotEmpty(paramName5))
                    conf.setProperty(paramName5, params5[i]);

                if (Strings.isNotEmpty(paramName6))
                    conf.setProperty(paramName6, params6[i]);

                String wfId = oozieClient.run(conf);
                activeJobs.add(wfId);
                logger.info("Job Submitted : " + wfId);

                logger.info("Has " + activeJobs.size() + " jobs in active queue ");

                // as long as the queue is full of there are no dims to load but queue isn't empty, go to sleep and check if queue is empty
                while (activeJobs.size() == arguments.getBatchSize()) {
                    sleep();
                }
            }

            while (activeJobs.size() > 0) {
                sleep();
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    private void sleep() throws Exception {
        logger.info("Reached limit, going to sleep");
        Thread.sleep(arguments.getSleepSeconds() * 1000);
        logger.info("Going to update list");

        // update queue
        updateSetWithFinishedJobs(oozieClient, activeJobs);
        logger.info("Has " + activeJobs.size() + " jobs in active queue ");
    }

    private void updateSetWithFinishedJobs(OozieClient wc, HashSet<String> activeJobs) throws Exception {
        StringBuilder filterList = new StringBuilder();
        boolean first = true;
        for (String jobId : activeJobs) {
            if (first)
                filterList = new StringBuilder(OozieClient.FILTER_ID + "=" + jobId);
            else
                filterList.append(";").append(OozieClient.FILTER_ID).append("=").append(jobId);
            first = false;
        }

        logger.info("Querying oozie with filter " + filterList);
        List<WorkflowJob> wfList = wc.getJobsInfo(filterList.toString());

        // remove done jobs
        for (WorkflowJob job : wfList) {
            if (job.getStatus() == WorkflowJob.Status.SUCCEEDED)
                logger.info(job.getId() + " is Done.");
            else
                logger.info(job.getId() + " is in " + job.getStatus() + " state, please fix and rerun from oozie scheduler");

            if (job.getStatus() != WorkflowJob.Status.PREP && job.getStatus() != WorkflowJob.Status.RUNNING && job.getStatus() != WorkflowJob.Status.SUSPENDED) {
                logger.info("Removing " + job.getId() + " from active queue");
                activeJobs.remove(job.getId());
            }
        }

    }
}
