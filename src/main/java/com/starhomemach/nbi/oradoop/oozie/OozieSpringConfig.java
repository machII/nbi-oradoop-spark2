package com.starhomemach.nbi.oradoop.oozie;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;


@Configuration
@ComponentScan(basePackages = {"com.starhomemach.nbi.oradoop.oozie"})
public class OozieSpringConfig {

}
