package com.starhomemach.nbi.oradoop.oozie;

import com.starhomemach.nbi.oradoop.conf.arguments.ArgumentsHolder;
import com.starhomemach.nbi.oradoop.conf.arguments.IsRequired;
import lombok.Getter;
import lombok.Setter;

import javax.ws.rs.DefaultValue;

@Getter
@Setter
public class WfLoopSubmitArgumentsHolder extends ArgumentsHolder
{


    @IsRequired
    private String oozieUrl;

    @IsRequired
    private String parentWfId;

    @IsRequired
    private String subWorkflowPath;

    private Integer batchSize =5;

    private Integer sleepSeconds =30;

    private String parametersDelimiter=";";

    @IsRequired
    private String repeatingParam1Name ="";
    @IsRequired
    private String repeatingParam1Values ="";
    @DefaultValue("")
    private String repeatingParam2Name ="";
    @DefaultValue("")
    private String repeatingParam2Values ="";
    @DefaultValue("")
    private String repeatingParam3Name ="";
    @DefaultValue("")
    private String repeatingParam3Values ="";
    @DefaultValue("")
    private String repeatingParam4Name ="";
    @DefaultValue("")
    private String repeatingParam4Values ="";
    @DefaultValue("")
    private String repeatingParam5Name ="";
    @DefaultValue("")
    private String repeatingParam5Values ="";
    @DefaultValue("")
    private String repeatingParam6Name ="";
    @DefaultValue("")
    private String repeatingParam6Values ="";

}