package com.starhomemach.nbi.oradoop.dataretention.date;

import org.joda.time.DateTime;
import org.joda.time.Months;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

/**
 * Created by tpin on 23/02/2017.
 */
@Service
public class DkDatMoService implements DateService {

    final private  static DateTime pInitialDate = new DateTime().withDate(1989,12,01); // doomsday lol;
    final private  static String pInitialDateStr = "1989-12-01"; // doomsday lol;

    @Override
    public long getCurrentValue() {
        Months d = Months.monthsBetween( new DateTime().withDayOfMonth(1), pInitialDate);
        return Math.abs(d.getMonths());
    }

    @Override
    public String getColumnName() {
        return "DK_DAT_MO";
    }

    @Override
    public String getStmtCalaulation() {
        return "cast((cast(year(CRE_DAT) as int) - cast(year('" + pInitialDateStr + "') as int) )*12 +  (cast(month(CRE_DAT) as int) - cast(month('" + pInitialDateStr + "') as int) ) as smallint)";
    }

}
