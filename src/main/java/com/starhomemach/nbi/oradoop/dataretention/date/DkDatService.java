package com.starhomemach.nbi.oradoop.dataretention.date;

import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

/**
 * Created by tpin on 23/02/2017.
 */
@Service
public class DkDatService implements DateService {

    final private  static LocalDate pInitialDate = LocalDate.of(1989,12,31);

    @Override
    public long getCurrentValue() {
        return  new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().toEpochDay() - pInitialDate.toEpochDay();
    }

    @Override
    public String getColumnName() {
        return "DK_DAT";
    }

    @Override
    public String getStmtCalaulation() {
        return null;
    }

}
