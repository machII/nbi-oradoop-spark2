package com.starhomemach.nbi.oradoop.dataretention.retentionhandler;

import com.starhomemach.nbi.oradoop.OradoopConf;
import com.starhomemach.nbi.oradoop.dataretention.RetentionHandler;
import com.starhomemach.nbi.oradoop.dimension.metadata.HcatQuery;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hive.conf.HiveConf;
import org.apache.hadoop.hive.metastore.HiveMetaStoreClient;
import org.apache.hadoop.hive.metastore.api.Partition;
import org.apache.hadoop.hive.metastore.PartitionDropOptions;
import org.apache.hadoop.hive.metastore.api.Table;
import org.joda.time.DateTime;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by pade on 22/11/17.
 */

@Component
public class PartitionBasedModificationTimeHandler extends RetentionHandler {

    private final Log logger = LogFactory.getLog(getClass());

    @Override
    public String getName() {
        return "partitionTimeHandler";
    }

    @Override
    protected int applyRetentionResource(AnnotationConfigApplicationContext context, String user, String resource) throws Exception {
        String[] param = new String[2];
          param[0] = resource.split("\\.")[0];
          param[1] = resource.split("\\.")[1];

        logger.info("Database: " + param[0] + "Table: "  + param[1]);


        int retPeriod = Integer.parseInt(getRetentionPeriod().get(0));

        logger.info("Retention Period : "  + retPeriod);


        HiveConf hConf = new HiveConf(new OradoopConf().getConf(),new OradoopConf().getConf().getClass());
        HiveMetaStoreClient client = new HiveMetaStoreClient(hConf);

        //MAX OF 32767 partitions will be processed at a time

        DateTime now = new DateTime();
        long millisNowMinusDays = now.minusDays(retPeriod).getMillis();

        logger.info(  "Processing Baseline timestamp Long value in MilliSecond "+ millisNowMinusDays);

        List <Partition> pr =client.listPartitions(param[0],param[1], (short) 32767);

        PartitionDropOptions prtops = new PartitionDropOptions().purgeData(true).ifExists(true);

        for (Partition prt : pr)
        {
            logger.info("processing " + prt.getSd().getLocation());
           // List<String> prv=prt.getValues();
          //client.dropPartition("default","validation_erros",prv);
            Map<String,String> prtparam = prt.getParameters();


               String value =(String) prtparam.get("transient_lastDdlTime");

                   logger.info(  "Last_Transient_ddl_time for above file "+ value);


                   if (millisNowMinusDays > Long.parseLong(value) * 1000 )
                   {   List<String> prv=prt.getValues();

                       logger.info(  "Dropping Partition related to Path "+ prt.getSd().getLocation());
                       client.dropPartition(param[0],param[1],prv,prtops);


                   }



        }


        return 0;
    }

}
