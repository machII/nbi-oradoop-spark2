package com.starhomemach.nbi.oradoop.dataretention.retentionhandler;

import com.starhomemach.nbi.oradoop.dataretention.RetentionHandler;
import com.starhomemach.nbi.oradoop.dataretention.date.DateServiceFactory;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * Created by armb on 29-09-2016.
 */
@Component
public class HiveDkDatPartitionHandler extends RetentionHandler {

    @Autowired
    private DateServiceFactory factory;

    private String HIVE_DRIVER = "org.apache.hive.jdbc.HiveDriver";
    private String dkColName = null;

    @Override
    public String getName() {
        return "dkDatHandler";
    }

    private String buildAlterStmt(String tableName) {
        String columns = getAllProps().getProperty("diffrentPolicyPerPartitionColumn", null);
        logger.info("policy retention partition columns: " + columns);

        String anyInnerPartitionColumn = getAllProps().getProperty("anyInnerPartitionColumn", null);
        logger.info("anyInnerPartitionColumn: " + anyInnerPartitionColumn);

        String alterSql = "ALTER TABLE " + tableName + " drop if exists partition(";
        logger.info("Building alter stmt: " + alterSql);

        String[] columnsList = columns.split(",");
        for (int i = 0; i < columnsList.length - 1; i++) {
            logger.info("Adding column: " + columnsList[i]);
            alterSql = alterSql + columnsList[i] + "=?,";
        }

        logger.info("Adding column: " + columnsList[columnsList.length - 1]);
        alterSql = alterSql + columnsList[columnsList.length - 1] + "<? ";

        if (anyInnerPartitionColumn != null)
            alterSql = alterSql + "," + anyInnerPartitionColumn + " > 0";

        alterSql = alterSql + ") PURGE";
        dkColName = columnsList[columnsList.length - 1].toUpperCase();
        logger.info("DK DAT COLUMN: " + dkColName);

        logger.info("alterSql= " + alterSql);
        return alterSql;
    }

    private void setPreparedStatmentsVars(PreparedStatement stmt, String retentionPolicy, long currentValue) throws SQLException {
        String[] columnVals = retentionPolicy.split(",");

        for (int i = 0; i < columnVals.length - 1; i++) {
            logger.info("set var: " + Integer.toString(i + 1) + " value: " + columnVals[i]);
            stmt.setInt(i + 1, Integer.parseInt(columnVals[i]));
        }

        logger.info("retention value: " + columnVals[columnVals.length - 1]);
        logger.info("set var: " + columnVals.length + " value: " + Long.toString(currentValue - Integer.parseInt(columnVals[columnVals.length - 1])));
        stmt.setLong(columnVals.length, currentValue - Integer.parseInt(columnVals[columnVals.length - 1]));
    }

    @Override
    protected int applyRetentionResource(AnnotationConfigApplicationContext context, String user, String resource) throws Exception {
        String hiveJDBCUrl = getAllProps().getProperty("hiveJDBCUrl");
        String isDryRun = getAllProps().getProperty("dryRun", "false");
        logger.info("Going to connect to: " + hiveJDBCUrl);

        Class.forName(HIVE_DRIVER);
        Connection hiveConn = null;
        PreparedStatement stmt = null;

        try {
            hiveConn = DriverManager.getConnection(hiveJDBCUrl, user, user);
            String alterSql = buildAlterStmt(resource);
            long todayDkDat = factory.getDateService(dkColName).getCurrentValue();
            logger.info("Current date value: " + todayDkDat);
            logger.info("Creating stmt for: " + alterSql);
            stmt = hiveConn.prepareStatement(alterSql);
            for (String retentionPolicy : getRetentionPeriod()) {
                logger.info("Retention Policy is: " + retentionPolicy);
                setPreparedStatmentsVars(stmt, retentionPolicy, todayDkDat);
                logger.info("Executing Statment");

                if (!Boolean.parseBoolean(isDryRun)) {
                    logger.info("This isnt a dry run - executing");

                    //trying to print final sql to log:
                    try
                    {
                        String finalSql = (String)FieldUtils.readField(stmt, "sql", true);
                        Map<Integer,String> parameters= (Map<Integer,String>)FieldUtils.readField(stmt, "parameters", true);

                        for(Integer param : parameters.keySet())
                        {
                            finalSql = finalSql.replaceFirst(Pattern.quote("?"),parameters.get(param));
                        }

                        logger.info("Executing: " + finalSql);
                    }
                    catch(Exception e)
                    {
                    }

                    stmt.execute();
                }
            }
        } catch (SQLException e) {
            throw e;
        } finally {
            if (hiveConn != null)
                hiveConn.close();
        }
        return 0;
    }

}
