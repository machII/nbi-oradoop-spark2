package com.starhomemach.nbi.oradoop.dataretention;

import com.starhomemach.nbi.oradoop.OradoopConf;
import com.starhomemach.nbi.oradoop.export.SpringConfig;
import org.apache.commons.beanutils.converters.IntegerArrayConverter;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: armb
 * Date: 20/7/16
 * Time: 2:57 PM
 * To change this template use File | Settings | File Templates.
 */

@Component
public class ProcessDataRetention {

    private final Log logger = LogFactory.getLog(getClass());

    @Autowired
    RetentionHandlerFactory retentionHandlerFactory;

    public void dataRetention(AnnotationConfigApplicationContext context,String user,String jobId,String resource,String retentionType, String retentionPropertyFile) throws Exception {
        RetentionHandler retentionHandler = retentionHandlerFactory.getRetentionHandler(retentionType);
        logger.info("Found handler " + retentionHandler + " for handler type requested: " + retentionType);
        retentionHandler.applyRetentionResource(context,user,resource,retentionPropertyFile,jobId);
    }


    public void run(String...args) throws Exception{
        logger.info("########################################################## START DATA RETENTION #############################");

        if (args.length < 5) {
            System.out.println("ERROR args should contain the following parameters 1)USER 2)WORKFLOW-ID 3)Resource For Retention(Path/db_name.table name) 4)RETENTION_TYPE 5)Retention policy file");
            return;
        }

        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SpringConfig.class);

        String user = args[0];
        String jobId = args[1];
        String resource = args[2];
        String retentionType = args[3];
        String retentionPropertyFile = args[4];
        dataRetention(context,user,jobId,resource,retentionType, retentionPropertyFile);



        //logger.info("User is: "+user+" jobId is "+jobId+" path is "+path+" Retention type is "+retentionType);

        logger.info("########################################################## END DATA RETENTION ###############################");
    }
}
