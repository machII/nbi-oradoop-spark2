package com.starhomemach.nbi.oradoop.dataretention;

import org.springframework.stereotype.Service;

/**
 * Created by tpin on 25/12/2017.
 */
@Service
public class CustomerPartitionKey implements PartitionKey {
    @Override
    public String getColumnName() {
        return "CL_ID";
    }

    @Override
    public String getStmtCalaulation() {
        return "CL_ID";
    }

}
