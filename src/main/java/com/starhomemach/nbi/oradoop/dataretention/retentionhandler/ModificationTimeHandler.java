package com.starhomemach.nbi.oradoop.dataretention.retentionhandler;

/**
 * Created by armb on 29-09-2016.
 */
import com.starhomemach.nbi.oradoop.OradoopConf;
import com.starhomemach.nbi.oradoop.dataretention.RetentionHandler;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.joda.time.DateTime;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

@Component
public  class ModificationTimeHandler extends RetentionHandler {
    private final Log logger = LogFactory.getLog(getClass());

    @Override
    public String getName() {
        return "modifyTimeHandler";
    }

    @Override
    protected int applyRetentionResource(AnnotationConfigApplicationContext context, String user, String resource) throws Exception {
        Path folder = new Path(resource);
        FileSystem fileSystem = FileSystem.newInstance(new OradoopConf().getConf());
        int retPeriod = Integer.parseInt(getRetentionPeriod().get(0));
        logger.info("Retention period is: " + retPeriod);
        List<String> files = getAllDirectoriesPath(folder,fileSystem,new Integer(retPeriod));
        checkAndDeleteFiles(files,fileSystem);
        return 0;
    }



    private List<String> getAllDirectoriesPath(Path genericDirPath, FileSystem fs, Integer genericRetentionPeriod) throws IOException
    {
        String fullPath;
        Integer count = 0;
        List<String> fileList = new ArrayList();
        FileStatus[] fileStatus = fs.listStatus(genericDirPath);
        for (FileStatus fileStat : fileStatus) {
            fullPath = fileStat.getPath().toString();
            if (isTimeIsLessThanDays(fileStat.getModificationTime(), genericRetentionPeriod)) {
                fileList.add(fullPath);
                count++;
            }
        }
        logger.info("Total number of files added from the path "+genericDirPath+" are "+count);
        return fileList;
    }

    public boolean isTimeIsLessThanDays(long fileModificationTime, Integer GenericRetentionPeriod) {
        DateTime now = new DateTime();
        long millisNowMinusDays = now.minusDays(GenericRetentionPeriod).getMillis();
        if (fileModificationTime < millisNowMinusDays) {
            return true;
        } else
            return false;
    }

    public void checkAndDeleteFiles(List<String> filesList,FileSystem fs) throws IOException {
        Boolean delete_non_empty_dir=true;
        Integer delCount = 0;

        if(filesList != null)
        {
            for(String flList : filesList)
            {
                Path DirListPath = new Path(flList);
                if((fs.exists(DirListPath)) && !(DirListPath.toString().contains(".metadata"))) {
                    fs.delete(DirListPath, delete_non_empty_dir);
                    delCount++;
                }
            }
        }


    }
 }
