package com.starhomemach.nbi.oradoop.dataretention.retentionhandler;

import com.starhomemach.nbi.oradoop.dataretention.RetentionHandler;
import com.starhomemach.nbi.oradoop.dataretention.date.DateServiceFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by armb on 29-09-2016.
 */
@Component
public class WfPartitionHandler extends RetentionHandler {

    private class Workflow
    {
        int tblGrp;
        String wfId;

        public Workflow(String pWfId, int pTblGrp)
        {
            tblGrp=pTblGrp;
            wfId=pWfId;
        }

        public String getWfId()
        {
            return wfId;
        }

        public int getTblGrp()
        {
            return tblGrp;
        }
    }

    private String HIVE_DRIVER="org.apache.hive.jdbc.HiveDriver";
    private String dkColName = null;

    @Override
    public String getName() {
        return "WfHandler";
    }

    @Override
    protected int applyRetentionResource(AnnotationConfigApplicationContext context, String user, String resource) throws Exception {
        String hiveJDBCUrl = getAllProps().getProperty("hiveJDBCUrl");
        String isDryRun = getAllProps().getProperty("dryRun","false");
        List<Workflow> wfList = new ArrayList<Workflow>();
        logger.info("Going to connect to: " + hiveJDBCUrl);

        Class.forName(HIVE_DRIVER);
        Connection hiveConn = null;

        String serviceTypeName = getAllProps().getProperty("serviceTypeName");
        String serviceTypeId = getAllProps().getProperty("serviceTypeid");

        try {
            hiveConn = DriverManager.getConnection(hiveJDBCUrl,user,user);
            String alterSql = "ALTER TABLE " + resource + " drop if exists partition(tbl_grp_id=?,wf_id=?) purge";
            String setStatusSql = "INSERT INTO METADATA_DB.T_WF_STATE (wf_id, tbl_grp_id, step_wf_id, step_wf_type_id, status, cre_dat, source_type_id)\n" +
                    " select ?,?,?,7,2,current_timestamp,"+serviceTypeId;
            String stepColumnsDoneStmt="SELECT WF_ID,TBL_GRP_ID FROM METADATA_DB.V_WF_INTERNAL WHERE " + getAllProps().getProperty("ColumnsToCheck").
                                        replace(",","='Done' and ").concat("='Done' and Retention_Status='Waiting' and Source_Type_Name='"+serviceTypeName+"' and staging_area_load_timestamp < date_add(current_date, -" + getRetentionPeriod().get(0) + ")");
            logger.info("Creating stmt for: " + alterSql);
            logger.info("Creating stmt for: " + setStatusSql);
            logger.info("Creating stmt for: " + stepColumnsDoneStmt);

            PreparedStatement selectSql = hiveConn.prepareStatement(stepColumnsDoneStmt);
            ResultSet wfToDR = selectSql.executeQuery();
            while(wfToDR.next()) {
                wfList.add(new Workflow(wfToDR.getString(1), wfToDR.getInt(2)));
                logger.info("added wf " + wfToDR.getString(1) + " with tbl grp " +  wfToDR.getInt(2));
            }
            wfToDR.close();
            selectSql.close();

            for (Workflow wf:wfList)
            {
                int tblGrpId = wf.getTblGrp();
                String wfId = wf.getWfId();
                logger.info("WF ID to handle: " + wfId);
                logger.info("Table Group to handle: " + tblGrpId);
                logger.info("JobId to handle: " + getAllProps().getProperty("JobId"));

                if (!Boolean.valueOf(isDryRun)) {
                    logger.info("This isnt a dry run - executing drop");
                    PreparedStatement stmt = hiveConn.prepareStatement(alterSql);
                    stmt.setInt(1,tblGrpId);
                    stmt.setString(2,wfId);
                    stmt.execute();
                    stmt.close();

                    logger.info("This isnt a dry run - executing status update");
                    PreparedStatement updteStmt = hiveConn.prepareStatement(setStatusSql);
                    updteStmt.setString(1,wfId);
                    updteStmt.setInt(2,tblGrpId);
                    updteStmt.setString(3,getAllProps().getProperty("JobId"));
                    updteStmt.execute();
                    updteStmt.close();
                }
            }
        }
        catch (SQLException e) {
            throw e;
        }
        finally {
            if (hiveConn != null)
                hiveConn.close();
        }
        return 0;
    }
}
