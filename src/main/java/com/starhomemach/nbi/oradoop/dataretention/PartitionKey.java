package com.starhomemach.nbi.oradoop.dataretention;

/**
 * Created by tpin on 25/12/2017.
 */
public interface PartitionKey {
    public String getColumnName();
    public String getStmtCalaulation();
}
