package com.starhomemach.nbi.oradoop.dataretention;


import com.starhomemach.nbi.oradoop.export.SpringConfig;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Created by IntelliJ IDEA.
 * User: armb
 * Date: 20/7/16
 * Time: 1:32 PM
 * To change this template use File | Settings | File Templates.
 */
public class GenericDataRetention {
    public static void main(String args[]) throws Exception
    {
        System.out.println("########################################################## START DATA RETENTION #############################");
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SpringConfig.class);
        context.getBean(ProcessDataRetention.class).run(args);
        System.out.println("########################################################## END  DATA RETENTION #############################");
    }
}
