package com.starhomemach.nbi.oradoop.dataretention.date;

import com.starhomemach.nbi.oradoop.dataretention.RetentionHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

/**
 * Created by tpin on 23/02/2017.
 */
@Service
public class DateServiceFactory {

    @Autowired
    private List<DateService> listOfHandlers;

    static private HashMap<String, DateService> handlerHashMap;

    @PostConstruct
    public void init() {
        handlerHashMap = new HashMap<String, DateService>();
        for (DateService dateService : listOfHandlers) {
            handlerHashMap.put(dateService.getColumnName(), dateService);
        }
    }

    public DateService getDateService(String colName) throws IOException {
        return handlerHashMap.get(colName);
    }
}