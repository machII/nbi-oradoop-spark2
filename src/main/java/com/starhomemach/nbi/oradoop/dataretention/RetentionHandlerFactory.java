package com.starhomemach.nbi.oradoop.dataretention;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

/**
 * Created by armb on 03-10-2016.
 */
@Service
public class RetentionHandlerFactory {

    private final Log logger = LogFactory.getLog(getClass());

    @Autowired
    private List<RetentionHandler> listOfHandlers;

    static private HashMap<String,RetentionHandler> handlerHashMap;

    @PostConstruct
    public void init()
    {
        handlerHashMap = new HashMap<String,RetentionHandler>();
       for(RetentionHandler retentionHandler: listOfHandlers)
       {
           handlerHashMap.put(retentionHandler.getName(),retentionHandler);
       }
    }

     public RetentionHandler getRetentionHandler(String retHandler) throws IOException {
        try {
             return handlerHashMap.get(retHandler);
        }catch(Exception e){
            logger.error("Failed to fetch the retention handler:" + retHandler + ". The exception is:" + e.getMessage() + " .going to throw exception");
            throw e;
        }


    }


}
