package com.starhomemach.nbi.oradoop.dataretention;

import com.starhomemach.nbi.oradoop.OradoopConf;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Set;


/**
 * Created by armb on 29-09-2016.
 */

public abstract class RetentionHandler {
    protected final Log logger = LogFactory.getLog(getClass());
    private final String RETENTION_PROP_PREFIX = "retention_policy";
    private Properties allProps;
    private List<String> retentionPeriod = new ArrayList<String>();
    private void readRetentionProperty(String retentionPropertyFile,String jobId) throws IOException
    {
        allProps = new Properties();
        Path path = new Path(retentionPropertyFile);
        FileSystem fileSystem = path.getFileSystem(new OradoopConf().getConf());
        FSDataInputStream schema = fileSystem.open(path);
        allProps.load(schema);
        allProps.put("JobId",jobId);

        Set<String> propNames = allProps.stringPropertyNames();

        // find all the retention props
        for (String name:propNames) {
            if (name.startsWith(RETENTION_PROP_PREFIX))
                retentionPeriod.add((String)(allProps.get(name)));
        }
    }

    abstract public String getName();

    public int applyRetentionResource(AnnotationConfigApplicationContext context, String user, String resource, String retentionPropertyFile, String jobId) throws Exception
    {
        readRetentionProperty(retentionPropertyFile, jobId);
        return applyRetentionResource(context, user, resource);
    }

    abstract protected int applyRetentionResource(AnnotationConfigApplicationContext context, String user, String resource) throws Exception;

    protected Properties getAllProps() {
        return allProps;
    }

    protected List<String> getRetentionPeriod() {
        return retentionPeriod;
    }
}
