package com.starhomemach.nbi.oradoop.dataretention;

import com.starhomemach.nbi.oradoop.dataretention.date.DateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

/**
 * Created by tpin on 25/12/2017.
 */
@Service
public class PartitionKeyFactory {

    @Autowired
    private List<PartitionKey> listOfHandlers;

    static private HashMap<String, PartitionKey> handlerHashMap;

    @PostConstruct
    public void init() {
        handlerHashMap = new HashMap<String, PartitionKey>();
        for (PartitionKey partitionKey : listOfHandlers) {
            handlerHashMap.put(partitionKey.getColumnName(), partitionKey);
        }
    }

    public PartitionKey getPartitionKey(String colName) throws IOException {
        return handlerHashMap.get(colName);
    }
}
