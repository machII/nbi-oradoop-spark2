package com.starhomemach.nbi.oradoop.dataretention.date;

import com.starhomemach.nbi.oradoop.dataretention.PartitionKey;

/**
 * Created by tpin on 23/02/2017.
 */
public interface DateService extends PartitionKey {
    public long getCurrentValue();
}
