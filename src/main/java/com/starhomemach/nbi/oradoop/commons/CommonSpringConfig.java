package com.starhomemach.nbi.oradoop.commons;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDateTime;


@Configuration
@ComponentScan(basePackages = {"com.starhomemach.nbi.oradoop.commons"})
public class CommonSpringConfig {
    @Bean
    public LocalDateTime etlStartTime() {
        return LocalDateTime.now();
    }

}
