package com.starhomemach.nbi.oradoop.eureg;

import com.starhomemach.nbi.oradoop.eureg.dto.T_ferg_ad;
import org.apache.avro.file.DataFileWriter;
import org.apache.avro.io.DatumWriter;
import org.apache.avro.specific.SpecificDatumWriter;

import java.io.File;
import java.io.IOException;

/**
 * Created by pazit shefet on 17/05/2017.
 */
public class CreateEuregActionsFile
{
    public static void main(String... args)
    {
        T_ferg_ad det1 = T_ferg_ad.newBuilder()
                .setDDIMSI("12345")
                .setDDMSISDN("23456")
                .setTMST("2017-05-17 10:10:10.111-1111")
                .setCLID(18L)
                .setCLGRPID(18)
                .setACTN("1,2")
                .setDKDAT(9999L)
                .setSPRVID(98L)
                .build();

        T_ferg_ad det2 = T_ferg_ad.newBuilder()
                .setDDIMSI("4412345")
                .setDDMSISDN("4423456")
                .setTMST("2017-05-18 10:10:10.111-1111")
                .setCLID(18L)
                .setCLGRPID(18)
                .setDKDAT(10000L)
                .setACTN("1,2")
                .setSPRVID(98L)
                .build();

        T_ferg_ad det3 = T_ferg_ad.newBuilder()
                .setDDIMSI("44aaaaa")
                .setDDMSISDN("44bbbbb")
                .setTMST("2017-05-19T10:10:10.111-1111")
                .setCLID(18L)
                .setCLGRPID(18)
                .setDKDAT(10001L)
                .setACTN("1,2")
                .setSPRVID(98L)
                .build();

        // Serialize det1-det3 to disk
        DatumWriter<T_ferg_ad> userDatumWriter = new SpecificDatumWriter<>(T_ferg_ad.class);
        DataFileWriter<T_ferg_ad> dataFileWriter = new DataFileWriter<>(userDatumWriter);
        try {
            dataFileWriter.create(det1.getSchema(), new File("d:\\BI\\nbi-oradoop\\o2uk_actions.avro"));
            dataFileWriter.append(det1);
            dataFileWriter.append(det2);
            dataFileWriter.append(det3);
            dataFileWriter.close();
        }
        catch (IOException e) {}
    }
}
