package com.starhomemach.nbi.oradoop.eureg;

import com.starhomemach.nbi.oradoop.eureg.dto.T_ferg_de;
import org.apache.avro.file.DataFileWriter;
import org.apache.avro.io.DatumWriter;
import org.apache.avro.specific.SpecificDatumWriter;

import java.io.File;
import java.io.IOException;

/**
 * Created by pazit shefet on 17/05/2017.
 */
public class CreateEuregDetectionFile
{
    public static void main(String... args)
    {
        T_ferg_de det1 = T_ferg_de.newBuilder()
                .setDDIMSI("12345")
                .setDDMSISDN("23456")
                .setTMST("2017-06-01T10:10")
                .setCLID(18L)
                .setCLGRPID(18)
                .setDKDATMO(330L)
                .setACTN("1,2")
                .setACTNTMST("2017-06-01T04:04")
                .setACTLBRCDDAT("2017-06-01T00:00")
                .setTHRTPID(51L)
                .setTIMRSLN("D")
                .setCFGTHR(50.5)
                .setDETHRCROSSEDBY(60.10)
                .setRUNM("rule name 1")
                .setSPID(null)
                .setWHITELSTIND("0")
                .build();

        T_ferg_de det2 = T_ferg_de.newBuilder()
                .setDDIMSI("12345")
                .setDDMSISDN("23456")
                .setTMST("2017-06-01T10:10")
                .setCLID(18L)
                .setCLGRPID(18)
                .setDKDATMO(330L)
                //.setACTN("1,2")
                //.setACTNTMST("2017-05-02 10:10:10.111-1111")
                .setACTLBRCDDAT("2017-06-01T00:00")
                .setTHRTPID(52L)
                .setTIMRSLN("D")
                .setCFGTHR(40.0)
                .setDETHRCROSSEDBY(400.20)
                .setRUNM("rule name 2")
                .setSPID(null)
                .setWHITELSTIND("1")
                .build();

        T_ferg_de det3 = T_ferg_de.newBuilder()
                .setDDIMSI("23456")
                .setDDMSISDN("34567")
                .setTMST("2017-06-01T10:10")
                .setCLID(18L)
                .setCLGRPID(18)
                .setDKDATMO(330L)
                //.setACTN("1,2")
                //.setACTNTMST("2017-05-03 10:10:10.111-1111")
                .setACTLBRCDDAT("2017-06-01T00:00")
                .setTHRTPID(53L)
                .setTIMRSLN("M")
                .setCFGTHR(30.0)
                .setDETHRCROSSEDBY(300.55)
                .setRUNM("rule name 3")
                .setSPID("303")
                .setWHITELSTIND("0")
                .build();

        // Serialize det1-det3 to disk
        DatumWriter<T_ferg_de> userDatumWriter = new SpecificDatumWriter<>(T_ferg_de.class);
        DataFileWriter<T_ferg_de> dataFileWriter = new DataFileWriter<>(userDatumWriter);
        try {
            dataFileWriter.create(det1.getSchema(), new File("d:\\BI\\nbi-oradoop\\o2uk_detections.avro"));
            dataFileWriter.append(det1);
            dataFileWriter.append(det2);
            dataFileWriter.append(det3);
            dataFileWriter.close();
        }
        catch (IOException e) {}
    }
}
