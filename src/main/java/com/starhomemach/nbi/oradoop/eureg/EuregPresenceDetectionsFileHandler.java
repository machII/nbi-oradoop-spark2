package com.starhomemach.nbi.oradoop.eureg;

/**
 * Created by pazit shefet on 15-05-2017.
 */

import com.google.common.collect.Lists;
import com.starhomemach.nbi.oradoop.eureg.dto.T_ferg_de;
import com.starhomemach.nbi.oradoop.export.file.AvroFsInput;
import com.starhomemach.nbi.oradoop.export.file.dto.AvroFileRecord;
import com.starhomemach.nbi.oradoop.export.file.filehandler.FileHandlerUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;


@Component
public class EuregPresenceDetectionsFileHandler extends EuregFileHandler {
    private final Log logger = LogFactory.getLog(getClass());
    public final int BATCH_SIZE=10000;

    @Autowired
    private FileHandlerUtil fileHandlerUtil;

    private final String INSERT_BI_T_FERG_DE_TMP = "insert into BI.HDP_T_FERG_DE_TMP " +
            "(DD_IMSI, DD_MSISDN, TMST, CL_ID, CL_GRP_ID, DK_DAT_MO, THR_TP_ID, TIM_RSLN, CFG_THR, DE_THR_CROSSED_BY, RU_NM, ACTN, ACTN_TMST, ACTL_BRCD_DAT, WHITE_LST_IND, SPID)" +
            " values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

    private final String MERGE_BI_T_FERG_DE =
            "MERGE /*+ INDEX_RS_ASC(de X_FERG_DE_NDD_IMSI) */INTO BI.T_FERG_DE de " +
                    "  using (select * from BI.HDP_T_FERG_DE_TMP) temp " +
                    "     ON ( de.DD_IMSI = temp.DD_IMSI AND " +
                    "          de.CL_ID = temp.CL_ID AND " +
                    "          de.DK_DAT_MO = temp.DK_DAT_MO AND " +
                    "          de.THR_TP_ID = temp.THR_TP_ID AND " +
                    "          de.TIM_RSLN = temp.TIM_RSLN AND " +
                    "        ( de.ACTN = temp.ACTN OR " +
                    "          temp.ACTN is null) AND " +
                    "          de.ACTL_BRCD_DAT = temp.ACTL_BRCD_DAT )  " +
                    "  WHEN MATCHED THEN " +
                    "    UPDATE SET de.CFG_THR = temp.CFG_THR, " +
                    "               de.DE_THR_CROSSED_BY = temp.DE_THR_CROSSED_BY, "+
                    "               de.RU_NM = temp.RU_NM , "+
                    "               de.TMST = temp.TMST "+
                    "  WHEN NOT MATCHED THEN " +
                    "    INSERT (DD_IMSI, DD_MSISDN, TMST, CL_ID, CL_GRP_ID, DK_DAT_MO, THR_TP_ID, TIM_RSLN, CFG_THR, DE_THR_CROSSED_BY, RU_NM, ACTN, ACTN_TMST, ACTL_BRCD_DAT, WHITE_LST_IND, SPID) " +
                    "    VALUES (temp.DD_IMSI, temp.DD_MSISDN, temp.TMST, temp.CL_ID, temp.CL_GRP_ID, temp.DK_DAT_MO, temp.THR_TP_ID, temp.TIM_RSLN, temp.CFG_THR, temp.DE_THR_CROSSED_BY, temp.RU_NM, temp.ACTN, temp.ACTN_TMST, temp.ACTL_BRCD_DAT, temp.WHITE_LST_IND, temp.SPID)";

    @Override
    public int getBatchSize()
    {
        return BATCH_SIZE;
    }

    @Override
    public String[] getInsertQuery() {
        return new String[]{INSERT_BI_T_FERG_DE_TMP};
    }

    @Override
    // merge is NOT required!!
    public String[] getMergeQuery() {
        return new String[]{MERGE_BI_T_FERG_DE};
    }

    @Override
    public List<AvroFileRecord> readFileToList(String avroFilePath) throws IOException {
        List<AvroFileRecord> avroRecords = Lists.newArrayList();
        Path path = new Path(avroFilePath);
        Configuration config = new Configuration();
        org.apache.avro.file.SeekableInput input = new AvroFsInput(path, config);
        org.apache.avro.io.DatumReader<T_ferg_de> tDetectDatumReader = new org.apache.avro.specific.SpecificDatumReader<>(T_ferg_de.class);
        org.apache.avro.file.FileReader<T_ferg_de> t_detectRecords = org.apache.avro.file.DataFileReader.openReader(input, tDetectDatumReader);
        while (t_detectRecords.hasNext()) {
            T_ferg_de record = t_detectRecords.next();
            avroRecords.add(record);
        }

        t_detectRecords.close();
        logger.info("succeeded to process file:" + avroFilePath + ". number of records:" + avroRecords.size());

        return avroRecords;
    }

    @Override
    public boolean isMergeOperation() {
        return true;
    }

    @Override
    public boolean isSequenceOperation() {
        return false;
    }

    @Override
    public String getHandlerKey() {
        return "T_ferg_de_presence";
    }


    public void setPreparedStatement(AvroFileRecord avroFileRecords, Long sequenceId, PreparedStatement preparedStatement) throws SQLException, ParseException {
        logger.info("going to set PreparedStatement for record:" + avroFileRecords);
        T_ferg_de tRecord = (T_ferg_de) avroFileRecords;

        // for insert
        preparedStatement.setString(1, tRecord.getDDIMSI().toString());
        preparedStatement.setString(2, tRecord.getDDMSISDN() == null ? " " : tRecord.getDDMSISDN().toString());
        preparedStatement.setTimestamp(3, fileHandlerUtil.getSqlTSWithT(tRecord.getTMST().toString()));
        preparedStatement.setLong(4, tRecord.getCLID());
        preparedStatement.setLong(5, tRecord.getCLGRPID());
        preparedStatement.setLong(6, tRecord.getDKDATMO() == null ? 329 : tRecord.getDKDATMO());
        preparedStatement.setLong(7, tRecord.getTHRTPID());
        preparedStatement.setString(8, tRecord.getTIMRSLN() == null ? null : String.valueOf(tRecord.getTIMRSLN().charAt(0)));
        preparedStatement.setDouble(9, tRecord.getCFGTHR());
        preparedStatement.setDouble(10, tRecord.getDETHRCROSSEDBY());
        preparedStatement.setString(11, tRecord.getRUNM().toString());
        preparedStatement.setString(12, tRecord.getACTN() == null ? null : tRecord.getACTN().toString());
        preparedStatement.setTimestamp(13, tRecord.getACTNTMST() == null ? null : fileHandlerUtil.getSqlTSWithT(tRecord.getACTNTMST().toString()));
        preparedStatement.setDate(14, tRecord.getACTLBRCDDAT() == null ? null : fileHandlerUtil.getSqlDateWithT(tRecord.getACTLBRCDDAT().toString()));
        preparedStatement.setString(15, tRecord.getWHITELSTIND() == null ? "0" : tRecord.getWHITELSTIND().toString());
        preparedStatement.setString(16, tRecord.getSPID() == null ? null : tRecord.getSPID().toString());
        preparedStatement.addBatch();
    }
}
