package com.starhomemach.nbi.oradoop.eureg.abs_delta;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.starhomemach.nbi.oradoop.OradoopConf;
import com.starhomemach.nbi.oradoop.export.util.HandleProcessConfiguration;
import com.starhomemach.nbi.oradoop.util.HbaseClient;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.*;

@Service
public class LoadAbsDeltaService
{
    @Autowired
    public OradoopConf oradoopConf;

    @Autowired
    HbaseClient hbaseClient;

    private final Log logger = LogFactory.getLog(getClass());

    private static final String PROCESSING = "/etl/abs-delta-load/processing";

    private void processHdfsDirectory(String userName, String jobId, String path, boolean isDebug) throws IOException, SQLException, ParseException {
        String inputUrl = path + userName + PROCESSING + "/" + jobId + "/";
        if (isDebug)
            System.out.println("going to process files from directory:" + inputUrl);
        logger.info("going to process directory:" + inputUrl);
        Path inputPath = new Path(inputUrl);
        FileSystem fs = inputPath.getFileSystem(oradoopConf.getConf());
        FileStatus[] filesList = fs.listStatus(inputPath);
        List<String> userRecords = Lists.newArrayList();
        try {
            for (FileStatus file : filesList) {
                processFile(fs, file, userRecords);
            }
            logger.info("Finished to process files. Number of record to process is:" + userRecords.size());
            if (isDebug)
                System.out.println("Number of record to process is:" + userRecords.size());
            processUserRecords(userRecords, userName, isDebug);
        } catch (Exception e) {
            logger.error("Failed to process url: " + inputUrl + ". The exception is: " + e.getMessage() + ".going to throw exception");
            throw e;
        }
    }


    /**
     * get all records from CSV file
     * @param fs
     * @param file
     * @param userRecords
     * @throws IOException
     */
    private void processFile(FileSystem fs, FileStatus file, List<String> userRecords) throws IOException
    {
        FSDataInputStream dataStream = fs.open(file.getPath());
        BufferedReader bf = new BufferedReader(new InputStreamReader(dataStream));
        String line;
        while ((line = bf.readLine()) != null )
        {
            userRecords.add(line);
        }
    }

    /**
     * process user record into HBASE
     * @param userRecords
     */
    private void processUserRecords(List<String> userRecords, String userName, boolean isDebug)
    {
        String tableName = userName+"_users";
        Table table;

        // connect to HBASE
        try {
            if (!hbaseClient.connectToHbase())
            {
                logger.error ("Failed to open users table: "+tableName);
                return;
            }
//            Properties zookeeperProperties = HandleProcessConfiguration.getPropertiesFile("/starhome/common/conf/zookeeper.properties");
//            String zookeeperHost = zookeeperProperties.getProperty("zookeeper.host");
//            org.apache.hadoop.conf.Configuration hBaseConfig = HBaseConfiguration.create();
//            String[] adrs = zookeeperHost.split(":");
//            hBaseConfig.set("hbase.zookeeper.quorum",adrs[0]);
//            hBaseConfig.set("hbase.zookeeper.property.clientPort", adrs[1]);
//            Connection connection = ConnectionFactory.createConnection(hBaseConfig);
            table = hbaseClient.getTable(tableName);
            //table = connection.getTable(TableName.valueOf(tableName));
        }
        catch (RuntimeException e)
        {
            logger.error ("Failed to open users table: "+tableName);
            return;
        }

        int nInserts = 0;
        int nUpdates = 0;
        int nDeletes = 0;

        // process all records
        for (String record : userRecords)
        {
            Map<String, String> values = splitRecord(record);
            if (values == null)
                continue;

            // get current record from HBASE
            Date updDate = convertStrToDate(values.get("UPDATE_DATE"));
            boolean isNew = false;
            boolean isDeleted = false;
            boolean wasFound = true;
            byte[] bRecDate = null;
            try
            {
                Get curRecord = new Get (Bytes.toBytes(values.get("MSISDN")));
                Result res = table.get(curRecord);
                if (res.isEmpty())
                    wasFound = false;
                else {
                    bRecDate = res.getValue(Bytes.toBytes("data"), Bytes.toBytes("upd_date"));
                    Date recDate = convertStrToDate(new String(bRecDate));
                    if (recDate.before(updDate))
                        isNew = true;
                    byte[] recDel = res.getValue(Bytes.toBytes("data"), Bytes.toBytes("is_deleted"));
                    isDeleted = (recDel[0] == '1');
                }
            }
            catch (IOException e)
            {
                logger.error("Failed to read from hbase. "+e);
                if (isDebug)
                    System.out.println("Failed to read from hbase. "+e);
                continue;
            }

            // decide which action to perform
            boolean doInsert = false;
            boolean doDelete = false;
            if (values.get("ACTION").equalsIgnoreCase("I")) {
                if (!wasFound) {
                    doInsert = true;
                } else {
                    if (!isDeleted) {
                        logger.error("Need to insert record: " + record + " but record already found in o2uk_users hbase table");
                        if (isDebug)
                            System.out.println("Need to insert msisdn: " + values.get("MSISDN") + " but record already found in o2uk_users hbase table");
                    }
                    if (isNew)
                        doInsert = true;
                    else {
                        logger.info("record: " + record + " is too old. will not be updated to users table");
                        if (isDebug)
                            System.out.println("record: " + record + " is too old. recDate="+new String(bRecDate)+". will not be updated to users table");
                    }
                }
            }
            if (values.get("ACTION").equalsIgnoreCase("U")) {
                if (!wasFound || isDeleted) {
                    logger.error("Need to update record: " + record + " but record not found in o2uk_users hbase table");
                    if (isDebug)
                        System.out.println("Need to update record: " + record + " but record not found in o2uk_users hbase table");
                    doInsert = true;
                } else {
                    if (isNew)
                        doInsert = true;
                    else{
                        logger.info("record: " + record + " is too old. will not be updated to users table");
                        if (isDebug)
                            System.out.println("record: " + record + " is too old. recDate="+new String(bRecDate)+". will not be updated to users table");
                    }
                }
            }
            if (values.get("ACTION").equalsIgnoreCase("D")) {
                if (!wasFound) {
                    doInsert = true;
                    doDelete = true;
                }
                else {
                    if (isNew)
                        doDelete = true;
                    else{
                        logger.info("record: " + record + " is too old. will not be deleted from users table");
                        if (isDebug)
                            System.out.println("record: " + record + ", is too old. will not be deleted from users table");
                    }
                }
            }

            // perform the actions
            if (doInsert) {
                // insert or modify
                Put dataToUpdate = new Put(Bytes.toBytes(values.get("MSISDN")));
                dataToUpdate.addColumn(Bytes.toBytes("data"), Bytes.toBytes("imsi"), Bytes.toBytes(values.get("IMSI")));
                dataToUpdate.addColumn(Bytes.toBytes("data"), Bytes.toBytes("sp_id"), Bytes.toBytes(values.get("SP_ID")));
                dataToUpdate.addColumn(Bytes.toBytes("data"), Bytes.toBytes("wl"), Bytes.toBytes(values.get("WHITE_LIST")));
                dataToUpdate.addColumn(Bytes.toBytes("data"), Bytes.toBytes("upd_date"), Bytes.toBytes(values.get("UPDATE_DATE")));
                if (doDelete)
                    dataToUpdate.addColumn(Bytes.toBytes("data"), Bytes.toBytes("is_deleted"), Bytes.toBytes("1"));
                else
                    dataToUpdate.addColumn(Bytes.toBytes("data"), Bytes.toBytes("is_deleted"), Bytes.toBytes("0"));
                try {
                    table.put(dataToUpdate);
                    if (doDelete) {
                        nDeletes++;
                        if (isDebug)
                            System.out.println("record: " + record + " was deleted from users table");
                    }
                    else {
                        if (values.get("ACTION").equalsIgnoreCase("I")) {
                            nInserts++;
                            if (isDebug)
                                System.out.println("record: " + record + " was inserted to users table");
                        } else {
                            nUpdates++;
                            if (isDebug)
                                System.out.println("record: " + record + " was updated in users table");
                        }
                    }
                } catch (IOException e) {
                    logger.error("Failed to write record: "+record+" to hbase. " + e);
                    if (isDebug)
                        System.out.println("Failed to write to hbase. " + e);
                }
            }
            else if (doDelete)
            {
                // delete
                Put dataToDelete = new Put(Bytes.toBytes(values.get("MSISDN")));
                dataToDelete.addColumn(Bytes.toBytes("data"),Bytes.toBytes("upd_date"),Bytes.toBytes(values.get("UPDATE_DATE")));
                dataToDelete.addColumn(Bytes.toBytes("data"),Bytes.toBytes("is_deleted"),Bytes.toBytes("1"));
                try {
                    table.put(dataToDelete);
                    nDeletes++;
                    if (isDebug)
                        System.out.println("record: "+record+" was deleted from users table");
                } catch (IOException e) {
                    logger.error("Failed to delete record: "+record+" from hbase. "+e);
                    if (isDebug)
                        System.out.println("Failed to delete from hbase. "+e);
                }
            }
        }
        logger.info("Finished processing all records. Inserts: "+ nInserts+", Updates: "+nUpdates+", Deletes: "+nDeletes);
        if (isDebug)
            System.out.println("Finished processing all records. Inserts: " + nInserts + ", Updates: " + nUpdates + ", Deletes: " + nDeletes);
    }

    /**
     * split 1 record in hash map of fields & values
     * @param record
     * @return Map
     */
    private Map<String,String> splitRecord(String record)
    {
        String[] fields = record.split(";");
        Map<String, String> values = Maps.newHashMap();
        if (fields.length != 6)
            return null;
        for (int i=0; i<fields.length; i++)
        {
            String[] data = fields[i].split("=");
            if (data.length != 2)
                continue;
            values.put (data[0], data[1]);
        }
        return values;
    }

    /**
     * convert date string to calendar date
     * @param date string
     * @return date calendar
     */
    private Date convertStrToDate(String date)
    {
        Calendar clndr = Calendar.getInstance();
        clndr.set(Calendar.DAY_OF_MONTH, Integer.valueOf(date.substring(0,2)));
        clndr.set(Calendar.MONTH, Integer.valueOf(date.substring(2,4)));
        clndr.set(Calendar.YEAR, Integer.valueOf(date.substring(4,6)));
        clndr.set(Calendar.HOUR_OF_DAY, Integer.valueOf(date.substring(7,9)));
        clndr.set(Calendar.MINUTE, Integer.valueOf(date.substring(9,11)));
        clndr.set(Calendar.SECOND, Integer.valueOf(date.substring(11,13)));
        return clndr.getTime();
    }

    /**
     * run - process all SFI files
     * @param args
     * @throws Exception
     */
    public void run(String... args) throws Exception {
        logger.info("################################### START ###############################################################");
        String userName = args[0];
        String jobId = args[1];

        if (userName.isEmpty() || jobId.isEmpty()) {
            logger.error("User name or jobId is incorrect going to return");
            return;
        }
        logger.info("User name to process files is: " + userName);
        logger.info("Job id to process files is: " + jobId);

        // load HDFS path from properties file or defaultVal
        String defVal = HandleProcessConfiguration.HADOOP_FILE_SYSTEM_USER_PATH;
        if (args.length > 3) {
            defVal = args[3];
        }
        boolean isDebug = false;
        if (args.length > 4 && args[4].equalsIgnoreCase("debug"))
        {
            isDebug = true;
        }

        String hdfsUserPath =  new HandleProcessConfiguration().handleCustomizedHdfsUserPath(args[2], defVal);

        // process files
        processHdfsDirectory(userName, jobId, hdfsUserPath, isDebug);
        logger.info("################################### END ###############################################################");
    }
}





