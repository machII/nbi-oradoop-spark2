package com.starhomemach.nbi.oradoop.eureg;

/**
 * Created by pazit shefet on 15-05-2017.
 */

import com.google.common.collect.Lists;
import com.starhomemach.nbi.oradoop.eureg.dto.T_ferg_ad;
import com.starhomemach.nbi.oradoop.export.file.AvroFsInput;
import com.starhomemach.nbi.oradoop.export.file.dto.AvroFileRecord;
import com.starhomemach.nbi.oradoop.export.file.filehandler.FileHandlerUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;


@Component
public class EuregActionsFileHandler extends EuregFileHandler {
    private final Log logger = LogFactory.getLog(getClass());
    private static final int BATCH_SIZE = 10000;
/*    CREATE TABLE BI.T_FERG_AD
    (
        DD_IMSI    VARCHAR2(100 BYTE),
        DD_MSISDN  VARCHAR2(100 BYTE),
        TMST       DATE,
        DK_DAT     NUMBER(38),
        CL_ID      NUMBER(38),
        ACTN       VARCHAR2(500 BYTE),
        CL_GRP_ID  NUMBER(38)                         NOT NULL,
        SPRV_ID    NUMBER(38)
    ) */

    @Autowired
    private FileHandlerUtil fileHandlerUtil;

    @Override
    public String[] getInsertQuery() {
        String INSERT_INTO_BI_T_FERG_AD = "insert into BI.T_FERG_AD " +
                "(DD_IMSI, DD_MSISDN, TMST, CL_ID, CL_GRP_ID, DK_DAT, ACTN, SPRV_ID,THR_ID)" +
                " values(?,?,?,?,?,?,?,?,?)";
        return new String[]{INSERT_INTO_BI_T_FERG_AD};
    }

    @Override
    public int getBatchSize()
    {
        return BATCH_SIZE;
    }

    @Override
    // merge is NOT required!!
    public String[] getMergeQuery() {
        return null;
    }

    @Override
    public List<AvroFileRecord> readFileToList(String avroFilePath) throws IOException {
        List<AvroFileRecord> avroRecords = Lists.newArrayList();
        Path path = new Path(avroFilePath);
        Configuration config = new Configuration();
        org.apache.avro.file.SeekableInput input = new AvroFsInput(path, config);
        org.apache.avro.io.DatumReader<T_ferg_ad> tDetectDatumReader = new org.apache.avro.specific.SpecificDatumReader<>(T_ferg_ad.class);
        org.apache.avro.file.FileReader<T_ferg_ad> t_detectRecords = org.apache.avro.file.DataFileReader.openReader(input, tDetectDatumReader);
        while (t_detectRecords.hasNext()) {
            T_ferg_ad record = t_detectRecords.next();
            avroRecords.add(record);
        }

        t_detectRecords.close();
        logger.info("succeeded to process file:" + avroFilePath + ". number of records:" + avroRecords.size());

        return avroRecords;
    }

    @Override
    public boolean isMergeOperation() {
        return false;
    }

    @Override
    public boolean isSequenceOperation() {
        return false;
    }

    @Override
    public String getHandlerKey() {
        return "T_ferg_ad";
    }


    public void setPreparedStatement(AvroFileRecord avroFileRecords, Long sequenceId, PreparedStatement preparedStatement) throws SQLException, ParseException {
        logger.info("going to set PreparedStatement for record:" + avroFileRecords);
        T_ferg_ad tRecord = (T_ferg_ad) avroFileRecords;

        preparedStatement.setString(1, tRecord.getDDIMSI().toString());
        preparedStatement.setString(2, tRecord.getDDMSISDN() == null ? " " : tRecord.getDDMSISDN().toString());
        preparedStatement.setTimestamp(3, fileHandlerUtil.getSqlTSWithT(tRecord.getTMST().toString()));
        preparedStatement.setLong(4, tRecord.getCLID());
        preparedStatement.setLong(5, tRecord.getCLGRPID());
        preparedStatement.setLong(6, tRecord.getDKDAT());
        preparedStatement.setString(7, tRecord.getACTN() == null ? "" : tRecord.getACTN().toString());
        preparedStatement.setLong(8, tRecord.getSPRVID() == null ? 0 : tRecord.getSPRVID());
        preparedStatement.setLong(9, tRecord.getTHRID() == null ? 0 : tRecord.getTHRID());
        preparedStatement.addBatch();

    }
}
