package com.starhomemach.nbi.oradoop.eureg;

import com.starhomemach.nbi.oradoop.export.file.dto.AvroFileRecord;
import com.starhomemach.nbi.oradoop.export.file.sequence.SequenceHandler;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

/**
 * Created by pazit shefet on 15/05/2017.
 */
@Component
public class EuregSequenceHandler extends SequenceHandler {
    public static final String SELECT_BI_NEW_Q_HADOOP_EUREG_NEXTVAL_FROM_DUAL = "select  BI.Q_HADOOP_EUREG_ID.NEXTVAL from dual";

    Long sequenceNextVal = -1l;

    private final Log logger = LogFactory.getLog(getClass());

    @Override
    public Long getSequenceID(AvroFileRecord record) {
        if (sequenceNextVal == -1l)
        {
            sequenceNextVal = jdbcTemplate.queryForObject(SELECT_BI_NEW_Q_HADOOP_EUREG_NEXTVAL_FROM_DUAL, Long.class);
            logger.info("Sequence next val is:" + sequenceNextVal);
        }
        return sequenceNextVal;

    }
}
