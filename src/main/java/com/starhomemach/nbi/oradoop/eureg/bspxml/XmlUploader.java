package com.starhomemach.nbi.oradoop.eureg.bspxml;

import com.starhomemach.nbi.oradoop.OradoopConf;
import net.schmizz.sshj.SSHClient;
import net.schmizz.sshj.transport.verification.HostKeyVerifier;
import net.schmizz.sshj.xfer.FileSystemFile;
import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.File;
import java.io.IOException;
import java.security.PublicKey;

/**
 * Created by yamar on 17/09/2017.
 */
public class XmlUploader
{
    private static final String PATH = "/starhome/eureg/user/";
    private static final String XML_LOC = "/etl/xmlReport/";
    private static String userName;
    private static String jobId;

    private static final Log logger = LogFactory.getLog(XmlUploader.class);
    private static FileSystem fs;
    private static String host;
    private static String sshUser;
    private static String sshPass;
    private static String dest;

    public static void main(String[] args) throws IOException {

        logger.info("*********START BSP UPLOAD********");
        userName = args[0];
        jobId = args[1];
        host = args[2];
        sshUser = args[3];
        sshPass = args [4];
        dest = args[5];

        if (userName.isEmpty() || jobId.isEmpty() || host.isEmpty() || sshUser.isEmpty() || sshPass.isEmpty() || dest.isEmpty()) {
            logger.error("some args are empty  !!!!!");
            throw new IllegalArgumentException("Check args ....");
        }
        logger.info("User name to process files is: " + userName);
        logger.info("Job id to process files is: " + jobId);
        logger.info("SSH params: host: " +host +" sshUser: "+ sshUser +" sshPass : "+sshPass+" dest dir : "+ dest);
        String inputUrl = PATH + userName + XML_LOC + "/" + jobId + "/bsp.xml";
        logger.info("going to process directory:" + inputUrl);
        Path inputPath = new Path(inputUrl);
        fs = inputPath.getFileSystem(new OradoopConf().getConf());
        FSDataInputStream inFile = fs.open(inputPath);

        String xml = inFile.readUTF();
        File fileToSend = new File("BSP_MONITOR_FRAF1.xml");
        FileUtils.writeStringToFile(fileToSend,xml,"UTF-8");
        logger.debug("XML: "+ xml);
        SSHClient ssh = new SSHClient();
        ssh.addHostKeyVerifier(
                new HostKeyVerifier() {
                    public boolean verify(String arg0, int arg1, PublicKey arg2) {
                        return true;  // don't bother verifying
                    }
                }
        );
        ssh.connect(host);
        ssh.authPassword(sshUser,sshPass);
        logger.info("SSH status: " + ssh.isConnected());

        try {
            ssh.newSCPFileTransfer().upload(new FileSystemFile(fileToSend), dest);
        } finally {
            ssh.disconnect();
        }
        logger.info("*********DONE BSP UPLOAD********");

    }
}
