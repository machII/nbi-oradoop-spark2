package com.starhomemach.nbi.oradoop.eureg;

import com.starhomemach.nbi.oradoop.export.file.filehandler.FileHandler;
import com.starhomemach.nbi.oradoop.export.file.sequence.M2MSequenceHandler;
import com.starhomemach.nbi.oradoop.export.file.sequence.SequenceHandler;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by pazit shefet on 15-05-2017.
 */
public abstract class EuregFileHandler extends FileHandler {
    @Autowired
    private EuregSequenceHandler sequence;

    public SequenceHandler getSequenceHandler() {
        return sequence;
    }

    @Override
    public boolean isAppendOperation() {
        return false;
    }

    @Override
    public String[] getAppendQuery() {
        return new String[]{""};
    }

    @Override
    public boolean useFinalizeProcedure() { return false; }

    @Override
    public String[] getFinalizeProcedure() { return new String[]{""}; }
}
