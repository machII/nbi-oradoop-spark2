package com.starhomemach.nbi.oradoop.eureg.bspxml;

/**
 * Created by yamar on 05/09/2017.
 */
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.starhomemach.nbi.oradoop.OradoopConf;
import com.starhomemach.nbi.oradoop.export.file.EtlFileStatus;
import com.starhomemach.nbi.oradoop.export.file.filehandler.FileHandler;
import com.starhomemach.nbi.oradoop.export.util.HandleProcessConfiguration;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.starhomemach.nbi.oradoop.eureg.dto.T_xml_report;
import com.starhomemach.nbi.oradoop.export.file.AvroFsInput;
import com.starhomemach.nbi.oradoop.export.file.dto.AvroFileRecord;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import javax.xml.bind.DatatypeConverter;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * Created by yamar on 30/08/2017.
 */
public class BspXmlGenerator
{
    private static final Log logger = LogFactory.getLog(BspXmlGenerator.class);
    private static final String XML_LOC =  "/etl/xml";
    static List<T_xml_report>  avroRecords = Lists.newArrayList();

    static FileSystem  fs;
    static String jobId ;
    static String userName;
    public static void main(String[] args) throws IOException, ParseException, JAXBException {

        System.out.println("*********START BSP XML********");
        userName = args[0];
        jobId = args[1];

        if (userName.isEmpty() || jobId.isEmpty()) {
            logger.error("User name or jobId is incorrect going to return");
            return;
        }
        logger.info("User name to process files is: " + userName);
        logger.info("Job id to process files is: " + jobId);
        processXmlData("/starhome/eureg/user/",userName,jobId);
        if(avroRecords.size()> 0){
            createXmlReport();
        }else {
            logger.error("NO Avro Records found to create XML ");
        }


    }

    private static void createXmlReport() throws ParseException, JAXBException, IOException {

        AlertReport report = new AlertReport();
        AlertReport.ReportHeader head = new AlertReport.ReportHeader();
        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss").withZone(ZoneId.systemDefault());
        String formatDateTime = now.format(formatter);
        head.setCreationTimestamp(formatDateTime);
        head.setSchemaVersion("1.0");
        head.setSender("STARHOME");
        head.setSeqNo(1984);
        report.setReportHeader(head);
        AlertReport.ReportBody alerts = new AlertReport.ReportBody();
        for(T_xml_report rec : avroRecords)
        {
            AlertReport.ReportBody.Alert currAlert = new AlertReport.ReportBody.Alert();
            currAlert.setIMSI(rec.getDDIMSI().toString());
            currAlert.setMSISDN(rec.getDDMSISDN().toString());
            currAlert.setCutOffFlag(0);
            currAlert.setThresholdLocation(rec.getCntryGrpDsc().toString());
            currAlert.setAlertingType("NULL");
            currAlert.setAlertingLevel(rec.getPROFILE().toString());
            AlertReport.ReportBody.Alert.ThresholdReached thr = new AlertReport.ReportBody.Alert.ThresholdReached();
            thr.setUnit("KB");
            thr.setValue(convertToKB(rec.getTHRVAL()));
            currAlert.setThresholdReached(thr);
            AlertReport.ReportBody.Alert.ActualUsage actl = new AlertReport.ReportBody.Alert.ActualUsage();
            actl.setUnit("KB");
            actl.setValue(convertToKB(rec.getACTLUSAGE()));
            currAlert.setActualUsage(actl);
            AlertReport.ReportBody.Alert.VPMN vpmn = new AlertReport.ReportBody.Alert.VPMN();
            vpmn.setMCCMNC(rec.getOrgMccMnc().toString());
            vpmn.setTADIG(rec.getNW().toString());
            currAlert.setVPMN(vpmn);
            String alertTime = formatter.format(Instant.parse(rec.getDTTIME().toString()));
            currAlert.setAlertTime(alertTime);
            //we sent SMS
            if(rec.getACTN().toString().contains("4")){
                currAlert.setSMSFlag(1);
            }else{
                currAlert.setSMSFlag(0);
            }
            alerts.getAlert().add(currAlert);
        }
        report.setReportBody(alerts);

        JAXBContext jaxbContext = null;

            StringWriter sw = new StringWriter();
            jaxbContext = JAXBContext.newInstance( AlertReport.class );
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            /* set this flag to true to format the output */
            jaxbMarshaller.setProperty( Marshaller.JAXB_FORMATTED_OUTPUT, true );
            jaxbMarshaller.setProperty(Marshaller.JAXB_ENCODING, "utf-8");
            jaxbMarshaller.marshal(report, sw);
            FSDataOutputStream pa = fs.create(new Path("/starhome/eureg/user/" + userName + "/etl/xmlReport/"+jobId+"/bsp.xml"));
            System.out.println(sw.toString());
            pa.writeUTF(sw.toString());
            pa.close();
          //  jaxbMarshaller.marshal( report, System.out );
    }

    private static String convertToKB(Double actlusage) {
        BigDecimal ans  = new BigDecimal(actlusage *1024);
        return ans.toString();
    }

    private static void processFileDirectory(FileSystem fs, FileStatus tableFile) throws IOException {
        if (tableFile.isDirectory()) {
            Path tableDirectoryPath = tableFile.getPath();
            String tableDirectoryName = tableDirectoryPath.getName();
            // take the name of the folder were the avro files are to see what handler is needed
            FileStatus[] perTableFiles = fs.listStatus(tableDirectoryPath);
            if (shouldProcessDirectory(perTableFiles).equalValue(EtlFileStatus._SUCCESS)) {
                logger.info("number of avro files to process in directory:" + tableDirectoryName + " is:" + (perTableFiles.length - 1));
                for (FileStatus perTableFile : perTableFiles) {
                    Path perTableFilePath = perTableFile.getPath();
                    String fileToProcessName = perTableFilePath.getName();
                    if (!fileToProcessName.equals(EtlFileStatus._SUCCESS.toString())) {
                        org.apache.avro.file.SeekableInput input = new AvroFsInput(perTableFilePath, new OradoopConf().getConf());
                        org.apache.avro.io.DatumReader<T_xml_report> tDetectDatumReader = new org.apache.avro.specific.SpecificDatumReader<>(T_xml_report.class);
                        org.apache.avro.file.FileReader<T_xml_report> t_detectRecords = org.apache.avro.file.DataFileReader.openReader(input, tDetectDatumReader);
                        while (t_detectRecords.hasNext()) {
                            T_xml_report record = t_detectRecords.next();
                            avroRecords.add(record);
                        }
                        t_detectRecords.close();
                    }
                }
            } else {
                //TODO throw exception
                logger.error("JOB status is not _SUCCESS going to end run and throw exception");
            }
            System.out.println( "XML ALERT:  COUNT "+ avroRecords.size());
        }
    }

    private static EtlFileStatus shouldProcessDirectory(FileStatus[] files) {
        EtlFileStatus returnEtlFileStatus = EtlFileStatus.OTHER;
        if (Arrays.stream(files).filter(file -> EtlFileStatus._SUCCESS.toString().equals(file.getPath().getName().toUpperCase())).count() > 0) {
            returnEtlFileStatus = EtlFileStatus._SUCCESS;
        } else if (Arrays.stream(files).filter(file -> EtlFileStatus._FAILED.toString().equals(file.getPath().getName().toUpperCase())).count() > 0) {
            returnEtlFileStatus = EtlFileStatus._FAILED;
        }
        logger.info("Directory status is:" + returnEtlFileStatus.toString());
        return returnEtlFileStatus;
    }


    private static void processXmlData(String path,String userName,String jobId) throws IOException {
        String inputUrl = path + userName + XML_LOC + "/" + jobId + "/";
        logger.info("going to process directory:" + inputUrl);
        Path inputPath = new Path(inputUrl);
        fs = inputPath.getFileSystem(new OradoopConf().getConf());
        FileStatus[] jobIdDirectories = fs.globStatus(inputPath);
        try {
            for (FileStatus jobIdDirectory : jobIdDirectories) {
                if (jobIdDirectory.isDirectory()) {
                    FileStatus[] jobDirectoryFiles = fs.listStatus(jobIdDirectory.getPath());
                    for (FileStatus tableFile : jobDirectoryFiles) {
                        {
                            System.out.println(tableFile);
                            processFileDirectory(fs, tableFile);
                        }
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Failed to process url: " + inputUrl + ". The exception is: " + e.getMessage() + ".going to throw exception");
        }
    }
}
