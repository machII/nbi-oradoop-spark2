package com.starhomemach.nbi.oradoop.eureg.FairRoaming;

import com.starhomemach.nbi.oradoop.export.util.HandleProcessConfiguration;

import java.util.Collection;
import java.util.Properties;

public class GenericFairRoamingApplication {

    public static void main(String[] args) throws Exception {

        if(args.length != 5){
            throw  new RuntimeException("Argument must include 5 parameters: user, maxWfId, prestoUrl, userName,  wfId     ");
        }
        int i=0;
        String user = args[i++];
        String maxWfToRun = args[i++];
        String prestoUrl = args[i++];
        String userName = args[i++];
        String wfId = args[i++];

        String hdfsPath = String.format(HandleProcessConfiguration.EUREG_USER_PROPERTIES_PATH, user);
        Properties userProperties = HandleProcessConfiguration.getPropertiesFile(hdfsPath);
        Collection<Object> availableSources = userProperties.values();
        GenericFairRoamingHandleSource handle = new GenericFairRoamingHandleSource(prestoUrl, userName);
        handle.run(availableSources, user, Integer.valueOf(maxWfToRun),  wfId);
    }

}
