package com.starhomemach.nbi.oradoop.eureg.FairRoaming;

import lombok.AllArgsConstructor;
import org.apache.commons.lang.StringUtils;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.*;
import java.util.Collection;
import java.util.Properties;

@AllArgsConstructor
public class GenericFairRoamingHandleSource {

    private static final String select = " select array_join(array_agg(concat('''', wf_id,'''')), ',') " +
            " from (select c.cre_dat,c.wf_id " +
            " from metadata_db.t_wf_state c where c.step_wf_type_id  = 2  and c.status = 2 and CAST(CAST(c.source_type_id AS VARCHAR) AS SMALLINT) = %d and c.tbl_grp_id = %d and c.wf_id not in\n" +
            " (select wf_id from metadata_db.t_wf_state where step_wf_type_id  = 3  and status != 0 and CAST(CAST(source_type_id AS VARCHAR) AS SMALLINT) = 4 and user_run = '%s')\n" +
            " order by cre_dat desc limit %d) as t ";

    private static final String update = " INSERT INTO metadata_db.t_wf_state (wf_id, tbl_grp_id, step_wf_id, step_wf_type_id, status, cre_dat, source_type_id, user_run)" +
            " SELECT wf_id, %d, '%s', 3, 1, cast (current_timestamp as timestamp), cast( 4 as smallint), '%s'\n" +
            " from metadata_db.t_wf_state where tbl_grp_id = %d and step_wf_type_id = 2\n" +
            " and status = 2 and wf_id in (%s)";

    String jdbcUrl;
    String userName;

    public void run(Collection<Object> availableSources, String user, int maxWfToRun, String wfId) throws Exception{

        String filePath = System.getProperty("oozie.action.output.properties");
        if (filePath != null) {
            Properties properties = new Properties();
            Connection con = null;
            Statement stmt = null;
            try {
                con = DriverManager.getConnection(jdbcUrl, userName, null);
                System.out.println("success create connection to : " +jdbcUrl);
                stmt = con.createStatement();
                String value;
                for (FairRoamingSource source : FairRoamingSource.values()) {
                    value = "-1";
                    if (availableSources.contains(source.name())) {
                        if (source.isWfIdSupport()) {
                            value = handleWfIdState(user, maxWfToRun, wfId,  stmt, source);
                        } else {
                            value = "True";
                        }
                    }
                    properties.setProperty(source.name(), value);
                }
                System.out.println("going to save Properties: " + properties.toString() + " to " + filePath );
                OutputStream outputStream = new FileOutputStream(filePath);
                properties.store(outputStream, "");
            } catch (SQLException e) {
                e.printStackTrace();
                System.out.println("Cannot connect to or exeption in query: " + jdbcUrl + " " + userName );
                throw e;
            } catch (IOException e) {
                e.printStackTrace();
                System.out.println("Cannot save properties to: " + filePath);
                throw e;
            } finally {
                try {
                    if (stmt != null)
                        stmt.close();
                    if (con != null)
                        con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    System.out.println("Cannot close connection to: " + jdbcUrl + " " + userName );
                }
            }
        }
    }

    private String handleWfIdState(String user, int maxWfToRun, String wfId, Statement stmt, FairRoamingSource source) throws SQLException {

        String value = "-1";
        String sqlWithParam = String.format(select, source.getSourceType(), source.getTableGrp(), user, maxWfToRun);
        System.out.println(" for source " + source.name() + " going to run " + sqlWithParam );
        try {
            ResultSet resultSet = stmt.executeQuery(sqlWithParam);
            if (resultSet.next()) {
                value = resultSet.getString(1);
                if (StringUtils.isEmpty(value)) {
                    System.out.println(" for source " + source.name() + " result is empty ");
                    value = "-1";
                } else {
                    String sqlForUpdateWithParam = String.format(update, source.getTableGrp(), wfId, user, source.getTableGrp(), value);
                    System.out.println(" for source " + source.name() + " going to update " + sqlForUpdateWithParam);
                    stmt.executeUpdate(sqlForUpdateWithParam);
                }
            } else {
                System.out.println("no result set for Sql ");
            }
        } catch (SQLException e){
           System.out.println("error while select " + sqlWithParam );
           throw e;
        }
        return value;
    }
}
