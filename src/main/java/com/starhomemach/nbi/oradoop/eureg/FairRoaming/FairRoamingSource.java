package com.starhomemach.nbi.oradoop.eureg.FairRoaming;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum FairRoamingSource {
    TAP (true, 1, 220),
    NRT (true, 2, 160),
    FILES (false, -1, -1),
    VISIT_DURATION (false, -1, -1);

    private boolean isWfIdSupport;
    private int sourceType;
    private int tableGrp;
}
