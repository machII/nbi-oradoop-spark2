package com.starhomemach.nbi.oradoop.eureg.abs_delta;

import com.starhomemach.nbi.oradoop.export.Application;
import com.starhomemach.nbi.oradoop.export.SpringConfig;
import com.starhomemach.nbi.oradoop.export.service.ProcessHdfsDirectoryService;
import com.starhomemach.nbi.oradoop.export.util.HandleProcessConfiguration;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Created by pazit shefet on 15-05-2017.
 */
public class EuregAbsDeltaApplication {

    public static void main(String[] args) throws Exception
    {
        System.out.println("####################################### START LOADING INTO HBASE #############################");
        if (args.length < 4) {
            System.out.println("ERROR args should contain the following parameters: ");
            System.out.println("  1) USER ");
            System.out.println("  2) SPARK-JOB-ID ");
            System.out.println("  3) oradoop properties file name ");
            System.out.println("  4) Eureg HDFS location ");
            return;
        }

        // load oradoop properties file
        HandleProcessConfiguration processConfiguration =  new HandleProcessConfiguration();
        processConfiguration.setPath(HandleProcessConfiguration.STARHOME_EUREG_SHARE_CONF);
        args[2] = processConfiguration.handleOradoopConf(args, Application.INDEX_DB_PROPERTIES_FILE);

        // activate ProcessHdfsDirectoryService
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SpringConfig.class);
        context.getBean(LoadAbsDeltaService.class).run(args);

        System.out.println("################################## END LOADING INTO HBASE #############################");

    }
}
