package com.starhomemach.nbi.oradoop.ooziemonitoring;

import com.starhomemach.nbi.oradoop.export.SpringConfig;
import com.starhomemach.nbi.oradoop.export.file.filehandler.FileHandlerUtil;
import com.starhomemach.nbi.oradoop.export.util.OradoopProcessArgs;
import com.starhomemach.nbi.oradoop.ooziemonitoring.dto.Workflow;
import com.starhomemach.nbi.oradoop.ooziemonitoring.query.JobHistory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.oozie.client.OozieClient;
import org.apache.oozie.client.OozieClientException;
import org.apache.oozie.client.WorkflowJob;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.apache.oozie.client.WorkflowJob.Status;


@Component
public class ProcessWorkflows {


    public static final String INSERT_INTO_BI_HDP_T_WORKFLOW_MONITORING_TMP_VALUES = "insert into  bi.HDP_T_WORKFLOW_MONITORING_TMP values (?,?,?,?,?,?,?,?,?,?,?,?,?)";
    public static final String MERGE_SQL = "MERGE INTO BI.T_WORKFLOW_MONITORING DY\n" +
            "USING (SELECT * FROM BI.HDP_T_WORKFLOW_MONITORING_TMP ) temp\n" +
            "ON (DY.workflow_id = temp.workflow_id )\n" +
            "WHEN MATCHED THEN UPDATE SET DY.status =  temp.status , DY.start_time = temp.start_time ,DY.end_time = temp.end_time ,DY.mod_dat = temp.mod_dat,DY.total_run_time = temp.total_run_time,DY.is_retries = temp.is_retries,DY.PROCESSED_NUM_FILES = temp.PROCESSED_NUM_FILES,DY.BACK_LOG_FILES = temp.BACK_LOG_FILES,DY.ORADOOP_SEQUENCES = temp.ORADOOP_SEQUENCES\n" +
            "WHERE DY.status not in ('SUCCEEDED')\n" +
            "WHEN NOT MATCHED THEN INSERT (DY.workflow_id,DY.workflow_name,DY.workflow_user,DY.status,DY.start_time,DY.end_time,DY.cre_dat,DY.mod_dat,DY.total_run_time,DY.is_retries,DY.PROCESSED_NUM_FILES,DY.BACK_LOG_FILES,DY.ORADOOP_SEQUENCES)\n" +
            "VALUES  (temp.workflow_id,temp.workflow_name,temp.workflow_user,temp.status,temp.start_time,temp.end_time,temp.cre_dat,temp.mod_dat,temp.total_run_time,temp.is_retries,temp.PROCESSED_NUM_FILES,temp.BACK_LOG_FILES,temp.ORADOOP_SEQUENCES)";
    public static final int END_LEN = 1000000000;
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private JobHistory jobHistory;

    @Autowired
    private FileHandlerUtil fileHandlerUtil;

    private final Log logger = LogFactory.getLog(getClass());


    public void run(String... args) throws Exception {
        logger.info("################################### START ###############################################################");

        String oozieUrl = args[0];
        logger.info(" Ozzie url to used:"+oozieUrl);
        OozieClient oozieClient = new OozieClient(oozieUrl);
        Connection connection = jdbcTemplate.getDataSource().getConnection();
        connection.setAutoCommit(false);

        int startIndex = jobHistory.getWorkflowStart().intValue();
        logger.info("start index is:"+startIndex);
        if(startIndex == -1)
        {
            logger.info("start index is:"+startIndex+" going to stop running");
            return;
        }
        //TODO change the value 1 and 10 to be in properties file
        List<WorkflowJob> jobsInfo = oozieClient.getJobsInfo(null,1, 1000);
        Collections.reverse(jobsInfo);

        logger.info(" number of workflows should process is:" + jobsInfo.size());

        PreparedStatement insertPreparedStatement = connection.prepareStatement(INSERT_INTO_BI_HDP_T_WORKFLOW_MONITORING_TMP_VALUES);
        PreparedStatement mergePreparedStatement = connection.prepareStatement(MERGE_SQL);
        boolean isUpdateStartIndex = true;
        WorkflowJob wfJobInfo = null;
        for (WorkflowJob jobInfo : jobsInfo) {
            Workflow workflow = new Workflow();
            try {
                //the only way it is working is to get job info again else the job info will have zero actions
                wfJobInfo = oozieClient.getJobInfo(jobInfo.getId());
                workflow.transform(wfJobInfo);
                setPreparedStatement(insertPreparedStatement, workflow);
            } catch (Exception e) {
                logger.error("Got exception while trying to transform workflow: " + wfJobInfo != null ? (wfJobInfo.getAppName() + ", id: "+ wfJobInfo.getId()) : "");
            }
        }

        executeBatch(connection, startIndex, insertPreparedStatement, mergePreparedStatement);

        logger.info("################################### END ###############################################################");
        System.out.println("################################### END ###############################################################");
    }

    private void executeBatch(Connection connection, int startIndex, PreparedStatement insertPreparedStatement, PreparedStatement mergePreparedStatement) throws SQLException {
        logger.info("going to run sql commit for all changes");
        logger.info("run insert :"+INSERT_INTO_BI_HDP_T_WORKFLOW_MONITORING_TMP_VALUES);
        insertPreparedStatement.executeBatch();
        insertPreparedStatement.clearBatch();
        insertPreparedStatement.close();

        logger.info("run merge :"+MERGE_SQL);
        mergePreparedStatement.executeUpdate();
        mergePreparedStatement.clearBatch();
        mergePreparedStatement.close();

        logger.info("Next index value is:"+startIndex+". going to update DB");
        jobHistory.updateJobHistory(startIndex);

        connection.commit();
    }


    private void setPreparedStatement(PreparedStatement insertPreparedStatement, Workflow workflow) throws SQLException, OozieClientException {
        insertPreparedStatement.setString(1, workflow.getWorkflowId());
        insertPreparedStatement.setString(2, workflow.getAppName());
        insertPreparedStatement.setString(3, workflow.getUser());
        insertPreparedStatement.setString(4, workflow.getStatus());
        insertPreparedStatement.setTimestamp(5, new Timestamp(workflow.getStartDate().getTime()));

        Date endDate = workflow.getEndDate();
        if (endDate != null) {
            insertPreparedStatement.setTimestamp(6, new Timestamp(endDate.getTime()));
            insertPreparedStatement.setLong(9, workflow.getTotalRunTimeInMilli());
        } else {
            insertPreparedStatement.setNull(6, Types.DATE);
            insertPreparedStatement.setNull(9, Types.BIGINT);
        }

        insertPreparedStatement.setTimestamp(7, new Timestamp(workflow.getCreateDate().getTime()));
        insertPreparedStatement.setTimestamp(8,  new Timestamp(workflow.getModifyDate().getTime()));
        insertPreparedStatement.setString(10, workflow.getRetries());

        fileHandlerUtil.setPreparedStatementIfNullOrLong(11, workflow.getNumOfFiles(), insertPreparedStatement);
        fileHandlerUtil.setPreparedStatementIfNullOrLong(12, workflow.getBacklogOfFiles(), insertPreparedStatement);
        insertPreparedStatement.setString(13, workflow.getOradoopSequences());


        insertPreparedStatement.addBatch();
    }


    public boolean isUpdateIndex(Status status)
    {
        if(status.compareTo(Status.RUNNING) == 0 || status.compareTo(Status.PREP) == 0 || status.compareTo(Status.SUSPENDED) == 0)
        {
            return false;
        }
        return true;
    }





//    public void handleDate(Date startTime) {
//
//        String year = String.valueOf(new DateTime(startTime).getYear());
//        String monthOfYear = String.valueOf(new DateTime(startTime).getMonthOfYear());
//        String dayOfMonth = String.valueOf(new DateTime(startTime).getDayOfMonth());
//        String hourOfDay = String.valueOf(new DateTime(startTime).getHourOfDay());
//        String minuteOfHour = String.valueOf(new DateTime(startTime).getMinuteOfHour());
//        String secondOfMinute = String.valueOf(new DateTime(startTime).getSecondOfMinute());
//        String millisOfSecond = String.valueOf(new DateTime(startTime).getMillisOfSecond());
//
//
//        Date d = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(year + "-" + monthOfYear + "-" + dayOfMonth + " " + hourOfDay + ":" + minuteOfHour + ":" + secondOfMinute + ":" + millisOfSecond);
//
//
//        System.out.println(d.toString());
//        System.out.println(d.getTime());
//        java.sql.Date date = new java.sql.Date(d.getTime());
//        System.out.println(date.getTime());
//
//    }





    public static void main(String[] args) throws Exception {

        OradoopProcessArgs.getInstance().monitoringUrl("http://10.135.11.24:11000/oozie").oracleDbUrl("jdbc:oracle:thin:@10.30.40.13:1521:BITG1")
                .user("oradoop").password("oradoop");

        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SpringConfig.class);
        context.getBean(ProcessWorkflows.class).run(OradoopProcessArgs.getInstance().getMonitoringUrl());

    }


}
