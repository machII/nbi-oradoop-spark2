package com.starhomemach.nbi.oradoop.ooziemonitoring.query;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class JobHistory {

    public static final String SELECT_WORKFLOW_START = "select WORKFLOW_START from bi.job_last_run where job_name  = ? AND user_name = ?";


    @Autowired
    private JdbcTemplate jdbcTemplate;


    private final Log logger = LogFactory.getLog(getClass());

    public Long getWorkflowStart() throws DataAccessException {
        Long sequenceNextVal = jdbcTemplate.queryForObject("select WORKFLOW_START from bi.job_last_run where job_name  = 'workflow-monitor' AND user_name = 'qos'", Long.class);
        return sequenceNextVal;
    }


    public void updateJobHistory(int start)
    {
        int update = jdbcTemplate.update("update bi.job_last_run set WORKFLOW_START = " + start + " where job_name  = 'workflow-monitor' AND user_name = 'qos'");
        logger.info(" update "+update+" number of rows");
    }


}
