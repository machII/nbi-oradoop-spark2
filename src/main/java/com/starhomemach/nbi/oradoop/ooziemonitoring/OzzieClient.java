package com.starhomemach.nbi.oradoop.ooziemonitoring;

import com.starhomemach.nbi.oradoop.export.SpringConfig;
import com.starhomemach.nbi.oradoop.export.util.HandleProcessConfiguration;
import com.starhomemach.nbi.oradoop.export.util.OradoopProcessArgs;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class OzzieClient {


    public static void main(String[] args) throws Exception {

        System.out.println("########################################################## START OOZIE WORKFLOW MONITORING #############################");
        new HandleProcessConfiguration().handleWorkflowMonitoringConf(args,1);
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SpringConfig.class);
        context.getBean(ProcessWorkflows.class).run(OradoopProcessArgs.getInstance().getMonitoringUrl());
        System.out.println("########################################################## END OOZIE WORKFLOW MONITORING #############################");
    }


}
