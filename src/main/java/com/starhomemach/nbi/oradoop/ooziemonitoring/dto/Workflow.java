package com.starhomemach.nbi.oradoop.ooziemonitoring.dto;

import org.apache.oozie.client.WorkflowAction;
import org.apache.oozie.client.WorkflowJob;

import java.util.Date;
import java.util.List;

public class Workflow {

    public static final String UNKNOWN = "unknown";
    public Long numOfFiles;
    public Long backlogOfFiles;
    public String appName;
    public String status;
    public Date startDate;
    public Date endDate;
    public Date modifyDate;
    public Date createDate;
    public String workflowId;
    public String user;
    public String oradoopSequences;
    public String retries;
    public Long totalRunTimeInMilli;


    public void transform(WorkflowJob jobInfo) {

        List<WorkflowAction> actions = jobInfo.getActions();
        this.appName = jobInfo.getAppName();
        this.status = jobInfo.getStatus().toString();
        this.startDate = jobInfo.getStartTime();
        this.endDate = jobInfo.getEndTime();



        this.workflowId = jobInfo.getId();
        this.modifyDate = new java.sql.Date(System.currentTimeMillis());
        this.createDate = new java.sql.Date(System.currentTimeMillis());
        this.user = jobInfo.getUser();
        this.oradoopSequences = handleOradoopSequences();
        this.retries = isRetries(actions);
        this.totalRunTimeInMilli = handleTotalRunTimeInMilli(startDate, endDate);
        handleFileDetails(actions);
    }

    private void handleFileDetails(List<WorkflowAction> actions) {

        for (WorkflowAction action : actions) {
            if ("java-mover".equals(action.getName())) {
                String data = action.getData();
                if (data != null) {
                    String[] split = data.split("\n");
                    if (split.length == 4) {
                        String numOfFiles = split[2];
                        String backlog = split[3];
                        this.numOfFiles = getNumberOutOfString(numOfFiles);
                        this.backlogOfFiles = getNumberOutOfString(backlog);
                        return;
                    }
                }
            }
        }
        numOfFiles = -1l;
        backlogOfFiles = -1l;
    }


    public Long getNumberOutOfString(String line) {
        if (line.contains("=")) {
            String[] split = line.split("=");
            if (split.length == 2) {
                return new Long(split[1].trim());
            }
        }
        return -1l;
    }


    public java.sql.Date getSqlDate(Date date) {
        return new java.sql.Date(date.getTime());
    }


    private Long handleTotalRunTimeInMilli(Date startDate, Date endDate) {
        if (endDate != null) {
            long endTime = endDate.getTime();
            long startTime = startDate.getTime();
            return endTime - startTime;
        }
        return 0L;
    }

    private String handleOradoopSequences() {
        return " ";
    }


    public String isRetries(List<WorkflowAction> jobInfoActions) {
        for (WorkflowAction wfa : jobInfoActions) {
            if (wfa.getRetries() > 0) {
                return "Y";
            }
        }
        return "N";
    }


    public Long getNumOfFiles() {
        return numOfFiles;
    }

    public void setNumOfFiles(Long numOfFiles) {
        this.numOfFiles = numOfFiles;
    }

    public Long getBacklogOfFiles() {
        return backlogOfFiles;
    }

    public void setBacklogOfFiles(Long backlogOfFiles) {
        this.backlogOfFiles = backlogOfFiles;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getWorkflowId() {
        return workflowId;
    }

    public void setWorkflowId(String workflowId) {
        this.workflowId = workflowId;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getOradoopSequences() {
        return oradoopSequences;
    }

    public void setOradoopSequences(String oradoopSequences) {
        this.oradoopSequences = oradoopSequences;
    }

    public String getRetries() {
        return retries;
    }

    public void setRetries(String retries) {
        this.retries = retries;
    }

    public Long getTotalRunTimeInMilli() {
        return totalRunTimeInMilli;
    }

    public void setTotalRunTimeInMilli(Long totalRunTimeInMilli) {
        this.totalRunTimeInMilli = totalRunTimeInMilli;
    }
}
