package com.starhomemach.nbi.oradoop.datacompaction;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;

/**
 * Created by Pragmatists on 2017-04-03.
 */
public class CompactTable {
    public static void main(String[] args) throws Exception {
        if (args.length < 3) {
            System.out.println("Usage: [INPUT_PATH] [OUTPUT_PATH] [PARTITIONS]");
            return;
        }

        SparkConf sparkConf = new SparkConf().setAppName("Spark Compaction");
        JavaSparkContext sc = new JavaSparkContext(sparkConf);

        CompactTableUtil ctutl = new CompactTableUtil();
        for(String partition : ctutl.splitPartitionList(args[2])) {
            System.out.println("Processing partition " + partition );
            Compact cmp = new Compact();
            cmp.compact(new String[]{"-i", ctutl.getPathForPartition(args[0], partition),
                                     "-o", ctutl.getPathForPartition(args[1], partition)}, sc );
        }
    }
}
