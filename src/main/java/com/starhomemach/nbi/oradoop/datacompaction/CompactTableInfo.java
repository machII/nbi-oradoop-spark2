package com.starhomemach.nbi.oradoop.datacompaction;

/**
 * Created by Pragmatists on 2017-04-03.
 */
public class CompactTableInfo {
    public static void main(String[] args) throws Exception {
        System.out.println("################################################ START ####################################################");
        if (args.length < 5) {
            System.out.println("Usage: [JDBC_DRIVER] [JDBC_URL] [USERNAME] [EXPORT_PATH] [DATABASE_NAME] [TABLE_NAME]");
            return;
        }

        CompactTableUtil ctutl = new CompactTableUtil();
        ctutl.getTableInfo( args[0], args[1], args[2], args[3], args[4], args[5] );

        System.out.println("################################################ END ####################################################");
    }
}