package com.starhomemach.nbi.oradoop.datacompaction;

import com.starhomemach.nbi.oradoop.datacompaction.hdfsdirectory.SignalingEvents;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ProcessDataCompaction {


    @Autowired
    private SignalingEvents signalingEvents;

    private final Log logger = LogFactory.getLog(getClass());


    public static final String ALL_VALUE = "ALL";

    public void run(String... args) throws Exception {
        logger.info("########################################################## START DATA COMPACTION #############################");
        if(args.length >= 4)
        {

            String user = args[0];
            String queueName = args[1];
            String hiveConnection = args[2];
            boolean dataValidation = new Boolean(args[3]);

            String listOfClients = ALL_VALUE;
            String listOfDkDat = ALL_VALUE;
            if(args.length > 5)
            {
                 listOfClients = args[4];
            }
            if(args.length == 6)
            {
                listOfDkDat = args[5];
            }
            logger.info("User is :"+user+" Queue name is:"+queueName+"  hive connection details:"+hiveConnection+ "list of clients is:"+listOfClients+" list of dk dat is:"+listOfDkDat+" Data validation before and after compaction is"+dataValidation);
            signalingEvents.process(queueName,user,hiveConnection,listOfClients,listOfDkDat,dataValidation);
        }
        else
        {
            logger.info("Error need user and queue name and hive connection details as main argument");
        }
        logger.info("########################################################## END DATA COMPACTION #############################");
    }

}
