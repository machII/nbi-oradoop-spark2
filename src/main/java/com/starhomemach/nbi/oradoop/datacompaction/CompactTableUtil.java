package com.starhomemach.nbi.oradoop.datacompaction;

import com.starhomemach.nbi.oradoop.OradoopConf;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.springframework.stereotype.Service;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Created by XDDA on 2017-03-30.
 */
@Service
public class CompactTableUtil {
    private final String TABLE_LOCATION = "TABLE_LOCATION";
    private final String PARTITIONS = "PARTITIONS";
    private final String NEEDS_COMPACTION = "NEEDS_COMPACTION";
    private final String OUTPUT_PATH = "OUTPUT_PATH";
    private final String NON_PARTITIONED = "NON_PARTITIONED";
    private final String FIND_STRING = ":8020/";


    public void getTableInfo(String jdbcDriver, String jdbcUrl, String userName, String outputPath,
                             String databaseName, String tableName) throws Exception {

        Connection con = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            Class.forName(jdbcDriver);

            // if no username given then try connecting without
            if (userName == null)
                con = DriverManager.getConnection(jdbcUrl);
            else
                con = DriverManager.getConnection(jdbcUrl, userName, userName);
            con.setAutoCommit(false);
            stmt = con.createStatement();

            String filePath = System.getProperty("oozie.action.output.properties");
            if (filePath != null) {
                OutputStream outputStream = new FileOutputStream(filePath);
                Properties p = new Properties();

                ResultSet res = stmt.executeQuery("DESCRIBE FORMATTED " + databaseName + "." + tableName );
                while (res.next()) {
                    if (res.getString("col_name").contains("Location:")) {
                        p.setProperty(TABLE_LOCATION, res.getString("data_type"));
                        System.out.println("[" + res.getString("col_name") + "] : [" + res.getString("data_type") + "]");
                        break;
                    }
                }

                p.setProperty(OUTPUT_PATH, getOutputPath(outputPath, p.getProperty(TABLE_LOCATION)));

                try {
                    res = stmt.executeQuery("SHOW PARTITIONS " + databaseName + "." + tableName);
                    String parts = "";
                    int i = 0;
                    while (res.next()) {
                        if (checkDirectory(getPathForPartition(p.getProperty(TABLE_LOCATION),
                                res.getString("partition")))) {
                            if (i == 0) {
                                parts = res.getString("partition");
                            } else {
                                parts += ";" + res.getString("partition");
                            }
                            System.out.println("Partition: [" + res.getString("partition").replace("/", ", ") + "]");
                            i++;
                        }
                    }

                    if (parts.length() > 0) {
                        p.setProperty(PARTITIONS, parts);
                        p.setProperty(NEEDS_COMPACTION, String.valueOf(1));
                    } else
                        p.setProperty(NEEDS_COMPACTION, String.valueOf(0));

                } catch (SQLException ex) {
                    String mes = ex.getMessage();
                    if (mes.contains("is not a partitioned table")) {
                        if (checkDirectory(p.getProperty(TABLE_LOCATION))) {
                            p.setProperty(PARTITIONS, NON_PARTITIONED);
                            p.setProperty(NEEDS_COMPACTION, String.valueOf(1));
                        }
                        else
                            p.setProperty(NEEDS_COMPACTION, String.valueOf(0));
                    }
                    else {
                        throw ex;
                    }
                }

                p.store(outputStream, "");
                outputStream.close();
            } else {
                throw new RuntimeException("oozie.action.output.properties System property not defined");
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            throw e;
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        } catch (SQLException e) {
            e.printStackTrace();
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            try {
                if (rs != null)
                    rs.close();
                if (stmt != null)
                    stmt.close();
                if (con != null)
                    con.close();
            } catch (Exception e) {}
        }
    }

    private String getOutputPath(String etlPath, String tablePath) {
        Integer ind = tablePath.indexOf(FIND_STRING);
        String finalPath = etlPath + tablePath.substring(ind + FIND_STRING.length());
        return finalPath;
    }

    public String[] splitPartitionList (String partitionList) {
        String[] partitionArray = partitionList.split(";");
        return partitionArray;
    }

    public String getOverwriteStatement(String databaseName, String tableName, String outputPath, String partitionName) {
        String stmnt = "LOAD DATA INPATH '" + getPathForPartition(outputPath, partitionName) + "' OVERWRITE INTO TABLE " + databaseName + "." + tableName;
        if (!partitionName.equals(NON_PARTITIONED)) {
            stmnt += " PARTITION (" + partitionName.replace("/", ",") + ")";
        }
        return stmnt;
    }

    public String[] getOverwriteArgs(String[] args)
    {
        String databaseName = args[4];
        String tableName = args[5];
        String outputPath = args[6];

        List<String> qrArgs = new ArrayList<>();

        qrArgs.add(args[0]);
        qrArgs.add(args[1]);

        String[] partitions = splitPartitionList(args[7]);
        qrArgs.add(String.valueOf(partitions.length));
        for ( String partition : partitions ) {
            qrArgs.add(getOverwriteStatement(databaseName, tableName, outputPath, partition));
        }

        qrArgs.add(args[2]);
        qrArgs.add(args[3]);

        String[] qsArgsArray = new String[qrArgs.size()];
        qsArgsArray = qrArgs.toArray(qsArgsArray);

        return qsArgsArray;
    }

    public String getPathForPartition(String sourcePath, String partitionName) {
        String finalPath;
        if (partitionName.equals(NON_PARTITIONED)) {
            finalPath = sourcePath;
        }
        else {
            finalPath = sourcePath + "/" + partitionName;
        }
        return finalPath;
    }

    private boolean checkDirectory (String path) throws Exception {
        boolean needsCompaction = false;
        Configuration conf = new OradoopConf().getConf();
        FileSystem fileSystem = FileSystem.newInstance(conf);
        Path srcPath = new Path(path);

//        System.out.println(fileSystem.getDefaultBlockSize()/1024/1024);

        FileStatus[] fstat = fileSystem.listStatus(srcPath);
        if (fstat.length > 1) {
            int smallFilesCount = 0;
            for (FileStatus fs : fstat) {
                if (fs.getLen()/1024/1024 < 5) {
                    smallFilesCount++;
                }
                if (smallFilesCount > 5) {
                    needsCompaction = true;
                    break;
                }
            }
        }

        return needsCompaction;
    }
}