package com.starhomemach.nbi.oradoop.datacompaction;

import com.starhomemach.nbi.oradoop.OradoopConf;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hive.conf.HiveConf;
import org.apache.hadoop.hive.metastore.HiveMetaStoreClient;
import org.apache.hadoop.hive.metastore.api.FieldSchema;
import org.apache.hadoop.hive.metastore.api.Partition;
import org.apache.hadoop.hive.metastore.api.Table;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.*;

/**
 * Created by pade on 7/9/18.
 */

@Component
public class CompactOrcTable {

    /**
     * Created by pade on 22/11/17.
     */

    public static void main(String [] args)  throws Exception {

        if (args.length < 15) {
            System.out.println("ERROR INVALID ARGUMENT COUNT PLS CHECK");
            return;
        }

        HiveConf hConf = new HiveConf(new OradoopConf().getConf(),new OradoopConf().getConf().getClass());
        HiveMetaStoreClient client = new HiveMetaStoreClient(hConf);

        long start = System.currentTimeMillis();

        Class.forName("org.apache.hive.jdbc.HiveDriver");

        //doesnt have any arugment validation since used internally and part of workflow wasnt expecting some weird here

        String Schema=args[4];
        String TblNm=args[5];
        double ratio=Double.parseDouble(args[9]);
        String bckSchema=args[10];
        String bckTbl=args[11];

        //declaring as final since they are used in pure functions for lambdas

        final String Monthwise=args[12];
        final String DkDatMostr=args[13];
        final String escapeLatestMonths=args[14];
        final String escapeMonthscnt=args[15];

        final String hdfsUserName=args[16];
        final String hdfsPassword=args[17];

        int year= ZonedDateTime.now(ZoneId.of("Etc/UTC")).getYear() ;
        int month=ZonedDateTime.now(ZoneId.of("Etc/UTC")).getMonthValue() ;
        int dk_dat_mo=(year-1989-1) * 12 +month;

        List<Integer> lstMonthsexl=new ArrayList<> ();

        for(int i=0; i<Integer.valueOf(escapeMonthscnt);i++) {
            lstMonthsexl.add(dk_dat_mo - i);
        }


        FileSystem fs = FileSystem.get(hConf);
        Table tbl = client.getTable(Schema,TblNm);
        if (tbl.getPartitionKeysSize() == 0) {
            System.out.println("The table has no Partitions..Cannot process using this module this module require ORC table with partitions");
            return;
        }
        else
            System.out.println("This table has " + tbl.getPartitionKeysSize()+ " partition keys");


        //assumptions CL_ID as first key in partition
        //DK_DAT_MO as second key in Partition
        //can be generalized based on  parameters

        List<Partition> pr= new ArrayList<>();
        if (args[7].equals("N")) {
            for (Partition prti : client.listPartitions(Schema,TblNm , (short) -1)) {
                final List<String> vals = prti.getValues();
                if (!(vals.get(1).equals( "__HIVE_DEFAULT_PARTITION__"))) pr.add(prti);
            }
        }
        else {
            for (Partition partition : client.listPartitions(Schema,TblNm , (short) -1)) {
                final List<String> values = partition.getValues();
                for (String s : args[8].split(","))
                {
                    if (values.get(0).equals(s) &&((Monthwise.equals("N")) || (Monthwise.equals("Y") && Arrays.asList(DkDatMostr.split(",")).stream().anyMatch(values.get(1)::contains))) && ((escapeLatestMonths.equals("N")) || (escapeLatestMonths.equals("Y") && !(values.get(1).equals( "__HIVE_DEFAULT_PARTITION__")) && !(lstMonthsexl.stream().anyMatch(x-> x.equals(Integer.valueOf(values.get(1))))) && Integer.valueOf(values.get(1))<=dk_dat_mo) )) {
                        pr.add(partition);
                        break;
                    }
                }
            }
        }



        Connection Hcon = DriverManager.getConnection(args[0], hdfsUserName, hdfsPassword);
        try {


            Statement Hstmt= Hcon.createStatement();
            Hstmt.execute("set hive.compute.query.using.stats=false");
            Hstmt.execute("set hive.fetch.task.conversion=none");
            Hstmt.execute("set hive.exec.dynamic.partition=true");
            Hstmt.execute("set hive.exec.dynamic.partition.mode=nonstrict");
            Hstmt.execute("set hive.exec.max.dynamic.partitions=1");

            String sql2="INSERT into metadata_db.t_hdp_orc_cmpct(tbl_nm,prt_key,hdfs_folder_size,fl_cnt_bf,fl_cnt_af,sts,mod_dat,cre_dat) VALUES (?,?,?,?,?,?,?,?)";


            //String sql3=" INSERT INTO METADATA_DB.T_HDP_ORC_CMPC(TBL_NM,PRT_KEY,STS,FL_CNT_BF,FL_CNT_AF,HDFS_FOLDER_SIZE,MOD_DAT)   etl_dnk.t_hdp_orc_cmpct SET STS='F',MOD_DAT=SYSDATE,FL_CNT_AF=? WHERE TBL_NM=? AND PRT_KEY= ?";
            PreparedStatement ps1 =Hcon.prepareStatement(sql2);
            //PreparedStatement ps2 =Ocon.prepareStatement(sql3);


            String singlequote="'";

            System.out.println("**********Preloading the metadata_db.v_hdp_orc_cmpct into Java Has Map *************");
            ResultSet Mres = Hstmt.executeQuery("select prt_key,sts from metadata_db.v_hdp_orc_cmpct where tbl_nm='"+TblNm+"'" + " AND STS='F'");
            Map< String,String> hm =new HashMap< String,String>();
            while (Mres.next())
            {
                hm.put(Mres.getString(1),Mres.getString(2));
            }
            System.out.println("**********Preloading Finished *************");

            System.out.println("Processing " + pr.size() + " partitions with a time threshold of " + Integer.parseInt(args[6]) +" minutes");
            for (Partition prt : pr)
            {
                long rstbfrcnt =0,rstaftcnt=0,finalaftcnt =0,bckupcnt=0;
                List<String> prv=prt.getValues();

                if (!hm.containsKey(String.join(",",prv)))
                {
                    Path filenamePath = new Path( prt.getSd().getLocation());
                    System.out.println( prt.getSd().getLocation());

                    if (Double.parseDouble(Long.toString((Integer.parseInt(args[6])-((System.currentTimeMillis()-start) / 1000 / 60)))) >
                        Double.parseDouble(Long.toString((fs.getContentSummary(filenamePath).getSpaceConsumed()/1024/1204/1024))) * ratio) {
                            ps1.setString(1,TblNm);
                            ps1.setString(2,String.join(",",prv));
                            ps1.setDouble(3, Double.parseDouble(Long.toString(fs.getContentSummary(filenamePath).getSpaceConsumed())));
                            Long flcnt=fs.getContentSummary(filenamePath).getFileCount();
                            ps1.setLong(4, flcnt);
                            ps1.setLong(5, 0);
                            ps1.setString(6,"S");
                            ps1.setString(7, new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSSSS").format(new java.util.Date().getTime()));
                            String credat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSSSS").format(new java.util.Date().getTime());
                            ps1.setString(8, credat);
                            ps1.executeUpdate();

                            StringBuilder template = new StringBuilder() ;
                            StringBuilder bcktemplate =new StringBuilder();
                            StringBuilder ogtemplate =new StringBuilder();
                            StringBuilder prttemplate =new StringBuilder();
                            template =template.append("ALTER TABLE ").append(Schema).append(".").append(TblNm).append(" PARTITION ").append("(");
                            //bcktemplate = bcktemplate.append("INSERT INTO ").append(bckSchema).append(".").append(bckTblNm).append("select  * from ").append(Schema).append(".").append(TblNm).append(" where ");
                            int j=0;

                            for (FieldSchema fieldSchema : tbl.getPartitionKeys()) {

                                ogtemplate =ogtemplate.append(fieldSchema.getName()).append('=');
                                bcktemplate=bcktemplate.append(fieldSchema.getName()).append('=');
                                prttemplate=prttemplate.append(fieldSchema.getName());
                                if (fieldSchema.getType().toUpperCase().equals("STRING"))
                                {
                                    ogtemplate = ogtemplate.append(singlequote).append(prv.get(j)).append(singlequote).append(",");
                                    bcktemplate=bcktemplate.append(singlequote).append(prv.get(j)).append(singlequote).append(" and ");

                                }
                                else
                                {
                                    ogtemplate = ogtemplate.append(prv.get(j)).append(",");
                                    bcktemplate =bcktemplate.append(prv.get(j)).append(" and ");
                                    prttemplate.append(",");
                                }
                                j=j+1;
                            }

                            System.out.println(String.valueOf(template) + String.valueOf(bcktemplate));
                            template=template.append(ogtemplate.deleteCharAt(ogtemplate.length()-1).append(")")).append(" CONCATENATE");
                            bcktemplate.setLength(bcktemplate.length()-4);
                            prttemplate.deleteCharAt(prttemplate.length()-1);


                           //our aim is to not loose any single row of data or duplication of data inany case or even  with release changes in hive.
                           //Below we are doing a backup of the partition until the compaction is done since we experienced a hive bug which will
                           //skip some files while reading since they have an extension like .orc to support these partitions we do them by direct copying from backup.
                           //so the count before backup, after backup and final after copy should match. if not matching it does by INSERT via hive way.
                           // the programs throws Application based exceptions if it is not matching in any case.
                           //Most of the below code using backup and alternate mechanism can be eliminated by upgrading to hive 3.0

                            System.out.println("Compacting the Partition"+TblNm +"("+String.join(",",prv)+")");
                            System.out.println("select count(*) from "+Schema+"."+TblNm + " where " + String.valueOf(bcktemplate));

                            ResultSet bfcntrs= Hstmt.executeQuery("select count(*) from "+Schema+"."+TblNm + " where " + String.valueOf(bcktemplate));
                            if (bfcntrs.next())
                               {  rstbfrcnt=bfcntrs.getLong(1);
                               }

                            Hstmt.execute("truncate table " + bckSchema +"."+bckTbl);
                            Hstmt.execute ("INSERT OVERWRITE TABLE "+ bckSchema  +"."+bckTbl + " SELECT * FROM " + Schema +"." + TblNm + " where " + bcktemplate);
                            ResultSet   bckpcntcrs=Hstmt.executeQuery("select count(*) from " +bckSchema + "."+bckTbl );

                            if (bckpcntcrs.next())
                               {     bckupcnt=bckpcntcrs.getLong(1);
                                   System.out.println("Backup Table Count" + bckupcnt);
                                   if (bckupcnt != rstbfrcnt) throw new IOException("Compaction Resulted incorrect no of rows..pls verify for backup " +bckpcntcrs.getLong(1) +" "+rstbfrcnt );
                               }


                            Hstmt.execute(String.valueOf(template));

                            ResultSet afcntrs= Hstmt.executeQuery("select count(*) from "+Schema+"."+TblNm + " where " + String.valueOf(bcktemplate));
                            if (afcntrs.next())
                               {
                                 rstaftcnt=afcntrs.getLong(1);

                               }

                            if ((rstbfrcnt != rstaftcnt) && (rstaftcnt <rstbfrcnt))
                               {
                                 System.out.println("INSERT OVERWRITE TABLE "+ Schema +"." + TblNm +   " PARTITION (" + String.valueOf(ogtemplate) +" select * FROM " + bckSchema  +"."+bckTbl);
                                 Hstmt.execute("INSERT OVERWRITE TABLE "+ Schema +"." + TblNm  + " PARTITION (" + String.valueOf(prttemplate) +")" +" select * FROM " + bckSchema  +"."+bckTbl);

                                 ResultSet finalcntrs= Hstmt.executeQuery("select count(*) from "+Schema+"."+TblNm + " where " + String.valueOf(bcktemplate));
                                 if (finalcntrs.next())
                                  {
                                    finalaftcnt=finalcntrs.getLong(1);
                                  }
                                 if (finalaftcnt !=rstbfrcnt)
                                  {
                                    System.out.println(finalaftcnt + " " + rstbfrcnt + " " +rstaftcnt+ " " + bckupcnt);
                                   throw new IOException("Compaction Resulted incorrect no of rows..pls verify");
                                  }

                               }
                            else if ((rstbfrcnt != rstaftcnt) && (rstaftcnt >rstbfrcnt))
                                       {System.out.println(finalaftcnt + " " + rstbfrcnt + " " +rstaftcnt+ " " + bckupcnt);
                                        throw new IOException("Compaction Resulted incorrect no of rows..pls verify aud");
                                       }
                            if ((rstbfrcnt==bckupcnt) && ((rstbfrcnt==finalaftcnt) ||(rstbfrcnt==rstaftcnt)))
                              {
                                  System.out.println(finalaftcnt + " " + rstbfrcnt + " " +rstaftcnt+ " " + bckupcnt);
                               ps1.setString(1, TblNm);
                               ps1.setString(2,String.join(",",prv));
                               ps1.setDouble(3, Double.parseDouble(Long.toString(fs.getContentSummary(filenamePath).getSpaceConsumed())));
                               ps1.setDouble(4,flcnt);
                               ps1.setLong(5, fs.getContentSummary(filenamePath).getFileCount());
                               ps1.setString(6, "F");
                               ps1.setString(7, new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSSSS").format(new java.util.Date().getTime()));
                               ps1.setString(8,credat);
                               ps1.executeUpdate();
                              }
                    }
                    else
                    {
                            System.out.println("--------------------------------------------------------------------------");
                            System.out.println("***Not proceeding further since this partition compaction size may take longer time than remaining time Threshold ***");
                            System.out.println("--------------------------------------------------------------------------");
                            System.out.println("Stopping at the Table Name" +TblNm + "Partition Key " + String.join(",",prv) );

                            Hcon.close();
                            break;
                    }
                }
                else
                {
                    System.out.println("Skipping the table(partition)"+TblNm +"("+String.join(",",prv) +")");
                }

                System.out.println("checking if it has crossed threshold in Minutes" + (System.currentTimeMillis()-start) / 1000 / 60 +" > " + Integer.parseInt( args[6]));

                if ((System.currentTimeMillis()-start) / 1000 / 60 > Integer.parseInt( args[6])) {
                    System.out.println("--------------------------------------------------------------------------");
                    System.out.println("***Not proceeding further since action exceeded time threshold.safe exit***");
                    System.out.println("--------------------------------------------------------------------------");
                    //Ocon.close();
                    Hcon.close();
                    break;
                }
            }

        }
        catch(SQLException e)
        {
            e.printStackTrace();
            throw e;

        }
        finally
        { if(Hcon !=null) Hcon.close();

        }
    }
}

