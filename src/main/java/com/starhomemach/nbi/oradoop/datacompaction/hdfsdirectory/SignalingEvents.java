package com.starhomemach.nbi.oradoop.datacompaction.hdfsdirectory;

import com.starhomemach.nbi.oradoop.OradoopConf;
import com.starhomemach.nbi.oradoop.datacompaction.ProcessDataCompaction;
import com.starhomemach.nbi.oradoop.export.SpringConfig;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.*;
import org.joda.time.DateTime;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.sql.*;
import java.util.*;


@Component
public class SignalingEvents {

    public static final String RUN_COMPACTION = "RUN_COMPACTION";
    public static final String MAX_MB = "MAX_MB";
    public static final int FILTER_BY_NUMBER_HOURS = 24;
    public static final int MIN_NUMBER_OF_FILES = 1;
    public static final String SIGNALING_EVENTS = "signaling_events";
    public static final String TEMP_SIGNALING_EVENTS = "temp_" + SIGNALING_EVENTS;
    public static final String CREATE_TABLE = " CREATE TABLE ";
    public static final String CREATE_TEMP_TABLE =   " STORED AS PARQUET AS SELECT " +
            "cl_grp_id,\t\n" +
            "dk_org_snw,\t\n" +
            "dk_org_hnw,\t\n" +
            "dk_org_cnp,\t\n" +
            "dk_dir,\t\n" +
            "dk_spe,\t\n" +
            "dk_sgt,\t\n" +
            "dk_ced,\t\n" +
            "dk_pet,\t\n" +
            "dk_sgs,\t\n" +
            "dk_sbp,\t\n" +
            "dk_ssc,\t\n" +
            "dk_rat,\t\n" +
            "dd_imsi,\n" +
            "dd_msisdn_min,\n" +
            "dd_hlr,\n" +
            "dd_msc,\n" +
            "dd_sgsn_gt,\n" +
            "dd_sgsn_ip,\n" +
            "dd_ssc,\n" +
            "dd_msrn\t,\n" +
            "dd_apn,dd_ggsn,dd_sgsn,dd_imei,dd_mcc,dd_mnc,dd_lac,dd_ci,dd_naspi\t,\n" +
            "dd_teid\t,dd_tmst\t,dd_end_tmst\t,msg_det,succ,fail,\t\n" +
            "dly,\t\n" +
            "sgc_uplnk_dur,\t\n" +
            "sgc_uplnk_byt,\t\n" +
            "sgc_uplnk_good_byt,\t\n" +
            "sgc_dwnlnk_dur,\t\n" +
            "sgc_dwnlnk_byt,\t\n" +
            "sgc_dwnlnk_good_byt,\t\n" +
            "sgc_sess_dur,\t\n" +
            "sgc_tcp_rdtrp_tim,\t\n" +
            "sgc_tcp_rdtrp_no,\t\n" +
            "sgc_tcp_pcks,\t\n" +
            "sgc_tcp_rtrns_pcks,\t\n" +
            "dk_tim,dk_svk,sim_iam_no_msg,sim_prn_no_msg,sim_acm_no_msg,sim_anm_no_msg,sim_rel_no_msg,sim_rlc_no_msg,sim_iam_2_acm_dly," +
            "sim_prn_2_acm_dly,sim_acm_2_anm_dly,sim_anm_2_rel_dly,sim_rel_2_rlc_dly FROM " + SIGNALING_EVENTS + " ";

    private final Log logger = LogFactory.getLog(getClass());

    private static String DRIVER_NAME = "org.apache.hive.jdbc.HiveDriver";


    public void process(String queueName, String user, String hiveConnection, String listOfClients, String listOfDays, boolean dataValidation) throws IOException, SQLException {
        Configuration conf = new OradoopConf().getConf();
        FileSystem fileSystem = FileSystem.newInstance(conf);
        String signalingEventsDirectory = getProcessingPath(user);
        Path srcPath = new Path(signalingEventsDirectory);
        printFileInDirectory(srcPath, fileSystem, "before");
        Map<String, Map<String, List<String>>> clientIdToDkDat = new HashMap();
        int maxMb = getMaxMb(user);
        List<String> clientIdsFileList;
        clientIdsFileList = getAllFilePath(srcPath, fileSystem, listOfClients);
        for (String clientIdFile : clientIdsFileList) {
            if (clientIdFile.toLowerCase().trim().startsWith("cl_id")) {
                Path clientIdPath = new Path(signalingEventsDirectory + "/" + clientIdFile);
                List<String> dkDatFiles = getAllFilePathFilteredByModificationTime(clientIdPath, fileSystem, listOfDays);
                for (String dkDat : dkDatFiles) {
                    if (dkDat.toLowerCase().trim().startsWith("dk_dat")) {
                        Path dkDatIdPath = new Path(clientIdPath + "/" + dkDat);

                        List<String> dkTimHrFiles = getAllFilePath(dkDatIdPath, fileSystem);
                        Collections.sort(dkTimHrFiles, comparatorStringListOfNumbers );

                        for (String dkTimHr : dkTimHrFiles) {
                                handleDkTimeHrFile(dkDatIdPath, fileSystem, clientIdToDkDat, clientIdFile, clientIdPath, dkDat, dkTimHr,maxMb);
                        }
                    } else {
                        logger.info("going to ignore file:" + dkDat);
                    }
                }
            } else {
                logger.info("going to ignore file:" + clientIdFile);
            }
        }
        runCompaction(user, queueName, clientIdToDkDat, hiveConnection, dataValidation);
        printFileInDirectory(srcPath, fileSystem, "after");
    }

    private Comparator<String> comparatorStringListOfNumbers =
        (String s1, String s2)-> {
            if(s1.length() == s2.length()) return s1.compareTo(s2);
            else return s1.length() - s2.length();} ;

    public void printFileInDirectory(Path directoryPath, FileSystem fs, String when) throws IOException {
        long numberOfFile = countFilesInDirectory(directoryPath, fs);
        double directorySize = getDirectorySize(directoryPath, fs);
        logger.info("Number Of files in directory:" + directoryPath.getName() + " " + when + " compaction is :" + numberOfFile + " and directory size is:" + directorySize + "GB");
    }


    public long countFilesInDirectory(Path directoryPath, FileSystem fs) throws IOException {
        ContentSummary cs = fs.getContentSummary(directoryPath);
        long fileCount = cs.getFileCount();
        return fileCount;
    }


    public double getDirectorySize(Path directoryPath, FileSystem fs) throws IOException {
        return fs.getContentSummary(directoryPath).getSpaceConsumed() / 1024 / 1024 / 1024;
    }

    private int getMaxMb(String user)throws IOException{
        Properties userProperties = getPropertiesFile(user);
        String propertyValue = userProperties.getProperty(MAX_MB, "40");
        logger.info(MAX_MB + " = " + propertyValue);
        return Integer.valueOf(propertyValue);
    }

    private boolean continueRun(String user) throws IOException{
        Properties userProperties = getPropertiesFile(user);
        String propertyValue = userProperties.getProperty(RUN_COMPACTION, "true");
        logger.info(RUN_COMPACTION + " = " + propertyValue);
        return Boolean.valueOf(propertyValue);
    }

    private Properties getPropertiesFile(String user) throws IOException {

        String pathStr  = getPropertiesPath(user);
        logger.info("Going to load properties file from:"+pathStr);
        Properties properties = new Properties();
        Path path = new Path(pathStr);
        FileSystem fileSystem = path.getFileSystem(new OradoopConf().getConf());
        FSDataInputStream schema = fileSystem.open(path);
        properties.load(schema);
        return properties;
    }


    private void handleDkTimeHrFile(Path dkDatIdPath, FileSystem fileSystem, Map<String, Map<String, List<String>>> clientIdToDkDat,
                                    String clientIdFile, Path clientIdPath, String dkDat, String dkTimHr, int maxMb) throws IOException {
        if (dkTimHr.toLowerCase().trim().startsWith("dk_tim_hr")) {
            String directoryPath = clientIdPath + "/" + dkDat + "/" + dkTimHr;
            Path dirPath =new Path(directoryPath);
            if (countNumberOfFileInDirectory(dirPath, fileSystem) > MIN_NUMBER_OF_FILES &&
                              (isAvgSizeFileInDirectoryLower(dirPath,fileSystem,maxMb))) {
                logger.info("Directory:" + directoryPath + " contains more then " + MIN_NUMBER_OF_FILES + " file going to continue ");
                setMapForNewDkTimHr(clientIdToDkDat, clientIdFile, dkDat);
                logger.info("mapped to :" + directoryPath);
                Map<String, List<String>> dkDatToDkTimHrs = clientIdToDkDat.get(clientIdFile);
                dkDatToDkTimHrs.get(dkDat).add(dkTimHr);
            } else {
                logger.info("going to ignore directory:" + directoryPath + " since its contain only one file or average of file size is " +
                        "greater then config");
            }
        } else {
            logger.info("going to ignore file:" + dkTimHr);
        }
    }

    private void setMapForNewDkTimHr(Map<String, Map<String, List<String>>> clientIdToDkDat, String clientIdFile, String dkDat) {
        if (!clientIdToDkDat.containsKey(clientIdFile)) {
            Map<String, List<String>> dkDatToDkTimHrs = new HashMap();
            updateNewArrayList(clientIdToDkDat, clientIdFile, dkDat, dkDatToDkTimHrs);
        } else if (!clientIdToDkDat.get(clientIdFile).containsKey(dkDat)) {
            Map<String, List<String>> dkDatToDkTimHrs = clientIdToDkDat.get(clientIdFile);
            updateNewArrayList(clientIdToDkDat, clientIdFile, dkDat, dkDatToDkTimHrs);
        }
    }

    private void updateNewArrayList(Map<String, Map<String, List<String>>> clientIdToDkDat, String clientIdFile, String dkDat, Map<String, List<String>> dkDatToDkTimHrs) {
        dkDatToDkTimHrs.put(dkDat, new ArrayList<>());
        clientIdToDkDat.put(clientIdFile, dkDatToDkTimHrs);
    }


    public int countNumberOfFileInDirectory(Path filePath, FileSystem fs) throws IOException {
        logger.info("Count Number of files in " + filePath.toString());
        return fs.listStatus(filePath).length;
    }


    public String getProcessingPath(String user) {
        return "/starhome/qos/user/" + user + "/data/fact/" + SIGNALING_EVENTS;
    }

    public String getPropertiesPath(String user) {
        return "/starhome/qos/user/" + user + "/conf/user.properties";
    }

    public String getUserDataPath(String user) {
        return "/starhome/qos/user/" + user + "/data/fact/";
    }


    public List<String> getAllFilePathFilteredByModificationTime(Path filePath, FileSystem fs, String listOfDays) throws IOException {

        if (listOfDays == null || listOfDays.isEmpty() || ProcessDataCompaction.ALL_VALUE.equals(listOfDays)) {
            logger.info("Going to get all days for file system:" + listOfDays);
            return getAllFilePathFilteredByModificationTime(filePath, fs);
        } else {
            logger.info("going to create the list of dk_dat from:" + listOfDays);
            String[] days = listOfDays.split(",");
            List<String> dkDats = new ArrayList<>();
            for (String id : days) {
                dkDats.add("dk_dat=" + id);
            }
            logger.info("List of days is:" + listOfDays.toString());
            return dkDats;
        }

    }


    public List<String> getAllFilePathFilteredByModificationTime(Path filePath, FileSystem fs) throws IOException {
        String fileName;
        String fullPath;
        List<String> fileList = new ArrayList();
        FileStatus[] fileStatus = fs.listStatus(filePath);
        for (FileStatus fileStat : fileStatus) {
            fullPath = fileStat.getPath().toString();
            fileName = fullPath.substring(fullPath.lastIndexOf('/') + 1, fullPath.length());
            if (isTimeIsLessThen(fileStat.getModificationTime())) {
                    fileList.add(fileName);
            } else {
                logger.info("Going to ignore file:" + fullPath + " since the file modifications time is higher then current time less " + FILTER_BY_NUMBER_HOURS + " hours");
            }
        }
        return fileList;
    }


    public boolean isTimeIsLessThen(long fileModificationTime) {
        DateTime now = new DateTime();
        long millisNowMinus48Hours = now.minusHours(FILTER_BY_NUMBER_HOURS).getMillis();
        if (fileModificationTime < millisNowMinus48Hours)
            return true;
        return false;
    }

    public boolean isAvgSizeFileInDirectoryLower(Path directoryPath, FileSystem fs, int maxMb) throws IOException {
        long numberOfFile = countFilesInDirectory(directoryPath, fs);
        double directorySize = getDirectorySize(directoryPath, fs)*1024;
        logger.info("Direcotry Path: " + directoryPath.toString() + " Number Of file = " + numberOfFile + " size of directory = " +
                directorySize + " MB ");
        if ((directorySize/numberOfFile) < maxMb ) {
            logger.info("Avergare lower then " + maxMb);
            return true;
        }
        logger.info("Avergare greater then " + maxMb);
        return false;
    }


    public List<String> getAllFilePath(Path filePath, FileSystem fs, String listOfClients) throws IOException {
        if (listOfClients == null || listOfClients.isEmpty() || ProcessDataCompaction.ALL_VALUE.equals(listOfClients)) {
            logger.info("Going to get all clients ids for file system:" + listOfClients);
            return getAllFilePath(filePath, fs);
        } else {
            logger.info("going to create the list of cl_id from:" + listOfClients);
            String[] ids = listOfClients.split(",");
            List<String> clientIds = new ArrayList<>();
            for (String id : ids) {
                clientIds.add("cl_id=" + id);
            }
            logger.info("List of clients is:" + listOfClients.toString());
            return clientIds;

        }
    }


    public List<String> getAllFilePath(Path filePath, FileSystem fs) throws IOException {
        String fileName;
        String fullPath;
        List<String> fileList = new ArrayList<>();
        FileStatus[] fileStatus = fs.listStatus(filePath);
        for (FileStatus fileStat : fileStatus) {
            fullPath = fileStat.getPath().toString();
            fileName = fullPath.substring(fullPath.lastIndexOf('/') + 1, fullPath.length());
            fileList.add(fileName);
        }
        return fileList;
    }


    public void runCompaction(String user, String queueName, Map<String, Map<String, List<String>>> clientIdToDkDat,
                              String hiveConnection, boolean dataValidation) throws SQLException, IOException {
        try {
            Class.forName(DRIVER_NAME);
        } catch (ClassNotFoundException e) {
            logger.error(e);
            return;
        }


        Connection con = DriverManager.getConnection(hiveConnection, user, user);
        Statement stmt = con.createStatement();
        stmt.execute("set hive.exec.dynamic.partition.mode=nonstrict");
        stmt.execute("set parquet.compression=SNAPPY");
        stmt.execute("set mapred.reduce.tasks=1");
        stmt.execute("set mapred.job.queue.name=" + queueName);


//        stmt.execute("set hive.merge.mapfiles=true");
//        stmt.execute("set hive.merge.mapredfiles=true");
//        stmt.execute("set hive.merge.smallfiles.avgsize=104857600");
//        stmt.execute("set hive.merge.size.per.task=209715200");
//        stmt.execute("set mapred.max.split.size=68157440");
//        stmt.execute("set mapred.min.split.size=68157440");


        Set<String> clientIds = clientIdToDkDat.keySet();
        for (String clientId : clientIds) {
            Map<String, List<String>> dkDatsToDkTimHr = clientIdToDkDat.get(clientId);
            Set<String> dkDats = dkDatsToDkTimHr.keySet();
            for (String dkdat : dkDats) {
                List<String> dkTimHrs = dkDatsToDkTimHr.get(dkdat);
                for (String dkTimHr : dkTimHrs) {
                    if(continueRun(user)) {
                        logger.info("Continue Run after check the properties file");
                        compactData(stmt, clientId, dkdat, dkTimHr, dataValidation, user);
                    } else {
                        break;
                    }
                }
            }
        }
    }


    private void compactData(Statement stmt, String clientId, String dkdat, String dkTimHr,boolean dataValidation,String user) throws SQLException {
        if(dataValidation)
        {
            compactWithDataValidation(stmt, clientId, dkdat, dkTimHr, user);
            return;
        }
        compactWithNoDataValidation(stmt, clientId, dkdat, dkTimHr, user);
    }


    private void compactWithNoDataValidation(Statement stmt, String clientId, String dkdat, String dkTimHr, String user) throws SQLException {
        String where = getWhereCluase(clientId, dkdat, dkTimHr);
        logger.info("Going to start checking compaction option for table " + SIGNALING_EVENTS + " " + where);

        String tempSignalingTableName = buildTempTableName(clientId);

        logger.info("going to create temp table "+  tempSignalingTableName);
        createTempSignalingTable(stmt, where,tempSignalingTableName);

        logger.info("going to run compaction");

        loadTempTableIntoSignalingTable(stmt, clientId, dkdat, dkTimHr,tempSignalingTableName, user);

        logger.info("Succeed to run Data compaction for table SIGNALING_EVENTS " + where);
    }

    private String buildTempTableName(String clientId) {
        return TEMP_SIGNALING_EVENTS+getNumberValue(clientId);
    }

    private String getWhereCluase(String clientId, String dkdat, String dkTimHr) {
        return " where " + clientId + " and " + dkdat + " and " + dkTimHr;
    }

    private void createTempSignalingTable(Statement stmt, String where, String tempSignalingTableName) throws SQLException {
        String dropTable = "DROP TABLE IF EXISTS " + tempSignalingTableName + " PURGE";
        stmt.execute(dropTable);

        String creteTable = CREATE_TABLE +tempSignalingTableName+ CREATE_TEMP_TABLE+where;
        stmt.execute(creteTable);
    }

    private void loadTempTableIntoSignalingTable(Statement stmt, String clientId, String dkdat, String dkTimHr, String tempSignalingTableName, String user) throws SQLException {
        String tempSignalingTAbleFullPath = getUserDataPath(user) + tempSignalingTableName;
        String loadDataInPath = "LOAD DATA INPATH '" + tempSignalingTAbleFullPath + "' OVERWRITE INTO TABLE "+ SIGNALING_EVENTS +
                " PARTITION(cl_id='" + getNumberValue(clientId) + "',dk_dat='" + getNumberValue(dkdat) + "',dk_tim_hr='" + getNumberValue(dkTimHr) + "')\n";

        logger.info("going to load temp table " + tempSignalingTAbleFullPath + " into signaling_events table");
        stmt.execute(loadDataInPath);
    }


    private void compactWithDataValidation(Statement stmt, String clientId, String dkdat, String dkTimHr, String user) throws SQLException {
        String where = getWhereCluase(clientId, dkdat, dkTimHr);
        String tempSignalingTableName = buildTempTableName(clientId);

        logger.info("Going to start checking compaction option for table " + SIGNALING_EVENTS + " " + where);
        long beforeSignalingEventsSize = getTableSize(stmt, where, SIGNALING_EVENTS);
        createTempSignalingTable(stmt, where,tempSignalingTableName);
        long tempSignalingEventsSize = getTableSize(stmt, "", tempSignalingTableName);


        if (beforeSignalingEventsSize == tempSignalingEventsSize) {
            logger.info("Since the number of records is equal between TABLE " + SIGNALING_EVENTS + " " + where + " and TABLE " + tempSignalingTableName + " going to run compaction");
            loadTempTableIntoSignalingTable(stmt, clientId, dkdat, dkTimHr,tempSignalingTableName, user);
        } else {
            logger.error("The number of records from table" + SIGNALING_EVENTS + " to table " + tempSignalingTableName + " is not equal going to throw exception");
            throw new EmptyStackException();
        }
        long afterSignalingEventsSize = getTableSize(stmt, where, SIGNALING_EVENTS);
        if (afterSignalingEventsSize != beforeSignalingEventsSize) {
            logger.error("The number of records for table" + SIGNALING_EVENTS + " before data compaction is not equal to after compaction going to throw exception");
            throw new EmptyStackException();
        } else {
            logger.info("Succeed to run Data compaction for table SIGNALING_EVENTS " + where);
        }
        logger.info("Finished to run Data compaction for table SIGNALING_EVENTS " + where);
    }




    private String getNumberValue(String row) {
        String[] split = row.split("=");
        if (split.length == 2) {
            return split[1];
        }
        return "";
    }

    private long getTableSize(Statement stmt, String where, String tableName) throws SQLException {
        String query = "select count(*) from " + tableName;
        if (!(where == null || where.isEmpty())) {
            query = query + where;
        }
        logger.info("Going to query:" + query);
        ResultSet beforeResultSet = stmt.executeQuery(query);
        Long numOfRecords = 0l;
        if (beforeResultSet.next()) {
            numOfRecords = new Long(beforeResultSet.getString(1).trim());
            logger.info("number of records, in table " + tableName + " " + where + " is:" + numOfRecords);
        }
        return numOfRecords;
    }


    public static void main(String[] args) throws Exception {

        Map<String, Map<String, List<String>>> clientIdToDkDat = tempBuildForTestingOnly();


        System.out.println("########################################################## START DATA COMPACTION  #############################");
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SpringConfig.class);

        context.getBean(SignalingEvents.class).runCompaction("telefonica", "default", clientIdToDkDat, "jdbc:hive2://isr-poc-1-hdf-1.starhome.com:10000/qos_telefonica_db",true);
        System.out.println("########################################################## END DATA COMPACTION  ##############################");
    }

    private static Map<String, Map<String, List<String>>> tempBuildForTestingOnly() {
        Map<String, Map<String, List<String>>> clientIdToDkDat = new HashMap();
        Map<String, List<String>> dkDataToDkTimHr = new HashMap();
        List<String> l = new ArrayList<>();

        //cl_id=18/dk_dat=9672/dk_tim_hr=14

        l.add("dk_tim_hr=15");
        dkDataToDkTimHr.put("dk_dat=9649", l);
        clientIdToDkDat.put("cl_id=18", dkDataToDkTimHr);
        return clientIdToDkDat;
    }


}
