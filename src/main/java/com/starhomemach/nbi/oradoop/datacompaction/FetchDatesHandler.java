package com.starhomemach.nbi.oradoop.datacompaction;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.perf4j.aop.Profiled;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.starhomemach.nbi.oradoop.OradoopConf;

@Service
public class FetchDatesHandler {

    private final Log logger = LogFactory.getLog(getClass());

    @Autowired
    private JdbcTemplate jdbcTemplate;


    private final static String fetchDatesQuery = "select c.dk_dat as dk_dat " +
            "from bi.t_ddat c "+
            "where c.dat = to_date(SYSDATE-%) ";

    public static final String NUM_OF_DAYS_BEFORE = "NUM_OF_DAYS_BEFORE";
    public static final String DEFAULT_NUM_OF_DAYS_BEFORE = "3";
    public static final String FETCH_INDICATION = "AUTO";


    @Profiled
    public void run(String user, String fetchValue) throws SQLException, IOException{
        if (fetchValue.equals(FETCH_INDICATION)) {
            String numOfDays = String.valueOf(getNumOfDays(user));

            logger.info("going to fetch DK DAT for " + numOfDays);
            Connection connection = jdbcTemplate.getDataSource().getConnection();
            PreparedStatement ps = connection.prepareStatement(fetchDatesQuery.replace("%", numOfDays));

            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                String dk_dat = rs.getString("dk_dat");
                System.out.println("fetch DK dat == " + dk_dat);
                saveResultToOozieProperty(dk_dat);
            }
        } else {
            logger.info("put the paramter value as DK DAT =  " + fetchValue);
            saveResultToOozieProperty(fetchValue);
        }
    }

    private void saveResultToOozieProperty(String value) throws SQLException {

        String filePath = System.getProperty("oozie.action.output.properties");
        try {
            if (filePath != null) {
                OutputStream outputStream = new FileOutputStream(filePath);
                Properties p = new Properties();
                p.setProperty("dk_dat", "" + value);
                p.store(outputStream, "");
                outputStream.close();
            } else {
                throw new RuntimeException("oozie.action.output.properties System property not defined");
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private int getNumOfDays(String user) throws IOException{
        Properties userProperties = getPropertiesFile(user);
        String propertyValue = userProperties.getProperty(NUM_OF_DAYS_BEFORE, DEFAULT_NUM_OF_DAYS_BEFORE);
        logger.info(NUM_OF_DAYS_BEFORE + " = " + propertyValue);
        return Integer.valueOf(propertyValue);
    }

    private Properties getPropertiesFile(String user) throws IOException {

        String pathStr  = "/starhome/qos/user/" + user + "/conf/user.properties";
        logger.info("Going to load properties file from:"+pathStr);
        Properties properties = new Properties();
        Path path = new Path(pathStr);
        FileSystem fileSystem = path.getFileSystem(new OradoopConf().getConf());
        FSDataInputStream schema = fileSystem.open(path);
        properties.load(schema);
        return properties;
    }


}
