package com.starhomemach.nbi.oradoop.datacompaction;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.starhomemach.nbi.oradoop.export.Application;
import com.starhomemach.nbi.oradoop.export.SpringConfig;
import com.starhomemach.nbi.oradoop.export.util.HandleProcessConfiguration;

public class FetchDataCompactionDateMain {

     public static void main(String[] args) throws Exception {

        if (args.length < 3) {
            System.out.println("ERROR args should contain the following parameters 1)USER 2)DK_DAT Indication 3)DB_PROPERTIES");
            return;
        }
        new HandleProcessConfiguration().handleOradoopConf(args, Application.INDEX_DB_PROPERTIES_FILE);
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SpringConfig.class);
        context.getBean(FetchDatesHandler.class).run(args[0],args[1]);
    }


}
