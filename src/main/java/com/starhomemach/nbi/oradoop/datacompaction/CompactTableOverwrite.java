package com.starhomemach.nbi.oradoop.datacompaction;

import com.starhomemach.nbi.oradoop.util.queryRunners.QueryAppHandler;
/**
 * Created by Pragmatists on 2017-04-03.
 */
public class CompactTableOverwrite {

    public static void main(String[] args) throws Exception {
        System.out.println("################################################ START ####################################################");
        if (args.length < 8) {
            System.out.println("Usage: [] [] [] [] [] [] [] []");
            return;
        }

        CompactTableUtil ctutl = new CompactTableUtil();

        String[] overwriteArgs = ctutl.getOverwriteArgs(args);

        int count = Integer.parseInt(overwriteArgs[2]);

        QueryAppHandler qr = new QueryAppHandler();

        qr.runQuery(overwriteArgs[0],
                overwriteArgs[1],
                count,
                overwriteArgs,
                overwriteArgs.length > 3+count?overwriteArgs[3+count]:null,
                overwriteArgs.length > 3+count?overwriteArgs[4+count]:null);

        System.out.println("################################################ END ####################################################");
    }
}
