package com.starhomemach.nbi.oradoop.datacompaction;

import com.starhomemach.nbi.oradoop.export.SpringConfig;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;



public class DataCompaction {

    public static void main(String[] args) throws Exception {



        System.out.println("########################################################## START DATA COMPACTION #############################");
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SpringConfig.class);
        //context.getBean(ProcessDataCompaction.class).run("telefonica","jdbc:hive2://isr-poc-1-hdf-1.starhome.com:10000/qos_telefonica_db");
        context.getBean(ProcessDataCompaction.class).run(args);
        System.out.println("########################################################## END  DATA COMPACTION #############################");
    }

}
