package com.starhomemach.nbi.oradoop.reprocess;

import com.starhomemach.nbi.oradoop.OradoopConf;
import com.starhomemach.nbi.oradoop.datacompaction.hdfsdirectory.SignalingEvents;
import com.starhomemach.nbi.oradoop.export.Application;
import com.starhomemach.nbi.oradoop.export.Reprocessing.ReprocessingHandler;
import com.starhomemach.nbi.oradoop.export.SpringConfig;
import com.starhomemach.nbi.oradoop.export.util.HandleProcessConfiguration;
import com.starhomemach.nbi.oradoop.util.GeneralUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Created by xdda on 2016-09-01.
 */
public class BiRebuildDailyDataFromHourly {

    public static final int INDEX_DB_PROPERTIES_FILE = 3;

    public static void main(String[] args) throws Exception {

        System.out.println("########################################################## START BI REBUILDING DAILY AGGREGATES FROM HOURLY #############################");

        if (args.length < 3) {
            System.out.println("ERROR args should contain the following parameters:\n" +
                    "1) USER\n" +
                    "2) MINIMAL DATE\n" +
                    "3) MAXIMAL DATE");
            return;
        }

        int i = 0;
        for (String arg: args) {
            System.out.println("Args[" + i + "]: " + arg);
            i++;
        }
        new HandleProcessConfiguration().handleOradoopConf(args, BiRebuildDailyDataFromHourly.INDEX_DB_PROPERTIES_FILE);

        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SpringConfig.class);
        Configuration conf = new OradoopConf().getConf();
        context.getBean(ReprocessingHandler.class).rebuildDailyDataFromHourlyData(
                context.getBean(GeneralUtils.class).getClientIdListFromFileList(
                        context.getBean(GeneralUtils.class).getClientDirectories(
                                FileSystem.newInstance(conf),
                                context.getBean(SignalingEvents.class).getProcessingPath(args[0]))),
                context.getBean(GeneralUtils.class).toLocalDateTime(args[1]),
                context.getBean(GeneralUtils.class).toLocalDateTime(args[2]));

        System.out.println("##########################################################  END BI REBUILDING DAILY AGGREGATES FROM HOURLY  #############################");
    }
}
