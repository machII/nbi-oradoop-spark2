package com.starhomemach.nbi.oradoop.reprocess;

import com.starhomemach.nbi.oradoop.OradoopConf;
import com.starhomemach.nbi.oradoop.datacompaction.hdfsdirectory.SignalingEvents;
import com.starhomemach.nbi.oradoop.export.SpringConfig;
import com.starhomemach.nbi.oradoop.util.GeneralUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.time.LocalDateTime;

/**
 * Created by tpin on 03/08/2016.
 */
public class DeleteSignalingEvents {

    public static void main(String[] args) throws Exception {

        System.out.println("########################################################## START PREPROCESS DELETE OLD SIGNALING EVENTS ON HDFS #############################");
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SpringConfig.class);

        Configuration conf = new OradoopConf().getConf();
        /*Configuration conf = new Configuration();
        conf.set("fs.defaultFS", "hdfs://10.135.11.24:8020");
        conf.set("fs.hdfs.impl", "org.apache.hadoop.hdfs.DistributedFileSystem");
        System.setProperty("HADOOP_USER_NAME", "telefonica");*/
        context.getBean(GeneralUtils.class).dropDailyPartitionsRangeForAllClients(FileSystem.newInstance(conf),
                                                    context.getBean(SignalingEvents.class).getProcessingPath(args[0]),
                                                    context.getBean(GeneralUtils.class).toLocalDateTime(args[1]),
                                                    context.getBean(GeneralUtils.class).toLocalDateTime(args[2]));
        System.out.println("########################################################## END PREPROCESS DELETE OLD SIGNALING EVENTS ON HDFS #############################");
    }
}
