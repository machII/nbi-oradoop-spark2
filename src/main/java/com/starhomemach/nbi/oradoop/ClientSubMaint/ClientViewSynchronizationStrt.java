package com.starhomemach.nbi.oradoop.ClientSubMaint;

import com.starhomemach.nbi.oradoop.datacompaction.FetchDatesHandler;
import com.starhomemach.nbi.oradoop.export.Application;
import com.starhomemach.nbi.oradoop.export.SpringConfig;
import com.starhomemach.nbi.oradoop.export.util.HandleProcessConfiguration;
import com.starhomemach.nbi.oradoop.export.util.OradoopProcessArgs;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Created by pade on 23/1/17.
 */




public class ClientViewSynchronizationStrt {

        public static void main(String[] args) throws Exception {

            if (args.length < 5) {
                System.out.println("ERROR should contain 1.Hive Connection String");
                System.out.println("ERROR should contain 2.biUserName & 3.biPassword");
                return;
            }
            //HandleProcessConfiguration ProcessConfiguration= new HandleProcessConfiguration();
            //ProcessConfiguration.handleOradoopConf(args, Application.INDEX_DB_PROPERTIES_FILE);
            String biUrl=args[3];
            String biUserName=args[1];
            String biPassword=args[2];
            OradoopProcessArgs.getInstance().oracleDbUrl(biUrl).user(biUserName).password(biPassword);
            AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SpringConfig.class);
            context.getBean(ClientViewSynchronization.class).run(args[0]);
            context.getBean(ClientViewSynchronizationPrsto.class).run(args[4],args[0]);

        }


    }

