package com.starhomemach.nbi.oradoop.ClientSubMaint;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.sql.*;

/**
 * Created by pade on 22/2/18.
 */

@Component
public class ClientViewSynchronizationPrsto {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    private final Log logger = LogFactory.getLog(getClass());

    public void run(String... args) throws Exception {

        if(args.length == 2)
        {


            String hiveConnection = args[0];

            String hivConnection=args[1];

            Connection Ocon= jdbcTemplate.getDataSource().getConnection();
            //Connection Oupdcon= jdbcTemplate.getDataSource().getConnection();
            Connection Hcon= DatabaseManager.getInstance().getPrestoConnection(hiveConnection,"tst_user","");



            Connection Hivcon = DriverManager.getConnection(hivConnection, "", "");



            Statement Ostmt = Ocon.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE,
                    ResultSet.HOLD_CURSORS_OVER_COMMIT);

            Statement Oupdstmt = Ocon.createStatement();

            //Statement Oupdstmt = Oupdcon.createStatement();
            Statement Hstmt= Hcon.createStatement();
            Statement Hivstmt=Hivcon.createStatement();

            String QueryString=" SELECT Z.TBL_CD,OWN_ID,PRST_STMT,'CL_'||O.MAIN_TAP_CD||'_'||CASE WHEN LENGTH(OWN_ID) < 4 THEN LPAD(OWN_ID,4,'0') ELSE TO_CHAR(OWN_ID) END||'_PRTO' DBnAME,COALESCE(D.HDP_CUST_NM,'T_'||D.TBL_TP_CD||Z.TBL_CD) view_nm, z.ver,vw_stmt1 FROM (\n" +
                    "                    (SELECT A.TBL_CD,A.OWN_ID,PRST_STMT,MAX(VER) OVER (PARTITION BY A.TBL_CD,A.OWN_ID) mxver,VER,PRST_CURR_STS,vw_stmt1,PRST_BI_FLG FROM  MD.T_HDP_CL_VWS a, MD.T_HDP_CL_SBSCR b where a.own_id=b.own_id )\n" +
                    "                    )Z,EMACH.PMN_ORG_T O,(SELECT  COALESCE(A.TBL_CD,B.TBL_CD) TBL_CD, A.TBL_TP_CD TBL_TP_CD, B.HDP_CUST_NM HDP_CUST_NM FROM MD.V_TBL A FULL JOIN ETL_DNK.HDP_CUST_IMPL_VIEWS B ON A.TBL_CD=B.TBL_CD ) d  WHERE VER=MXVER AND NOT EXISTS (SELECT 1 FROM MD.T_HDP_CL_TBL_SKIP D WHERE Z.TBL_CD=D.TBL_CD AND Z.OWN_ID=D.OWN_ID) AND O.ORG_TECH_ID=Z.OWN_ID\n" +
                    "                    and Z.tbl_cd=d.tbl_cd AND Z.PRST_CURR_STS='I'  AND BI_FLG IN (2,3) ORDER BY OWN_ID";


            logger.debug("running Query on Oracle\n" +QueryString);
            System.out.println("running Query on Oracle\n" +QueryString);

            try {
                ResultSet Ores=Ostmt.executeQuery(QueryString);

                ResultSet Hres;
                ResultSet HresSch;

                ResultSet Hiveres;

                String CurrentClient=null;
                String CurrentDbNm=null;

                while (Ores.next())
                {
                    if (CurrentClient == null )
                    {
                        CurrentClient=Ores.getString(2);
                        CurrentDbNm=Ores.getString(4);
                    }


                    if (!CurrentClient.equals(Ores.getString(2)))
                    {
                        //String hisql = "show databases like '" + CurrentDbNm + "'";

                        //logger.debug("running Query on Hive\n" + hisql);

                        //Hres = Hstmt.executeQuery(
                          //      "select * from information_schema.schemata where  UPPER(schema_name)="+"'"+CurrentDbNm+ "'"
                        //);
                        //if (!Hres.next())
                        //{
                          //  System.out.println("drop schema if exists " + CurrentDbNm);
                            //    Hstmt.execute("drop schema if exists " + CurrentDbNm);


                        //}

                        String hisql = "show databases like '" + CurrentDbNm + "'";

                        logger.debug("running Query on Hive\n" + hisql);

                        Hiveres = Hivstmt.executeQuery(hisql);
                        if (Hiveres.next())
                        {
                            Hivstmt.execute("use "+ CurrentDbNm);

                            Hiveres=Hivstmt.executeQuery("show tables");
                            if (!Hiveres.next())
                                Hivstmt.execute("drop database " + CurrentDbNm);


                        }

                        CurrentClient=Ores.getString(2);
                        CurrentDbNm=Ores.getString(4);
                    }

                    String updateTableSQL = "UPDATE MD.T_HDP_CL_VWS"
                            + " SET PRST_CURR_STS = 'C' " + ","
                            + " MOD_TMST = systimestamp "
                            + " WHERE TBL_CD = " + "'" + Ores.getString(1) + "'"
                            + " AND OWN_ID   = " + Ores.getString(2)
                            + " AND VER      = " + Ores.getString(6);



                    String sql = "show databases like '" + Ores.getString(4) + "'";
                    logger.debug("running Query on Hive " + sql);
                    System.out.println("running Query on Hive " + sql);

                    Hiveres = Hivstmt.executeQuery(sql);
                    if (Hiveres.next())
                    {
                        System.out.println(Hiveres.getString(1));
                    }
                    else
                    {
                        logger.debug("running Query on Hive\n" + "create database "+ Ores.getString(4));
                        System.out.println("running Query on Hive\n" + "create database " + Ores.getString(4));
                        Hivstmt.execute("create database "+ Ores.getString(4));
                    }
                    logger.debug("running Query on Hive\n" + "use "+ Ores.getString(4));



                    //HresSch = Hstmt.executeQuery(
                      //      "select * from information_schema.schemata where  UPPER(schema_name)="+"'"+Ores.getString(4)+ "'"
                    //);

                    //if (!HresSch.next())
                    //{
                      //  System.out.println("create SCHEMA " + Ores.getString(4));
                       // Hstmt.execute("create SCHEMA "+ Ores.getString(4));


                    //}


                    //logger.debug("running Query on Hive\n" + "use "+ Ores.getString(4));

                    //Hstmt.execute("use "+ Ores.getString(4));


                    //logger.debug("running Query on Hive\n" + "drop view IF EXISTS " + Ores.getString(5));
                    //System.out.println("drop view IF EXISTS " + Ores.getString(4)+"."+Ores.getString(5));

                    logger.debug("running Query on presto " + Ores.getString(4)+"."+Ores.getString(5));
                    System.out.println("running Query on presto " + Ores.getString(4) + "." + Ores.getString(5));
                    Hstmt.execute("drop view IF EXISTS " + Ores.getString(4)+"."+Ores.getString(5));

                    System.out.println(Ores.getString(3) + Ores.getString(7)== null ? "":Ores.getString(7));

                    logger.debug("running Query on presto " + Ores.getString(3) + (Ores.getString(7) == null ? "" : Ores.getString(7)));
                    System.out.println("running Query on presto " + Ores.getString(3) + (Ores.getString(7) == null ? "" : Ores.getString(7)));

                    if (Ores.getString(3)!=null && !Ores.getString(3).isEmpty())
                        Hstmt.execute(Ores.getString(3) + (Ores.getString(7)== null ? "":Ores.getString(7)));

                    logger.debug("running Query on Oracle\n" + updateTableSQL);
                    Oupdstmt.execute(updateTableSQL);
                    Ocon.commit();

                }


                if (CurrentClient != null && !CurrentClient.isEmpty() && CurrentDbNm !=null && !CurrentDbNm.isEmpty())
                {



                    String hisql = "show databases like '" + CurrentDbNm + "'";

                    logger.debug("running Query on Hive\n" + hisql);
                    System.out.println("running Query on Hive\n" + hisql);

                    Hiveres = Hivstmt.executeQuery(hisql);
                    if (Hiveres.next())
                    {
                        Hivstmt.execute("use "+ CurrentDbNm);

                        Hiveres=Hivstmt.executeQuery("show tables");
                        if (!Hiveres.next())
                            Hivstmt.execute("drop database  " + CurrentDbNm);


                    }
                    //Hres = Hstmt.executeQuery(
                     //       "select * from information_schema.tables where  UPPER(table_schema)="+"'"+CurrentDbNm+ "'"
                    //);
                    //if (!Hres.next())
                    //{

                      //      Hstmt.execute("drop SCHEMA IF EXISTS  " + CurrentDbNm);


                    //}
                }
            }
            catch(SQLException e)
            {
                logger.error(e);
                e.printStackTrace();
                throw e;

            }
            finally
            {
                if (Ocon != null)
                    Ocon.close();
                if (Hcon != null)
                    Hcon.close();
                if (Hivcon !=null)
                    Hivcon.close();
            }
        }
        else
        {

        }

    }
}
