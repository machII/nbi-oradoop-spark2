package com.starhomemach.nbi.oradoop.ClientSubMaint;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.perf4j.aop.Profiled;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

/**
 * Created by pade on 23/1/17.
 */
@Service
public class DatabaseManager {


        private static DatabaseManager dbIsntance;
        private static Connection Hivecon ;
        private static Connection Oraclecon ;
        private static Statement stmt;
        private static Connection Impalacon;
        private static Connection Prestocon;


        private final Log logger = LogFactory.getLog(getClass());

        private DatabaseManager() {

        }

        public static DatabaseManager getInstance(){
            if(dbIsntance==null){
                dbIsntance= new DatabaseManager();
            }
            return dbIsntance;
        }

        public  Connection getHiveConnection(String host,String username,String password){

            if(Hivecon==null){
                try {

                    Hivecon = DriverManager.getConnection( host, username, password );
                } catch (SQLException ex) {

                    logger.error(ex);
                }
            }

            return Hivecon;
        }
        public  Connection getImpalaConnection(String host){

        if(Impalacon==null){
            try {
                Class.forName("org.apache.hive.jdbc.HiveDriver");
                Impalacon = DriverManager.getConnection( host );
            } catch (SQLException ex) {

                logger.error(ex);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }

        return Impalacon;
    }

    public  Connection getPrestoConnection(String host,String username,String password){

        if(Prestocon==null){
            try {
                Class.forName("com.facebook.presto.jdbc.PrestoDriver");
                Prestocon = DriverManager.getConnection( host,username,password );
            } catch (SQLException ex) {

                logger.error(ex);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }

        return Prestocon;
    }


    public  Connection getOracleConnection(String host,String username,String password){

        if(Oraclecon==null){
            try {
                Class.forName("oracle.jdbc.driver.OracleDriver");
                Oraclecon = DriverManager.getConnection( host,username,password );
            } catch (SQLException ex) {

                logger.error(ex);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }

        return Oraclecon;
    }

}


