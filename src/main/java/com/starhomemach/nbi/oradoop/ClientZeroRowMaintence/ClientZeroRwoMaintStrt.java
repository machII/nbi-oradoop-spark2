package com.starhomemach.nbi.oradoop.ClientZeroRowMaintence;


import com.starhomemach.nbi.oradoop.export.SpringConfig;
import com.starhomemach.nbi.oradoop.export.util.OradoopProcessArgs;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Created by pade on 24/4/17.
 */
public class ClientZeroRwoMaintStrt {


        public static void main(String[] args) throws Exception {

            if (args.length < 4) {
                System.out.println("ERROR should contain 1.Impala Connection String");
                System.out.println("ERROR should contain 2.biUserName & 3.biPassword");
                return;
            }
            //HandleProcessConfiguration ProcessConfiguration= new HandleProcessConfiguration();
            //ProcessConfiguration.handleOradoopConf(args, Application.INDEX_DB_PROPERTIES_FILE);
            String biUrl=args[3];
            String biUserName=args[1];
            String biPassword=args[2];
            OradoopProcessArgs.getInstance().oracleDbUrl(biUrl).user(biUserName).password(biPassword);
            AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SpringConfig.class);
            context.getBean(ClientZeroRowMaint.class).run(args[0]);
        }


    }

