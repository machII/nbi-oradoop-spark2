package com.starhomemach.nbi.oradoop.ClientZeroRowMaintence;

/**
 * Created by pade on 24/4/17.
 */
  import com.starhomemach.nbi.oradoop.ClientSubMaint.DatabaseManager;
  import org.apache.commons.logging.Log;
  import org.apache.commons.logging.LogFactory;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.jdbc.core.JdbcTemplate;
        import org.springframework.stereotype.Component;

        import java.sql.Connection;
        import java.sql.ResultSet;
        import java.sql.SQLException;
        import java.sql.Statement;

@Component
public class ClientZeroRowMaint {

    @Autowired
    private JdbcTemplate jdbcTemplate;


    private final Log logger = LogFactory.getLog(getClass());

    public void run(String... args) throws Exception {

    Connection Ocon= jdbcTemplate.getDataSource().getConnection();

        String impalaConnection = args[0];

        Statement Ostmt = Ocon.createStatement();
        Statement Oclstmt = Ocon.createStatement();

        Connection Icon= DatabaseManager.getInstance().getImpalaConnection(impalaConnection);
        Statement Istmt =Icon.createStatement();

        String QueryString="SELECT TBL_CD,TBL_NM_BI,RLS_POL,CL_ID_PK,clmn_cd,cl_val,cl_clmn_cd FROM ETL_DNK.T_CL_ZERO_gen";

        try {
            ResultSet Ores=Ostmt.executeQuery(QueryString);



            while (Ores.next())
            {
             String ImpalQuery = buildquery(Oclstmt,Ores.getString(1),Ores.getString(2),Ores.getString(3),Ores.getString(4),Ores.getString(5),Ores.getString(6),Ores.getString(7));
             System.out.println(ImpalQuery);
                if (Ores.getString(3).contentEquals("1") && Ores.getString(4).contentEquals("N"))
                {

                    Istmt.execute("set num_nodes=1");
                }
                else
                { Istmt.execute("set num_nodes=0");

                }
                Istmt.execute(ImpalQuery);
            }
        }

        catch(SQLException e)
        {
            logger.error(e);
            e.printStackTrace();
            throw e;

        }
        finally
        {
            if (Ocon != null)
                Ocon.close();
            if (Icon != null)
                Icon.close();
        }

    }

    public String buildquery(Statement oclstmt,String tblcd, String tblNmBI,String rlspol,String clIdpk,String clmncd,String clval,String cl_clmn_cd  ) throws Exception
    {
        String clidclause="";

        if (rlspol.contentEquals("1") && clIdpk.contentEquals("N"))
        {

            clidclause=" AND " + cl_clmn_cd + " =0 ";
        }

     String querystr = "INSERT INTO dimension_db." + tblNmBI + "(" + clmncd + ")"
                         + " SELECT " + clval + " FROM stgin_db.bi_cl_tbl A"
                          + " WHERE tbl_cd="+"'"+tblcd + "'"+ clidclause + " AND NOT EXISTS    ( SELECT 1 FROM dimension_db."+ tblNmBI + " B"
                          + " where dk_" + tblcd + "=0";
        if (rlspol.contentEquals("1") && clIdpk.contentEquals("Y"))
        {

            querystr = querystr + " AND " + "b."+ cl_clmn_cd +" = "+" A." + cl_clmn_cd;
        }

        querystr = querystr + ")";

        return querystr;
    }

}
